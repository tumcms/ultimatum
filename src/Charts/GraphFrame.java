/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Charts;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.concurrent.locks.ReentrantLock;


public class GraphFrame extends JFrame{
    private GraphData data;
    private int lines;
    private static final Color[] COLORS=new Color[]{Color.RED,Color.BLUE,Color.BLACK,new Color(140,140,0),new Color(0,200,0),Color.ORANGE,Color.CYAN,Color.MAGENTA,Color.BLACK, Color.DARK_GRAY};
    private JComboBox[] combos;
    private JCheckBox scalex;
    private GraphPanel graph;
    private JPanel topPanel;
    private boolean repaint;

    public GraphFrame(GraphData data, int lines) {
        super("Graphical Output");
        this.data = data;
        this.lines = lines;
        this.repaint=true;
        constructTopPanel();
        setLayout(new BorderLayout());
        add(topPanel,BorderLayout.NORTH);
        graph=new GraphPanel(data,getLines());
        add(graph,BorderLayout.CENTER);

        setSize(1104,664);
        //setResizable(false);
        setVisible(true);
    }
    public void constructTopPanel(){
        topPanel=new JPanel(new GridLayout(2,(int)Math.ceil((lines+1)/2.0)));
        if (scalex!=null)scalex=new JCheckBox("Scale x axis",scalex.isSelected());
        else scalex=new JCheckBox("Scale x axis",true);
        scalex.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                repaint=true;
                repaint();
            }
        });
        topPanel.add(scalex);
        String[] labels=new String[data.getDatalabels().length+1];
        for (int x=0;x<data.getDatalabels().length;x++){
            labels[x]=data.getDatalabels()[x];
        }
        if (combos==null||combos.length!=lines)combos=new JComboBox[lines];
        labels[data.getDatalabels().length]="<None>";
        for (int x=0;x<lines;x++){
            JComboBox newcombo=new JComboBox(labels);
            newcombo.setForeground(COLORS[x]);
            if (combos!=null&&combos[x]!=null&&combos[x].getSelectedIndex()<labels.length)newcombo.setSelectedIndex(combos[x].getSelectedIndex());
            else newcombo.setSelectedIndex(x);
            newcombo.addActionListener(new Listener());
            topPanel.add(newcombo);
            combos[x]=newcombo;
        }
    }

    public void setData(GraphData data, int lines){
        this.data=data;
        this.lines=lines;
        remove(topPanel);
        constructTopPanel();
        add(topPanel,BorderLayout.NORTH);
        graph.setData(data,getLines());
        topPanel.updateUI();
        graph.updateUI();
        repaint=true;
        repaint();
    }

    class Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            graph.setLines(getLines());
            repaint=true;
            repaint();
        }
    }
    public int[][] getLines(){
        int act=0;
        for (int x=0;x<combos.length;x++){
            if (!combos[x].getSelectedItem().equals("<None>"))act++;
        }
        int[][] out=new int[act][2];
        int counter=0;
        for (int x=0;x<combos.length;x++){
            if (!combos[x].getSelectedItem().equals("<None>")){
                out[counter][0]=combos[x].getSelectedIndex();
                out[counter][1]=x;
                counter++;
            }
        }

        return out;
    }

    class GraphPanel extends JPanel {
        private GraphData data;
        private int[][] lines;
        private BufferedImage buffer;
        private ReentrantLock lock;
        private final int BORDER=20;

        GraphPanel(GraphData data, int[][] lines) {
            this.data = data;
            this.lines = lines;
            lock=new ReentrantLock();
        }
        public void setLines(int[][] lines){
            lock.lock();
            this.lines=lines;
            lock.unlock();
        }

        public void setData(GraphData data, int[][] lines){
            lock.lock();
            this.data = data;
            this.lines = lines;
            lock.unlock();

        }

        public void repaint(){
            repaint=true;
            super.repaint();
        }

        public void paintComponent(Graphics g){

            if (buffer==null||buffer.getWidth()!=getWidth()||buffer.getHeight()!=getHeight()){
                buffer=new BufferedImage(getWidth(),getHeight(),BufferedImage.TYPE_BYTE_INDEXED);
                repaint=true;
            }
            
            if (data.getDataCount()==0){
                g.setColor(Color.WHITE);
                g.fillRect(0,0,getWidth(),getHeight());
                return;
            }

            Graphics2D g2=(Graphics2D)buffer.getGraphics();

            if (data.hasDataChanged()||repaint){
                repaint=false;
                g2.setColor(Color.WHITE);
                g2.fillRect(0,0,getWidth(),getHeight());
                FontMetrics fM=g2.getFontMetrics();
                double ext=data.getExtremes(lines)[1];
                int mw=(int)fM.getStringBounds("0",g2).getWidth();
                mw=Math.max(mw,(int)fM.getStringBounds(ext+"",g2).getWidth());
                int w=getWidth()-mw-(2*BORDER);
                //if (scalex.isSelected()){
                    w-=Math.round(fM.getStringBounds("0000000",g2).getWidth())+20;
                //}
                int h=getHeight()-((3*fM.getHeight())+(2*BORDER));
                int datawidth=data.getDataCount();
                if (scalex.isSelected())datawidth=data.getDataCount();
                double xratio=(double)w/(datawidth-1);
                double yratio=(double)h/(ext);
                int xoffset=BORDER+mw;
                int yoffset=BORDER+fM.getHeight();
                for (int x=1;x<data.getDataCount();x++){
                    for (int y=0;y<lines.length;y++){
                        double data1=data.getdata(x-1,lines[y][0]);
                        double data2=data.getdata(x,lines[y][0]);
                        g2.setColor(COLORS[lines[y][1]]);
                        int x1=(int)Math.round(xoffset+((x-1)*xratio));
                        int y1=getHeight()-(int)Math.round(yoffset+((data1)*yratio));
                        int x2=(int)Math.round(xoffset+((x)*xratio));
                        int y2=getHeight()-(int)Math.round(yoffset+((data2)*yratio));
                        g2.drawLine(x1,y1,x2,y2);
                    }
                }

                if (data.getDataCount()>0){//scalex.isSelected()
                    int labeloffset=(int)Math.round((double)(getHeight()-40)/lines.length);
                    int x2=xoffset+w+20;
                    int x1=(int)Math.round(xoffset+Math.max(0,((data.getDataCount()-1)*xratio)));
                    int[][] yvals=new int[lines.length][3];
                    for (int y=0;y<lines.length;y++){
                        double data1=data.getdata(data.getDataCount()-1,lines[y][0]);
                        yvals[y][0]=(int)Math.round(10*data1);
                        yvals[y][1]=getHeight()-(int)Math.round(yoffset+((data1)*yratio));
                        yvals[y][2]=y;

                    }
                    bubbleSort(yvals);
                    for (int y=0;y<lines.length;y++){
                        String data1=data.getdata(data.getDataCount()-1,lines[yvals[y][2]][0])+"";
                        g2.setColor(COLORS[lines[yvals[y][2]][1]]);
                        int yo=BORDER+(labeloffset*(lines.length-y-1));
                        Stroke oldstroke=g2.getStroke();
                        Stroke thindashed = new BasicStroke(0.8f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_BEVEL, 1.0f,new float[] { 8.0f, 3.0f, 2.0f, 3.0f },8.0f);
                        g2.setStroke(thindashed);
                        g2.drawLine(x1,yvals[y][1],x2,yo+(fM.getHeight()/2));
                        g2.setStroke(oldstroke);
                        int fw=(int)fM.getStringBounds(data1,g2).getWidth();
                        g2.drawRect(x2,yo,fw+5,fM.getHeight()+4);
                        g2.drawString(data1,x2+2,yo+fM.getHeight());
                    }
                }

                g2.setColor(Color.BLACK);

                //Y AXIS
                g2.drawLine(xoffset,getHeight()-yoffset+5,xoffset,getHeight()-(yoffset+h));
                int ystep=1;
                while(ext/ystep>10){
                    ystep*=2;
                    if (ystep>5)ystep-=ystep%5;
                    if (ystep>10)ystep-=ystep%10;
                    if (ystep>100)ystep-=ystep%100;
                    if (ystep>1000)ystep-=ystep%1000;
                }
                for (int x=ystep;x<ext-Math.ceil(ystep/2);x+=ystep){
                    int ty=getHeight()-(int)Math.round(yoffset+(x*yratio));
                    g2.drawString(x+"",10,ty+(fM.getHeight()/2));
                    g2.drawLine(xoffset-5,ty,xoffset+5,ty);
                }
                int ty=getHeight()-(int)Math.round(yoffset+((ext)*yratio));
                g2.drawString(ext+"",10,ty+(fM.getHeight()/2));
                g2.drawLine(xoffset-5,ty,xoffset+5,ty);


                //X AXIS
                g2.drawLine(xoffset-5,getHeight()-yoffset,xoffset+w,getHeight()-yoffset);
                int xstep=1;
                int dc=datawidth;
                while(dc/xstep>10){
                    xstep*=2;
                    if (xstep>5)xstep-=xstep%5;
                    if (xstep>10)xstep-=xstep%10;
                    if (xstep>100)xstep-=xstep%100;
                    if (xstep>1000)xstep-=xstep%1000;
                }
                for (int x=xstep;x<dc-Math.ceil(xstep/2);x+=xstep){
                    int tx=(int)Math.round(xoffset+((x)*xratio));
                    g2.drawLine(tx,getHeight()-yoffset+5,tx,getHeight()-yoffset-5);
                    String s=x+"";
                    g2.drawString(s,(int)Math.round(tx-(fM.getStringBounds(s,g2).getWidth()/2)),getHeight()-yoffset+5+(fM.getHeight()));
                }

                int tx=(int)Math.round(xoffset+((datawidth-1)*xratio));
                g2.drawLine(tx,getHeight()-yoffset+5,tx,getHeight()-yoffset-5);
                String s=(datawidth-1)+"";
                g2.drawString(s,(int)Math.round(tx-(fM.getStringBounds(s,g2).getWidth()/2)),getHeight()-yoffset+5+(fM.getHeight()));

                g2.drawString("0",xoffset-15,getHeight()-yoffset+20);
            }

            g.drawImage(buffer,0,0,null);
        }

        public void bubbleSort(int[][] arr) {

              boolean swapped = true;

              int j = 0;

              int[] tmp;

              while (swapped) {

                    swapped = false;

                    j++;

                    for (int i = 0; i < arr.length - j; i++) {

                          if (arr[i][0] > arr[i + 1][0]) {

                                tmp = arr[i];

                                arr[i] = arr[i + 1];

                                arr[i + 1] = tmp;

                                swapped = true;

                          }

                    }

              }

        }
    }

}
