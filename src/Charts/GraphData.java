/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Charts;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;


public class GraphData {
    private ArrayList<double[]> data;
    private ReentrantLock lock;
    private String[] datalabels;
    private boolean datachanged;

    public GraphData(String[] datalabels) {
        this.datalabels = datalabels;
        data=new ArrayList<double[]>();
        lock=new ReentrantLock();
        this.datachanged=true;
    }

    public boolean hasDataChanged(){
        lock.lock();
        boolean out=datachanged;
        datachanged=false;
        lock.unlock();
        return out;
    }

    public double getdata(int index, int row){
        lock.lock();
        double out=data.get(index)[row];
        lock.unlock();
        return out;
    }

    public int getDataCount(){
        lock.lock();
        int out=data.size();
        lock.unlock();
        return out;
    }

    public String[] getDatalabels() {
        return datalabels;
    }

    public void addData(double[] newdata){
        lock.lock();
        data.add(newdata);
        datachanged=true;
        lock.unlock();
    }

    public void clear(){
        lock.lock();
        data.clear();
        datachanged=true;
        lock.unlock();
    }

    public double[] getExtremes(int[][] columns){
        lock.lock();
        double[] out=new double[2];
        out[0]=Integer.MAX_VALUE;
        out[1]=-Integer.MAX_VALUE;
        for (double[] v:data){
            for (int y=0;y<columns.length;y++){
                if (v[columns[y][0]]<out[0])out[0]=Math.floor(v[columns[y][0]]);
                if (v[columns[y][0]]>out[1])out[1]=Math.ceil(v[columns[y][0]]);
            }
        }
        lock.unlock();
        return out;
    }

}
