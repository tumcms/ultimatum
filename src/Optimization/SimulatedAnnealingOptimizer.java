/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Optimization;

import Common.Pair;
import Main.Version;
import Scenario.*;
import Simulator.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;

public class SimulatedAnnealingOptimizer implements Optimizer{
    private OptimizerListener listener;
    private Scenario scenario,result;
    private HashMap<String, Long> resultingSchedule;
    private long resultingDuration;
    private boolean stop;

    private int decreaseDepth, currentDepth;
    private double timeWindow;
    private int threads;
    private int configState;
    private boolean onlyConsiderNewSwaps=false;
    private boolean includeDelays=true;
    private Random[] rnd;

    public boolean configure(JFrame parent){
        this.configState=0;
        this.decreaseDepth=3;
        this.threads=1;
        this.timeWindow=10;

        final JFrame frame=new JFrame(Version.ULTIMATUM_VERSION+" - Limited Depth Tree Search Settings");

        frame.setLayout(new BorderLayout());
        JPanel mainPanel=new JPanel(new GridLayout(5,1));
        final JTextField decreaseField=new JTextField(decreaseDepth+"");
        final JTextField timeField=new JTextField(timeWindow+"");
        final JTextField threadField=new JTextField(threads+"");
        final JCheckBox newSwapBox=new JCheckBox("Only consider new swaps at each level",onlyConsiderNewSwaps);
        final JCheckBox delayBox=new JCheckBox("Include delays",includeDelays);
        JPanel fieldPanel=new JPanel(new GridLayout(1,2));

        fieldPanel.add(new JLabel("Temperature decrease iterations:",JLabel.RIGHT));
        decreaseField.setToolTipText("The depth of the tree where temperature reaches 0");
        fieldPanel.add(decreaseField);
        mainPanel.add(fieldPanel);

        fieldPanel=new JPanel(new GridLayout(1,2));
        fieldPanel.add(new JLabel("Time Window in %:",JLabel.RIGHT));
        fieldPanel.add(timeField);
        mainPanel.add(fieldPanel);

        fieldPanel=new JPanel(new GridLayout(1,2));
        fieldPanel.add(new JLabel("Threads:",JLabel.RIGHT));
        fieldPanel.add(threadField);
        mainPanel.add(fieldPanel);

        fieldPanel=new JPanel(new FlowLayout());
        fieldPanel.add(delayBox);
        mainPanel.add(fieldPanel);

        fieldPanel=new JPanel(new FlowLayout());
        fieldPanel.add(newSwapBox);
        mainPanel.add(fieldPanel);

        fieldPanel=new JPanel(new FlowLayout());
        JButton okButton=new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                configState=1;
                frame.setEnabled(false);
            }
        });
        JButton cancelButton=new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                configState=-1;
                frame.setEnabled(false);
            }
        });

        fieldPanel.add(okButton);
        fieldPanel.add(cancelButton);
        frame.add(fieldPanel, BorderLayout.SOUTH);

        JTextArea description=new JTextArea("Swap Based Simulated Annealing Tree Search Algorithm\n\nM. Bügler, G. Dori, A. Borrmann:\n" +
                "Swap Based Process Schedule Optimization using Discrete-Event Simulation\n" +
                "In: Proceedings of the CONVR International Conference on Construction Applications of Virtual Reality, London, 2013\n\n"+
                "Temperature is defined as the probability of accepting an inferior swap.\n"+
                "It decreases linearly to reach 0 at the specified tree depth.\n"+
                "Depth is increased towards specified tree depth over the iterations");
        description.setEditable(false);
        description.setBorder(null);
        description.setLineWrap(true);

        frame.add(description,BorderLayout.NORTH);
        frame.add(mainPanel,BorderLayout.CENTER);
        frame.setSize(800,300);
        frame.setResizable(false);
        int x=(int)(Toolkit.getDefaultToolkit().getScreenSize().getWidth()-frame.getWidth())/2;
        int y=(int)(Toolkit.getDefaultToolkit().getScreenSize().getHeight()-frame.getHeight())/2;
        frame.setLocation(x,y);

        //try{Thread.sleep(500);}catch (Exception ex){}
        frame.setVisible(true);
        //frame.requestFocus();
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                configState=-1;
            }
        });


        while(configState==0){
            try{
                Thread.sleep(100);
            }
            catch (Exception ex){}
        }
        boolean res=configState==1;
        frame.setVisible(false);
        frame.dispose();
        decreaseDepth=Math.max(1, Integer.parseInt(decreaseField.getText()));
        timeWindow=Double.parseDouble(timeField.getText());
        threads=Integer.parseInt(threadField.getText());
        onlyConsiderNewSwaps=newSwapBox.isSelected();
        includeDelays=delayBox.isSelected();
        this.rnd=new Random[threads];
        for (int t=0;t<threads;t++)
            this.rnd[t]=new Random();
        return res;
    }




    @Override
    public void optimize(Scenario scenario) {
        this.scenario=scenario;
        this.stop=false;
        run();
    }



    @Override
    public long getResultingDuration() {
        return resultingDuration;
    }

    @Override
    public HashMap<String, Long> getResultingSchedule() {
        return resultingSchedule;
    }

    @Override
    public void stop() {
        stop=true;
    }

    @Override
    public void setOptimizerListener(OptimizerListener listener) {
        this.listener=listener;
    }

    @Override
    public Scenario getResultingScenario() {
        return result;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void run(){
        //System.out.println("-------------------------------------------------");
        //System.out.println("Running limited depth tree search optimization...");
        //System.out.println("-------------------------------------------------");

        Simulator.resetSimCounter();
        Simulator simulator=new Simulator(scenario);
        simulator.setIncludeDelays(includeDelays);
        simulator.disableOutput();
        simulator.simulate();

        Pair<Scenario,Long> res=new Pair<Scenario, Long>(scenario,simulator.getClock());
        long minScore=res.getValue();
        int iteration=1;
        for (currentDepth=1;currentDepth<=decreaseDepth;currentDepth++){
            String configString="SA(d="+decreaseDepth+";c="+currentDepth+";t="+timeWindow+";o="+(onlyConsiderNewSwaps?"1":"0")+";threads="+threads+")";
            simulator=new Simulator(res.getKey());
            simulator.setIncludeDelays(includeDelays);
            simulator.disableOutput();
            simulator.simulate();
            long start=simulator.getClock();


            if (listener!=null)
                listener.setProgressTitle(configString+" - Iteration "+(iteration++));
            res=treeSearch(res.getKey());



            boolean done=currentDepth>1 && res.getValue()==start;
            while (!done){

                if (stop)
                    break;
                if (listener!=null){
                    configString="SA(d="+decreaseDepth+";c="+currentDepth+";t="+timeWindow+";o="+(onlyConsiderNewSwaps?"1":"0")+";threads="+threads+")";
                    listener.setProgressTitle(configString+" - Iteration "+(iteration++));
                }
                res=treeSearch(res.getKey());
                if (res.getValue()==minScore)
                    done=true;
                else if (res.getValue()<minScore){
                    minScore=res.getValue();
                    if (listener!=null)
                        listener.setCurrentMinimum(res.getValue());

                }
            }
            //if (res==null||res.getKey()==null){
            //    res=new Pair<Scenario,Long>(res,minScore);
            //}
        }

        simulator=new Simulator(res.getKey());
        simulator.setIncludeDelays(includeDelays);
        simulator.disableOutput();
        simulator.simulate();
        resultingDuration=simulator.getClock();
        resultingSchedule=simulator.getExecutionTimes();
        result=simulator.getPrioritizedScenario();
        listener.setSimCount(Simulator.getSimCounter());
        //System.out.println(resultingSchedule);

    }

    public Pair<Scenario,Long> treeSearch(Scenario scenario){
        Simulator simulator=new Simulator(scenario);
        simulator.setIncludeDelays(includeDelays);
        simulator.enableSwapCollection();
        simulator.disableOutput();
        simulator.simulate();
        if (listener!=null)
            listener.setCurrentMinimum(simulator.getClock());
        Scenario prioritizedScenario=simulator.getPrioritizedScenario();
        if (threads>1)
            return treeSearchMultiThreaded(prioritizedScenario, simulator.getSwaps(), new LinkedList<TaskSwap>(), new Pair<Scenario, Long>(prioritizedScenario, simulator.getClock()), 0, -1);
        return treeSearch(prioritizedScenario, simulator.getSwaps(),new LinkedList<TaskSwap>(),new Pair<Scenario, Long>(prioritizedScenario,simulator.getClock()), 0,-1,0);
    }

    public Pair<Scenario,Long> treeSearch(Scenario scenario, LinkedList<TaskSwap> swaps, LinkedList<TaskSwap> previousSwaps,Pair<Scenario,Long> min, int depth, long lastSwapTime, int thread){
        if (depth>currentDepth)
            return min;
        Simulator simulator=new Simulator(scenario);
        simulator.setIncludeDelays(includeDelays);
        int x=0;
        double p=1-(depth/(double)decreaseDepth);
        System.out.println(p);
        for (TaskSwap swap:swaps){
            if (!onlyConsiderNewSwaps||!previousSwaps.contains(swap)){
                if (stop)
                    return min;

                if (Simulator.getSimCounter()%100==0 && listener!=null)
                    listener.setSimCount(Simulator.getSimCounter());
                if (depth==0){
                    double progress=1000*(x++)/(double)swaps.size();
                    //System.err.println(min.getValue()+" "+depth+" "+progress);
                    if (listener!=null)
                        listener.setProgress((int)Math.round(progress));

                }
                if (swap.getTime()>=lastSwapTime && ( depth==0 || swap.getTime()<=lastSwapTime+(timeWindow*simulator.getClock()/100.0)        )){
                    long nextSwapTime=swap.getTime();
                    simulator.setAppliedSwap(swap);
                    simulator.disableOutput();
                    simulator.enableSwapCollection();
                    simulator.simulate();
                    if (simulator.getClock()<min.getValue()){
                        min=new Pair<Scenario, Long>(simulator.getPrioritizedScenario(),simulator.getClock());
                        //if (listener!=null)
                        //    listener.setCurrentMinimum(min.getValue());
                    }

                    if (depth<currentDepth-1 && (p==1 || simulator.getClock()<min.getValue() || rnd[thread].nextDouble()<p)){
                        Pair<Scenario,Long> res=treeSearch(simulator.getPrioritizedScenario(),simulator.getSwaps(),swaps,min,depth+1,nextSwapTime,thread);
                        if (res.getValue()<min.getValue()){
                            min=res;
                            //System.err.println("min: "+min.getValue());
                            if ((depth==0||threads<2)&&listener!=null)
                                listener.setCurrentMinimum(min.getValue());
                        }

                    }
                }
            }
        }
        return min;
    }


    public Pair<Scenario,Long> treeSearchMultiThreaded(Scenario scenario, LinkedList<TaskSwap> swaps, LinkedList<TaskSwap> previousSwaps,Pair<Scenario,Long> min, int depth, long lastSwapTime){
        if (depth>currentDepth)
            return min;
        Simulator simulator=new Simulator(scenario);
        simulator.setIncludeDelays(includeDelays);
        int x=0;
        TreeSearchThread[] threads=new TreeSearchThread[this.threads];
        for (int t=0;t<threads.length;t++)
            threads[t]=new TreeSearchThread(t);

        Pair<Scenario,Long> res;
        boolean assigned=false;

        double p=1-(depth/(double)decreaseDepth);
        for (TaskSwap swap:swaps){
            if (!onlyConsiderNewSwaps||!previousSwaps.contains(swap)){
                if (stop)
                    return min;

                if (Simulator.getSimCounter()%100==0 && listener!=null)
                    listener.setSimCount(Simulator.getSimCounter());
                if (depth==0){
                    double progress=1000*(x++)/(double)swaps.size();
                    //System.err.println(min.getValue()+" "+depth+" "+progress);
                    if (listener!=null)
                        listener.setProgress((int)Math.round(progress));

                }
                if (swap.getTime()>=lastSwapTime && ( depth==0 || swap.getTime()<=lastSwapTime+(timeWindow*simulator.getClock()/100.0)        )){
                    long nextSwapTime=swap.getTime();
                    simulator.setAppliedSwap(swap);
                    simulator.disableOutput();
                    simulator.enableSwapCollection();
                    simulator.simulate();

                    if (simulator.getClock()<min.getValue()){
                        min=new Pair<Scenario, Long>(simulator.getPrioritizedScenario(),simulator.getClock());
                        if (listener!=null)
                            listener.setCurrentMinimum(min.getValue());
                    }

                    if (depth>currentDepth-1 && (p==1 || simulator.getClock()<min.getValue() || rnd[0].nextDouble()<p)){
                        assigned=false;
                        while(!assigned){
                            for (int tx=0;tx<threads.length;tx++){
                                if (threads[tx].isDone()){
                                    res=threads[tx].getRes();
                                    if (res!=null){
                                        //System.err.println("Thread "+tx+" finished");
                                        if (res.getValue()<min.getValue()){
                                            min=res;
                                            //System.err.println("min: "+min.getValue());
                                            if (listener!=null)
                                                listener.setCurrentMinimum(min.getValue());
                                        }
                                    }
                                    assigned=true;
                                    threads[tx]=new TreeSearchThread(tx);
                                    threads[tx].setData(simulator.getPrioritizedScenario(),simulator.getSwaps(),swaps,min,depth+1,nextSwapTime);
                                    threads[tx].start();
                                    //System.err.println("Thread "+tx+" started");
                                    break;
                                }
                            }
                            if (assigned)
                                break;
                            try{Thread.sleep(10);}catch (Exception ex){}
                        }
                    }
                }
            }
        }

        boolean done=false;
        while(!done){
            done=true;
            int tx=0;
            for (TreeSearchThread t:threads){
                if (t.isDone()){
                    res=t.getRes();
                    if (res!=null){
                        t.reset();
                        //System.err.println("Thread "+tx+" finished");
                        if(res.getValue()<min.getValue()){
                            min=res;
                            //System.err.println("min: "+min.getValue());
                            if (listener!=null)
                                listener.setCurrentMinimum(min.getValue());
                        }
                    }
                }
                else
                    done=false;
                tx++;
            }
        }

        return min;
    }


    private class TreeSearchThread extends Thread{
        private Scenario scenario;
        private LinkedList<TaskSwap> swaps, previousSwaps;
        private int depth;
        private Pair<Scenario,Long> min;
        private long nextSwapTime;
        private Pair<Scenario,Long> res;
        private boolean done;
        private int thread;

        private TreeSearchThread(int thread) {
            this.thread=thread;
            this.done=true;
        }

        private void setData(Scenario scenario, LinkedList<TaskSwap> swaps, LinkedList<TaskSwap> previousSwaps, Pair<Scenario,Long> min, int depth, long nextSwapTime) {
            this.scenario = scenario;
            this.swaps = swaps;
            this.previousSwaps = previousSwaps;
            this.min = min;
            this.depth = depth;
            this.nextSwapTime = nextSwapTime;
            this.done=false;
        }

        @Override
        public void run() {
            try{
                this.res=treeSearch(scenario,swaps,previousSwaps,min,depth,nextSwapTime,thread);
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
            this.done=true;
        }

        private Pair<Scenario, Long> getRes() {
            return res;
        }

        private boolean isDone() {
            return done;
        }

        private void reset(){
            this.res=null;
        }
    }
}
