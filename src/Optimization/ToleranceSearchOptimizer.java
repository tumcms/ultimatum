
/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Optimization;

import Main.Version;
import Scenario.*;
import Simulator.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.LinkedList;


public class ToleranceSearchOptimizer implements Optimizer {
    private OptimizerListener listener;
    private Simulator simulator;
    private Scenario scenario,result;
    private HashMap<String, Long> resultingSchedule;
    private long resultingDuration;
    private boolean stop;
    private double tolerance;
    private int threads;

    private int configState;
    private boolean veryGreedy=false;
    private boolean includeDelays=true;

    @Override
    public void optimize(Scenario scenario) {
        this.scenario=scenario;
        this.stop=false;
        run();
    }


    @Override
    public boolean configure(JFrame parent){
        this.configState=0;
        this.tolerance=10;
        this.threads=1;

        final JFrame frame=new JFrame(Version.ULTIMATUM_VERSION+" - Limited Depth Tree Search Settings");

        frame.setLayout(new BorderLayout());
        JPanel mainPanel=new JPanel(new GridLayout(3,1));
        final JTextField toleranceField=new JTextField(tolerance+"");
        //final JTextField threadsField=new JTextField(threads+"");
        final JCheckBox delayBox=new JCheckBox("Include delays",includeDelays);
        JPanel fieldPanel=new JPanel(new GridLayout(1,2));

        fieldPanel.add(new JLabel("Tolerance:",JLabel.RIGHT));
        toleranceField.setToolTipText("Tolerance in % of makespan");
        fieldPanel.add(toleranceField);
        mainPanel.add(fieldPanel);

        /*fieldPanel.add(new JLabel("Threads:",JLabel.RIGHT));
        threadsField.setToolTipText("Threads");
        fieldPanel.add(threadsField);
        mainPanel.add(fieldPanel);*/


        fieldPanel=new JPanel(new FlowLayout());
        fieldPanel.add(delayBox);
        mainPanel.add(fieldPanel);

        fieldPanel=new JPanel(new FlowLayout());
        JButton okButton=new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                configState=1;
                frame.setEnabled(false);
            }
        });
        JButton cancelButton=new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                configState=-1;
                frame.setEnabled(false);
            }
        });

        fieldPanel.add(okButton);
        fieldPanel.add(cancelButton);
        frame.add(fieldPanel, BorderLayout.SOUTH);

        JTextArea description=new JTextArea("Swap Based Tolerance Search Algorithm\n\nM. Bügler, G. Dori, A. Borrmann:\n" +
                "Swap Based Process Schedule Optimization using Discrete-Event Simulation\n" +
                "In: Proceedings of the CONVR International Conference on Construction Applications of Virtual Reality, London, 2013\n\n"+
                "Temperature is defined as the probability of accepting an inferior swap.\n"+
                "It decreases linearly to reach 0 at the specified tree depth.\n"+
                "Depth is increased towards specified tree depth over the iterations");
        description.setEditable(false);
        description.setBorder(null);
        description.setLineWrap(true);

        frame.add(description,BorderLayout.NORTH);
        frame.add(mainPanel,BorderLayout.CENTER);
        frame.setSize(800,300);
        frame.setResizable(false);
        int x=(int)(Toolkit.getDefaultToolkit().getScreenSize().getWidth()-frame.getWidth())/2;
        int y=(int)(Toolkit.getDefaultToolkit().getScreenSize().getHeight()-frame.getHeight())/2;
        frame.setLocation(x,y);

        //try{Thread.sleep(500);}catch (Exception ex){}
        frame.setVisible(true);
        //frame.requestFocus();
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                configState=-1;
            }
        });


        while(configState==0){
            try{
                Thread.sleep(100);
            }
            catch (Exception ex){}
        }
        boolean res=configState==1;
        frame.setVisible(false);
        frame.dispose();
        includeDelays=delayBox.isSelected();
        return res;
    }
    @Override
    public long getResultingDuration() {
        return resultingDuration;
    }

    @Override
    public HashMap<String, Long> getResultingSchedule() {
        return resultingSchedule;
    }

    @Override
    public void stop() {
        stop=true;
    }

    @Override
    public void setOptimizerListener(OptimizerListener listener) {
        this.listener=listener;
    }


    public void run(){

        String configString="TS(t="+tolerance+")";

        if (listener!=null)
            listener.setProgressTitle(configString+" - Depth 0 - Frontier: 1");

        Simulator.resetSimCounter();
        Simulator simulator=new Simulator(scenario);
        simulator.setIncludeDelays(includeDelays);
        simulator.disableOutput();
        simulator.enableSwapCollection();
        simulator.simulate();


        TaskSwapSet minSet=new TaskSwapSet(simulator.getClock());

        LinkedList<TaskSwapSet> frontier=new LinkedList<TaskSwapSet>();
        LinkedList<TaskSwap> swaps=simulator.getSwaps();


        //if (listener!=null)
        //    listener.setProgressTitle(configString+" - Depth 1 - Frontier: "+swaps.size());

        for (TaskSwap s:swaps){
            if (stop)
                break;
            simulator.setAppliedSwap(s);
            simulator.simulate();
            if (listener!=null && Simulator.getSimCounter()%100==0)
                listener.setSimCount(Simulator.getSimCounter());
            frontier.add(new TaskSwapSet(s,simulator.getClock()));
            if (simulator.getClock()<minSet.getScore()){
                minSet=new TaskSwapSet(s,simulator.getClock());
                if (listener!=null)
                    listener.setCurrentMinimum(simulator.getClock());

            }
        }

        int depth=1;
        TaskSwapSet currentSet=null;
        long acceptCount;
        double currentTolerance=tolerance;
        while (!frontier.isEmpty()){





            currentTolerance=Math.max(0, ((tolerance - 3 * depth)));            //TODO ANNAHME  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX






            acceptCount=0;
            if (stop)
                break;
            if (listener!=null)
                listener.setProgressTitle(configString+" - Depth "+depth+" - Frontier: "+frontier.size());
            LinkedList<TaskSwapSet> nextFrontier=new LinkedList<TaskSwapSet>();

            for (TaskSwapSet s:frontier){
                if (stop)
                    break;

                if (s.getScore()<minSet.getScore()*(1+(currentTolerance/100.0))) {

                    acceptCount++;


                    simulator.setAppliedSwaps(s);
                    simulator.simulate();
                    if (listener!=null && Simulator.getSimCounter()%100==0)
                        listener.setSimCount(Simulator.getSimCounter());
                    swaps=simulator.getSwaps();
                    for (TaskSwap s2:swaps){
                        if (stop)
                            break;

                        currentSet=new TaskSwapSet(s.getScore());
                        for (TaskSwap s3:s)
                            currentSet.add(s3);
                        currentSet.add(s2);


                        simulator.setAppliedSwaps(currentSet);
                        simulator.simulate();
                        if (listener!=null && Simulator.getSimCounter()%100==0)
                            listener.setSimCount(Simulator.getSimCounter());

                        currentSet.setScore(simulator.getClock());
                        nextFrontier.add(currentSet);
                        if (simulator.getClock()<minSet.getScore()){
                            minSet=new TaskSwapSet(currentSet,simulator.getClock());
                            if (listener!=null)
                                listener.setCurrentMinimum(simulator.getClock());
                        }
                    }
                }

            }
            frontier=nextFrontier;
            depth++;
            System.out.println("Accepted nodes: "+acceptCount+" - Current tolerance: "+currentTolerance);
        }




        simulator.setAppliedSwaps(minSet);
        simulator.simulate();
        resultingDuration=simulator.getClock();
        resultingSchedule=simulator.getExecutionTimes();
        result=simulator.getPrioritizedScenario();
        listener.setSimCount(Simulator.getSimCounter());
        //System.out.println(resultingSchedule);

    }

    @Override
    public Scenario getResultingScenario() {
        return result;
    }
}
