/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Optimization;

import Scenario.ExampleGenerator;
import Main.Version;
import Scenario.*;
import Simulator.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.LinkedList;

public class GreedyOptimizer implements Optimizer {
    private OptimizerListener listener;
    private Simulator simulator;
    private Scenario scenario,result;
    private HashMap<String, Long> resultingSchedule;
    private long resultingDuration;
    private boolean stop;

    private int configState;
    private boolean includeDelays=true;

    @Override
    public void optimize(Scenario scenario) {
        this.scenario=scenario;
        this.stop=false;
        run();
    }



    @Override
    public boolean configure(JFrame parent){
        this.configState=0;
        this.includeDelays=true;
        final JFrame frame=new JFrame(Version.ULTIMATUM_VERSION+" - Greedy Tree Search Algorithm");

        frame.setLayout(new BorderLayout());

        JPanel fieldPanel=new JPanel(new FlowLayout());
        final JCheckBox includeDelaysBox=new JCheckBox("Include delays",includeDelays);
        fieldPanel.add(includeDelaysBox);
        frame.add(fieldPanel,BorderLayout.CENTER);

        fieldPanel=new JPanel(new FlowLayout());
        JButton okButton=new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                configState=1;
                frame.setEnabled(false);
            }
        });
        JButton cancelButton=new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                configState=-1;
                frame.setEnabled(false);
            }
        });

        fieldPanel.add(okButton);
        fieldPanel.add(cancelButton);
        frame.add(fieldPanel, BorderLayout.SOUTH);

        JTextArea description=new JTextArea("Swap based Greedy Search Algorithm\n\nM. Bügler, G. Dori, A. Borrmann:\n" +
                "Swap Based Process Schedule Optimization using Discrete-Event Simulation\n" +
                "In: Proceedings of the CONVR International Conference on Construction Applications of Virtual Reality, London, 2013\n");
        description.setEditable(false);
        description.setBorder(null);
        description.setLineWrap(true);

        frame.add(description,BorderLayout.NORTH);

        frame.setSize(800,300);
        frame.setResizable(false);
        int x=(int)(Toolkit.getDefaultToolkit().getScreenSize().getWidth()-frame.getWidth())/2;
        int y=(int)(Toolkit.getDefaultToolkit().getScreenSize().getHeight()-frame.getHeight())/2;
        frame.setLocation(x,y);

        //try{Thread.sleep(500);}catch (Exception ex){}
        frame.setVisible(true);
        //frame.requestFocus();
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                configState=-1;
            }
        });


        while(configState==0){
            try{
                Thread.sleep(100);
            }
            catch (Exception ex){}
        }
        boolean res=configState==1;
        frame.setVisible(false);
        frame.dispose();
        includeDelays=includeDelaysBox.isSelected();
        return res;
    }

    @Override
    public long getResultingDuration() {
        return resultingDuration;
    }

    @Override
    public HashMap<String, Long> getResultingSchedule() {
        return resultingSchedule;
    }

    @Override
    public void stop() {
        stop=true;
    }

    @Override
    public void setOptimizerListener(OptimizerListener listener) {
        this.listener=listener;
    }


    private void run() {
        System.out.println("------------------------------");
        System.out.println("Running greedy optimization...");
        System.out.println("------------------------------");
        //scenario.shuffleTasks();
        Simulator.resetSimCounter();

        simulator=new Simulator(scenario);
        simulator.setIncludeDelays(includeDelays);
        simulator.disableOutput();
        simulator.enableSwapCollection();
        simulator.simulate();
        if (listener!=null)
            listener.setCurrentMinimum(simulator.getClock());


        System.err.print(simulator.getClock()+",");

        Scenario newScen=simulator.getPrioritizedScenario();





        long minTime=simulator.getClock();
        LinkedList<TaskSwap> swaps=simulator.getSwaps();
        LinkedList<TaskSwap> appliedSwaps=new LinkedList<TaskSwap>();

        //Scenario newScen=scenarioGenerator.generateScenario();
        simulator=new Simulator(newScen);
        simulator.setIncludeDelays(includeDelays);
        simulator.disableOutput();
        simulator.enableSwapCollection();

        LinkedList<TaskSwap> bestSwaps=new LinkedList<TaskSwap>();


        boolean newOpt=true;

        int count=0;
        int progress=0;
        int iteration=1;
        while(newOpt){
            if (stop)
                break;
            if (listener!=null)
                listener.setProgressTitle("Greedy(d="+(includeDelays?"1":"0")+") - Iteration "+iteration++);
            newOpt=false;
            TaskSwap bestSwap=null;
            count=0;
            progress=0;
            for (TaskSwap swap:swaps){
                //if (swap.isAntiDelay())
                //    System.err.println("ANTI");
                if (stop)
                    break;

                count++;
                if (listener!=null&&progress!=(1000*count)/swaps.size()){
                    progress=(1000*count)/swaps.size();
                    listener.setProgress(progress);
                }
                //System.out.print("testing: "+swap.getTaskA()+" <> "+swap.getTaskB()+" = ");
                appliedSwaps.clear();
                for (TaskSwap ts:bestSwaps)
                    appliedSwaps.add(ts);
                appliedSwaps.add(swap);
                simulator.setAppliedSwaps(appliedSwaps);
                simulator.simulate();
                //scenario.createXMLFile(new File("/tmp/fun.xml"),simulator.getExecutionTimes());
                if (listener!=null && Simulator.getSimCounter()%100==0)
                    listener.setSimCount(Simulator.getSimCounter());
                //System.exit(0);
                //System.out.println(simulator.getClock());
                if (simulator.getClock()<minTime){
                    minTime=simulator.getClock();
                    bestSwap=swap;
                    newOpt=true;
                    System.err.print(simulator.getClock()+", ");
                    if (listener!=null)
                        listener.setCurrentMinimum(simulator.getClock());
                }
            }
            //System.err.println(">>>>>>>>>>>>>>>NEWOPT "+newOpt);
            if (newOpt){
                //System.err.println(">>>>>>>>>>>>>>>>>>>>>>SWAPS "+swaps.size());
                bestSwaps.add(bestSwap);
                appliedSwaps.clear();
                for (TaskSwap ts:bestSwaps)
                    appliedSwaps.add(ts);
                simulator.setAppliedSwaps(appliedSwaps);
                simulator.simulate();
                swaps=simulator.getSwaps();
                //System.err.println(">>>>>>>>>>>>>>>>>>>>>>!!! SWAPS "+swaps.size());
                /*for (TaskSwap swap:swaps){
                    if (swap.isAntiDelay()){
                        System.err.println(">ANTIDELAY "+swap.getTaskA());
                    }
                } */
            }
        }

        appliedSwaps.clear();
        for (TaskSwap ts:bestSwaps)
            appliedSwaps.add(ts);
        simulator.setAppliedSwaps(appliedSwaps);
        simulator.simulate();


        resultingSchedule=simulator.getExecutionTimes();
        resultingDuration=minTime;

        result=simulator.getPrioritizedScenario();
        if (listener!=null)
            listener.setSimCount(Simulator.getSimCounter());
    }

    @Override
    public Scenario getResultingScenario() {
        return result;
    }

    public static void main(String[] args){
        GreedyOptimizer optimizer=new GreedyOptimizer();
        optimizer.optimize(ExampleGenerator.generate());
    }


}
