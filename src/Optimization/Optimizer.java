/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Optimization;

import Scenario.Scenario;

import javax.swing.*;
import java.util.HashMap;


public interface Optimizer{
    public boolean configure(JFrame parent);
    public void optimize(Scenario scenario);
    public long getResultingDuration();
    public HashMap<String,Long> getResultingSchedule();
    public Scenario getResultingScenario();
    public void stop();
    public void setOptimizerListener(OptimizerListener listener);

}
