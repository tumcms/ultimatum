/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package ReducedSimulator;




import Common.Pair;
import Scenario.*;

import java.util.*;

public class ReducedSimulator {


    public static int simcounter=0;

    public static ReducedSimulationResults simulate(ReducedScenario in, List<int[]> swaps) {
        simcounter++;

        List<Integer> waitingTasks = new LinkedList<Integer>();
        List<Pair<Long,Integer>> runningTasks = new LinkedList<Pair<Long, Integer>>();
        List<Pair<Long,Integer>> finishedTasks = new LinkedList<Pair<Long, Integer>>();

        long[][] tasks=in.getTasks();

        boolean[] finished=new boolean[tasks.length];

        int[] delays=new int[tasks.length];
        /*for (int x=0;x<dels.length;x++){
            delays[dels[x]]++;
        } */

        int[] resources = new int[in.getResources().length];
        for (int x = 0; x < in.getResources().length; x++) {
            resources[x] = in.getResources()[x];
        }

        for (int x = 0; x < tasks.length; x++) {
            int y = x;
            for (int[] swap:swaps) {
                if (swap[1]<0){
                    if (y == swap[0]) {
                        delays[swap[0]] += (swap[1] == -1 ? 1 : -1);    // Swap(t,-1) => delay, Swap(t,-2) => antidelay
                        //System.out.println(swap[0] + " delay " + delays[swap[0]]);
                    }
                }
                else if (y == swap[0])
                    y = swap[1];
                else if (y == swap[1])
                    y = swap[0];
            }
            waitingTasks.add(y);
            finished[y]=false;
        }


        long time = 0;
        long nextStep=0;
        long temp;

        while (waitingTasks.size() > 0 || runningTasks.size() >0){

            if (!runningTasks.isEmpty()) {
                nextStep = 0;
                for (Pair<Long, Integer> currentTask : runningTasks) {
                    temp = currentTask.getKey() + tasks[currentTask.getValue()][0];
                    if (nextStep == 0 || nextStep > temp)
                        nextStep = temp;
                }
                time = nextStep;
            }

            Pair<Long,Integer> currentTask;
            for (Iterator<Pair<Long,Integer>> i=runningTasks.iterator(); i.hasNext();) {
                currentTask=i.next();
                if (currentTask.getKey()+tasks[currentTask.getValue()][0]==time){
                    freeResources(tasks[currentTask.getValue()],resources);
                    finishedTasks.add(currentTask);
                    finished[currentTask.getValue()]=true;
                    i.remove();
                }
            }


            for (Iterator<Integer> i=waitingTasks.iterator(); i.hasNext();) {
                currentTask=new Pair<Long, Integer>(time,i.next());
                if (checkResources(tasks[currentTask.getValue()],resources)){
                    if (checkDependencies(tasks[currentTask.getValue()],finished)) {
                        if (delays[currentTask.getValue()]>0){
                            delays[currentTask.getValue()]--;
                        }
                        else {
                            useResources(tasks[currentTask.getValue()], resources);
                            runningTasks.add(currentTask);
                            i.remove();
                        }
                    }
                }
            }
        }
        long[] res=new long[tasks.length];
        for (Pair<Long,Integer> ft:finishedTasks){
            res[ft.getValue()]=ft.getKey();
        }
        return new ReducedSimulationResults(res,time);
    }

    public static ReducedSimulationResults simulate(ReducedScenario in, List<int[]> swaps, double stdev) {
        simcounter++;

        List<Integer> waitingTasks = new LinkedList<Integer>();
        List<Pair<Long,Integer>> runningTasks = new LinkedList<Pair<Long, Integer>>();
        List<Pair<Long,Integer>> finishedTasks = new LinkedList<Pair<Long, Integer>>();
        HashMap<Integer,Long> durations=new HashMap<Integer, Long>();

        Random rnd=new Random();

        long[][] tasks=in.getTasks();

        boolean[] finished=new boolean[tasks.length];

        int[] delays=new int[tasks.length];
        /*for (int x=0;x<dels.length;x++){
            delays[dels[x]]++;
        } */

        int[] resources = new int[in.getResources().length];
        for (int x = 0; x < in.getResources().length; x++) {
            resources[x] = in.getResources()[x];
        }

        for (int x = 0; x < tasks.length; x++) {
            int y = x;
            for (int[] swap:swaps) {
                if (swap[1]<0){
                    if (y == swap[0]) {
                        delays[swap[0]] += (swap[1] == -1 ? 1 : -1);    // Swap(t,-1) => delay, Swap(t,-2) => antidelay
                        //System.out.println(swap[0] + " delay " + delays[swap[0]]);
                    }
                }
                else if (y == swap[0])
                    y = swap[1];
                else if (y == swap[1])
                    y = swap[0];
            }
            waitingTasks.add(y);
            finished[y]=false;
            durations.put(y, Math.max(1,Math.round(tasks[y][0] + rnd.nextGaussian() * stdev)));
        }


        long time = 0;
        long nextStep=0;
        long temp;

        while (waitingTasks.size() > 0 || runningTasks.size() >0){

            if (!runningTasks.isEmpty()) {
                nextStep = 0;
                for (Pair<Long, Integer> currentTask : runningTasks) {
                    temp = currentTask.getKey() + durations.get(currentTask.getValue());
                    if (nextStep == 0 || nextStep > temp)
                        nextStep = temp;
                }
                time = nextStep;
            }

            Pair<Long,Integer> currentTask;
            for (Iterator<Pair<Long,Integer>> i=runningTasks.iterator(); i.hasNext();) {
                currentTask=i.next();
                if (currentTask.getKey()+durations.get(currentTask.getValue())==time){
                    freeResources(tasks[currentTask.getValue()],resources);
                    finishedTasks.add(currentTask);
                    finished[currentTask.getValue()]=true;
                    i.remove();
                }
            }


            for (Iterator<Integer> i=waitingTasks.iterator(); i.hasNext();) {
                currentTask=new Pair<Long, Integer>(time,i.next());
                if (checkResources(tasks[currentTask.getValue()],resources)){
                    if (checkDependencies(tasks[currentTask.getValue()],finished)) {
                        if (delays[currentTask.getValue()]>0){
                            delays[currentTask.getValue()]--;
                        }
                        else {
                            useResources(tasks[currentTask.getValue()], resources);
                            runningTasks.add(currentTask);
                            i.remove();
                        }
                    }
                }
            }
        }
        long[] res=new long[tasks.length];
        for (Pair<Long,Integer> ft:finishedTasks){
            res[ft.getValue()]=ft.getKey();
        }
        return new ReducedSimulationResults(res,time);
    }


    public static ReducedSimulationResults simulateAndCollectSwaps(ReducedScenario in, List<int[]> swaps) {
        simcounter++;
        List<Integer> waitingTasks = new LinkedList<Integer>();
        List<Pair<Long,Integer>> runningTasks = new LinkedList<Pair<Long, Integer>>();
        List<Pair<Long,Integer>> finishedTasks = new LinkedList<Pair<Long, Integer>>();

        long[][] tasks=in.getTasks();

        boolean[] finished=new boolean[tasks.length];

        int[] delays=new int[tasks.length];
        /*for (int x=0;x<dels.length;x++){
            delays[dels[x]]++;
        } */

        int[] resources = new int[in.getResources().length];
        for (int x = 0; x < in.getResources().length; x++) {
            resources[x] = in.getResources()[x];
        }

        for (int x = 0; x < tasks.length; x++) {
            int y = x;
            for (int[] swap:swaps) {
                if (swap[1]<0){
                    if (y == swap[0]) {
                        delays[swap[0]] += (swap[1] == -1 ? 1 : -1);    // Swap(t,-1) => delay, Swap(t,-2) => antidelay
                        //System.out.println(swap[0] + " delay " + delays[swap[0]]);
                    }
                }
                else if (y == swap[0])
                    y = swap[1];
                else if (y == swap[1])
                    y = swap[0];
            }
            waitingTasks.add(y);
            finished[y]=false;
        }


        long time = 0;
        long nextStep=0;
        long temp;

        boolean[] executable =new boolean[tasks.length];
        List<int[]> feasibleSwaps=new LinkedList<int[]>();
        
        
        while (waitingTasks.size() > 0 || runningTasks.size() >0){

            if (!runningTasks.isEmpty()) {
                nextStep = 0;
                for (Pair<Long, Integer> currentTask : runningTasks) {
                    temp = currentTask.getKey() + tasks[currentTask.getValue()][0];
                    if (nextStep == 0 || nextStep > temp)
                        nextStep = temp;
                }
                time = nextStep;
            }

            Pair<Long,Integer> currentTask;
            for (Iterator<Pair<Long,Integer>> i=runningTasks.iterator(); i.hasNext();) {
                currentTask=i.next();
                if (currentTask.getKey()+tasks[currentTask.getValue()][0]==time){
                    freeResources(tasks[currentTask.getValue()],resources);
                    finishedTasks.add(currentTask);
                    finished[currentTask.getValue()]=true;
                    i.remove();
                }
            }


            for (Integer t:waitingTasks) {
                executable[t]=false;
                if (checkResources(tasks[t],resources)){
                    if (checkDependencies(tasks[t],finished)) {
                        if (delays[t]<=0){
                            executable[t]=true;
                        }
                    }
                }
            }            
            
            for (Iterator<Integer> i=waitingTasks.iterator(); i.hasNext();) {
                currentTask=new Pair<Long, Integer>(time,i.next());
                if (checkResources(tasks[currentTask.getValue()],resources)){
                    if (checkDependencies(tasks[currentTask.getValue()],finished)) {
                        if (delays[currentTask.getValue()]>0){
                            delays[currentTask.getValue()]--;
                            feasibleSwaps.add(new int[]{currentTask.getValue(),-2});
                        }
                        else {
                            useResources(tasks[currentTask.getValue()], resources);
                            if (!runningTasks.isEmpty()){
                                feasibleSwaps.add(new int[]{currentTask.getValue(),-1});
                            }
                            runningTasks.add(currentTask);
                            i.remove();
                            boolean stillexecutable;
                            for (Integer t:waitingTasks) {
                                if (executable[t]) {
                                    stillexecutable = false;
                                    if (checkResources(tasks[t], resources)) {
                                        if (checkDependencies(tasks[t], finished)) {
                                            if (delays[t] <= 0) {
                                                stillexecutable = true;
                                            }
                                        }
                                    }
                                    if (!stillexecutable && currentTask.getValue()!=t){
                                        feasibleSwaps.add(new int[]{currentTask.getValue(),t});
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        long[] res=new long[tasks.length];
        for (Pair<Long,Integer> ft:finishedTasks){
            res[ft.getValue()]=ft.getKey();
        }

        for (Iterator<int[]> i=feasibleSwaps.iterator();i.hasNext();){
            int[] swap=i.next();
            for (int[] swap2:swaps){
                if (swap[0]==swap2[0] && swap[1]==swap2[1]){
                    i.remove();
                    break;
                }
                else if (swap[0]==swap2[1] && swap[1]==swap2[0]){
                    i.remove();
                    break;
                }
                else if (swap[1]==-1 && swap2[1]==-2 && swap[0]==swap2[0]){
                    i.remove();
                    break;
                }
                else if (swap[1]==-2 && swap2[1]==-1 && swap[0]==swap2[0]){
                    i.remove();
                    break;
                }
            }
        }

        return new ReducedSimulationResults(res,feasibleSwaps,time);
        //return new Pair<long[], List<int[]>>(res,feasibleSwaps);
    }



    private static boolean checkDependencies(long[] task, List<Pair<Long,Integer>> finishedTasks){
        int nRes=(int)task[1];
        boolean satisfied;
        for (int x=2+2*nRes;x<task.length;x++){
            satisfied=false;
            for (Pair<Long,Integer> ft:finishedTasks){
                if (ft.getValue()==task[x]) {
                    satisfied = true;
                    break;
                }
            }
            if (!satisfied)
                return false;
        }
        return true;
    }

    private static boolean checkDependencies(long[] task, boolean[] finishedTasks){
        int nRes=(int)task[1];
        for (int x=2+2*nRes;x<task.length;x++){
            if (!finishedTasks[(int)task[x]])
                return false;
        }
        return true;
    }


    private static boolean checkResources(long[] task, int[] resources){
        long nRes=task[1];
        for (int x=0;x<nRes;x++){
            if (resources[(int)task[2+2*x]]<task[2+2*x+1])
                return false;
        }
        return true;
    }

    private static void useResources(long[] task, int[] resources){
        long nRes=task[1];
        for (int x=0;x<nRes;x++){
            resources[(int)task[2+2*x]]-=task[2+2*x+1];
        }
    }

    private static void freeResources(long[] task, int[] resources){
        long nRes=task[1];
        for (int x=0;x<nRes;x++){
            resources[(int)task[2+2*x]]+=task[2+2*x+1];
        }
    }

    public static void main(String[] args) throws Exception{


       /* ReducedSimulationResults res3=simulateAndCollectSwaps(new ReducedScenario(ExampleGenerator.generateRandomScenario(1,3,3,3,3)),new LinkedList<int[]>());
        System.out.println(res3.getFeasibleSwaps().size());
        System.out.println("\n\nFeasible Swaps:\n\n");
        for (int[] swap:res3.getFeasibleSwaps()) {
            System.out.println(swap[0] + " <> " + swap[1]);
        }

        System.exit(0);*/

        int mode=5;


        long start=System.currentTimeMillis();

        if (mode==0) {  //Load File
            String file="/home/homer/Documents/TUM/FAUST/MySim3/random-20.xml";

            ReducedScenario scenario = new ReducedScenario(XMLParser.parseXML(file));

            ReducedSimulationResults res = simulate(scenario, new LinkedList<int[]>());

            System.out.println("\n\nRes:\n\n");
            for (int x = 0; x < res.getStarttimes().length; x++) {
                System.out.println(x + " " + res.getStarttimes()[x]);
            }
        }
        else if (mode==1) { //Check runtime complexity

            int count=1000;
            int runsPerN=20;
            int step=1;

            double[] nt = new double[count/step];
            for (int n = 1; n < count; n+=step) {
                System.out.println(n);

                long ts = 0;
                for (int x = 0; x < runsPerN; x++) {
                    ReducedScenario scenario = new ReducedScenario(ExampleGenerator.generateRandomScenario(n, 10, 10, 10, 10));
                    long st = System.currentTimeMillis();
                    simulate(scenario, new LinkedList<int[]>());
                    ts += System.currentTimeMillis() - st;
                }
                nt[n/step] = ts / (double)runsPerN;
            }

            System.out.print("data=[");
            for (int x = 0; x < nt.length; x++) {
                System.out.print(nt[x] + ",");
            }
            System.out.println("];");
        }


        else if (mode==2) { //Check runtime complexity with swap collection

            int count=1000;
            int runsPerN=20;
            int step=1;

            double[] nt = new double[count/step];
            for (int n = 1; n < count; n+=step) {
                System.out.println(n);

                long ts = 0;
                for (int x = 0; x < runsPerN; x++) {
                    ReducedScenario scenario = new ReducedScenario(ExampleGenerator.generateRandomScenario(n, 10, 10, 10, 10));
                    long st = System.currentTimeMillis();
                    simulateAndCollectSwaps(scenario, new LinkedList<int[]>());
                    ts += System.currentTimeMillis() - st;
                }
                nt[n/step] = ts / (double)runsPerN;
            }

            System.out.print("data=[");
            for (int x = 0; x < nt.length; x++) {
                System.out.print(nt[x] + ",");
            }
            System.out.println("];");
        }

        if (mode==3) {  //Run Example


            ReducedScenario scenario = new ReducedScenario(ExampleGenerator.generate());

            LinkedList<int[]> swaps=new LinkedList<int[]>();
            swaps.add(new int[]{8,12});
            swaps.add(new int[]{0,-1});
            swaps.add(new int[]{0,-1});
            swaps.add(new int[]{0,-2});

            ReducedSimulationResults res = simulate(scenario, swaps);

            System.out.println("\n\nRes:\n\n");
            for (int x = 0; x < res.getStarttimes().length; x++) {
                System.out.println(x + " " + res.getStarttimes()[x]);
            }

            ReducedSimulationResults res2 = simulateAndCollectSwaps(scenario, swaps);

            System.out.println("\n\nRes:\n\n");
            for (int x = 0; x < res2.getStarttimes().length; x++) {
                System.out.println(x + " " + res2.getStarttimes()[x]);
            }

            System.out.println("\n\nFeasible Swaps:\n\n");
            for (int[] swap:res2.getFeasibleSwaps()) {
                System.out.println(swap[0] + " <> " + swap[1]);
            }

        }

        if (mode==4) {  //Run Random Scenario

            int n=1000;

            ReducedScenario scenario = new ReducedScenario(ExampleGenerator.generateRandomScenario(n, 10, 10, 10, 10));

            ReducedSimulationResults res = simulate(scenario, new LinkedList<int[]>());

            System.out.println("\n\nRes:\n\n");
            for (int x = 0; x < res.getStarttimes().length; x++) {
                System.out.println(x + " " + res.getStarttimes()[x]);
            }
        }
        if (mode==5){
            ReducedScenario scenario = new ReducedScenario(ExampleGenerator.generateRandomScenario(30, 10, 10, 10, 10));
            for (int x=0;x<1000;x++){


                ReducedSimulationResults res = simulate(scenario, new LinkedList<int[]>(),2);
                System.out.print(res.getDuration()+", ");
            }

        }

        if (mode==6){

        }


        System.out.println("\n\n\nExecution took "+(System.currentTimeMillis()-start)+"ms\n");
    }
}
