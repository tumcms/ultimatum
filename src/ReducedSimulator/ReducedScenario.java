/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package ReducedSimulator;

import Scenario.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.List;

public class ReducedScenario {
    private long[][] tasks; //each Task {duration,no_resources, resource1_id, resource1_count,... ,deps,...}
    private int[] resources;

    private HashMap<Integer,String> resourceMap, taskMap;

    public ReducedScenario(Scenario in){
        this(in,false);
    }

    public ReducedScenario(Scenario in, boolean randomize){
        resourceMap=new HashMap<Integer, String>();
        taskMap=new HashMap<Integer, String>();

        HashMap<String, Integer> invResourceMap=new HashMap<String, Integer>();
        HashMap<String, Integer> invTaskMap=new HashMap<String, Integer>();

        Resource[] res=in.getResources();
        resources=new int[res.length];

        for (int x=0;x<res.length;x++){
            if (!resourceMap.containsValue(res[x].getType())) {
                resourceMap.put(x, res[x].getType());
                resources[x]=res[x].getCount();
            }
            else{
                resources[invResourceMap.get(res[x].getType())]+=res[x].getCount();
            }
            invResourceMap.put(res[x].getType(),x);
        }

        Task[] t;
        if (randomize)
            t=in.getTasks(true);
        else
            t=in.getTasks();

        tasks=new long[t.length][];

        for (int x=0;x<t.length;x++){
            taskMap.put(x,t[x].getName());
            invTaskMap.put(t[x].getName(),x);
        }
        for (int x=0;x<t.length;x++){
            int gr=0;
            for (int y=0;y<t[x].getResourceCount();y++){
                if (t[x].getResource(y).isGeometric())
                    gr++;
            }
            tasks[x]=new long[2+2*(t[x].getResourceCount()-gr)+t[x].getDependencyCount()];
            tasks[x][0]=t[x].getDuration();
            tasks[x][1]=t[x].getResourceCount()-gr;
            int z=0;
            for (int y=0;y<t[x].getResourceCount();y++){
                if (!t[x].getResource(y).isGeometric()) {
                    //System.out.println(invResourceMap.get(t[x].getResource(y).getType()));
                    tasks[x][2 + 2 * z] = invResourceMap.get(t[x].getResource(y).getType());
                    tasks[x][2 + 2 * z + 1] = t[x].getResource(y).getCount();
                    z++;
                }
            }
            for (int y=0;y<t[x].getDependencyCount();y++){
                tasks[x][2+2*(t[x].getResourceCount()-gr)+y]=invTaskMap.get(t[x].getDependency(y).getTask());
            }
        }

        /*System.out.println("Resources:\n");
        for (int x=0;x<resources.length;x++){
            System.out.println(x+" "+resourceMap.get(x)+":"+ resources[x]);
        }

        System.out.println("\n\nTasks:\n");
        for (int x=0;x<tasks.length;x++){
            System.out.print(x+" "+taskMap.get(x)+": ");
            for (int y=0;y<tasks[x].length;y++){
                System.out.print(tasks[x][y] + ", ");
            }
            System.out.println();
        } */

    }

    public long[][] getTasks(){
        return tasks;
    }

    public int[] getResources(){
        return resources;
    }

    public String getTaskName(int id){
        return taskMap.get(id);
    }



    public static void main(String[] args){
        new ReducedScenario(ExampleGenerator.generate());
    }
}
