/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package ReducedSimulator;




import Common.Pair;
import Scenario.*;

import java.util.*;

public class ReducedSimulatorSerial {


    public static int simcounter=0;



    private static long gcd(long a, long b) { return b==0 ? a : gcd(b, a%b); }

    public static ReducedSimulationResults simulate(ReducedScenario in, List<int[]> swaps) {
        simcounter++;

        List<Integer> waitingTasks = new LinkedList<Integer>();
        HashMap<Integer,Long> scheduledTasks = new HashMap<Integer, Long>();


        long[][] tasks=in.getTasks();


        long gcd=-1;
        long sum=0;

        for (int x = 0; x < tasks.length; x++) {
            sum+=tasks[x][0];
            if (tasks[x][0]>0) {
                if (gcd < 0)
                    gcd = tasks[x][0];
                else {
                    if (tasks[x][0] % gcd != 0) {
                        gcd = gcd(gcd, tasks[x][0]);
                    }
                }
            }
        }


        int[] resources = new int[in.getResources().length];
        for (int x = 0; x < in.getResources().length; x++) {
            resources[x] = in.getResources()[x];
        }
        int[][] resourceusage = new int[resources.length][(int)(sum/gcd)];

        boolean[] added=new boolean[tasks.length];

        boolean done=false;
        while (!done) {
            done=true;
            for (int x = 0; x < tasks.length; x++) {
                int y = x;
                for (int[] swap : swaps) {
                    if (swap[1] < 0) {
                    } else if (y == swap[0])
                        y = swap[1];
                    else if (y == swap[1])
                        y = swap[0];
                }
                if (!added[y]) {
                    if (checkDependencies(tasks[y], waitingTasks)) {
                        waitingTasks.add(y);
                        added[y] = true;
                    }
                    else
                        done=false;
                }
            }
        }

        long maxTime=0;

        long duration;
        int nRes;
        long startTime;
        int x,y;
        int predecessor;
        boolean foundSlot;
        for (Integer t:waitingTasks){
            duration=tasks[t][0];
            nRes=(int)tasks[t][1];
            startTime=0;
            for (x=2+2*nRes;x<tasks[t].length;x++){
                predecessor=(int)tasks[t][x];
                startTime=Math.max(scheduledTasks.get(predecessor)+tasks[predecessor][0],startTime);
            }
            foundSlot=false;
            while (!foundSlot){
                foundSlot=true;
                for (x=0;x<nRes;x++){
                    for (y=0;y<duration;y+=gcd){
                        if (resourceusage[(int)tasks[t][2+2*x]][(int)((startTime+y)/gcd)]>resources[(int)tasks[t][2+2*x]]-(int)tasks[t][2+2*x+1]){
                            foundSlot=false;
                            break;
                        }
                    }
                    if (!foundSlot)
                        break;
                }
                if (!foundSlot) {
                    startTime += gcd;
                    if (startTime>maxTime)
                        System.err.println("ERROR startTime>maxTime");
                }
            }
            scheduledTasks.put(t,startTime);
            for (x=0;x<nRes;x++){
                for (y=0;y<duration;y+=gcd){
                    resourceusage[(int)tasks[t][2+2*x]][(int)((startTime+y)/gcd)]+=(int)tasks[t][2+2*x+1];
                }
            }
            maxTime=Math.max(startTime+duration,maxTime);
        }


        long[] res=new long[tasks.length];
        for (Integer t:scheduledTasks.keySet()){
            res[t]=scheduledTasks.get(t);
        }
        return new ReducedSimulationResults(res,maxTime);
    }





    private static boolean checkDependencies(long[] task, List<Integer> waitingTasks){
        int nRes=(int)task[1];
        for (int x=2+2*nRes;x<task.length;x++){
            if (!waitingTasks.contains((int)task[x]))
                return false;
        }
        return true;
    }



    public static void main(String[] args) throws Exception{

        long start=System.currentTimeMillis();

        ReducedScenario scen=new ReducedScenario(ExampleGenerator.generateRandomScenario(5,3,3,3,3));
        ReducedSimulationResults res3=simulate(scen,new LinkedList<int[]>());
        System.out.println(res3.getDuration());

        for (int x=0;x<scen.getTasks().length;x++){
            for (int y=0;y<scen.getTasks()[x].length;y++){
                System.out.print(scen.getTasks()[x][y]+"\t");
            }
            System.out.println(res3.getStarttimes()[x]+"\t");
        }
        //System.out.println("\n\n\nExecution took "+(System.currentTimeMillis()-start)+"ms\n");


        int count=1000;
        int runsPerN=500;
        int step=1;

        double[] nt = new double[count/step];
        for (int n = 1; n < count; n+=step) {
            System.out.println(n);

            long ts = 0;
            for (int x = 0; x < runsPerN; x++) {
                ReducedScenario scenario = new ReducedScenario(ExampleGenerator.generateRandomScenario(n, 10, 10, 10, 10));
                long st = System.currentTimeMillis();
                simulate(scenario, new LinkedList<int[]>());
                ts += System.currentTimeMillis() - st;
            }
            nt[n/step] = ts / (double)runsPerN;
        }

        System.out.print("data=[");
        for (int x = 0; x < nt.length; x++) {
            System.out.print(nt[x] + ",");
        }
        System.out.println("];");

        //System.out.println(res3.getFeasibleSwaps().size());
        //System.out.println("\n\nFeasible Swaps:\n\n");
        //for (int[] swap:res3.getFeasibleSwaps()) {
        //System.out.println(swap[0] + " <> " + swap[1]);
        //}

        //System.exit(0);*/
/*

        int mode=5;




        if (mode==0) {  //Load File
            String file="/home/homer/Documents/TUM/FAUST/MySim3/random-20.xml";

            ReducedScenario scenario = new ReducedScenario(XMLParser.parseXML(file));

            ReducedSimulationResults res = simulate(scenario, new LinkedList<int[]>());

            System.out.println("\n\nRes:\n\n");
            for (int x = 0; x < res.getStarttimes().length; x++) {
                System.out.println(x + " " + res.getStarttimes()[x]);
            }
        }
        else if (mode==1) { //Check runtime complexity

            int count=1000;
            int runsPerN=20;
            int step=1;

            double[] nt = new double[count/step];
            for (int n = 1; n < count; n+=step) {
                System.out.println(n);

                long ts = 0;
                for (int x = 0; x < runsPerN; x++) {
                    ReducedScenario scenario = new ReducedScenario(ExampleGenerator.generateRandomScenario(n, 10, 10, 10, 10));
                    long st = System.currentTimeMillis();
                    simulate(scenario, new LinkedList<int[]>());
                    ts += System.currentTimeMillis() - st;
                }
                nt[n/step] = ts / (double)runsPerN;
            }

            System.out.print("data=[");
            for (int x = 0; x < nt.length; x++) {
                System.out.print(nt[x] + ",");
            }
            System.out.println("];");
        }


        else if (mode==2) { //Check runtime complexity with swap collection

            int count=1000;
            int runsPerN=20;
            int step=1;

            double[] nt = new double[count/step];
            for (int n = 1; n < count; n+=step) {
                System.out.println(n);

                long ts = 0;
                for (int x = 0; x < runsPerN; x++) {
                    ReducedScenario scenario = new ReducedScenario(ExampleGenerator.generateRandomScenario(n, 10, 10, 10, 10));
                    long st = System.currentTimeMillis();
                    simulateAndCollectSwaps(scenario, new LinkedList<int[]>());
                    ts += System.currentTimeMillis() - st;
                }
                nt[n/step] = ts / (double)runsPerN;
            }

            System.out.print("data=[");
            for (int x = 0; x < nt.length; x++) {
                System.out.print(nt[x] + ",");
            }
            System.out.println("];");
        }

        if (mode==3) {  //Run Example


            ReducedScenario scenario = new ReducedScenario(ExampleGenerator.generate());

            LinkedList<int[]> swaps=new LinkedList<int[]>();
            swaps.add(new int[]{8,12});
            swaps.add(new int[]{0,-1});
            swaps.add(new int[]{0,-1});
            swaps.add(new int[]{0,-2});

            ReducedSimulationResults res = simulate(scenario, swaps);

            System.out.println("\n\nRes:\n\n");
            for (int x = 0; x < res.getStarttimes().length; x++) {
                System.out.println(x + " " + res.getStarttimes()[x]);
            }

            ReducedSimulationResults res2 = simulateAndCollectSwaps(scenario, swaps);

            System.out.println("\n\nRes:\n\n");
            for (int x = 0; x < res2.getStarttimes().length; x++) {
                System.out.println(x + " " + res2.getStarttimes()[x]);
            }

            System.out.println("\n\nFeasible Swaps:\n\n");
            for (int[] swap:res2.getFeasibleSwaps()) {
                System.out.println(swap[0] + " <> " + swap[1]);
            }

        }

        if (mode==4) {  //Run Random Scenario

            int n=1000;

            ReducedScenario scenario = new ReducedScenario(ExampleGenerator.generateRandomScenario(n, 10, 10, 10, 10));

            ReducedSimulationResults res = simulate(scenario, new LinkedList<int[]>());

            System.out.println("\n\nRes:\n\n");
            for (int x = 0; x < res.getStarttimes().length; x++) {
                System.out.println(x + " " + res.getStarttimes()[x]);
            }
        }
        if (mode==5){
            ReducedScenario scenario = new ReducedScenario(ExampleGenerator.generateRandomScenario(30, 10, 10, 10, 10));
            for (int x=0;x<1000;x++){


                ReducedSimulationResults res = simulate(scenario, new LinkedList<int[]>(),2);
                System.out.print(res.getDuration()+", ");
            }

        }

        if (mode==6){

        }

*/


    }
}
