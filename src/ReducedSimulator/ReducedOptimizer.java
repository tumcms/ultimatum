/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package ReducedSimulator;

import Common.Pair;
import Scenario.*;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.util.*;

public class ReducedOptimizer {



    public static ReducedOptimizationResults greedySearch(ReducedScenario in){

        ReducedSimulationResults res= ReducedSimulator.simulateAndCollectSwaps(in,new LinkedList<int[]>());

        long minDuration=res.getDuration();

        List<int[]> appliedSwaps=new LinkedList<int[]>();

        int[] minSwap;

        do {
            minSwap=null;
            for (int[] swap : res.getFeasibleSwaps()) {
                appliedSwaps.add(swap);

                ReducedSimulationResults res2 = ReducedSimulator.simulateAndCollectSwaps(in, appliedSwaps);

                if (res2.getDuration() < minDuration) {
                    minDuration=res2.getDuration();
                    minSwap=swap;
                }

                appliedSwaps.remove(appliedSwaps.size() - 1);
            }
            if (minSwap!=null) {
                appliedSwaps.add(minSwap);
                res = ReducedSimulator.simulateAndCollectSwaps(in, appliedSwaps);
            }
        }
        while(minSwap!=null);

        res = ReducedSimulator.simulateAndCollectSwaps(in, appliedSwaps);

        return new ReducedOptimizationResults(res.getStarttimes(),appliedSwaps,res.getDuration());
    }


    public static ReducedOptimizationResults toleranceSearch(ReducedScenario in, int maxdepth, double tolerance){
        return toleranceSearch(in,new LinkedList<int[]>(),Long.MAX_VALUE,0,maxdepth,tolerance);
    }

    public static ReducedOptimizationResults toleranceSearch(ReducedScenario in, List<int[]> appliedSwaps, long minDuration, int depth, int maxdepth, double tolerance){

        ReducedSimulationResults res= ReducedSimulator.simulateAndCollectSwaps(in,appliedSwaps);

        List<int[]> minSwaps;


        //if (depth==0)
        //    System.out.print("Swaps " + res.getFeasibleSwaps().size());

        minSwaps=null;
        for (int[] swap : res.getFeasibleSwaps()) {
            //if (depth==0)
            //    System.out.print(".");
            appliedSwaps.add(swap);

            ReducedSimulationResults res2 = ReducedSimulator.simulate(in, appliedSwaps);

            if (depth<maxdepth && res2.getDuration() < minDuration*tolerance) {
                LinkedList<int[]> newSwaps=new LinkedList<int[]>();
                for (int[] nswap: appliedSwaps)
                    newSwaps.add(nswap);

                ReducedOptimizationResults res3=toleranceSearch(in, newSwaps, minDuration, depth + 1, maxdepth, tolerance);
                if (res3.getDuration()<minDuration){
                    minDuration=res3.getDuration();
                    minSwaps=res3.getAppliedSwaps();
                }
            }
            else if (res2.getDuration()<minDuration){
                minDuration=res2.getDuration();
                minSwaps=new LinkedList<int[]>();
                for (int[] nswap: appliedSwaps)
                    minSwaps.add(nswap);
            }

            appliedSwaps.remove(appliedSwaps.size() - 1);
        }
        if (minSwaps!=null) {
            appliedSwaps=minSwaps;
//            res = ReducedSimulator.simulateAndCollectSwaps(in, appliedSwaps);
        }




        res = ReducedSimulator.simulate(in, appliedSwaps);

        /*if (depth==0) {
            System.out.println();

            for (int[] swap : appliedSwaps) {
                System.out.println("Swap " + swap[0] + " <> " + swap[1]);
            }
            for (long l:res.getStarttimes()){
                System.out.println("Start " + l);
            }
            System.out.println("Duration "+res.getDuration());
        }*/

        return new ReducedOptimizationResults(res.getStarttimes(),appliedSwaps,res.getDuration());
    }


    public static ReducedOptimizationResults limitedDepthSearch(ReducedScenario in,int maxdepth){
        List<int[]> swaps=new LinkedList<int[]>();
        ReducedSimulationResults res= ReducedSimulator.simulate(in,swaps);
        long minDuration=res.getDuration();
        boolean improvement=true;
        int run=0;
        while (improvement){
            //System.err.println("Run "+run++);
            ReducedOptimizationResults res2=limitedDepthSearch(in,swaps,minDuration,0,maxdepth);
            if (res2.getDuration()<minDuration){
                swaps=res2.getAppliedSwaps();
                minDuration=res2.getDuration();
                improvement=true;
            }
            else{
                improvement=false;
            }
        }
        res = ReducedSimulator.simulate(in, swaps);
        return new ReducedOptimizationResults(res.getStarttimes(),swaps,res.getDuration());

    }

    public static ReducedOptimizationResults limitedDepthSearch(ReducedScenario in, List<int[]> appliedSwaps, long minDuration, int depth, int maxdepth){

        ReducedSimulationResults res= ReducedSimulator.simulateAndCollectSwaps(in,appliedSwaps);

        List<int[]> minSwaps;


        //if (depth==0)
        //    System.out.print("Swaps " + res.getFeasibleSwaps().size());

        minSwaps=null;
        for (int[] swap : res.getFeasibleSwaps()) {
            //if (depth==0)
            //    System.out.print(".");
            appliedSwaps.add(swap);

            ReducedSimulationResults res2 = ReducedSimulator.simulate(in, appliedSwaps);
            if (res2.getDuration()<minDuration){
                minDuration=res2.getDuration();
                minSwaps=new LinkedList<int[]>();
                for (int[] nswap: appliedSwaps)
                    minSwaps.add(nswap);
            }

            if (depth<maxdepth) {
                LinkedList<int[]> newSwaps=new LinkedList<int[]>();
                for (int[] nswap: appliedSwaps)
                    newSwaps.add(nswap);

                ReducedOptimizationResults res3=limitedDepthSearch(in, newSwaps, minDuration, depth + 1, maxdepth);
                if (res3.getDuration()<minDuration){
                    minDuration=res3.getDuration();
                    minSwaps=res3.getAppliedSwaps();
                }
            }

            appliedSwaps.remove(appliedSwaps.size() - 1);
        }
        if (minSwaps!=null) {
            appliedSwaps=minSwaps;
//            res = ReducedSimulator.simulateAndCollectSwaps(in, appliedSwaps);
        }




        res = ReducedSimulator.simulate(in, appliedSwaps);

        /*if (depth==0) {
            System.out.println();

            for (int[] swap : appliedSwaps) {
                System.out.println("Swap " + swap[0] + " <> " + swap[1]);
            }
            for (long l:res.getStarttimes()){
                System.out.println("Start " + l);
            }
            System.out.println("Duration "+res.getDuration());
        }*/

        return new ReducedOptimizationResults(res.getStarttimes(),appliedSwaps,res.getDuration());
    }


    public static ReducedOptimizationResults geneticAlgorithm(ReducedScenario in, int population, int iterations, double mutationp, int maxswaps){
        List<int[]>[] appliedSwaps=new List[population];
        for (int x=0;x<population;x++)
            appliedSwaps[x]=new LinkedList<int[]>();

        ReducedSimulationResults[] res=new ReducedSimulationResults[population];

        boolean[] mating=new boolean[population];
        long[] score=new long[population];
        long[] cumulativescore=new long[population];

        Random rnd=new Random();

        ReducedOptimizationResults best=null;

        for (int i=0;i<iterations;i++){
            //System.out.print(i);
            for (int p=0;p<population;p++) {
                if (appliedSwaps[p].size()>maxswaps)
                    appliedSwaps[p].clear();


                res[p] = ReducedSimulator.simulateAndCollectSwaps(in, appliedSwaps[p]);
                score[p]=res[p].getDuration();

                if (best==null || res[p].getDuration()<best.getDuration()){
                    best=new ReducedOptimizationResults(res[p].getStarttimes(),appliedSwaps[p],res[p].getDuration());
                }

                if ((appliedSwaps[p].isEmpty() || rnd.nextDouble()<=mutationp ) && !res[p].getFeasibleSwaps().isEmpty()) {
                    appliedSwaps[p].add(res[p].getRandomSwap(rnd));
                    mating[p]=false;
                }
                else{
                    mating[p]=true;
                }

            }
            //System.out.print(".");
            long fullscore=0;
            for (int p2=0;p2<population;p2++) {
                fullscore+=score[p2];
                if (p2==0)
                    cumulativescore[p2]=score[p2];
                else
                    cumulativescore[p2]=cumulativescore[p2-1]+score[p2];
            }
            //System.out.print(".");
            for (int p=0;p<population;p++) {
                if (mating[p]){
                    long sample=rnd.nextLong() % fullscore;
                    for (int p2=0;p2<population;p2++) {
                        if (p2!=p) {
                            if (sample < cumulativescore[p2]) {
                                for (int s = 0; s < appliedSwaps[p2].size() - (mating[p2] ? 1 : 0); s++) {
                                    appliedSwaps[p].add(appliedSwaps[p2].get(s));
                                }
                                break;
                            }
                        }
                    }
                }
            }
            //System.out.print(".");
        }
        return best;
    }


    public static ReducedOptimizationResults simulatedAnnealingSearch(ReducedScenario in,long maxsteps, double mintemp, double tempreduction){
        List<int[]> swaps=new LinkedList<int[]>();
        List<int[]> bestswaps=new LinkedList<int[]>();
        ReducedSimulationResults res= ReducedSimulator.simulateAndCollectSwaps(in, swaps);
        Random rnd=new Random();
        long minDuration=res.getDuration();
        long curDuration=minDuration;
        double temp=100;
        double p;
        long steps=0;
        int[] swap;
        while (temp>mintemp&&steps<maxsteps){
            steps++;
            //System.out.println(swaps.size()+" "+res.getFeasibleSwaps().size());
            res.getFeasibleSwaps().addAll(swaps);
            swap=res.getFeasibleSwaps().get(rnd.nextInt(res.getFeasibleSwaps().size()));

            swaps.add(swap);
            res=ReducedSimulator.simulateAndCollectSwaps(in, swaps);

            if (res.getDuration()<minDuration){
                //System.out.println("NEWOPT "+res2.getDuration());
                minDuration=res.getDuration();
                curDuration=res.getDuration();
                bestswaps.clear();
                for (int[] s:swaps)
                    bestswaps.add(s);
            }
            else if (res.getDuration()<curDuration){
                //System.out.println("NEWCOPT "+res2.getDuration());
                curDuration=res.getDuration();
            }
            else{
                p=Math.exp(-(res.getDuration()-curDuration)/temp);
                //System.out.print("P( " + res2.getDuration() + " , " + curDuration + " , " + temp + " ) = " + p);
                if (rnd.nextDouble()>=p){
                    swaps.remove(swaps.size() - 1);
                //    System.out.println(" R");
                }
                //else{
                //    System.out.println(" A");
                //}
            }

            temp*=tempreduction;
            steps++;
            //System.out.println("TEMP "+temp+" STEP "+steps);
        }
        res = ReducedSimulator.simulate(in, bestswaps);
        return new ReducedOptimizationResults(res.getStarttimes(),bestswaps,res.getDuration());

    }




    public static void main(String[] args) throws Exception{


        int mode=4;

        args=new String[]{"0","120","4","1000","100","0.3","50"};

        if (args.length>0){
            mode=Integer.parseInt(args[0]);
        }

        mode=4;

        if (mode==0) {
            int set = 120;
            if (args.length>1){
                set=Integer.parseInt(args[1]);
            }
            String inputpath="C:\\Users\\ga75xux\\Desktop\\MySim3\\psplib-data\\";
            inputpath="/home/homer/Documents/TUM/FAUST/MySim3/psplib-data/";
            File f = new File(inputpath + set + File.separator);
            File[] files = f.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".sm");
                }
            });
            List<File> sorted = new LinkedList<File>();
            for (File file : files) {
                if (!file.isDirectory())
                    sorted.add(file);
            }
            Collections.sort(sorted, new Comparator<File>() {
                @Override
                public int compare(File o1, File o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });

            for (int x=0;x<args.length;x++){
                System.out.print(args[x]+" ");
            }
            System.out.println();

            int algo=3;
            if (args.length>2){
                algo=Integer.parseInt(args[2]);
            }



            String filename="psp"+set;
            switch(algo){
                case 0:
                    filename+="greedy";
                    break;
                case 1:
                    filename+="tol";
                    break;
                case 2:
                    filename+="limdep";
                    break;
                case 3:
                    filename+="simann";
                    break;
                case 4:
                    filename+="ga";
                    break;

            }
            filename+=args[3];

            String path="C:\\Users\\ga75xux\\Desktop\\";
            path="/tmp/";

            FileWriter writer=new FileWriter(new File(path+filename+".txt"));
            System.out.println(path+filename+".txt");

            for (File file : sorted) {

                int parameter = Integer.parseInt(file.getName().substring(1 + (set + "").length(), 3 + (set + "").length()));
                int instance = Integer.parseInt(file.getName().substring(4 + (set + "").length(), 6 + (set + "").length()));
                System.out.print(parameter + ", " + instance + ", ");
                writer.write(parameter + ", " + instance + ", ");
                ReducedScenario scen=new ReducedScenario(SMParser.parseSM(file.getPath()));

                String s;
                int depth;
                switch(algo){
                    case 0:
                        s=greedySearch(scen).getDuration()+" ;...";
                        System.out.println(s);
                        writer.write(s+"\n");
                        writer.flush();
                        break;
                    case 1:
                        depth=2;
                        if (args.length>3) depth=Integer.parseInt(args[3]);
                        double tolerance=1.5;
                        if (args.length>4) tolerance=Double.parseDouble(args[4]);
                        s=toleranceSearch(scen, depth, tolerance).getDuration() + " ;...";
                        System.out.println(s);
                        writer.write(s+"\n");
                        writer.flush();
                        break;
                    case 2:
                        depth=2;
                        if (args.length>3) depth=Integer.parseInt(args[3]);
                        s=limitedDepthSearch(scen, depth).getDuration() + " ;...";
                        System.out.println(s);
                        writer.write(s+"\n");
                        writer.flush();
                        break;
                    case 3:
                        long d=Long.MAX_VALUE;
                        double tr=0.99;
                        if (args.length>3) tr=Double.parseDouble(args[3]);
                        int count=10;
                        if (args.length>4) count=Integer.parseInt(args[4]);
                        for (int x=0;x<count;x++){
                            d=Math.min(simulatedAnnealingSearch(scen, Long.MAX_VALUE, 0.1, tr).getDuration(),d);
                        }
                        s=d+ " ;...";
                        System.out.println(s);
                        writer.write(s+"\n");
                        writer.flush();

                        break;

                    case 4:
                        int population=100;
                        int iterations=1000;
                        double mutationp=0.5;
                        int maxswaps=20;
                        if (args.length>3) population=Integer.parseInt(args[3]);
                        if (args.length>4) iterations=Integer.parseInt(args[4]);
                        if (args.length>5) mutationp=Double.parseDouble(args[5]);
                        if (args.length>6) maxswaps=Integer.parseInt(args[6]);
                        s=geneticAlgorithm(scen, population, iterations, mutationp, maxswaps).getDuration() + " ;...";
                        System.out.println(s);
                        writer.write(s+"\n");
                        writer.flush();
                        break;
                }
            }
            writer.close();
        }
        else if (mode==1){
            System.out.println("SIMCOUNT LIMDEP2");
            for (int x=1;x<100;x++){
                ReducedSimulator.simcounter=0;
                for (int y=1;y<10;y++){
                    limitedDepthSearch(new ReducedScenario(ExampleGenerator.generateRandomScenario(x, 3, 5, 3, 3)), 2);
                }
                System.out.print(x+", "+ReducedSimulator.simcounter/10.0+"; ");
            }
        }
        else if (mode==2) {
            File f = new File("/home/homer/Documents/TUM/FAUST/MySim3/psplib-data/patterson.sm/");
            File[] files = f.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".rcp");
                }
            });
            List<File> sorted = new LinkedList<File>();
            for (File file : files) {
                if (!file.isDirectory())
                    sorted.add(file);
            }
            Collections.sort(sorted, new Comparator<File>() {
                @Override
                public int compare(File o1, File o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });
            for (int x=0;x<args.length;x++){
                System.out.print(args[x]+" ");
            }
            System.out.println();

            int algo=4;
            if (args.length>2){
                algo=Integer.parseInt(args[2]);
            }

            FileWriter writer=new FileWriter(new File("/home/homer/Documents/TUM/FAUST/MySim3/psplib-data/patterson.random"));

            for (File file : sorted) {

                int instance = Integer.parseInt(file.getName().substring(3, file.getName().indexOf('.')));
                System.out.print(instance + ", ");
                writer.write(instance + ", ");
                ReducedScenario scen=new ReducedScenario(RCPParser.parseRCP(file.getPath()));
                int depth;
                long d;
                switch(algo){
                    case 0:
                        System.out.println(greedySearch(scen).getDuration()+" ;...");
                        break;
                    case 1:
                        depth=2;
                        if (args.length>3) depth=Integer.parseInt(args[3]);
                        double tolerance=1.5;
                        if (args.length>4) tolerance=Double.parseDouble(args[4]);
                        System.out.println(toleranceSearch(scen, depth, tolerance).getDuration() + " ;...");
                        break;
                    case 2:
                        depth=2;
                        if (args.length>3) depth=Integer.parseInt(args[3]);
                        d=Long.MAX_VALUE;
                        for (int x=0;x<50;x++){
                            d=Math.min(d,limitedDepthSearch(scen, depth).getDuration());
                            scen=new ReducedScenario(RCPParser.parseRCP(file.getPath()),true);
                        }
                        System.out.println(d + " ;...");
                        break;
                    case 3:
                        d=Long.MAX_VALUE;
                        double tr=0.95;
                        if (args.length>3) tr=Double.parseDouble(args[3]);
                        int count=10;
                        if (args.length>4) count=Integer.parseInt(args[4]);
                        for (int x=0;x<count;x++){
                            d=Math.min(simulatedAnnealingSearch(scen, Long.MAX_VALUE, 0.1, tr).getDuration(),d);
                        }
                        System.out.println(d+ " ;...");
                        break;

                    case 4:

                        ReducedSimulationResults min=null;
                        for (int x=0;x<500000;x++){
                            if (x%10000==0)
                                System.err.print(".");
                            scen=new ReducedScenario(RCPParser.parseRCP(file.getPath()),true);
                            //System.err.print(";");

                            ReducedSimulationResults res=ReducedSimulator.simulate(scen, new LinkedList<int[]>());
                            if (min==null || res.getDuration()<min.getDuration()) {
                                min = res;
                                min.saveToFile(new File(file.getPath()+".rnd.optres"),scen);
                                System.err.print(min.getDuration()+";");
                            }

                        }

                        System.out.println(min.getDuration()+ " ;...");
                        writer.write(min.getDuration() + " ;...\n");
                        writer.flush();
                }
            }
            writer.flush();
            writer.close();
        }

        else if (mode==3) {
	    int[][] selected=new int[][]{
                {9, 1},
                {9, 3},
                {9, 5},
                {9, 6},
                {9, 7},
                {9, 8},
                {9, 9},
                {9, 10},
                {13, 1},
                {13, 2},
                {13, 3},
                {13, 4},
                {13, 5},
                {13, 6},
                {13, 7},
                {13, 8},
                {13, 9},
                {13, 10},
                {25, 2},
                {25, 4},
                {25, 5},
                {25, 6},
                {25, 7},
                {25, 8},
                {25, 10},
                {29, 1},
                {29, 2},
                {29, 3},
                {29, 4},
                {29, 5},
                {29, 6},
                {29, 7},
                {29, 8},
                {29, 9},
                {29, 10},
                {30, 2},
                {41, 3},
                {41, 5},
                {41, 10},
                {45, 1},
                {45, 2},
                {45, 3},
                {45, 4},
                {45, 5},
                {45, 6},
                {45, 7},
                {45, 8},
                {45, 9},
                {45, 10}};
            int set = 60;
            File f = new File("/home/homer/Documents/TUM/FAUST/MySim3/psplib-data/" + set + "/");
            File[] files = f.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".sm");
                }
            });
            List<File> sorted = new LinkedList<File>();
            for (File file : files) {
                if (!file.isDirectory())
                    sorted.add(file);
            }
            Collections.sort(sorted, new Comparator<File>() {
                @Override
                public int compare(File o1, File o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });
            System.out.println("LIMDEP3-SELECTED-60");
            for (File file : sorted) {

                int parameter = Integer.parseInt(file.getName().substring(1 + (set + "").length(), 3 + (set + "").length()));
                int instance = Integer.parseInt(file.getName().substring(4 + (set + "").length(), 6 + (set + "").length()));
                boolean found=false;
                for (int s=0;s<selected.length;s++){
                    if (selected[s][0]==parameter && selected[s][1]==instance){
                        found=true;
                        break;
                    }
                }
                if (found){
                    System.out.print(parameter + ", " + instance + ", ");
                    //ReducedScenario scen=new ReducedScenario(SMParser.parseSM(file.getPath()));
                    //System.out.println(greedySearch(scen).getDuration()+" ;...");
                    //System.out.println(toleranceSearch(scen, 2, 1.5).getDuration() + " ;...");
                    //System.out.println(limitedDepthSearch(scen, 5).getDuration() + " ;...");

                    ReducedOptimizationResults min=null;
                    for (int x=0;x<50;x++){
                        System.err.print(".");
                        ReducedScenario scen=new ReducedScenario(SMParser.parseSM(file.getPath()),true);
                        System.err.print(";");

                        ReducedOptimizationResults res=limitedDepthSearch(scen,3);//limitedDepthSearch(scen, 2);
                        if (min==null || res.getDuration()<min.getDuration()) {
                            min = res;
                            min.saveToFile(new File(file.getPath()+".ld3.optres"),scen);
                            System.err.print(min.getDuration());
                        }

                    }

                    System.out.println(min.getDuration()+ " ;...");

                }
            }
        }
        else if (mode==4) {
            int[][] selected=new int[][]{
                    {9, 1},
                    {9, 3},
                    {9, 5},
                    {9, 6},
                    {9, 7},
                    {9, 8},
                    {9, 9},
                    {9, 10},
                    {13, 1},
                    {13, 2},
                    {13, 3},
                    {13, 4},
                    {13, 5},
                    {13, 6},
                    {13, 7},
                    {13, 8},
                    {13, 9},
                    {13, 10},
                    {25, 2},
                    {25, 4},
                    {25, 5},
                    {25, 6},
                    {25, 7},
                    {25, 8},
                    {25, 10},
                    {29, 1},
                    {29, 2},
                    {29, 3},
                    {29, 4},
                    {29, 5},
                    {29, 6},
                    {29, 7},
                    {29, 8},
                    {29, 9},
                    {29, 10},
                    {30, 2},
                    {41, 3},
                    {41, 5},
                    {41, 10},
                    {45, 1},
                    {45, 2},
                    {45, 3},
                    {45, 4},
                    {45, 5},
                    {45, 6},
                    {45, 7},
                    {45, 8},
                    {45, 9},
                    {45, 10}};
            int set = 120;
            File f = new File("/home/homer/Documents/TUM/FAUST/MySim3/psplib-data/" + set + "/");
            File[] files = f.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".sm");
                }
            });
            List<File> sorted = new LinkedList<File>();
            for (File file : files) {
                if (!file.isDirectory())
                    sorted.add(file);
            }
            Collections.sort(sorted, new Comparator<File>() {
                @Override
                public int compare(File o1, File o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });
            System.out.println("RANDOM-"+set);
            for (File file : sorted) {

                int parameter = Integer.parseInt(file.getName().substring(1 + (set + "").length(), 3 + (set + "").length()));
                int instance = Integer.parseInt(file.getName().substring(4 + (set + "").length(), 6 + (set + "").length()));
                boolean found=true;     //TODO
                for (int s=0;s<selected.length&&found==false;s++){
                    if (selected[s][0]==parameter && selected[s][1]==instance){
                        found=true;
                        break;
                    }
                }
                if (found){
                    System.out.print(parameter + ", " + instance + ", ");
                    //ReducedScenario scen=new ReducedScenario(SMParser.parseSM(file.getPath()));
                    //System.out.println(greedySearch(scen).getDuration()+" ;...");
                    //System.out.println(toleranceSearch(scen, 2, 1.5).getDuration() + " ;...");
                    //System.out.println(limitedDepthSearch(scen, 5).getDuration() + " ;...");

                    ReducedSimulationResults min=null;
                    long start=System.currentTimeMillis();
                    for (int x=0;x<500000;x++){
                        if (x%10000==0)
                            System.err.print(".");
                        ReducedScenario scen=new ReducedScenario(SMParser.parseSM(file.getPath()),true);
                        //System.err.print(";");

                        ReducedSimulationResults res=ReducedSimulator.simulate(scen, new LinkedList<int[]>());
                        if (min==null || res.getDuration()<min.getDuration()) {
                            min = res;
                            min.saveToFile(new File(file.getPath()+".rnd.optres"),scen);
                            System.err.print(min.getDuration()+";");
                        }

                    }

                    System.out.println(min.getDuration()+ ", "+(System.currentTimeMillis()-start)+" ;...");

                }
            }
        }


    }
}
