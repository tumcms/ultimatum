/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package ReducedSimulator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.List;

public class ReducedOptimizationResults {
    private long[] starttimes;
    private List<int[]> appliedSwaps;
    private long duration;

    public ReducedOptimizationResults(long[] starttimes, List<int[]> appliedSwaps, long duration) {
        this.starttimes = starttimes;
        this.appliedSwaps = appliedSwaps;
        this.duration = duration;
    }

    public long[] getStarttimes() {
        return starttimes;
    }

    public List<int[]> getAppliedSwaps() {
        return appliedSwaps;
    }

    public long getDuration() {
        return duration;
    }

    public void saveToFile(File f,ReducedScenario scen) throws Exception{
        BufferedWriter writer=new BufferedWriter(new FileWriter(f));
        for (int x=0;x<starttimes.length;x++){
            writer.write(scen.getTaskName(x)+" "+starttimes[x]+"\n");
        }
        writer.write(duration+"\n");
        writer.flush();
        writer.close();

    }
}
