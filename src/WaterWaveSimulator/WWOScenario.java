/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package WaterWaveSimulator;

import Scenario.ExampleGenerator;
import Scenario.Resource;
import Scenario.Scenario;
import Scenario.Task;

import java.util.HashMap;
import java.util.Random;

public class WWOScenario {
    private long[][] tasks; //each Task {duration,no_resources, resource1_id, resource1_count,... ,deps,...}
    private int[] resources;

    private double[] priorities;

    private HashMap<Integer,Resource> resourceMap;
    private HashMap<Integer,Task>  taskMap;


    public WWOScenario(Scenario in){
        this(in,false,1);
    }


    public WWOScenario(Scenario in, boolean randomize){
        this(in,randomize,1);
    }

    public WWOScenario(Scenario in, boolean randomize, double maxpriority){
        resourceMap=new HashMap<Integer, Resource>();
        taskMap=new HashMap<Integer, Task>();

        HashMap<String, Integer> invResourceMap=new HashMap<String, Integer>();
        HashMap<String, Integer> invTaskMap=new HashMap<String, Integer>();

        Resource[] res=in.getResources();
        resources=new int[res.length];

        for (int x=0;x<res.length;x++){
            if (!resourceMap.containsValue(res[x])) {
                resourceMap.put(x, res[x]);
                resources[x]=res[x].getCount();
            }
            else{
                resources[invResourceMap.get(res[x].getType())]+=res[x].getCount();
            }
            invResourceMap.put(res[x].getType(),x);
        }

        Task[] t=in.getTasks();

        Random rnd=new Random();

        tasks=new long[t.length][];
        priorities=new double[t.length];

        double prioCount=0;

        for (int x=0;x<t.length;x++){
            taskMap.put(x,t[x]);
            invTaskMap.put(t[x].getName(),x);
        }
        for (int x=0;x<t.length;x++){
            int gr=0;
            for (int y=0;y<t[x].getResourceCount();y++){
                if (t[x].getResource(y).isGeometric())
                    gr++;
            }
            tasks[x]=new long[2+2*(t[x].getResourceCount()-gr)+t[x].getDependencyCount()];
            tasks[x][0]=t[x].getDuration();
            tasks[x][1]=t[x].getResourceCount()-gr;
            int z=0;
            for (int y=0;y<t[x].getResourceCount();y++){
                if (!t[x].getResource(y).isGeometric()) {
                    //System.out.println(invResourceMap.get(t[x].getResource(y).getType()));
                    tasks[x][2 + 2 * z] = invResourceMap.get(t[x].getResource(y).getType());
                    tasks[x][2 + 2 * z + 1] = t[x].getResource(y).getCount();
                    z++;
                }
            }
            for (int y=0;y<t[x].getDependencyCount();y++){
                tasks[x][2+2*(t[x].getResourceCount()-gr)+y]=invTaskMap.get(t[x].getDependency(y).getTask());
            }
            if (randomize)
                priorities[x]=rnd.nextDouble()*maxpriority;
            else
                priorities[x]=prioCount+=(maxpriority/t.length);
        }

        /*System.out.println("Resources:\n");
        for (int x=0;x<resources.length;x++){
            System.out.println(x+" "+resourceMap.get(x)+":"+ resources[x]);
        }

        System.out.println("\n\nTasks:\n");
        for (int x=0;x<tasks.length;x++){
            System.out.print(x+" "+taskMap.get(x)+": ");
            for (int y=0;y<tasks[x].length;y++){
                System.out.print(tasks[x][y] + ", ");
            }
            System.out.println();
        } */

    }

    public long[][] getTasks(){
        return tasks;
    }

    public int[] getResources(){
        return resources;
    }

    public String getTaskName(int id){
        return taskMap.get(id).getName();
    }

    public String getResourceName(int id) { return resourceMap.get(id).getType(); }

    public Task getTask(int id){
        return taskMap.get(id);
    }

    public Resource getResource(int id) { return resourceMap.get(id); }

    public double[] getPriorities(){
        return priorities;
    }

    public double[] clonePriorities(){
        double[] out=new double[priorities.length];
        for (int x=0;x<out.length;x++)
            out[x]=priorities[x];
        return out;
    }


    public static void main(String[] args){
        new WWOScenario(ExampleGenerator.generate());
    }
}
