/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package WaterWaveSimulator;

public class WWOSimulationResultsFuzzy {
    private WWOSimulationResults[] results;
    private long max,min,sum,sum2, runs;
    private long simCount;

    public WWOSimulationResultsFuzzy(WWOSimulationResults[] results, long max, long min, long sum, long sum2, long runs) {
        this.results = results;
        this.max = max;
        this.min = min;
        this.sum = sum;
        this.sum2 = sum2;
        this.runs = runs;
    }

    public WWOSimulationResults[] getResults() {
        return results;
    }

    public long getMax() {
        return max;
    }

    public long getMin() {
        return min;
    }

    public long getSum() {
        return sum;
    }

    public long getSum2() {
        return sum2;
    }

    public long getRuns() {
        return runs;
    }

    public double getMean(){
        return sum/(double)runs;
    }

    public double getVariance(){
        return sum2/(double)runs-((sum/(double)runs)*(sum/(double)runs));
    }

    public double getStDev(){ return Math.sqrt(getVariance());}

    public long getSimCount() {
        return simCount;
    }

    public void setSimCount(long simCount) {
        this.simCount = simCount;
    }

    public double getMeasure(int x){
        switch (x){
            case 0:
                return getMin();
            case 1:
                return getMean();
            case 2:
                return getMax();
            case 3:
                return getStDev();
            case 4:
                return getMean()+getStDev();
            case 5:
                return getMean()+2*getStDev();
            case 6:
                return getMax()+getStDev();
            case 7:
                return getMax()+2*getStDev();
            default:
                return getMean();
        }

    }

    public boolean isBetterThan(WWOSimulationResultsFuzzy res, int measure){
        if (measure<8)
            return getMeasure(measure)<res.getMeasure(measure);
        switch (measure) {
            case 8:
                return (getMean()<res.getMean() && getVariance()<=res.getVariance()) ||
                        (getMean()<=res.getMean() && getVariance()<res.getVariance());
            case 9:
                return (getMax()<res.getMax() && getVariance()<=res.getVariance()) ||
                        (getMax()<=res.getMax() && getVariance()<res.getVariance());
            default:
                return (getMin()<res.getMin() && getVariance()<=res.getVariance()) ||
                        (getMin()<=res.getMin() && getVariance()<res.getVariance());

        }
    }
}
