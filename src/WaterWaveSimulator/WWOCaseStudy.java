/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package WaterWaveSimulator;

import Scenario.*;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class WWOCaseStudy {

    public static void main(String[] args) throws Exception{


        String inputpath = "/home/homer/Documents/TUM/Projekte/FAUST/ProjektBaustelle Luise-Kisselbackplatz/Mit Resourcen Ohne SS.xml";

        Scenario in = XMLParser.parseXML(inputpath);




        int population = 100;
        int generations = 100;
        int k_max = Math.min(12, in.getTasks().length / 2);
        int h_max = 6;
        double beta = 0.25;
        double alpha = 1.026;

        int maxDelays=0; //ONLY IF NOT SERIAL
        double initialMaxPrio=1; //ONLY IF NOT SERIAL
        boolean expandLd=false; //ONLY IF NOT SERIAL

        int trials = 1;

        boolean serial=true;


        long best = Long.MAX_VALUE;
        long start=System.currentTimeMillis();

        if (serial)
            WWOSimulatorSerialImproved.simcounter=0;
        else
            WWOSimulator.simcounter=0;

        for (int x = 0; x < trials; x++) {
            WWOSimulationResults res;
            if (serial) {
                res = WWOSerial.waterWaveOptimization(in, population, h_max, alpha, beta, k_max, generations);
            }
            else {
                res = WWO.waterWaveOptimization(in, population, h_max, alpha, beta, k_max, generations, maxDelays, initialMaxPrio,expandLd);
            }
            if (res.getDuration() < best) {
                best = res.getDuration();
                res.generateScenario().createXMLFile(new File("/tmp/wwores.xml"),res.getSchedule());
                res.saveToFile(new File("/tmp/wwores.txt"));
            }
        }
        double duration=(System.currentTimeMillis()-start)/(double)trials;
        int simcounter=(serial ? WWOSimulatorSerialImproved.simcounter : WWOSimulator.simcounter)/trials;
        System.out.print(best + ", \t"+duration+", \t"+simcounter+";... \n");

        }
}
