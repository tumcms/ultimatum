/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package WaterWaveSimulator;


import Common.Pair;
import Scenario.*;

import java.io.File;
import java.io.FileWriter;
import java.util.*;

public class WWOSimulator {


    public static int simcounter=0;

    public static WWOSimulationResultsFuzzy simulate(WWOScenarioFuzzy in, int monteCarloCount) {
        WWOSimulationResults[] res=new WWOSimulationResults[monteCarloCount];
        long min=Long.MAX_VALUE;
        long max=-Long.MAX_VALUE;
        long sum=0;
        long sum2=0;
        for (int x=0;x<monteCarloCount;x++){
            res[x]=simulate(in);
            max=Math.max(max,res[x].getDuration());
            min=Math.min(min,res[x].getDuration());
            sum+=res[x].getDuration();
            sum2+=Math.pow(res[x].getDuration(),2);
        }
        return new WWOSimulationResultsFuzzy(res,max,min,sum,sum2,monteCarloCount);
    }


    public static WWOSimulationResults simulate(WWOScenario in) {
        simcounter++;

        List<Integer> waitingTasks = new LinkedList<Integer>();
        List<Pair<Long,Integer>> runningTasks = new LinkedList<Pair<Long, Integer>>();
        List<Pair<Long,Integer>> finishedTasks = new LinkedList<Pair<Long, Integer>>();

        long[][] tasks=in.getTasks();



        boolean[] finished=new boolean[tasks.length];

        //int[] delays=new int[tasks.length];
        /*for (int x=0;x<dels.length;x++){
            delays[dels[x]]++;
        } */

        int[] resources = new int[in.getResources().length];
        for (int x = 0; x < in.getResources().length; x++) {
            resources[x] = in.getResources()[x];
        }

        double[] priorities=in.clonePriorities();


        for (int x = 0; x < tasks.length; x++) {
            int y;
            for (y=0;y<waitingTasks.size();y++) {
                if (priorities[waitingTasks.get(y)]>=priorities[x])
                    break;

            }
            waitingTasks.add(y,x);
            finished[x]=false;
        }


        long time = 0;
        long nextStep=0;
        long temp;

        while (waitingTasks.size() > 0 || runningTasks.size() >0){

            if (!runningTasks.isEmpty()) {
                nextStep = 0;
                for (Pair<Long, Integer> currentTask : runningTasks) {
                    temp = currentTask.getKey() + tasks[currentTask.getValue()][0];
                    if (nextStep == 0 || nextStep > temp)
                        nextStep = temp;
                }
                time = nextStep;
            }

            Pair<Long,Integer> currentTask;
            for (Iterator<Pair<Long,Integer>> i=runningTasks.iterator(); i.hasNext();) {
                currentTask=i.next();
                if (currentTask.getKey()+tasks[currentTask.getValue()][0]==time){
                    freeResources(tasks[currentTask.getValue()],resources);
                    finishedTasks.add(currentTask);
                    finished[currentTask.getValue()]=true;
                    i.remove();
                }
            }


            for (Iterator<Integer> i=waitingTasks.iterator(); i.hasNext();) {
                currentTask=new Pair<Long, Integer>(time,i.next());
                if (checkResources(tasks[currentTask.getValue()],resources)){
                    if (checkDependencies(tasks[currentTask.getValue()],finished)) {
                        if (priorities[currentTask.getValue()]>1 && !runningTasks.isEmpty()){
                            priorities[currentTask.getValue()]-=1;  //TODO MAXIMUM DELAYS PER TASK
                        }
                        else {
                            useResources(tasks[currentTask.getValue()], resources);
                            runningTasks.add(currentTask);
                            i.remove();
                        }
                    }
                }
            }
        }


        long[] res=new long[tasks.length];
        for (Pair<Long,Integer> ft:finishedTasks){
            res[ft.getValue()]=ft.getKey();
        }
        /*try {
            FileWriter writer = new FileWriter("/tmp/datafun.txt", true);
            for (int x = 0; x < tasks.length; x++) {
                writer.write(res[x] + " ");
            }
            for (int x = 0; x < tasks.length; x++) {
                writer.write(tasks[x][0] + " ");
            }
            writer.write(";...\n");
            writer.flush();
            writer.close();
            System.out.print(".");
        }
        catch (Exception ex){ex.printStackTrace();}*/
        return new WWOSimulationResults(in,res,time);
    }







    private static boolean checkDependencies(long[] task, List<Pair<Long,Integer>> finishedTasks){
        int nRes=(int)task[1];
        boolean satisfied;
        for (int x=2+2*nRes;x<task.length;x++){
            satisfied=false;
            for (Pair<Long,Integer> ft:finishedTasks){
                if (ft.getValue()==task[x]) {
                    satisfied = true;
                    break;
                }
            }
            if (!satisfied)
                return false;
        }
        return true;
    }

    private static boolean checkDependencies(long[] task, boolean[] finishedTasks){
        int nRes=(int)task[1];
        for (int x=2+2*nRes;x<task.length;x++){
            if (!finishedTasks[(int)task[x]])
                return false;
        }
        return true;
    }


    private static boolean checkResources(long[] task, int[] resources){
        long nRes=task[1];
        for (int x=0;x<nRes;x++){
            if (resources[(int)task[2+2*x]]<task[2+2*x+1])
                return false;
        }
        return true;
    }

    private static void useResources(long[] task, int[] resources){
        long nRes=task[1];
        for (int x=0;x<nRes;x++){
            resources[(int)task[2+2*x]]-=task[2+2*x+1];
        }
    }

    private static void freeResources(long[] task, int[] resources){
        long nRes=task[1];
        for (int x=0;x<nRes;x++){
            resources[(int)task[2+2*x]]+=task[2+2*x+1];
        }
    }

    public static void main(String[] args) throws Exception{


       /* WWOSimulationResults res3=simulateAndCollectSwaps(new WWOScenario(ExampleGenerator.generateRandomScenario(1,3,3,3,3)),new LinkedList<int[]>());
        System.out.println(res3.getFeasibleSwaps().size());
        System.out.println("\n\nFeasible Swaps:\n\n");
        for (int[] swap:res3.getFeasibleSwaps()) {
            System.out.println(swap[0] + " <> " + swap[1]);
        }

        System.exit(0);*/

        int mode=0;


        long start=System.currentTimeMillis();

        if (mode==0) {  //Load File
            String file="/home/homer/Documents/TUM/Projekte/FAUST/MySim3/psplib-data/30/j3001_01.sm";

            SMParser.parseSM(file).createGraphVizFile(new File("/tmp/j3001_01.gv"));

            //WWOScenarioFuzzy scenario = new WWOScenarioFuzzy(SMParser.parseSM(file), PSPLIBFuzzy.getFuzzyParameters(30,1,1));

            //WWOSimulationResultsFuzzy res = simulate(scenario,100000);


            /*System.out.println("Res:"+res.getDuration());

            double[] priorities=scenario.getPriorities();
            Random rnd=new Random();
            for (int x=0;x<priorities.length;x++){
                priorities[x]=rnd.nextDouble();
            }

            res = simulate(scenario);


            System.out.println("Res:"+res.getDuration());*/


        }
    }
}
