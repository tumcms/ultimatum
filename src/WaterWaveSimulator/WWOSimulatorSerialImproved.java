/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package WaterWaveSimulator;

import ReducedSimulator.ReducedScenario;
import ReducedSimulator.ReducedSimulationResults;
import Scenario.ExampleGenerator;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class WWOSimulatorSerialImproved {



    public static int simcounter=0;


    public static WWOSimulationResultsFuzzy simulateFuzzy(WWOScenarioFuzzy in, int monteCarloCount){
        WWOSimulationResults[] res=new WWOSimulationResults[monteCarloCount];
        long min=Long.MAX_VALUE;
        long max=-Long.MAX_VALUE;
        long sum=0;
        long sum2=0;

        for (int x=0;x<monteCarloCount;x++){
            res[x]=simulate(in);
            max=Math.max(max,res[x].getDuration());
            min=Math.min(min,res[x].getDuration());
            sum+=res[x].getDuration();
            sum2+=Math.pow(res[x].getDuration(),2);
        }
        return new WWOSimulationResultsFuzzy(res,max,min,sum,sum2,monteCarloCount);
    }


    private static long gcd(long a, long b) { return b==0 ? a : gcd(b, a%b); }

    public static WWOSimulationResults simulate(WWOScenario in)  {
        simcounter++;

        List<Integer> waitingTasks = new LinkedList<Integer>();
        HashMap<Integer,Long> scheduledTasks = new HashMap<Integer, Long>();


        long[][] tasks=in.getTasks();


        long gcd=-1;
        long sum=0;

        for (int x = 0; x < tasks.length; x++) {
            sum+=tasks[x][0];
            if (tasks[x][0]>0) {
                if (gcd < 0)
                    gcd = tasks[x][0];
                else {
                    if (tasks[x][0] % gcd != 0) {
                        gcd = gcd(gcd, tasks[x][0]);
                    }
                }
            }
        }

        //System.out.println("GCD: "+gcd+" Slots: "+(int)(sum/gcd));

        int[] resources = new int[in.getResources().length];
        for (int x = 0; x < in.getResources().length; x++) {
            resources[x] = in.getResources()[x];
        }
        int[][] resourceusage = new int[resources.length][(int)(sum/gcd)];

        boolean[] added=new boolean[tasks.length];

        double[] priorities=in.getPriorities();

        boolean done=false;


        while (!done) {
            done=true;

            int mintask=-1;
            double minprio=Double.MAX_VALUE;
            for (int x = 0; x < tasks.length; x++) {
                if (!added[x]) {
                    if (checkDependencies(tasks[x], waitingTasks)) {
                        if (priorities[x]<minprio){
                            minprio=priorities[x];
                            mintask=x;
                        }
                    }
                    else
                        done=false;

                }
            }
            if (mintask>=0) {

                waitingTasks.add(mintask);
                added[mintask] = true;
            }
        }

        long maxTime=0;

        long duration;
        int nRes;
        long startTime;
        int x,y;
        int predecessor;
        boolean foundSlot;

        for (Integer t:waitingTasks){


            duration=tasks[t][0];
            nRes=(int)tasks[t][1];
            startTime=0;
            for (x=2+2*nRes;x<tasks[t].length;x++){
                predecessor=(int)tasks[t][x];
                startTime=Math.max(scheduledTasks.get(predecessor)+tasks[predecessor][0],startTime);
            }

            foundSlot=false;
            while (!foundSlot){
                foundSlot=true;
                for (x=0;x<nRes;x++){
                    for (y=0;y<duration;y+=gcd){
                        if (resourceusage[(int)tasks[t][2+2*x]][(int)((startTime+y)/gcd)]>resources[(int)tasks[t][2+2*x]]-(int)tasks[t][2+2*x+1]){
                            foundSlot=false;
                            break;
                        }
                    }
                    if (!foundSlot)
                        break;
                }
                if (!foundSlot) {
                    long minNextTime=Long.MAX_VALUE;
                    for (int prevTask:scheduledTasks.keySet()){
                        long nextTime=scheduledTasks.get(prevTask)+tasks[prevTask][0];
                        if (nextTime>startTime && nextTime<minNextTime){
                            minNextTime=nextTime;
                        }
                    }
                    /*startTime += gcd;
                    if (startTime>maxTime)
                        System.err.println("ERROR startTime>maxTime");*/
                    if (minNextTime==Long.MAX_VALUE)
                        System.err.println("ERROR startTime=MAX_VALUE");
                    startTime=minNextTime;
                }
            }

            scheduledTasks.put(t,startTime);
            for (x=0;x<nRes;x++){
                for (y=0;y<duration;y+=gcd){
                    resourceusage[(int)tasks[t][2+2*x]][(int)((startTime+y)/gcd)]+=(int)tasks[t][2+2*x+1];
                }
            }
            maxTime=Math.max(startTime+duration,maxTime);
        }


        long[] res=new long[tasks.length];
        for (Integer t:scheduledTasks.keySet()){
            res[t]=scheduledTasks.get(t);
        }
        return new WWOSimulationResults(in,res,maxTime);
    }





    private static boolean checkDependencies(long[] task, List<Integer> waitingTasks){
        int nRes=(int)task[1];
        for (int x=2+2*nRes;x<task.length;x++){
            if (!waitingTasks.contains((int)task[x]))
                return false;
        }
        return true;
    }




}
