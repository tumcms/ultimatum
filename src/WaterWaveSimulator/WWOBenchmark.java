/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package WaterWaveSimulator;

import Scenario.*;

import java.io.*;
import java.util.*;

public class WWOBenchmark {
    public static void main(String[] args)throws Exception {
        for (int set=120;set<=120;set+=30) {
            //int set = 30;

            long[][] bestDurations;
            if (set!=120){
                bestDurations=new long[48][10];
            }
            else{
                bestDurations=new long[60][10];
            }
            BufferedReader reader=new BufferedReader(new FileReader("/home/homer/Documents/TUM/Projekte/FAUST/MySim3/psplib-data/best/"+set+".best"));
            String inputstring=reader.readLine();
            while (inputstring!=null) {
                StringTokenizer tok=new StringTokenizer(inputstring,",",false);
                int parameter=Integer.parseInt(tok.nextToken().trim());
                int instance=Integer.parseInt(tok.nextToken().trim());
                long value=Long.parseLong(tok.nextToken().trim());
                bestDurations[parameter-1][instance-1]=value;
                inputstring = reader.readLine();
            }

            int population = 100;
            int generations = 1000;
            int k_max = Math.min(12, set / 2);
            int h_max = 6;
            double beta = 0.25;
            double alpha = 1.026;

            int maxDelays=2; //ONLY IF NOT SERIAL
            double initialMaxPrio=1.1; //ONLY IF NOT SERIAL
            boolean expandLd=true; //ONLY IF NOT SERIAL

            int trials = 100;

            boolean serial=false;


            String path = "C:\\Users\\ga75xux\\Desktop\\WWOresults\\";
            path = "/home/homer/Documents/TUM/Diss/images/psplib/rawdata/";//home/homer/Documents/TUM/FAUST/MySim3/WWOresults/final/";
            String filename = "OPTQ_PSP" + set + "_WWO_";
            if (serial)
                filename+="SERIAL_";
            filename+=trials + "_" + population + "_" + generations + "_" + k_max + "_" + h_max + "_" + alpha + "_" + beta +".txt";

            System.out.println("writing to " + path + filename);

            if (args.length > 1) {
                set = Integer.parseInt(args[1]);
            }
            String inputpath = "C:\\Users\\ga75xux\\Desktop\\MySim3\\psplib-data\\";
            inputpath = "/home/homer/Documents/TUM/Projekte/FAUST/MySim3/psplib-data/";
            File f = new File(inputpath + set + File.separator);
            File[] files = f.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".sm");
                }
            });
            List<File> sorted = new LinkedList<File>();
            for (File file : files) {
                if (!file.isDirectory())
                    sorted.add(file);
            }
            Collections.sort(sorted, new Comparator<File>() {
                @Override
                public int compare(File o1, File o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });

            FileWriter writer = new FileWriter(new File(path + filename));

            for (File file : sorted) {

                int parameter = Integer.parseInt(file.getName().substring(1 + (set + "").length(), 3 + (set + "").length()));
                int instance = Integer.parseInt(file.getName().substring(4 + (set + "").length(), 6 + (set + "").length()));
                System.out.print(parameter + ", " + instance + ", ");
                writer.write(parameter + ", " + instance + ", ");
                Scenario in = SMParser.parseSM(file.getPath());
                long best = Long.MAX_VALUE;
                long start=System.currentTimeMillis();
                if (serial)
                    WWOSimulatorSerialImproved.simcounter=0;
                else
                    WWOSimulator.simcounter=0;
                int actualTrials=trials;
                for (int x = 0; x < trials; x++) {
                    long res;
                    if (serial) {
                        res = WWOSerial.waterWaveOptimization(in, population, h_max, alpha, beta, k_max, generations).getDuration();
                    }
                    else {
                        res = WWO.waterWaveOptimization(in, population, h_max, alpha, beta, k_max, generations, maxDelays, initialMaxPrio,expandLd,bestDurations[parameter-1][instance-1]).getDuration();
                    }
                    if (res < best)
                        best = res;
                    if (best <= bestDurations[parameter-1][instance-1]){
                        actualTrials=x+1;
                        break;
                    }
                }
                double duration=(System.currentTimeMillis()-start)/(double)actualTrials;
                int simcounter=(serial ? WWOSimulatorSerialImproved.simcounter : WWOSimulator.simcounter)/actualTrials;
                System.out.print(best + ", \t"+duration+", \t"+simcounter+";... \n");
                writer.write(best + ", "+duration+", "+simcounter+";... \n");
                writer.flush();

            }
            writer.close();
        }
    }
}
