/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package WaterWaveSimulator;

import Scenario.*;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class WWOBenchmarkFuzzy {
    public static void main(String[] args)throws Exception {


        //int set=30;
        for (int set=120;set<=120;set+=30)
        for (int measure=1;measure<=7;measure++){
            //int set = 30;
            int monteCarloRuns=10;
            //int measure=0;
            int population = 10;
            int generations = 1000;
            int k_max = Math.min(12, set / 2);
            int h_max = 6;
            double beta = 0.25;
            double alpha = 1.0026;

            int maxDelays=2; //ONLY IF NOT SERIAL
            double initialMaxPrio=1; //ONLY IF NOT SERIAL
            boolean expandLd=true; //ONLY IF NOT SERIAL

            int trials = 1;

            boolean serial=true;


            String path = "C:\\Users\\ga75xux\\Desktop\\WWOresults\\";
            path = "/home/homer/Documents/TUM/Diss/images/fpsplib/rawdata/FUZZ/NFC_";//home/homer/Documents/TUM/FAUST/MySim3/WWOresults/final/";
            String filename = "FUZZY_PSP" + set + "_WWO_MEASURE"+measure+"_";
            if (serial)
                filename+="SERIAL_";
            filename+=trials + "_" + population + "_" + generations + "_" + k_max + "_" + h_max + "_" + alpha + "_" + beta +"_"+maxDelays+"_"+initialMaxPrio+".txt";

            System.out.println("writing to " + path + filename);

            if (args.length > 1) {
                set = Integer.parseInt(args[1]);
            }
            String inputpath = "C:\\Users\\ga75xux\\Desktop\\MySim3\\psplib-data\\";
            inputpath = "/home/homer/Documents/TUM/Projekte/FAUST/MySim3/psplib-data/";
            File f = new File(inputpath + set + File.separator);
            File[] files = f.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".sm");
                }
            });
            List<File> sorted = new LinkedList<File>();
            for (File file : files) {
                if (!file.isDirectory())
                    sorted.add(file);
            }
            Collections.sort(sorted, new Comparator<File>() {
                @Override
                public int compare(File o1, File o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });

            FileWriter writer = new FileWriter(new File(path + filename));

            for (File file : sorted) {

                int parameter = Integer.parseInt(file.getName().substring(1 + (set + "").length(), 3 + (set + "").length()));
                int instance = Integer.parseInt(file.getName().substring(4 + (set + "").length(), 6 + (set + "").length()));
                System.out.print(parameter + ", " + instance + ", ");
                writer.write(parameter + ", " + instance + ", ");
                Scenario in = SMParser.parseSM(file.getPath());
                WWOSimulationResultsFuzzy best = null;
                long start=System.currentTimeMillis();
                if (serial)
                    WWOSimulatorSerialImproved.simcounter=0;
                else
                    WWOSimulator.simcounter=0;

                for (int x = 0; x < trials; x++) {
                    WWOSimulationResultsFuzzy res;
                    if (serial) {
                        res = WWOSerial.waterWaveOptimizationFuzzy(in, PSPLIBFuzzy.getFuzzyParameters(in.getTasks().length-2,parameter-1,instance-1), monteCarloRuns,population, h_max, alpha, beta, k_max, generations,measure);
                    }
                    else {
                        res = WWO.waterWaveOptimizationFuzzy(in, PSPLIBFuzzy.getFuzzyParameters(in.getTasks().length-2,parameter-1,instance-1), monteCarloRuns,population, h_max, alpha, beta, k_max, generations, maxDelays, initialMaxPrio,expandLd,measure);
                    }
                    if (best==null || res.isBetterThan(best,measure))
                        best = res;
                }
                double duration=(System.currentTimeMillis()-start)/(double)trials;
                int simcounter=(serial ? WWOSimulatorSerialImproved.simcounter : WWOSimulator.simcounter)/trials;
                System.out.print(best.getMin() + ", \t"+best.getMean() + ", \t"+best.getMax() + ", \t"+best.getStDev() + ", \t"+duration+", \t"+simcounter+";... \n");
                writer.write(best.getMin() + ", "+best.getMean() + ", "+best.getMax() + ", "+best.getStDev() + ", "+duration+", "+simcounter+";... \n");
                writer.flush();

            }
            writer.close();
        }
    }
}
