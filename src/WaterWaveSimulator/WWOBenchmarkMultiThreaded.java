/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package WaterWaveSimulator;

import Scenario.*;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class WWOBenchmarkMultiThreaded {
    public static void main(String[] args)throws Exception {

        int population=1000;
        //for (int population=10;population<=10;population+=100)
        int generations=1000;
            //for (int generations=1000;generations<=1000;generations+=100)
                for (int set=30;set<=120;set+=30) {
                    //int set = 30;
                    int n_threads=4;
                    int k_max = Math.min(12, set / 2);
                    int h_max = 6;
                    double beta = 0.25;
                    double alpha = 1.026;

                    int maxDelays=0; //ONLY IF NOT SERIAL
                    double initialMaxPrio=1; //ONLY IF NOT SERIAL
                    boolean expandLd=false; //ONLY IF NOT SERIAL

                    int trials = 10;

                    boolean serial=true;

                    DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");


                    String path = "C:\\Users\\ga75xux\\Desktop\\WWOresults\\";
                    path = "/tmp/";//home/homer/Documents/TUM/FAUST/MySim3/WWOresults/final/";
                    String filename = "FINAL_PSP" + set + "_WWO3D_";
                    if (serial)
                        filename+="SERIAL_";
                    filename+=trials + "_" + population + "_" + generations + "_" + k_max + "_" + h_max + "_" + alpha + "_" + beta +".txt";

                    System.out.println("writing to " + path + filename);

                    if (args.length > 1) {
                        set = Integer.parseInt(args[1]);
                    }
                    String inputpath = "C:\\Users\\ga75xux\\Desktop\\MySim3\\psplib-data\\";
                    inputpath = "/home/homer/Documents/TUM/Projekte/FAUST/MySim3/psplib-data/";
                    File f = new File(inputpath + set + File.separator);
                    File[] files = f.listFiles(new FilenameFilter() {
                        @Override
                        public boolean accept(File dir, String name) {
                            return name.endsWith(".sm");
                        }
                    });
                    List<File> sorted = new LinkedList<File>();
                    for (File file : files) {
                        if (!file.isDirectory())
                            sorted.add(file);
                    }
                    Collections.sort(sorted, new Comparator<File>() {
                        @Override
                        public int compare(File o1, File o2) {
                            return o1.getName().compareTo(o2.getName());
                        }
                    });

                    FileWriter writer = new FileWriter(new File(path + filename));



                    WWOThread[] threads=new WWOThread[n_threads];
                    int thread_counter=0;

                    for (File file : sorted) {

                        int parameter = Integer.parseInt(file.getName().substring(1 + (set + "").length(), 3 + (set + "").length()));
                        int instance = Integer.parseInt(file.getName().substring(4 + (set + "").length(), 6 + (set + "").length()));
                        //if (parameter<5) {
                            Scenario in = SMParser.parseSM(file.getPath());


                            WWOThread c_thread = threads[thread_counter];
                            while (c_thread != null && !c_thread.isDone()) {
                                try {
                                    Thread.sleep(1);
                                } catch (Exception ex) {
                                }
                            }

                            if (c_thread != null) {
                                //System.out.println(c_thread.isDone());
                                System.out.print(df.format(new Date()) + ":: " + c_thread.getParamInstance() + c_thread.getRes().getDuration() + ", \t" + c_thread.getDuration() + ", \t" + c_thread.getRes().getSimCount() + ";... \n");
                                writer.write(c_thread.getParamInstance() + c_thread.getRes().getDuration() + ", " + c_thread.getDuration() + ", " + c_thread.getRes().getSimCount() + ";... \n");
                                writer.flush();
                            }

                            threads[thread_counter] = new WWOThread(parameter, instance, in, population, h_max, k_max, generations, trials, maxDelays, alpha, beta, initialMaxPrio, serial,expandLd);
                            threads[thread_counter].start();
                            thread_counter++;
                            if (thread_counter >= n_threads) thread_counter = 0;
                        //}
                    }
                    for (int x=0;x<n_threads;x++){
                        WWOThread c_thread=threads[thread_counter];
                        while (c_thread!=null && !c_thread.isDone()){
                            try{Thread.sleep(1);}catch (Exception ex){}
                        }

                        if (c_thread!=null) {
                            //System.out.println(c_thread.isDone());
                            System.out.print(df.format(new Date())+":: "+c_thread.getParamInstance()+c_thread.getRes().getDuration() + ", \t" + c_thread.getDuration() + ", \t" + c_thread.getRes().getSimCount() + ";... \n");
                            writer.write(c_thread.getParamInstance()+c_thread.getRes().getDuration() + ", " + c_thread.getDuration() + ", " + c_thread.getRes().getSimCount() + ";... \n");
                            writer.flush();
                            threads[thread_counter]=null;
                        }
                        thread_counter++;
                        if (thread_counter>=n_threads)thread_counter=0;

                    }
                    writer.close();
                }
    }

    private static class WWOThread extends Thread{
        private Scenario in;
        private int population,h_max,k_max,generations, trials,maxDelays;
        private double alpha, beta, initialMaxPrio;
        private WWOSimulationResults res;
        private boolean serial, done,expandLd;
        private double duration;
        private int parameter, instance;

        public WWOThread(int parameter, int instance, Scenario in, int population, int h_max, int k_max, int generations, int trials, int maxDelays, double alpha, double beta, double initialMaxPrio, boolean serial, boolean expandLd) {
            this.parameter=parameter;
            this.instance=instance;
            this.in = in;
            this.population = population;
            this.h_max = h_max;
            this.k_max = k_max;
            this.generations = generations;
            this.trials = trials;
            this.maxDelays = maxDelays;
            this.alpha = alpha;
            this.beta = beta;
            this.initialMaxPrio = initialMaxPrio;
            this.serial = serial;
            this.expandLd=expandLd;
            this.done=false;
        }

        public void run(){
            long time=System.currentTimeMillis();
            WWOSimulationResults cur;
            for (int x = 0; x < trials; x++) {

                if (serial) {
                    cur = WWOSerial.waterWaveOptimization(in, population, h_max, alpha, beta, k_max, generations);
                }
                else {
                    cur = WWO.waterWaveOptimization(in, population, h_max, alpha, beta, k_max, generations, maxDelays, initialMaxPrio,expandLd);
                }
                if (res==null || cur.getDuration() < res.getDuration()){
                    res=cur;
                }
            }
            duration=(System.currentTimeMillis()-time)/(double)trials;
            done=true;
        }

        public boolean isDone() {
            return done;
        }

        public WWOSimulationResults getRes() {
            return res;
        }

        public double getDuration() {
            return duration;
        }

        public String getParamInstance(){
            return parameter + ", " + instance + ", ";
        }
    }
}
