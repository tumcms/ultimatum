/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package WaterWaveSimulator;

import Scenario.Scenario;
import org.apache.commons.math3.distribution.BetaDistribution;

public class WWOScenarioFuzzy extends WWOScenario{
    private double[][] fuzzyParameters;
    private BetaDistribution[] distributions;
    public WWOScenarioFuzzy(Scenario in, double[][] fuzzyParameters) {
        super(in);
        this.fuzzyParameters = fuzzyParameters;
        initDistributions();
    }

    public WWOScenarioFuzzy(Scenario in, double[][] fuzzyParameters, boolean randomize) {
        super(in, randomize);
        this.fuzzyParameters = fuzzyParameters;
        initDistributions();
    }

    public WWOScenarioFuzzy(Scenario in, double[][] fuzzyParameters, boolean randomize, double maxpriority) {
        super(in, randomize, maxpriority);
        this.fuzzyParameters = fuzzyParameters;
        initDistributions();
    }

    private void initDistributions(){
        distributions=new BetaDistribution[fuzzyParameters.length];
        for (int x=0;x<fuzzyParameters.length;x++){
            distributions[x]=new BetaDistribution(fuzzyParameters[x][1],fuzzyParameters[x][2]);
        }
    }

    public double[][] getFuzzyParameters() {
        return fuzzyParameters;
    }

    public long[][] getTasks(){
        long[][] t=super.getTasks();
        long[][] out=new long[t.length][];

        for (int x=0;x<t.length;x++){
            out[x]=new long[t[x].length];
            if (t[x][0]==0)
                out[x][0]=0;
            else
                out[x][0]=Math.round(t[x][0]+fuzzyParameters[x-1][0]*distributions[x-1].sample());
            for (int y=1;y<t[x].length;y++){
                out[x][y]=t[x][y];
            }
        }
        return out;
    }
}
