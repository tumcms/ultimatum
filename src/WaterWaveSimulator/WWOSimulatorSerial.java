/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package WaterWaveSimulator;

import Common.Pair;
import Main.ExampleGenerator;
import Scenario.SMParser;
import Scenario.Scenario;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class WWOSimulatorSerial {

    public static int simcounter=0;



    public static WWOSimulationResults simulate(WWOScenario in) {
        simcounter++;

        List<Integer> waitingTasks = new LinkedList<Integer>();
        List<Pair<Long,Integer>> runningTasks = new LinkedList<Pair<Long, Integer>>();
        List<Pair<Long,Integer>> finishedTasks = new LinkedList<Pair<Long, Integer>>();

        long[][] tasks=in.getTasks();



        boolean[] finished=new boolean[tasks.length];

        int[] resources = new int[in.getResources().length];
        for (int x = 0; x < in.getResources().length; x++) {
            resources[x] = in.getResources()[x];
        }

        double[] priorities=in.getPriorities();

        List<Integer> inputTasks = new LinkedList<Integer>();
        for (int x = 0; x < tasks.length; x++) {
            inputTasks.add(x);
        }

        //Iterator<Integer> i=inputTasks.iterator();
        Integer t;
        int x;
        while(!inputTasks.isEmpty()){
            double minprio=Double.MAX_VALUE;
            int mintask=0;
            for (x=0;x<inputTasks.size();x++) {
                t=inputTasks.get(x);
                if (checkDependencies(tasks[t], finished)){
                    if (priorities[t] < minprio) {
                        minprio = priorities[t];
                        mintask = x;
                    }
                }
            }
            waitingTasks.add(inputTasks.get(mintask));

            finished[inputTasks.remove(mintask)]=true;
        }

        //finished=new boolean[tasks.length];

        for (x = 0; x < tasks.length; x++) {
            finished[x]=false;
        }

        long time = 0;
        long nextStep;
        long temp;
        boolean done;

        while (waitingTasks.size() > 0 || runningTasks.size() >0){
            //System.out.println(waitingTasks.size()+" "+runningTasks.size());
            if (!runningTasks.isEmpty()) {
                nextStep = 0;
                for (Pair<Long, Integer> currentTask : runningTasks) {
                    temp = currentTask.getKey() + tasks[currentTask.getValue()][0];
                    if (nextStep == 0 || nextStep > temp)
                        nextStep = temp;
                }
                time = nextStep;
            }
            Pair<Long,Integer> currentTask;
            for (Iterator<Pair<Long,Integer>> i=runningTasks.iterator(); i.hasNext();) {
                currentTask=i.next();
                if (currentTask.getKey()+tasks[currentTask.getValue()][0]==time){
                    freeResources(tasks[currentTask.getValue()],resources);
                    finishedTasks.add(currentTask);
                    finished[currentTask.getValue()]=true;
                    i.remove();
                }
            }
            //System.out.println(waitingTasks.size()+" "+runningTasks.size());
            done=false;
            while (!done) {
                done=true;
                if (waitingTasks.size() > 0) {
                    currentTask = new Pair<Long, Integer>(time, waitingTasks.get(0));
                    if (checkResources(tasks[currentTask.getValue()], resources)) {
                        if (checkDependencies(tasks[currentTask.getValue()], finished)) {
                            useResources(tasks[currentTask.getValue()], resources);
                            runningTasks.add(currentTask);
                            waitingTasks.remove(0);
                            done=false;
                        }
                    }
                }
            }
            //try{Thread.sleep(10);}catch (Exception e){}
        }

        long[] res=new long[tasks.length];
        for (Pair<Long,Integer> ft:finishedTasks){
            res[ft.getValue()]=ft.getKey();
        }
        return new WWOSimulationResults(in,res,time);
    }




    private static boolean checkDependencies(long[] task, boolean[] finishedTasks){
        int nRes=(int)task[1];
        for (int x=2+2*nRes;x<task.length;x++){
            if (!finishedTasks[(int)task[x]])
                return false;
        }
        return true;
    }


    private static boolean checkResources(long[] task, int[] resources){
        long nRes=task[1];
        for (int x=0;x<nRes;x++){
            if (resources[(int)task[2+2*x]]<task[2+2*x+1])
                return false;
        }
        return true;
    }

    private static void useResources(long[] task, int[] resources){
        long nRes=task[1];
        for (int x=0;x<nRes;x++){
            resources[(int)task[2+2*x]]-=task[2+2*x+1];
        }
    }

    private static void freeResources(long[] task, int[] resources){
        long nRes=task[1];
        for (int x=0;x<nRes;x++){
            resources[(int)task[2+2*x]]+=task[2+2*x+1];
        }
    }

    public static void main(String[] args) throws Exception{


       /* WWOSimulationResults res3=simulateAndCollectSwaps(new WWOScenario(ExampleGenerator.generateRandomScenario(1,3,3,3,3)),new LinkedList<int[]>());
        System.out.println(res3.getFeasibleSwaps().size());
        System.out.println("\n\nFeasible Swaps:\n\n");
        for (int[] swap:res3.getFeasibleSwaps()) {
            System.out.println(swap[0] + " <> " + swap[1]);
        }

        System.exit(0);*/

        int mode=1;


        long start=System.currentTimeMillis();

        if (mode==0) {  //Load File
            String file="/home/homer/Documents/TUM/FAUST/MySim3/psplib-data/120/j12012_01.sm";

            WWOScenario scenario = new WWOScenario(SMParser.parseSM(file));

            WWOSimulationResults res = simulate(scenario);


            System.out.println("Res:"+res.getDuration());

            double[] priorities=scenario.getPriorities();
            Random rnd=new Random();
            for (int x=0;x<priorities.length;x++){
                priorities[x]=rnd.nextDouble();
            }

            res = simulate(scenario);


            System.out.println("Ressss:"+res.getDuration());


        }

        if (mode==1){
            for (int x=1;x<1000;x++){
                long gts=0;
                long gtps=0;
                for (int y=0;y<5000;y++){
                    Scenario s=ExampleGenerator.generateRandomScenario(x, 5, 10, 10, 5);
                    WWOScenario ws=new WWOScenario(s,true);
                    long t=System.currentTimeMillis();
                    WWOSimulatorSerial.simulate(ws);
                    gts+=(System.currentTimeMillis()-t);
                    t=System.currentTimeMillis();
                    WWOSimulator.simulate(ws);
                    gtps+=(System.currentTimeMillis()-t);

                }
                System.out.println(x+",\t"+(gts/5000.0)+",\t"+(gtps/5000.0)+";...");
            }
        }
    }

}
