/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package WaterWaveSimulator;

import Scenario.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class WWOSimulationResults {
    private long[] starttimes;
    private List<int[]> feasibleSwaps;
    private long duration;
    private long simCount;
    private WWOScenario scen;

    public WWOSimulationResults(WWOScenario scen, long[] starttimes, List<int[]> feasibleSwaps, long duration) {
        this.starttimes = starttimes;
        this.feasibleSwaps = feasibleSwaps;
        this.duration = duration;
        this.scen=scen;
    }

    public WWOSimulationResults(WWOScenario scen, long[] starttimes, long duration) {
        this.starttimes = starttimes;
        this.duration = duration;
        this.scen=scen;
    }

    public long[] getStarttimes() {
        return starttimes;
    }


    public int[] getRandomSwap(Random rnd){
        return feasibleSwaps.get(rnd.nextInt(feasibleSwaps.size()));
    }

    public List<int[]> getFeasibleSwaps() {
        return feasibleSwaps;
    }

    public long getDuration() {
        return duration;
    }

    public long getSimCount() {
        return simCount;
    }

    public void setSimCount(long simCount) {
        this.simCount = simCount;
    }

    public WWOScenario getScen(){ return scen; }



    public void saveToFile(File f) throws Exception{
        BufferedWriter writer=new BufferedWriter(new FileWriter(f));
        for (int x=0;x<starttimes.length;x++){
            String deps="";
            for (int y=0;y<scen.getTask(x).getDependencyCount();y++){
                deps+=scen.getTask(x).getDependency(y).getTask()+", ";
            }
            writer.write(scen.getTaskName(x)+" || "+deps+" || \t :: "+starttimes[x]+"\n");

        }
        writer.write(duration+"\n");
        writer.flush();
        writer.close();
    }

    public HashMap<String,Long> getSchedule(){
        HashMap<String,Long> res=new HashMap<String,Long>();
        for (int x=0;x<starttimes.length;x++){
            res.put(scen.getTaskName(x),starttimes[x]);
        }
        return res;
    }

    public Scenario generateScenario(){


        int[] res=scen.getResources();
        Resource[] resources=new Resource[res.length];
        for (int x=0;x<res.length;x++){
            resources[x]=scen.getResource(x);
        }

        Task[] tasks=new Task[starttimes.length];

        for (int x=0;x<starttimes.length;x++){
            tasks[x]=scen.getTask(x).prioritize(1-(starttimes[x]/(double)duration));
        }

        return new Scenario("WWOResult",tasks,resources,new String[]{});
    }
}
