/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package WaterWaveSimulator;

import Scenario.*;

import java.util.Random;

public class WWO {
    public static WWOSimulationResults waterWaveOptimization(Scenario in, int population, int h_max, double alpha, double beta, int k_max, int generations, int maxDelays, double initialMaxPrio, boolean expandLd){
        return waterWaveOptimization(in,population,h_max,alpha,beta,k_max,generations,maxDelays,initialMaxPrio,expandLd,0);
    }

    public static WWOSimulationResults waterWaveOptimization(Scenario in, int population, int h_max, double alpha, double beta, int k_max, int generations, int maxDelays, double initialMaxPrio, boolean expandLd, long bestKnownSolution){



        Random rnd=new Random();
        double L_d=1+(expandLd ? maxDelays : 0); //All priorities are in (0,1)

        long simCount=0;


        WWOScenario[] pop=new WWOScenario[population];
        long[] fitness=new long[population];
        int[] height=new int[population];
        double[] lambda=new double[population];

        WWOScenario candidate=new WWOScenario(in,true,initialMaxPrio);

        long f_min=Long.MAX_VALUE;
        long f_max=-Long.MAX_VALUE;
        long bestFitness=0;

        int p_max=0;

        for (int p=0;p<population;p++) {
            pop[p] = new WWOScenario(in, true,initialMaxPrio);
            long res=-WWOSimulator.simulate(pop[p]).getDuration();
            simCount++;
            fitness[p]=res;
            f_min= Math.min(f_min, res);
            if (res>f_max){
                f_max=res;
                bestFitness=res;
                p_max=p;
            }
            height[p]=h_max;
            lambda[p]=0.5;
        }
        double[] currPrio;
        double[] newPrio=candidate.getPriorities();
        double[] bestPrio=new double[newPrio.length];
        for (int x=0;x<bestPrio.length;x++){
            bestPrio[x]=pop[p_max].getPriorities()[x];
        }

        long newFitness;

        double betareduction=Math.max(0,beta-0.001)/(double)generations;



        for (int g=0;g<generations;g++){
            for (int p=0;p<population;p++) {
                currPrio = pop[p].getPriorities();


                //Propagate
                for (int x = 0; x < currPrio.length; x++) {
                    newPrio[x] = currPrio[x] + 2 * (rnd.nextDouble() - 0.5) * lambda[p] * L_d;       //Eq 6
                    if (newPrio[x]>1+maxDelays || newPrio[x]<0)        //Prio above 1 >> delay
                        newPrio[x]=rnd.nextDouble()*L_d;
                }
                newFitness = -WWOSimulator.simulate(candidate).getDuration();
                simCount++;

                if (newFitness > fitness[p]) {

                    //System.out.println("IMPROVE: "+newFitness+" > "+fitness[p]+" ("+bestFitness+")");
                    if (newFitness > bestFitness) {

                        for (int x = 0; x < currPrio.length; x++) {        //x* = x
                            currPrio[x] = newPrio[x];
                        }
                        fitness[p] = newFitness;

                        int k = 1 + rnd.nextInt(k_max);                         //Sample k

                        for (int d = 0; d < k; d++) {
                            for (int x = 0; x < currPrio.length; x++) {
                                newPrio[x] = currPrio[x] + rnd.nextGaussian() * beta * L_d;       //Eq 10
                            }
                            newFitness = -WWOSimulator.simulate(candidate).getDuration();
                            simCount++;
                            if (newFitness > fitness[p]) {
                                for (int x = 0; x < currPrio.length; x++) {
                                    currPrio[x] = newPrio[x];
                                }
                                fitness[p] = newFitness;
                            }
                        }
                        if (fitness[p] > bestFitness) {
                            bestFitness = fitness[p];

                            //System.out.println("NEW OPT: "+bestFitness);
                            for (int x = 0; x < currPrio.length; x++) {
                                bestPrio[x] = currPrio[x];
                            }
                        }
                    } else {
                        for (int x = 0; x < currPrio.length; x++) {
                            currPrio[x] = newPrio[x];
                        }
                        fitness[p] = newFitness;
                    }
                    height[p] = h_max;
                } else {
                    height[p] -= 1;
                    if (height[p] == 0) {
                        for (int x = 0; x < currPrio.length; x++) {
                            currPrio[x] = rnd.nextGaussian() * ((bestPrio[x] - currPrio[x]) / 2.0) + (bestPrio[x] + currPrio[x]) / 2.0;       //Eq 8
                        }
                        newFitness = -WWOSimulator.simulate(pop[p]).getDuration();
                        simCount++;
                        lambda[p] = lambda[p] * fitness[p] / newFitness;  //Eq 9
                        fitness[p] = newFitness;

                    }
                }
                lambda[p]=lambda[p]*Math.pow(alpha,-(fitness[p]-f_min+0.0000001)/(f_max-f_min+0.0000001));
            }
            f_max=0;
            f_min=Long.MAX_VALUE;
            for (int p=0;p<population;p++) {
                f_max=Math.max(f_max,fitness[p]);
                f_min=Math.min(f_min,fitness[p]);
            }
            beta-=betareduction;

            if (-bestFitness<=bestKnownSolution){
                break;
            }
            //if (g%10==0)
                //System.out.println(-bestFitness+", ");
        }

        for (int x = 0; x < bestPrio.length; x++) {
            newPrio[x]=bestPrio[x];
        }

        WWOSimulationResults res= WWOSimulator.simulate(candidate);
        res.setSimCount(simCount+1);
        return res;

    }


    public static WWOSimulationResultsFuzzy waterWaveOptimizationFuzzy(Scenario in, double[][] fuzzyParameters, int monteCarloCount, int population, int h_max, double alpha, double beta, int k_max, int generations, int maxDelays, double initialMaxPrio, boolean expandLd, int measure){


        Random rnd=new Random();
        double L_d=1+(expandLd ? maxDelays : 0); //All priorities are in (0,1)

        long simCount=0;


        WWOScenarioFuzzy[] pop=new WWOScenarioFuzzy[population];
        double[] fitness=new double[population];
        int[] height=new int[population];
        double[] lambda=new double[population];

        WWOScenarioFuzzy candidate=new WWOScenarioFuzzy(in,fuzzyParameters,true,initialMaxPrio);

        double f_min=Double.MAX_VALUE;
        double f_max=-Double.MAX_VALUE;
        double bestFitness=0;

        int p_max=0;

        for (int p=0;p<population;p++) {
            pop[p] = new WWOScenarioFuzzy(in, fuzzyParameters,true,initialMaxPrio);
            double res=-WWOSimulator.simulate(pop[p],monteCarloCount).getMeasure(measure);
            simCount++;
            fitness[p]=res;
            f_min= Math.min(f_min, res);
            if (res>f_max){
                f_max=res;
                bestFitness=res;
                p_max=p;
            }
            height[p]=h_max;
            lambda[p]=0.5;
        }
        double[] currPrio;
        double[] newPrio=candidate.getPriorities();
        double[] bestPrio=new double[newPrio.length];
        for (int x=0;x<bestPrio.length;x++){
            bestPrio[x]=pop[p_max].getPriorities()[x];
        }

        double newFitness;

        double betareduction=Math.max(0,beta-0.001)/(double)generations;



        for (int g=0;g<generations;g++){
            for (int p=0;p<population;p++) {
                currPrio = pop[p].getPriorities();


                //Propagate
                for (int x = 0; x < currPrio.length; x++) {
                    newPrio[x] = currPrio[x] + 2 * (rnd.nextDouble() - 0.5) * lambda[p] * L_d;       //Eq 6
                    if (newPrio[x]>1+maxDelays || newPrio[x]<0)        //Prio above 1 >> delay
                        newPrio[x]=rnd.nextDouble()*L_d;
                }
                newFitness = -WWOSimulator.simulate(candidate,monteCarloCount).getMeasure(measure);
                simCount++;

                if (newFitness > fitness[p]) {

                    //System.out.println("IMPROVE: "+newFitness+" > "+fitness[p]+" ("+bestFitness+")");
                    if (newFitness > bestFitness) {

                        for (int x = 0; x < currPrio.length; x++) {        //x* = x
                            currPrio[x] = newPrio[x];
                        }
                        fitness[p] = newFitness;

                        int k = 1 + rnd.nextInt(k_max);                         //Sample k

                        for (int d = 0; d < k; d++) {
                            for (int x = 0; x < currPrio.length; x++) {
                                newPrio[x] = currPrio[x] + rnd.nextGaussian() * beta * L_d;       //Eq 10
                            }
                            newFitness = -WWOSimulator.simulate(candidate,monteCarloCount).getMeasure(measure);
                            simCount++;
                            if (newFitness > fitness[p]) {
                                for (int x = 0; x < currPrio.length; x++) {
                                    currPrio[x] = newPrio[x];
                                }
                                fitness[p] = newFitness;
                            }
                        }
                        if (fitness[p] > bestFitness) {
                            bestFitness = fitness[p];

                            //System.out.println("NEW OPT: "+bestFitness);
                            for (int x = 0; x < currPrio.length; x++) {
                                bestPrio[x] = currPrio[x];
                            }
                        }
                    } else {
                        for (int x = 0; x < currPrio.length; x++) {
                            currPrio[x] = newPrio[x];
                        }
                        fitness[p] = newFitness;
                    }
                    height[p] = h_max;
                } else {
                    height[p] -= 1;
                    if (height[p] == 0) {
                        for (int x = 0; x < currPrio.length; x++) {
                            currPrio[x] = rnd.nextGaussian() * ((bestPrio[x] - currPrio[x]) / 2.0) + (bestPrio[x] + currPrio[x]) / 2.0;       //Eq 8
                        }
                        newFitness = -WWOSimulator.simulate(pop[p],monteCarloCount).getMeasure(measure);
                        simCount++;
                        lambda[p] = lambda[p] * fitness[p] / newFitness;  //Eq 9
                        fitness[p] = newFitness;

                    }
                }
                lambda[p]=lambda[p]*Math.pow(alpha,-(fitness[p]-f_min+0.0000001)/(f_max-f_min+0.0000001));
            }
            f_max=0;
            f_min=Long.MAX_VALUE;
            for (int p=0;p<population;p++) {
                f_max=Math.max(f_max,fitness[p]);
                f_min=Math.min(f_min,fitness[p]);
            }
            beta-=betareduction;
            //if (g%10==0)
            //System.out.println(-bestFitness+", ");
        }

        for (int x = 0; x < bestPrio.length; x++) {
            newPrio[x]=bestPrio[x];
        }

        WWOSimulationResultsFuzzy res= WWOSimulator.simulate(candidate,monteCarloCount*100);
        res.setSimCount(simCount+1);
        return res;

    }


    public static void main(String[] args) throws Exception {

        String file="/home/homer/Documents/TUM/FAUST/MySim3/psplib-data/120/j12012_02.sm";

        Scenario scenario = SMParser.parseSM(file);


        WWOSimulationResults res = WWOSimulator.simulate(new WWOScenario(scenario));

        System.out.println("Res:"+res.getDuration());

        res = waterWaveOptimization(scenario, 200, 6, 1.001, 0.01, 60, 1000,5,2,false);


        System.out.println("Res:" + res.getDuration());
    }
}
