/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Geometry;

import java.awt.*;


public class RectangularArea extends Area{
    private int width,height; //Dimensions
    private double angle;   //Tilt //TODO

    /**
     * Construct new rectangular Area
     * @param xposition center x position
     * @param yposition center y position
     * @param width Width
     * @param height Height
     * @param angle Angle/Tilt
     */
    public RectangularArea(int xposition, int yposition, int width, int height, double angle) {
        super(xposition, yposition);
        this.width = width;
        this.height = height;
        this.angle = angle;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public double getAngle() {
        return angle;
    }

    @Override
    public int getSurfaceArea() {
        return width*height;
    }


    @Override
    public boolean isIntersecting(Area a) {

        if (a==null){
            throw new NullPointerException("DARN IT");
        }

        if (a instanceof RectangularArea){
            //TODO angle
            Rectangle r1=new Rectangle(getXposition(),getYposition(),width,height);
            RectangularArea area2=(RectangularArea)a;
            Rectangle r2=new Rectangle(area2.getXposition(),area2.getYposition(),area2.getWidth(),area2.getHeight());
            return r1.intersects(r2);
        }
        else if (a instanceof CircularArea){
            /*
            Rotate to 0 angle
            bool intersects(CircleType circle, RectType rect)
{
    circleDistance.x = abs(circle.x - rect.x);
    circleDistance.y = abs(circle.y - rect.y);

    if (circleDistance.x > (rect.width/2 + circle.r)) { return false; }
    if (circleDistance.y > (rect.height/2 + circle.r)) { return false; }

    if (circleDistance.x <= (rect.width/2)) { return true; }
    if (circleDistance.y <= (rect.height/2)) { return true; }

    cornerDistance_sq = (circleDistance.x - rect.width/2)^2 +
                         (circleDistance.y - rect.height/2)^2;

    return (cornerDistance_sq <= (circle.r^2));
}
             */

            //ToDo
        }
        return false;
    }

    @Override
    public boolean equals(Object e){
        if (e instanceof RectangularArea){
            RectangularArea area=(RectangularArea)e;
            return area.getXposition()==getXposition() &&
                    area.getYposition()==getYposition() &&
                    area.getWidth()==width &&
                    area.getHeight()==height &&
                    area.getAngle()==angle;
        }
        return false;
    }

    @Override
    public boolean isInside(int x, int y) {
        return new Rectangle(getXposition(),getYposition(),width,height).contains(x,y);
    }

    @Override
    public int getMinX() {
        return getXposition();
    }

    @Override
    public int getMinY() {
        return getYposition();
    }

    @Override
    public int getMaxX() {
        return getXposition()+width;
    }

    @Override
    public int getMaxY() {
        return getYposition()+height;
    }

    public String toString() {
        return "RECTANGLE("+getXposition()+";"+getYposition()+";"+getWidth()+";"+getHeight()+";"+getAngle()+")";
    }

}
