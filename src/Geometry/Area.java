/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Geometry;


public abstract class Area {
    
    private int xposition,yposition; //Position

    public abstract int getSurfaceArea(); //Surface area

    public abstract boolean isIntersecting(Area a);

    public abstract boolean isInside(int x, int y);

    public abstract String toString();

    public abstract int getMinX();
    public abstract int getMinY();
    public abstract int getMaxX();
    public abstract int getMaxY();
    /**
     * Create a new Area
     * @param xposition
     * @param yposition
     */
    protected Area(int xposition, int yposition) {
        this.xposition = xposition;
        this.yposition = yposition;
    }

    /**
     * Get X Position of Area
     * @return x position
     */
    public int getXposition() {
        return xposition;
    }

    /**
     * Get Y Position of Area
     * @return y position
     */
    public int getYposition() {
        return yposition;
    }


    public static Area parseGeometricResourceUsage(String text){
        if (text.startsWith("RECTANGLE(")){
            String[] vals=text.substring(10,text.length()-2).split(";");
            int xp=Integer.parseInt(vals[0]);
            int yp=Integer.parseInt(vals[1]);
            int w=Integer.parseInt(vals[2]);
            int h=Integer.parseInt(vals[3]);
            double a=Double.parseDouble(vals[4]);
            return new RectangularArea(xp,yp,w,h,a);
        }
        else if (text.startsWith("CIRCLE(")){
            String[] vals=text.substring(10).split(";");
            int xp=Integer.parseInt(vals[0]);
            int yp=Integer.parseInt(vals[1]);
            int r=Integer.parseInt(vals[2]);
            return new CircularArea(xp,yp,r);
        }
        return null;
    }


}
