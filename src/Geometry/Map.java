/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Geometry;


public abstract class Map {

    private String name;

    public Map(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract boolean isAreaUsable(Area a);
    public abstract boolean useArea(Area a);
    public abstract void freeArea(Area a);
    public abstract Map clone();


}
