/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Geometry;



public class CircularArea extends Area{
    private int radius;

    public CircularArea(int xposition, int yposition, int radius) {
        super(xposition, yposition);
        this.radius=radius;
    }

    @Override
    public int getSurfaceArea() {
        return (int)Math.round(radius*radius*Math.PI);
    }


    public double getRadius() {
        return radius;
    }

    @Override
    public boolean isIntersecting(Area a) {
        if (a instanceof CircularArea){
            return Math.sqrt(Math.pow(a.getXposition()-getXposition(),2)+Math.pow(a.getYposition()-getYposition(),2))<radius+((CircularArea)a).getRadius();
        }
        if (a instanceof RectangularArea){
            //TODO
            return false;
        }
        //TODO
        return false;
    }

    @Override
    public boolean equals(Object e){
        if (e instanceof CircularArea){
            CircularArea area=(CircularArea)e;
            return  area.getXposition()==getXposition() &&
                    area.getYposition()==getYposition() &&
                    area.getRadius()==radius;
        }
        return false;
    }

    @Override
    public boolean isInside(int x, int y) {
        return Math.pow(x-getXposition(),2)+Math.pow(y-getYposition(),2)<Math.pow(radius,2);
    }

    @Override
    public int getMinX() {
        return getXposition()-radius;
    }

    @Override
    public int getMinY() {
        return getYposition()-radius;
    }

    @Override
    public int getMaxX() {
        return getXposition()+radius;
    }

    @Override
    public int getMaxY() {
        return getYposition()+radius;
    }

    @Override
    public String toString() {
        return "CIRCLE("+getXposition()+";"+getYposition()+";"+getRadius()+")";
    }
}
