/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Geometry;

import java.util.LinkedList;
import java.util.List;


public class ListMap extends Map {
    private List<Area> usedAreas;

    public ListMap(String name){
        super(name);
        usedAreas=new LinkedList<Area>();
    }

    @Override
    public boolean isAreaUsable(Area a) {
        for (Area area:usedAreas){
            if (area.isIntersecting(a))
                return false;
        }
        return true;
    }

    @Override
    public boolean useArea(Area a) {
        if (!isAreaUsable(a))
            return false;
        usedAreas.add(a);
        return true;
    }

    @Override
    public void freeArea(Area a) {
        usedAreas.remove(a);
    }

    public Map clone(){
        return new ListMap(getName());
    }

}
