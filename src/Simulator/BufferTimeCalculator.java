/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Simulator;

import GUI.BufferChart;
import Scenario.*;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.*;
import java.util.List;

public class BufferTimeCalculator {
    public static HashMap<String,long[]> calculateBufferTimes(HashMap<String,Long> schedule, Scenario scenario){
        ScenarioGenerator gen=new ScenarioGenerator("Reverse");

        HashMap<String,Resource> resources=new HashMap<String, Resource>();
        for (Resource r:scenario.getResources()){
            resources.put(r.getType(),r);
        }


        Task[] inTasks=scenario.getTasks();

        final HashMap<String,Long> sched=schedule;

        ArrayList<Task> tasks=new ArrayList<Task>();
        for (Task t:inTasks)
            tasks.add(t);
        Collections.sort(tasks,new Comparator<Task>(){
            @Override
            public int compare(Task o1, Task o2) {
                return sched.get(o1.getName()).compareTo(sched.get(o2.getName()));
            }
        });
        boolean done;
        for (int x=0;x<tasks.size();x++){
            Task taskA=tasks.get(x);
            List<TaskDependency> newDeps=new LinkedList<TaskDependency>();
            if (scenario.getReverseDependency(taskA.getName())!=null){
                for (TaskDependency tDep:scenario.getReverseDependency(taskA.getName())){
                    newDeps.add(new TaskDependency(tDep.getTask(),tDep.getType()));
                }
            }
            for (int a=0;a<taskA.getResourceCount();a++){
                ResourceUsage ruA=taskA.getResource(a);
                if (!ruA.isGeometric()){
                    int lastResourceCount=0;

                    int avail=resources.get(ruA.getType()).getCount();
                    int count=ruA.getCount();
                    for (int y=0;y<tasks.size();y++){
                        Task taskB=tasks.get(y);

                        if (schedule.get(taskB.getName())>=schedule.get(taskA.getName())&&
                            !dependsOn(scenario,taskB.getName(),taskA.getName())&&
                            !dependsOn(scenario,taskA.getName(),taskB.getName())
                            ){
                            done=false;
                            for (int z=0;z<taskB.getResourceCount();z++){
                                ResourceUsage ruB=taskB.getResource(z);
                                if (ruA.getType().equals(ruB.getType())){
                                    boolean check=false;
                                    if (count>=avail){
                                        if (ruB.getCount()<=lastResourceCount){
                                            check=true;
                                        }
                                    }
                                    else{
                                        count+=ruB.getCount();   //todo All Violators at first violating time instance
                                        check=true;
                                        lastResourceCount=ruB.getCount();
                                    }
                                    if (count>=avail&&check){

                                        if (scenario.getReverseDependency(taskB.getName())!=null){
                                            for (TaskDependency req:scenario.getReverseDependency(taskB.getName())){
                                                if (!dependsOn(scenario,req.getTask(),taskA.getName())){
                                                    newDeps.add(new TaskDependency(req.getTask(),req.getType()));
                                                    System.out.println("ADDING DEP "+taskA.getName()+" depends on "+req.getTask());
                                                }
                                            }
                                            done=true;
                                        }
                                    }
                                }
                                if (done)
                                    break;
                            }
                        }
                    }
                }
            }
            gen.addTask(new Task(
                    taskA.getName(),
                    taskA.getProvides(),
                    taskA.cloneResources(),
                    newDeps.toArray(new TaskDependency[0]),
                    taskA.getDuration(),
                    x+1,
                    taskA.getCost(),
                    taskA.getColor()
            ));
        }
        for (Resource r:scenario.getResources()){
            gen.addResource(r);
        }
        for (String m:scenario.getMaps()){
            gen.addMap(m);
        }



        Simulator sim=new Simulator(gen.generateScenario());
        sim.disableOutput();
        sim.simulate();
        HashMap<String,Long> backwardSchedule=sim.getExecutionTimes();


        //gen.generateScenario().createXMLFile(new File("/home/homer/Documents/TUM/FAUST/MySim/data/gergoe/backwards.xml"),backwardSchedule);




        HashMap<String,long[]> res=new HashMap<String, long[]>();
        for (String s:schedule.keySet()){
            res.put(s,new long[]{
                    schedule.get(s),
                    sim.getClock()-backwardSchedule.get(s)-scenario.getTask(s).getDuration(),
                    schedule.get(s)+scenario.getTask(s).getDuration(),
                    (sim.getClock()-backwardSchedule.get(s))
            });
        }

        return res;


    }

    public static boolean dependsOn(Scenario s,String a, String b){
        if (a.equals(b))
            return true;
        return dependsOn(s,s.getTask(a),b);
    }

    public static boolean dependsOn(Scenario s,Task a, String b){
        if (a.getName().equals(b))
            return true;
        for (int x=0;x<a.getDependencyCount();x++){
            TaskDependency d=a.getDependency(x);
            if (d.getTask().equals(b)){
                return true;
            }
            else{
                if (dependsOn(s,d.getTask(),b)){
                    return true;
                }
            }
        }
        return false;
    }}
