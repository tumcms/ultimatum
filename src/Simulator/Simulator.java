/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Simulator;

import Charts.GraphData;
import Charts.GraphFrame;
import Common.Pair;
import GUI.GanttPanel;
import Geometry.ListMap;
import Scenario.*;

import javax.swing.*;
import java.util.*;


public class Simulator {
    private Scenario scenario;
    private long clock,lastSimTime;
    private int lastFails;

    private LinkedList<Task> todo;
    private HashMap<String, Pair<Task,Long>> executed;
    private HashMap<String, Pair<Task,Long>> running;
    private HashMap<String,Integer> resources;

    private ArrayList<long[]> resourceUtilization;
    private ArrayList<double[]> costGraph;

    private HashMap<String,Integer> resourceIDs;

    private ArrayList<Task> previousTasks;
    private ArrayList<Task> newlyExecutedTasks;

    private HashMap<String, ListMap> maps;

    private ArrayList<Task> taskList;

    private LinkedList<TaskSwap> swaps;

    private GanttPanel ganttChart;
    private GraphFrame resourceGraph;
    private GraphData resourceGraphData;

    private LinkedList<TaskSwap> appliedSwaps;

    private HashMap<String, Integer> delays;
    private HashSet<String> activeDelays;


    private double costs;

    private Comparator<Task> taskComparator=new Comparator<Task>() {
        @Override
        public int compare(Task o1, Task o2) {
            return (int)Math.signum(o2.getPriority()-o1.getPriority());
        }
    };

    private boolean output;
    private boolean showResourceGraph;
    private boolean collectSwaps;
    private boolean includeDelays;
    private boolean collectResourceUtilization;
    private boolean collectCostData;

    private static long simCounter=0;


    public Simulator(Scenario scenario) {
        this.scenario = scenario;
        taskList = new ArrayList<Task>();
        todo = new LinkedList<Task>();
        resources = new HashMap<String, Integer>();
        executed = new HashMap<String, Pair<Task,Long>>();
        running = new HashMap<String, Pair<Task,Long>>();
        previousTasks = new ArrayList<Task>();
        newlyExecutedTasks = new ArrayList<Task>();
        maps = new HashMap<String, ListMap>();

        this.output = true;
        this.collectSwaps=false;
        this.showResourceGraph=false;
        this.collectResourceUtilization=false;
        this.collectCostData=false;
        this.includeDelays=true;

        this.delays=new HashMap<String, Integer>();
        this.activeDelays=new HashSet<String>();
    }

    public void setAppliedSwaps(LinkedList<TaskSwap> appliedSwaps){
        this.appliedSwaps=appliedSwaps;
    }

    public void setAppliedSwap(TaskSwap appliedSwap){
        this.appliedSwaps=new LinkedList<TaskSwap>();
        this.appliedSwaps.add(appliedSwap);


    }

    public void disableOutput(){
        this.output = false;
    }

    public void enableSwapCollection(){
        this.collectSwaps=true;
        swaps = new LinkedList<TaskSwap>();
    }

    public void setIncludeDelays(boolean delays){
        this.includeDelays=delays;
    }

    public void enableCostDataCollection(){
        this.collectCostData=true;
        costGraph=new ArrayList<double[]>();
    }

    public void enableGanttChart(){

        JFrame frame=new JFrame("Gantt");
        ganttChart=new GanttPanel(null);
        JScrollPane pane = new JScrollPane(ganttChart,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.getVerticalScrollBar().setUnitIncrement(16);
        ganttChart.setVisible(true);
        frame.add(pane);
        frame.setSize(600,600);
        frame.setVisible(true);
    }

    public void enableResourceGraph(){
        showResourceGraph=true;
    }

    public void enableResourceUtilizationCollection(){
        collectResourceUtilization =true;
        resourceUtilization=new ArrayList<long[]>();
        resourceIDs=new HashMap<String, Integer>();
    }

    private void reset(){
        if (ganttChart!=null){
            ganttChart.reset();
            ganttChart.repaint();
        }
        if (resourceGraph!=null){
            resourceGraphData.clear();
            resourceGraph.repaint();
        }
        if (collectResourceUtilization){
            resourceUtilization.clear();
        }
        if (collectCostData){
            costGraph.clear();
        }

        delays.clear();
        activeDelays.clear();

        HashMap<String,Task> taskMap=new HashMap<String, Task>();
        for (Task t:scenario.getTasks())
            taskMap.put(t.getName(),t);

        for (Task t:scenario.getTasks()){
            Integer delay=scenario.getDelay(t.getName());
            if (delay!=null){
                delays.put(t.getName(),delay);
            }
        }

        if (appliedSwaps!=null){
            for (TaskSwap swap:appliedSwaps){
                if (swap.isDelay()){
                    if (!swap.isAntiDelay()){
                        if (delays.containsKey(swap.getTaskA()))
                            delays.put(swap.getTaskA(),delays.get(swap.getTaskA())+1);
                        else
                            delays.put(swap.getTaskA(),1);
                        //System.err.println("APPLY DELAY "+swap.getTaskA());
                    }
                    //System.err.println("Adding delay for "+swap.getTaskA());
                }
                else{
                    Task taskA=taskMap.get(swap.getTaskA());
                    Task taskB=taskMap.get(swap.getTaskB());
                    double prioB=taskB.getPriority();
                    if (taskA.getPriority()==prioB)
                        prioB=taskA.getPriority()+1;   //todo think about this
                    taskB=taskB.prioritize(taskA.getPriority());
                    taskA=taskA.prioritize(prioB);
                    taskMap.put(taskA.getName(),taskA);
                    taskMap.put(taskB.getName(),taskB);
                }
            }
            for (TaskSwap swap:appliedSwaps){
                if (swap.isAntiDelay()){
                    if (delays.containsKey(swap.getTaskA())){
                        if (delays.get(swap.getTaskA())>1)
                            delays.put(swap.getTaskA(),delays.get(swap.getTaskA())-1);
                        else
                            delays.remove(swap.getTaskA());
                    }
                    //System.err.println("APPLY ANTIDELAY "+swap.getTaskA());
                }
            }
        }

        todo.clear();
        for (Task t:scenario.getTasks())
            todo.add(t);

        maps.clear();
        for (String m:scenario.getMaps())
            maps.put(m,new ListMap(m));
        resources.clear();
        Resource[] scenRes=scenario.getResources();
        for (int x=0;x<scenRes.length;x++){
            //if (resources.containsKey(r.getType())){
                //resources.put(r.getType(),resources.get(r.getType())+r.getCount());
            //}
            //else{
                resources.put(scenRes[x].getType(),scenRes[x].getCount());

                if (collectResourceUtilization)
                    resourceIDs.put(scenRes[x].getType(),x);

            //}
        }

        executed.clear();
        clock=0;
        costs=0;

        if (collectSwaps){
            swaps=new LinkedList<TaskSwap>();
        }

        if (showResourceGraph){
            resourceGraphData=new GraphData(resources.keySet().toArray(new String[0]));
            resourceGraph=new GraphFrame(resourceGraphData,Math.min(6,resources.size()));
        }


    }

    private void updateExecutableTasks(){
        taskList.clear();
        for (Task t:todo){
            //if (!activeDelays.contains(t.getName())){
            //    System.err.println("C "+clock);
                boolean b=!activeDelays.contains(t.getName());
                //if (!b && running.isEmpty()){
                //    b=true;
                //    System.err.println("Invalid Delay of "+t.getName());
                //}
                //if (!b)
                  //  System.err.println("[ "+clock+" ] Delayed "+t.getName());
                if (b){
                    for (int x=0;x<t.getDependencyCount();x++){
                        //System.err.println("D "+clock);
                        if (t.getDependency(x).getType()==TaskDependency.TYPE_FS){
                            if (!executed.containsKey(t.getDependency(x).getTask())){
                                b=false;
                            }
                        }
                        else{
                            if (!running.containsKey(t.getDependency(x).getTask()) &&
                                    !executed.containsKey(t.getDependency(x).getTask())){
                                b=false;
                            }
                        }
                        //System.err.println("E "+clock);
                    }
                }

                if (b){
                    //System.err.println("F "+clock);
                    for (int x=0;x<t.getResourceCount();x++){
                        if (t.getResource(x).isGeometric()){
                            GeometricResourceUsage geores=(GeometricResourceUsage)t.getResource(x);
                            if (!maps.get(geores.getMap()).isAreaUsable(geores.getArea()))
                                b=false;
                        }
                        else if (resources.get(t.getResource(x).getType())<t.getResource(x).getCount()){
                            b=false;
                        }
                    }
                    if (b){
                        taskList.add(t);
                    }
                    //System.err.println("G "+clock);
                }

            //}
            //else{
            //    System.err.println("[ "+clock+" ] Delayed "+taskList.get(0).getName());
            //}
            //System.err.println("D "+clock);
        }
        for (Task t:todo){
            if (!taskList.contains(t)){
                //if (!activeDelays.contains(t.getName())){
                //    System.err.println("C "+clock);
                boolean b=!activeDelays.contains(t.getName()) || (running.isEmpty() && taskList.isEmpty());
                //if (!b && running.isEmpty()){
                //    b=true;
                //    System.err.println("Invalid Delay of "+t.getName());
                //}
                //if (!b)
                //  System.err.println("[ "+clock+" ] Delayed "+t.getName());
                if (b){
                    for (int x=0;x<t.getDependencyCount();x++){
                        //System.err.println("D "+clock);
                        if (t.getDependency(x).getType()==TaskDependency.TYPE_FS){
                            if (!executed.containsKey(t.getDependency(x).getTask())){
                                b=false;
                            }
                        }
                        else{
                            if (!running.containsKey(t.getDependency(x).getTask()) &&
                                    !executed.containsKey(t.getDependency(x).getTask())){
                                b=false;
                            }
                        }
                        //System.err.println("E "+clock);
                    }
                }

                if (b){
                    //System.err.println("F "+clock);
                    for (int x=0;x<t.getResourceCount();x++){
                        if (t.getResource(x).isGeometric()){
                            GeometricResourceUsage geores=(GeometricResourceUsage)t.getResource(x);
                            if (!maps.get(geores.getMap()).isAreaUsable(geores.getArea()))
                                b=false;
                        }
                        else if (resources.get(t.getResource(x).getType())<t.getResource(x).getCount()){
                            b=false;
                        }
                    }
                    if (b){
                        taskList.add(t);
                    }
                    //System.err.println("G "+clock);
                }

                //}
                //else{
                //    System.err.println("[ "+clock+" ] Delayed "+taskList.get(0).getName());
                //}
                //System.err.println("D "+clock);
            }
        }

    }

    private void executeTask(Task task){

        costs+=task.getCost();

        if (task.getDuration()==0){
            executed.put(task.getName(),new Pair<Task, Long>(task,clock));
        }
        else{
            running.put(task.getName(),new Pair<Task, Long>(task,clock));
            for (int x=0;x<task.getResourceCount();x++){
                if (task.getResource(x).isGeometric()){
                    GeometricResourceUsage geoRes=(GeometricResourceUsage)task.getResource(x);
                    if (!maps.get(geoRes.getMap()).useArea(geoRes.getArea())){
                        System.err.println("ERROR: "+task.getName()+" uses an unavailable area");
                    }
                }
                else {
                    int y=resources.get(task.getResource(x).getType());
                    y-=task.getResource(x).getCount();
                    if (y<0){
                        System.err.println("ERROR: "+task.getName()+" uses more of "+task.getResource(x).getType()+" than available");
                    }
                    resources.put(task.getResource(x).getType(),y);
                }
            }
        }
        todo.remove(task);
        if (output)
            System.out.println("[ "+clock+" ] Starting "+task.getName());

        if (ganttChart!=null){
            ganttChart.addData(
                    task,
                    clock,
                    task.getColor(),
                    false
            );
        }


    }

    private void finishTask(Task task){
        Pair<Task,Long> p=running.get(task.getName());
        running.remove(task.getName());
        executed.put(task.getName(),p);
        for (int x=0;x<task.getResourceCount();x++){
            if (task.getResource(x).isGeometric()){
                GeometricResourceUsage geoRes=(GeometricResourceUsage)task.getResource(x);
                maps.get(geoRes.getMap()).freeArea(geoRes.getArea());
            }
            else {
                int y=resources.get(task.getResource(x).getType());
                y+=task.getResource(x).getCount();
                resources.put(task.getResource(x).getType(),y);
            }
        }

        if (output) System.out.println("[ "+clock+" ] Finishing "+task.getName());
    }

    private boolean finishTasks(){
        boolean res=false;
        boolean done=false;
        while (!done){
            done=true;
            for (Pair<Task,Long> p:running.values()) {
                if (clock>=p.getValue()+p.getKey().getDuration()){
                    finishTask(p.getKey());
                    res=true;
                    done=false;
                    break;
                }
            }
        }
        return res;
    }

    private void incrementClock(){
        long l=Long.MAX_VALUE;
        if (running.isEmpty()){
            if (output) System.out.println("Nothing running");
            return;
        }

        for (Pair<Task,Long> p:running.values()) {
            l=Math.min(l,p.getValue()+p.getKey().getDuration());
        }
        clock=l;

        //if (output) System.out.println("TIME: "+clock);
    }

    private boolean simulationStep(){
        boolean res=false;
        activeDelays.clear();

        newlyExecutedTasks.clear();

        //System.err.println("A "+clock);

        updateExecutableTasks();

        //If we're collecting swaps
        if (collectSwaps){

            //Copy list of executable tasks prior to update
            previousTasks.clear();
            for (int x=0;x<taskList.size();x++){
                previousTasks.add(taskList.get(x));
            }
        }
        //As long as we execute something at this time step
        while(!taskList.isEmpty()){


            //System.err.println("B "+clock);
            //Sort executable tasks by priority
            Collections.sort(taskList,taskComparator);

            //System.out.println(taskList.get(0).getPriority());

            //Or shuffle them
            //Collections.shuffle(taskList);


            Integer delay=delays.get(taskList.get(0).getName());
            if (delay!=null && delay>0){
                delays.put(taskList.get(0).getName(),delay-1);
                activeDelays.add(taskList.get(0).getName());
                if (collectSwaps && includeDelays){
                    swaps.add(new TaskSwap(taskList.get(0).getName(),true));
                    //System.err.println("[ " + clock + " ] Antidelay swap " + taskList.get(0).getName());
                }
                //System.err.println("[ " + clock + " ] Delaying " + taskList.get(0).getName());
                //taskList.remove(0);
            }                                                               //TODO DELAYS
            else{
                if (includeDelays && collectSwaps && (taskList.size()>1 || running.size()>0)){
                        swaps.add(new TaskSwap(taskList.get(0).getName(),false)); //We have a new delay possibility
                        //if (output)
                            //System.err.println("[ " + clock + " ] DELAY: " + taskList.get(0).getName()+" "+taskList.size()+ " "+running.size());
                            //for (Task t:taskList){
                            //    System.err.println("TASK "+t.getName());
                            //}
                }

                //Execute first task in sorted list
                executeTask(taskList.get(0));

                //Note task as executed in this time step
                newlyExecutedTasks.add(taskList.get(0));

                //This time step did execute a new task
                res=true;

            }





            //Update list of executable tasks
            updateExecutableTasks();


            if (collectSwaps){ //Add tasks that became executable due to SS constraints
                for (int x=0;x<taskList.size();x++){
                    if (!previousTasks.contains(taskList.get(x)))
                        previousTasks.add(taskList.get(x));
                }
            }

        }
        //If we're collecting swaps
        if (collectSwaps){

            //For each task on our tasklist in the previous iteration
            for (Task t2:previousTasks){
                if (!activeDelays.contains(t2)){
                    boolean found=false;

                    // For each task on our new task list
                    for (Task t1:taskList){

                        //If tasks match they are on both lists, and therefore not introducing a new swap possibility.
                        if (t1.getName().equals(t2.getName())){
                            found=true;
                            break; //We break out of this loop
                        }
                    }

                    // If the task was removed during the last update
                    if (!found){
                        boolean match=false;

                        //We want to check for all tasks that were executed in this timestep that do
                        //share a resource with insufficient available amount for parallel execution
                        for (Task t3:newlyExecutedTasks){ //For each task executed in this time step
                            if (!t3.getName().equals(t2.getName())){ //If the tasks are not the same one
                                for (int r3=0;r3<t3.getResourceCount();r3++){ //For each resource
                                    ResourceUsage res3=t3.getResource(r3);
                                    for (int r2=0;r2<t2.getResourceCount();r2++){ //For each resource
                                        ResourceUsage res2=t2.getResource(r2);
                                        if (res2.isGeometric() == res3.isGeometric()){ //If resources have same type
                                            if (res2.isGeometric()){ //If they are geometric
                                                if (((GeometricResourceUsage)res2).getMap().equals(((GeometricResourceUsage)res3).getMap())){ //If they use the same map
                                                    if (((GeometricResourceUsage)res2).getArea().isIntersecting(((GeometricResourceUsage)res3).getArea())){ //If the areas are intersecting
                                                        match=true; //We have a resource collision
                                                        break;
                                                    }
                                                }
                                            }
                                            else{ //If resources are non geometric
                                                if (res2.getType().equals(res3.getType())){ //If they are of the same type
                                                    if (resources.get(res2.getType())<res2.getCount()){ //If insufficient amounts are available for executing the removed task
                                                        match=true; //We have a resource collision
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (match)break; //If this task was already confirmed to have a resource collision, we do not need to check further
                                }
                            }
                            if (match){ //If we have a resource collision
                                //if task was not executed in the meantime
                                for (Task t:newlyExecutedTasks){
                                    if (t.getName().equals(t2.getName())){
                                        match=false;
                                        break;
                                    }
                                }
                                if (match){
                                    match=false;                               //Check whether t2 would be executable...
                                    for (int x=0;x<t2.getResourceCount();x++){ //given t3 wouldn't have been executed.
                                        ResourceUsage ru2=t2.getResource(x);
                                        if (!ru2.isGeometric()){               //Normal resources
                                            int rc2=resources.get(ru2.getType());
                                            if (rc2<ru2.getCount()){
                                                for (int y=0;y<t3.getResourceCount();y++){
                                                    ResourceUsage ru3=t3.getResource(y);
                                                    if (ru3.getType().equals(ru2.getType())){
                                                        if (rc2+ru3.getCount()>=ru2.getCount()){
                                                            match=true;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else{                                  //Geometric resources
                                            for (int y=0;y<t3.getResourceCount();y++){
                                                ResourceUsage ru3=t3.getResource(y);
                                                if (ru3.isGeometric()){ //todo check for other blocking tasks
                                                                        //if area for t2 is blocked by another task than t3...
                                                                        //the swap would be futile.
                                                    if (((GeometricResourceUsage)ru3).getMap().equals(((GeometricResourceUsage)ru2).getMap())){
                                                        if (((GeometricResourceUsage)ru3).getArea().isIntersecting(((GeometricResourceUsage)ru2).getArea())){
                                                            match=true;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (match)break;
                                    }
                                }
                                if (match){
                                    swaps.add(new TaskSwap(t3.getName(),t2.getName(),clock)); //We have a new swap
                                    if (output)
                                        System.out.println("[ "+clock+" ] SWAP: "+t3.getName()+" <> "+t2.getName());
                                }
                            }
                        }
                    }
                }
            }
        }


        if (resourceGraph!=null){
            double[] vals=new double[resources.size()];
            int x=0;
            for (String s:resources.keySet()){
                vals[x++]=resources.get(s);
            }
            resourceGraphData.addData(vals);
        }

        if (collectResourceUtilization){
            long[] vals=new long[resources.size()+1];
            int x=0;
            vals[x++]=clock;
            for (String s:resources.keySet()){
                vals[resourceIDs.get(s)+1]=resources.get(s);
            }
            resourceUtilization.add(vals);
        }

        if (collectCostData){
            costGraph.add(new double[]{clock,costs});
        }

        //We go to the next time step
        incrementClock();

        //The step did do something if either a task was started or a task was finished.
        res=finishTasks()||res;

        //Return whether step did do something.
        return res;
    }

    public void simulate(){

        simCounter++;

        //Reset simulation
        reset();

        //Measure start time
        long time=System.currentTimeMillis();

        //Initialize step counter
        int step=0;

        //While steps do something, we loop
        while(simulationStep())
            step++; //And count steps


        lastSimTime=System.currentTimeMillis()-time;
        lastFails=todo.size();
        //If there are tasks that could not be executed, we list them
        if (!todo.isEmpty()){
            System.out.println("----------------------------------------------------");
            System.out.println("The following tasks were not executed");
            System.out.println("----------------------------------------------------");

            for (Task t:todo){
                System.out.print("\t"+t.getName()+" Depends on");
                for (int x=0;x<t.getDependencyCount();x++){
                    System.out.print(t.getDependency(x).getTask()+" <"+t.getDependency(x).getType()+"> ");
                }
                System.out.println();
            }
            System.out.println();
        }


        if (output){
            //Print summary of simulation
            System.out.println("----------------------------------------------------");
            System.out.println("Simulation Summary");
            System.out.println("----------------------------------------------------");
            System.out.println("\tCPU TIME:              \t"+(System.currentTimeMillis()-time)+"ms");
            System.out.println("\tSIM TIME:              \t"+clock);
            System.out.println("\tSIM STEPS:             \t"+step);
            System.out.println("\tTASK COUNT:            \t"+(executed.size()+todo.size()));
            System.out.println("\tUNFINISHED TASK COUNT: \t"+todo.size());
            if (collectSwaps)
                System.out.println("\tPOSSIBLE SWAPS:        \t"+swaps.size());
            System.out.println("----------------------------------------------------");
            System.out.println();
        }

        /*for (TaskSwap swap:swaps){
            if (swap.isAntiDelay()){
                System.err.println(">>>!!!! ANTIDELAY "+swap.getTaskA());
            }
        } */

        if (ganttChart!=null)
            ganttChart.repaint();
        if (resourceGraph!=null)
            resourceGraph.repaint();
    }

    public long getClock(){
        return clock;
    }

    public long getLastSimTime() {
        return lastSimTime;
    }

    public int getLastFails() {
        return lastFails;
    }

    public LinkedList<TaskSwap> getSwaps() {
        return swaps;
    }

    public ArrayList<long[]> getResourceUtilization() {
        return resourceUtilization;
    }

    public ArrayList<double[]> getCostData() {
        return costGraph;
    }

    public HashMap<String,Long> getExecutionTimes(){
        HashMap<String,Long> res=new HashMap<String, Long>();
        for (String s:executed.keySet()){
            res.put(s,executed.get(s).getValue());
        }
        return res;
    }

    public Scenario getPrioritizedScenario(){
        ScenarioGenerator scenarioGenerator=new ScenarioGenerator("Opt");
        for (Task t:scenario.getTasks()){
            scenarioGenerator.addTask(t.prioritize(clock-executed.get(t.getName()).getValue()));
            Integer delay=scenario.getDelay(t.getName());
            if (delay!=null && delay>0){
                scenarioGenerator.addDelay(t.getName(),delay);
            }
        }
        for (Resource r:scenario.getResources())
            scenarioGenerator.addResource(r);
        for (String m:scenario.getMaps())
            scenarioGenerator.addMap(m);
        if (appliedSwaps!=null){
            for (TaskSwap swap:appliedSwaps){
                if (swap.isDelay()){
                    if (!swap.isAntiDelay()){
                        scenarioGenerator.addDelay(swap.getTaskA(), 1);
                        //System.err.println("Adding delay to scenario "+swap.getTaskA());
                    }
                }
            }
            for (TaskSwap swap:appliedSwaps){
                if (swap.isAntiDelay())
                    scenarioGenerator.addAntiDelay(swap.getTaskA(), 1);
            }

        }


        return scenarioGenerator.generateScenario();
    }


    public static long getSimCounter(){
        return simCounter;
    }

    public static void resetSimCounter(){
        simCounter=0;
    }
}

