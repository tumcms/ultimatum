/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;


import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;


public class MapPanel extends JPanel {
    private JTable table;
    private TableModel tableModel;
    private ArrayList<String> maps;
    private GUIListener guiListener;

    public MapPanel(GUIListener listener){
        this.guiListener=listener;
        maps=new ArrayList<String>();
        tableModel=new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return maps.size()+1;
            }

            @Override
            public int getColumnCount() {
                return 1;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                if (rowIndex>=maps.size())
                    return "";
                return maps.get(rowIndex);
            }

            @Override
            public String getColumnName(int column) {
                return "Mapname";
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return true;
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                if (rowIndex>=maps.size())
                    maps.add(rowIndex,(String)aValue);
                else
                    maps.set(rowIndex, (String) aValue);
                guiListener.update();
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return String.class;
            }

            @Override
            public int findColumn(String columnName) {
                return 0;
            }
        };
        table=new JTable(tableModel);
        JScrollPane pane=new JScrollPane(table,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setLayout(new BorderLayout());
        add(pane,BorderLayout.CENTER);

        JButton button=new JButton("Delete Map");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int row=table.getSelectedRow();
                if (row<maps.size()){
                    maps.remove(row);
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            table.updateUI();
                        }
                    });
                    guiListener.update();
                }
            }
        });

        JPanel buttonpanel=new JPanel(new FlowLayout());
        buttonpanel.add(button);
        add(buttonpanel,BorderLayout.SOUTH);
        add(new JLabel("Maps:"),BorderLayout.NORTH);
    }

    public String[] getMaps(){
        return maps.toArray(new String[0]);
    }

    public void setMaps(String[] m){
        maps.clear();
        for (String s:m)
            maps.add(s);
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                table.updateUI();
            }
        });

    }


}
