/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import Geometry.RectangularArea;
import Scenario.GeometricResourceUsage;
import Scenario.Task;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

public class TaskListPanel extends JPanel {
    private GUIListener listener;
    private JTable table;
    private ArrayList<Task> tasks;
    private AbstractTableModel tableModel;
    private int sortColumn;
    private String selectedTask;

    public TaskListPanel(GUIListener listener) {
        super(new GridLayout(1,1));
        this.listener = listener;
        this.sortColumn=2;
        this.selectedTask=null;
        this.tasks=new ArrayList<Task>();
        tableModel=new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return tasks.size();
            }

            @Override
            public int getColumnCount() {
                return 7;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                if (rowIndex>=tasks.size())
                    return "";
                Task task=tasks.get(rowIndex);
                switch (columnIndex){
                    case 0:
                        return task.getColor();
                    case 1:
                        return task.getName();
                    case 2:
                        return task.getDuration();
                    case 3:
                        return task.getPriority();
                    case 4:
                        return task.getCost();
                    case 5:
                        return task.getDependencyCount();
                    case 6:
                        return task.getResourceCount();
                    default:
                        return "";
                }
            }

            @Override
            public String getColumnName(int column) {
                String c;

                switch (column){
                    case 1:
                        c="Name";
                        break;
                    case 2:
                        c="Duration";
                        break;
                    case 3:
                        c="Priority";
                        break;
                    case 4:
                        c="Cost";
                        break;
                    case 5:
                        c="Dependencies";
                        break;
                    case 6:
                        c="Resources";
                        break;
                    default:
                        c="";
                    break;
                }
                if (column==sortColumn-1)
                    c+=" /\\";
                else if (column==-sortColumn-1)
                    c+=" \\/";
                return c.trim();
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {}

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex){
                    case 0:
                        return Color.class;
                    case 1:
                        return String.class;
                    case 2:
                        return Long.class;
                    case 3:
                    case 4:
                        return Double.class;
                    case 5:
                    case 6:
                        return Integer.class;
                    default:
                        return String.class;
                }
            }

            @Override
            public int findColumn(String columnName) {
                if(columnName.startsWith("Name"))
                    return 1;
                if(columnName.startsWith("Duration"))
                    return 2;
                if(columnName.startsWith("Priority"))
                    return 3;
                if(columnName.startsWith("Cost"))
                    return 4;
                if(columnName.startsWith("Dependencies"))
                    return 5;
                if(columnName.startsWith("Resources"))
                    return 6;

                return 0;
            }
        };
        table=new JTable(tableModel);

        table.getColumnModel().getColumn(0).setCellRenderer(new TableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                JLabel res = new JLabel();
                res.setOpaque(true);
                if (row < tasks.size())
                    res.setBackground(tasks.get(row).getColor());
                return res;
            }
        });

        table.getColumnModel().getColumn(0).setPreferredWidth(20);
        table.getColumnModel().getColumn(0).setMaxWidth(20);
        table.getColumnModel().getColumn(0).setWidth(20);
        table.getColumnModel().getColumn(1).setPreferredWidth(200);
        table.getColumnModel().getColumn(1).setWidth(200);



        table.getTableHeader().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int index = table.convertColumnIndexToModel(table.columnAtPoint(e.getPoint()))+1;
                int oldcol=sortColumn;
                if (index==Math.abs(sortColumn))
                    sortColumn*=-1;
                else
                    sortColumn=index;


                table.getColumnModel().getColumn(Math.abs(oldcol)-1).setHeaderValue(table.getModel().getColumnName(Math.abs(oldcol)-1));
                table.getColumnModel().getColumn(Math.abs(sortColumn)-1).setHeaderValue(table.getModel().getColumnName(Math.abs(sortColumn)-1));
                sort();
                //table.getTableHeader().repaint();


            }
        });


        table.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                selectedTask=tasks.get(table.getSelectionModel().getMinSelectionIndex()).getName();
                TaskListPanel.this.listener.selectTask(selectedTask,"TaskList");

            }
        });

        JScrollPane scrollPane=new JScrollPane(table,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        add(scrollPane);
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                table.updateUI();
            }
        });
    }

    public void setSelected(String s){
        if (selectedTask!=null && selectedTask.equals(s))
            return;

        for (int x=0;x<tasks.size();x++){
            if (tasks.get(x).getName().equals(s)){
                final int index=x;
                selectedTask=s;
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        if (table.getSelectionModel().getMinSelectionIndex()!=index){
                            table.getSelectionModel().setSelectionInterval(index,index);
                            Rectangle rect =
                                    table.getCellRect(index, 0, true);
                            table.scrollRectToVisible(rect);

                            SwingUtilities.invokeLater(new Runnable() {
                                public void run() {
                                    table.updateUI();
                                }
                            });
                        }
                    }
                });

                break;
            }
        }
    }

    public void update(){
        Task[] newTasks=listener.getTasks();
        tasks.clear();
        for (Task t:newTasks)
            tasks.add(t);
        sort();

    }

    public void sort(){
        Collections.sort(tasks, new Comparator<Task>() {

            @Override
            public int compare(Task o1, Task o2) {
                switch (Math.abs(sortColumn)){
                    case 1:
                        return (int)Math.signum(Math.signum(sortColumn)*((Math.pow(255*255*o1.getColor().getRed(),2)+Math.pow(255*o1.getColor().getGreen(),2)+Math.pow(o1.getColor().getBlue(),2))
                                -(Math.pow(255*255*o2.getColor().getRed(),2)+Math.pow(255*o2.getColor().getGreen(),2)+Math.pow(o2.getColor().getBlue(),2))));
                    case 3:
                        return (int)Math.signum(Math.signum(sortColumn)*(o1.getDuration()-o2.getDuration()));
                    case 4:
                        return (int)Math.signum(Math.signum(sortColumn)*(o1.getPriority()-o2.getPriority()));
                    case 5:
                        return (int)Math.signum(Math.signum(sortColumn)*(o1.getCost()-o2.getCost()));
                    case 6:
                        return (int)Math.signum(Math.signum(sortColumn)*(o1.getDependencyCount()-o2.getDependencyCount()));
                    case 7:
                        return (int)Math.signum(Math.signum(sortColumn)*(o1.getResourceCount()-o2.getResourceCount()));
                }

                String[] s1 = splitNumeric(o1.getName());
                String[] s2 = splitNumeric(o2.getName());
                for (int x = 0; x < s1.length && x < s2.length; x++) {
                    if (!s1[x].equals(s2[x])) {
                        if (s1[x].charAt(0) == 'N' && s2[x].charAt(0) == 'N') {
                            long l1 = Long.parseLong(s1[x].substring(1));
                            long l2 = Long.parseLong(s2[x].substring(1));
                            return (int) Math.signum(Math.signum(sortColumn)*(l1 - l2));
                        }
                        break;
                    }
                }
                return (int) Math.signum(Math.signum(sortColumn)*o1.getName().compareTo(o2.getName()));
            }
        });

        for (int x=0;x<tasks.size();x++){
            if (tasks.get(x).getName().equals(selectedTask)){
                table.getSelectionModel().setSelectionInterval(x,x);
                Rectangle rect =
                        table.getCellRect(x, 0, true);
                table.scrollRectToVisible(rect);
            }
        }
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {

                table.updateUI();
            }
        });
    }

    private String[] splitNumeric(String s){
        final String numbers="0123456789";
        LinkedList<String> out=new LinkedList<String>();
        int state=-1;
        for (int x=0;x<s.length();x++){
            if (numbers.contains(s.charAt(x)+"")){
                if (state==1)
                    out.set(out.size()-1,out.getLast()+s.charAt(x));
                else{
                    state=1;
                    out.add("N"+s.charAt(x));
                }
            }
            else{
                if (state==0)
                    out.set(out.size()-1,out.getLast()+s.charAt(x));
                else{
                    state=0;
                    out.add("S"+s.charAt(x)+"");
                }
            }
        }
        return out.toArray(new String[0]);
    }

}
