/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.*;
import java.util.List;

public class BufferChart extends JPanel{

    private List<GanttElement> elements;
    private BufferedImage buffer;
    private long maxTime;
    private GanttPanel graph;
    private JScrollPane pane;
    private List<Long> ticks;
    private boolean update;


    public BufferChart(){
        super(new GridLayout(1,1));


        reset();

        this.graph=new GanttPanel();

        this.pane=new JScrollPane(graph,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        this.pane.getVerticalScrollBar().setUnitIncrement(16);
        this.pane.getHorizontalScrollBar().setUnitIncrement(16);

        super.add(pane,BorderLayout.CENTER);

        graph.addMouseListener(new MouseList());

    }

    class MouseList extends MouseAdapter{
        @Override
        public void mouseClicked(MouseEvent e) {
            requestFocus();
            if (e.getButton()!=MouseEvent.BUTTON3||e.getClickCount()<2){
                return;
            }

            JFileChooser chooser=new JFileChooser(".");
            chooser.setFileFilter(new FileFilter() {
                @Override
                public boolean accept(File f) {
                    return f.isDirectory() || f.getName().toLowerCase().endsWith(".png");
                }

                @Override
                public String getDescription() {
                    return "PNG File";  //To change body of implemented methods use File | Settings | File Templates.
                }
            });
            int res=chooser.showSaveDialog(graph);
            if (res==JFileChooser.APPROVE_OPTION){
                try{
                    ImageIO.write(buffer,"png",chooser.getSelectedFile());
                }catch(Exception ex){}

            }
        }
    }

    public void reset(){
        this.elements=new ArrayList<GanttElement>();
        this.ticks=new ArrayList<Long>();
        this.maxTime=0;
        this.update=true;

    }

    public void addData(String name, long start, long start2, long end, long end2, Color color){
        //System.out.println(name+" "+start+" "+start2+" "+end+" "+end2);
        //System.out.println(name+" x "+start.toString()+" - "+end.toString());
        elements.add(new GanttElement(name,start,start2, end, end2,color));
        if (end>maxTime){
            maxTime=end;
        }
        Collections.sort(elements, new Comparator<GanttElement>(){
            @Override
            public int compare(GanttElement o1, GanttElement o2) {
                int res=(int)Math.signum(o1.getStart1()-o2.getStart1());
                if (res==0){
                    return (int)Math.signum(o1.getEnd1()-o2.getEnd1());
                }
                return res;
            }
        });
        /*if (!ticks.contains(new Long(start.toLong()))){
            ticks.add(start.toLong());
        }*/
        if (!ticks.contains(new Long(end))){
            ticks.add(end);
        }
        update=true;
    }

    public BufferedImage getImage(int width, int height) {
        return buffer;
    }

    private class GanttPanel extends JPanel{

        public void paintComponent(Graphics g2){
            try{
                if (buffer==null || buffer.getWidth()!=getWidth() || buffer.getHeight()!=getHeight()){
                    buffer=new BufferedImage(getWidth(),getHeight(),BufferedImage.TYPE_INT_RGB);
                    update=true;
                }
                if (update){
                    Graphics2D g=(Graphics2D)buffer.getGraphics();
                    g.setColor(Color.WHITE);
                    g.fillRect(0,0,getWidth(),getHeight());
                    //g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);

                    double elementHeight=(getHeight()-50)/(double)elements.size();


                    double scale=1;

                    int counter=0;
                    FontMetrics fM=g.getFontMetrics();

                    int maxWidth=0;
                    for (GanttElement element:elements){
                        maxWidth=Math.max(maxWidth,(int)Math.ceil(fM.getStringBounds(element.getName(),g).getWidth()));
                    }
                    if (maxTime>0)
                        scale=(getWidth()-20-maxWidth)/(double)maxTime;

                    long minTime=Long.MAX_VALUE;
                    for (GanttElement element:elements){
                        long l=element.getEnd1()-element.getStart1();
                        if (l>0)
                            minTime=Math.min(minTime,l);
                    }

                    if (elementHeight!=15||((getWidth()-maxWidth-20)/(double)maxTime)*minTime!=10){//||getWidth()<3*elements.size()){
                        int minHeight=15*elements.size()+50;
                        int minWidth=(int)Math.ceil(maxWidth+20+(10.0/minTime)*maxTime);
                        minWidth=Math.min(minWidth,2048);

                        if ((getPreferredSize().getHeight()!=minHeight && getMinimumSize().getHeight()!=minHeight) || (getPreferredSize().getWidth()!=minWidth && getMinimumSize().getWidth()!=minWidth)){
                            setPreferredSize(new Dimension(minWidth,minHeight));
                            setMinimumSize(new Dimension(minWidth,minHeight));
                            revalidate();
                            repaint();
                            //return;
                        }
                    }

                    g.setColor(new Color(200,200,200));
                    g.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER, 2.0f, new float[]{2.0f}, 0.0f));

                    for (Long tick:ticks){
                        int xp=(int)Math.round(maxWidth+ 10 + scale * tick);
                        g.drawLine(xp,0,xp,getHeight());
                    }

                    for (GanttElement element:elements){
                        int xp=(int)Math.round(maxWidth+ 10 + scale * element.getStart1());
                        int w=(int)Math.round(scale*(element.getEnd1()-element.getStart1()));
                        int yp=(int)Math.round(25+counter*elementHeight);
                        int h=(int)Math.round(elementHeight-4);
                        int xpl=(int)Math.round(maxWidth+ 10 + scale * Math.min(element.getStart1(),element.getStart2()));
                        int xpr=(int)Math.round(maxWidth+ 10 + scale * Math.max(element.getEnd1(),element.getEnd2()));

                        g.setStroke(new BasicStroke(1));
                        g.setColor(Color.BLACK);
                        g.drawLine(xpl,yp+h/2,xpr,yp+h/2);
                        g.drawLine(xpl,yp,xpl,yp+h);
                        g.drawLine(xpr,yp,xpr,yp+h);

                        g.setColor(element.getColor());
                        g.fillRect(xp,yp,w,h);
                        if (counter%2==0)
                            g.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER, 5.0f, new float[]{5.0f}, 0.0f));
                        else
                            g.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER, 5.0f, new float[]{5.0f,2.0f,1.0f,3.0f}, 0.0f));
                        int textWidth=(int)fM.getStringBounds(element.getName(),g).getWidth();
                        g.drawLine(6+textWidth,yp+h/2,xp,yp+h/2);
                        g.setStroke(new BasicStroke(1));

                        g.setColor(Color.BLACK);
                        if (element.getStart1()==element.getStart2())
                            g.setStroke(new BasicStroke(3));
                        g.drawRect(xp, yp, w, h);
                        g.setStroke(new BasicStroke(1));
                        g.drawString(element.getName(),5,yp+h-2);
                        counter++;
                    }
                    update=false;
                }



                g2.drawImage(buffer,0,0,null);
            }
            catch (ConcurrentModificationException ex){}
        }

    }


    private class GanttElement{
        private long start,end, start2, end2;
        private String name;
        private Color color;

        private GanttElement(String name,long start, long end) {
            this.start = start;
            this.end = end;
            this.name=name;
            this.color=Color.RED;
        }

        private GanttElement(String name,long start, long start2, long end, long end2, Color color) {
            this.start = start;
            this.end = end;
            this.start2=start2;
            this.end2=end2;
            this.name=name;
            if (color==null)
                this.color=Color.RED;
            else
                this.color=color;
        }

        public long getStart1() {
            return start;
        }

        public long getEnd1() {
            return end;
        }

        private long getStart2() {
            return start2;
        }

        private long getEnd2() {
            return end2;
        }

        public String getName() {
            return name;
        }

        public Color getColor() {
            return color;
        }
    }
}
