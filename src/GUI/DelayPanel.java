/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import Scenario.Task;
import Common.Pair;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;


public class DelayPanel extends JPanel {
    private JTable table;
    private TableModel tableModel;
    private ArrayList<Pair<String, Integer>> delays;
    private GUIListener guiListener;
    private JComboBox<String> comboBox;
    private Task[] avaiableTasks;

    public DelayPanel(GUIListener guiListener){
        this.guiListener = guiListener;
        this.avaiableTasks=guiListener.getTasks();
        delays=new ArrayList<Pair<String, Integer>>();

        tableModel=new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return delays.size()+1;
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {

                if (rowIndex>=delays.size())
                    return "";
                if (columnIndex==0)
                    return delays.get(rowIndex).getKey();
                else
                    return delays.get(rowIndex).getValue();
            }

            @Override
            public String getColumnName(int column) {
                if (column==0)
                    return "Task";
                else
                    return "Delay";
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return true;
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                //System.out.println("VAL "+aValue+ " "+rowIndex+" "+columnIndex+" "+delays.size());
                if (rowIndex>=delays.size()){
                    if (columnIndex==0)
                        delays.add(rowIndex, new Pair<String, Integer>((String) aValue, 1));
                    //else
                    //    delays.add(rowIndex, new Pair<String, Integer>(delays.get(rowIndex).getKey(), (Integer) aValue));
                }
                else{
                    //System.out.println("FUN");
                    if (columnIndex==0)
                        delays.set(rowIndex, new Pair<String, Integer>((String) aValue, delays.get(rowIndex).getValue()));
                    else
                        delays.set(rowIndex, new Pair<String, Integer>(delays.get(rowIndex).getKey(), (Integer) aValue));
                }

                /*for (Pair<String, Integer> p:delays){
                    System.out.println(p.getKey()+" "+p.getValue());
                } */

                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        table.updateUI();
                    }
                });
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return columnIndex==0 ? String.class : Integer.class;
            }

            @Override
            public int findColumn(String columnName) {
                return columnName.equals("Task") ? 0 : 1;
            }
        };
        table=new JTable(tableModel);
        comboBox=new JComboBox<String>(new ComboBoxModel<String>() {
            private int selected=0;
            @Override
            public void setSelectedItem(Object anItem) {
                for (int x=0;x<avaiableTasks.length;x++){
                    if (avaiableTasks[x].getName().equals(anItem))
                        selected=x;
                }
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        table.updateUI();
                    }
                });

            }

            @Override
            public Object getSelectedItem() {
                return selected<avaiableTasks.length ? avaiableTasks[selected].getName() : null;
            }

            @Override
            public int getSize() {
                return avaiableTasks.length;
            }

            @Override
            public String getElementAt(int index) {
                return index<avaiableTasks.length ? avaiableTasks[index].getName() : null;
            }

            @Override
            public void addListDataListener(ListDataListener l) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void removeListDataListener(ListDataListener l) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        table.getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(comboBox));

        JScrollPane pane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setLayout(new BorderLayout());
        add(pane,BorderLayout.CENTER);

        JButton button=new JButton("Delete Delay");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int row=table.getSelectedRow();
                if (row<delays.size()){
                    delays.remove(row);
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            table.updateUI();
                        }
                    });

                }
            }
        });

        JPanel buttonpanel=new JPanel(new FlowLayout());
        buttonpanel.add(button);
        add(buttonpanel,BorderLayout.SOUTH);
        add(new JLabel("Delays:"),BorderLayout.NORTH);

    }

    public HashMap<String, Integer> getDelays(){
        HashMap<String, Integer> res=new HashMap<String, Integer>();
        for (Pair<String, Integer> delay:delays){
            if (delay.getValue()>0){
                if (res.containsKey(delay.getKey())){
                    res.put(delay.getKey(),res.get(delay.getKey())+delay.getValue());
                }
                else{
                    res.put(delay.getKey(),delay.getValue());
                }
            }
        }
        return res;
    }


    public void setDelays(Pair<String, Integer>[] d){
        avaiableTasks=guiListener.getTasks();
        delays.clear();
        for (Pair<String, Integer> delay:d)
            delays.add(delay);
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                table.updateUI();
            }
        });

    }
}
