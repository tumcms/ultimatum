/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import Main.Version;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;

public class AboutFrame extends JFrame {
    public AboutFrame(){
        super(Version.ULTIMATUM_VERSION+" - About");
        JTextArea description=new JTextArea(" "+Version.ULTIMATUM_VERSION+" - Intelligent Scheduling System\n\n" +
                " Written 2013 by Maximilian Bügler <max.buegler@tum.de>\n\n" +
                " Chair of Computational Modeling and Simulation\n"+
                " Faculty of Civil Engineering and Surveying\n"+
                " Technische Universität München\n"+
                " Germany\n\n"+
                " http://www.cms.bgu.tum.de/\n\n\n"+
                " Using the following libraries:\n\n"+
                "  - MPXJ ( http://mpxj.sourceforge.net/ )\n"+
                "  \tMS Project import\n"+
                "  - EpsGraphics2D by Paul James Mutton ( http://www.jibble.org/ )\n"+
                "  \tEPS output");

        description.setEditable(false);
        description.setBorder(null);
        description.setLineWrap(true);

        description.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    Desktop.getDesktop().browse(new URL("http://www.cms.bgu.tum.de").toURI());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });


        setLayout(new BorderLayout());
        add(new LogoComponent(),BorderLayout.NORTH);
        add(description,BorderLayout.CENTER);
        setSize(450,450);
    }
}
