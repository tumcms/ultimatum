/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import Scenario.Resource;
import Scenario.ResourceUsage;
import Scenario.Task;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ResourceUsagePanel extends JPanel {
    private JTable table;
    private TableModel tableModel;
    private ArrayList<ResourceUsage> resources;
    private GUIListener guiListener;
    private JComboBox comboBox;
    private Resource[] availableResources;
    private ActionListener changeListener;

    public ResourceUsagePanel(GUIListener listener){
        this.guiListener = listener;
        availableResources=guiListener.getResources();
        resources=new ArrayList<ResourceUsage>();
        tableModel=new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return resources.size()+1;
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                if (rowIndex>=resources.size())
                    return columnIndex==0 ? "" : 0;
                return columnIndex==0 ? resources.get(rowIndex).getType() : resources.get(rowIndex).getCount();
            }

            @Override
            public String getColumnName(int column) {
                return column==0 ? "Resource" : "Count";
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                if (rowIndex<resources.size() && resources.get(rowIndex).isGeometric())
                    return false;
                return columnIndex==0 || rowIndex<resources.size();
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                if (rowIndex>=resources.size() && columnIndex==0)
                    resources.add(rowIndex,new ResourceUsage((String)aValue,1));
                else{
                    if (resources.get(rowIndex).isGeometric())
                        return;
                    String name=resources.get(rowIndex).getType();
                    int count=resources.get(rowIndex).getCount();
                    if (columnIndex==0)name=(String)aValue;
                    else count=(Integer)aValue;
                    resources.set(rowIndex,new ResourceUsage(name,count));
                }
                //guiListener.update();
                if (changeListener!=null)
                    changeListener.actionPerformed(null);
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return columnIndex==0 ? String.class : Integer.class;
            }

            @Override
            public int findColumn(String columnName) {
                return columnName.equals("Resource") ? 0 : 1;
            }
        };
        table=new JTable(tableModel);
        comboBox=new JComboBox<String>(new ComboBoxModel<String>() {
            private int selected=0;
            @Override
            public void setSelectedItem(Object anItem) {
                for (int x=0;x<availableResources.length;x++){
                    if (availableResources[x].getType().equals(anItem))
                        selected=x;
                }
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        table.updateUI();
                    }
                });

            }

            @Override
            public Object getSelectedItem() {
                return selected<availableResources.length ? availableResources[selected].getType() : null;
            }

            @Override
            public int getSize() {
                return availableResources.length;
            }

            @Override
            public String getElementAt(int index) {
                return index<availableResources.length ? availableResources[index].getType() : null;
            }

            @Override
            public void addListDataListener(ListDataListener l) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void removeListDataListener(ListDataListener l) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        table.getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(comboBox));

        JScrollPane pane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setLayout(new BorderLayout());
        add(pane,BorderLayout.CENTER);

        JButton button=new JButton("Delete Resource");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int row=table.getSelectedRow();
                if (row<resources.size()){
                    resources.remove(row);
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            table.updateUI();
                        }
                    });

                }
            }
        });

        JPanel buttonpanel=new JPanel(new FlowLayout());
        buttonpanel.add(button);
        add(buttonpanel,BorderLayout.SOUTH);
        add(new JLabel("Required resources:"),BorderLayout.NORTH);

    }

    public void setChangeListener(ActionListener l){
        this.changeListener=l;
    }

    public void update(){
        availableResources=guiListener.getResources();
    }

    public ResourceUsage[] getResources(){
        return resources.toArray(new ResourceUsage[0]);
    }

    public void setResources(ResourceUsage[] res){
        resources.clear();
        for (ResourceUsage r:res)
            resources.add(r);
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                table.updateUI();
            }
        });

    }


}
