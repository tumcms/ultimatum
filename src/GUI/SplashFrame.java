/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;


public class SplashFrame extends JDialog {
    private BufferedImage img;
    public SplashFrame(JFrame parent){
        super(parent, "ultimaTUM Splashscreen");

        try{
            img=ImageIO.read(SplashFrame.class.getResource("splash.png"));
        }
        catch(Exception e){e.printStackTrace();}
        if (img!=null){
            setSize(556,496);
            setResizable(false);
            setUndecorated(true);
            setAlwaysOnTop(true);
            int x=(int)(Toolkit.getDefaultToolkit().getScreenSize().getWidth()-556)/2;
            int y=(int)(Toolkit.getDefaultToolkit().getScreenSize().getHeight()-496)/2;
            setLocation(x,y);
            setVisible(true);

            addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    setVisible(false);
                }
            });
            new Thread(){
                public void run(){
                    try{
                        Thread.sleep(4000);
                    }
                    catch(Exception e){}
                    setVisible(false);
                    dispose();
                }
            }.start();
        }
    }
    public void paint(Graphics g){
        if (img!=null)g.drawImage(img,0,0,null);
    }
}
