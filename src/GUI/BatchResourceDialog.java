/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import Scenario.Resource;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class BatchResourceDialog extends JFrame {
    private JTextField taskFilter, count;
    private JComboBox<String> resources;
    private GUIListener listener;
    private boolean ok;


    public BatchResourceDialog(GUIListener listener){
        super("Batch resource assignment");
        this.listener=listener;
        setLayout(new GridLayout(3,1));
        JPanel topPanel=new JPanel(new FlowLayout());
        topPanel.add(new JLabel("Task filter:"));
        taskFilter=new JTextField(10);
        topPanel.add(taskFilter);

        add(topPanel);


        JPanel bottomPanel=new JPanel(new FlowLayout());
        resources=new JComboBox<String>();
        bottomPanel.add(new JLabel("Resource to add:"));
        bottomPanel.add(resources);
        bottomPanel.add(new JLabel("x"));
        count=new JTextField("1",3);
        bottomPanel.add(count);

        add(bottomPanel);

        this.ok=false;

        JPanel buttonPanel=new JPanel(new GridLayout(1,2));
        JButton okButton=new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ok=true;
                setVisible(false);
                dispose();
            }
        });
        JButton cancelButton=new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ok=false;
                setVisible(false);
                dispose();
            }
        });

        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        add(buttonPanel);
    }

    public boolean showDialog(){
        this.ok=false;
        this.taskFilter.setText("");
        this.resources.setModel(new ComboBoxModel<String>() {
            int selected=0;
            Resource[] resources=listener.getResources();
            @Override
            public void setSelectedItem(Object anItem) {
                for (int x=0;x<resources.length;x++){
                    if (resources[x].getType().equals(anItem))
                        selected=x;
                }
            }

            @Override
            public Object getSelectedItem() {
                return selected<resources.length ? resources[selected].getType() : null;
            }

            @Override
            public int getSize() {
                return resources.length;
            }

            @Override
            public String getElementAt(int index) {
                return index<resources.length ? resources[index].getType() : null;
            }

            @Override
            public void addListDataListener(ListDataListener l) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void removeListDataListener(ListDataListener l) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        setSize(400,150);
        setResizable(false);
        setVisible(true);
        pack();
        revalidate();
        while(isVisible()){
            try{Thread.sleep(10);}catch (Exception ex){}
        }
        return ok;
    }

    public String getFilter(){
        return taskFilter.getText();
    }

    public String getResource(){
        return (String)resources.getSelectedItem();
    }

    public int getCount() {
        return Integer.parseInt(count.getText());
    }
}
