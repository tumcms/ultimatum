/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import Scenario.Resource;
import Scenario.Task;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;


public class MapUsagePanel extends JPanel {
    private JTable table;
    private TableModel tableModel;
    private ArrayList<String> maps;
    private GUIListener guiListener;
    private JComboBox comboBox;
    private String[] avaiableMaps;

    public MapUsagePanel(GUIListener guiListener){
        this.guiListener = guiListener;
        avaiableMaps=guiListener.getMaps();
        maps=new ArrayList<String>();
        tableModel=new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return maps.size()+1;
            }

            @Override
            public int getColumnCount() {
                return 1;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                if (rowIndex>=maps.size())
                    return "";
                return maps.get(rowIndex);
            }

            @Override
            public String getColumnName(int column) {
                return "Map";
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return true;
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                if (rowIndex>=maps.size())
                    maps.add(rowIndex,(String)aValue);
                else{
                    maps.set(rowIndex,(String)aValue);
                }
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return String.class;
            }

            @Override
            public int findColumn(String columnName) {
                return 0;
            }
        };
        table=new JTable(tableModel);
        comboBox=new JComboBox<String>(new ComboBoxModel<String>() {
            private int selected=0;
            @Override
            public void setSelectedItem(Object anItem) {
                for (int x=0;x<avaiableMaps.length;x++){
                    if (avaiableMaps[x].equals(anItem))
                        selected=x;
                }
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        table.updateUI();
                    }
                });

            }

            @Override
            public Object getSelectedItem() {
                return selected<avaiableMaps.length ? avaiableMaps[selected] : null;
            }

            @Override
            public int getSize() {
                return avaiableMaps.length;
            }

            @Override
            public String getElementAt(int index) {
                return index<avaiableMaps.length ? avaiableMaps[index] : null;
            }

            @Override
            public void addListDataListener(ListDataListener l) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void removeListDataListener(ListDataListener l) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        table.getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(comboBox));

        JScrollPane pane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setLayout(new BorderLayout());
        add(pane,BorderLayout.CENTER);

        JButton button=new JButton("Delete Map");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int row=table.getSelectedRow();
                if (row<maps.size()){
                    maps.remove(row);
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            table.updateUI();
                        }
                    });

                }
            }
        });

        JPanel buttonpanel=new JPanel(new FlowLayout());
        buttonpanel.add(button);
        add(buttonpanel,BorderLayout.SOUTH);
        add(new JLabel("Resources:"),BorderLayout.NORTH);

    }

    public Resource[] getMaps(){
        return maps.toArray(new Resource[0]);
    }


}
