/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import Common.Pair;
import Scenario.ExampleGenerator;
import Main.Version;
import Optimization.*;
import Scenario.*;
import Simulator.*;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;


public class MonolithicGUI extends JFrame {
    private ResourcePanel resourcePanel;
    private MapPanel mapPanel;
    private TaskPanel taskPanel;
    private GanttPanel ganttPanel;
    private SimInfoPanel simInfoPanel;
    private ConsolePanel consolePanel;
    private ResourceGraphPanel resourceGraphPanel;
    private CostGraphPanel costGraphPanel;
    private GeometryPanel geometryPanel;
    private DelayPanel delayPanel;
    private BufferChart bufferChart;
    private TaskListPanel taskListPanel;
    private ArrayList<Task> tasks;
    private HashMap<String, Task> taskMap;
    private HashMap<String,Long> schedule;

    private JFrame parent;
    private GUIListener listener;

    private JSplitPane topPane, rightPane, leftVerticalPane, leftBottomPane;
    private JTabbedPane mainPane;
    private JCheckBox autoUpdate;
    private AboutFrame aboutFrame;

    //private JCheckBoxMenuItem delayCheckbox;


    public MonolithicGUI(){
        super(Version.ULTIMATUM_VERSION);
        //JavaWMClassCorrector.fixIt("ultimaTUM");

        SplashFrame splashFrame=new SplashFrame(this);

        this.listener=new Listener();

        this.parent=this;

        this.tasks=new ArrayList<Task>();
        this.taskMap=new HashMap<String, Task>();

        resourcePanel=new ResourcePanel(listener);
        mapPanel=new MapPanel(listener);
        taskPanel=new TaskPanel(listener);
        geometryPanel=new GeometryPanel(listener);
        delayPanel=new DelayPanel(listener);
        simInfoPanel=new SimInfoPanel();
        resourceGraphPanel=new ResourceGraphPanel();
        costGraphPanel=new CostGraphPanel();
        bufferChart=new BufferChart();
        ganttPanel=new GanttPanel(listener);
        taskListPanel=new TaskListPanel(listener);
        aboutFrame=new AboutFrame();

        //JPanel chartPanel=new JPanel(new BorderLayout());

        mainPane=new JTabbedPane();

        mainPane.addTab("Gantt Chart", ganttPanel);
        mainPane.addTab("Task List", taskListPanel);

        //JPanel resGraphPanel=new JPanel();
        //resGraphPanel.add(new JLabel("Resource Graphs"));
        mainPane.addTab("Resource Graphs",resourceGraphPanel);

        mainPane.addTab("Cost Graph", costGraphPanel);

        mainPane.addTab("Geometry", geometryPanel);

        //mainPane.addTab("Buffer Times",bufferChart);

        consolePanel=new ConsolePanel();
        mainPane.addTab("Console", consolePanel);


        mainPane.updateUI();


        //chartPanel.add(mainPane,BorderLayout.CENTER);

        JPanel chartBottomPanel=new JPanel(new FlowLayout());
        JButton updateButton=new JButton("Update charts");
        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                /*ScenarioGenerator gen=new ScenarioGenerator("New");
                for (String s:mapPanel.getMaps()){
                    gen.addMap(s);
                }
                for (Resource r:resourcePanel.getResources()){
                    gen.addResource(r);
                }
                for (Task t:tasks){
                    gen.addTask(t);
                }*/
                taskPanel.saveEdits();
                loadScenario(new Scenario("New", tasks.toArray(new Task[0]), resourcePanel.getResources(), mapPanel.getMaps(),delayPanel.getDelays()));
            }
        });

        chartBottomPanel.add(simInfoPanel);
        chartBottomPanel.add(updateButton);

        autoUpdate=new JCheckBox("Auto-update",false);
        chartBottomPanel.add(autoUpdate);

        //chartPanel.add(chartBottomPanel,BorderLayout.SOUTH);

        JSplitPane chartPane=new JSplitPane(JSplitPane.VERTICAL_SPLIT,mainPane,chartBottomPanel);
        chartPane.setOneTouchExpandable(true);

        leftBottomPane=new JSplitPane(JSplitPane.VERTICAL_SPLIT,mapPanel,delayPanel);

        leftVerticalPane=new JSplitPane(JSplitPane.VERTICAL_SPLIT,resourcePanel,leftBottomPane);



        leftVerticalPane.setMinimumSize(new Dimension(250,leftVerticalPane.getMinimumSize().height));

        JPanel leftPanel=new JPanel(new BorderLayout());

        leftPanel.add(leftVerticalPane,BorderLayout.CENTER);


        JPanel logoPanel=new JPanel(new GridLayout(1,1));
        logoPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));//TitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), ""));
        logoPanel.add(new LogoComponent());
        leftPanel.add(logoPanel,BorderLayout.SOUTH);



        rightPane=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,chartPane,taskPanel);
        rightPane.setOneTouchExpandable(true);

        topPane=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,leftPanel,rightPane);
        topPane.setOneTouchExpandable(true);


        setLayout(new BorderLayout());
        add(topPane,BorderLayout.CENTER);



        JMenuBar menuBar=new JMenuBar();

        JMenu menu=new JMenu("File");

        JMenuItem item=new JMenuItem("New");
        menu.add(item);
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loadScenario(new ScenarioGenerator("New Scenario").generateScenario());
            }
        });
        item=new JMenuItem("Load Example");
        menu.add(item);
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loadScenario(ExampleGenerator.generate());
            }
        });
        item=new JMenuItem("Create Random Scenario...");
        menu.add(item);
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(){
                    public void run(){
                        setEnabled(false);
                        try{
                            Scenario newScenario=ExampleGenerator.generateRandomScenario();
                            if (newScenario!=null)
                                loadScenario(newScenario);
                        }
                        catch (Exception ex){
                            ex.printStackTrace();
                        }
                        setEnabled(true);
                        requestFocus();
                    }
                }.start();
            }
        });

        menu.addSeparator();
        item=new JMenuItem("Import XML...");
        menu.add(item);
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc=new JFileChooser(".");
                fc.setFileFilter(new FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        return f.isDirectory() || f.getName().toLowerCase().endsWith(".xml");
                    }

                    @Override
                    public String getDescription() {
                        return "XML File";
                    }
                });
                int ans=fc.showOpenDialog(parent);
                if (ans==JFileChooser.APPROVE_OPTION){
                    try{
                        loadScenario(XMLParser.parseXML(fc.getSelectedFile().getPath()));
                    }catch (Exception ex){ex.printStackTrace();}
                }
            }
        });

        item=new JMenuItem("Import MS-Project-XML...");
        menu.add(item);
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser(".");
                fc.setFileFilter(new FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        return f.isDirectory() || f.getName().toLowerCase().endsWith(".xml");
                    }

                    @Override
                    public String getDescription() {
                        return "XML File";
                    }
                });

                int ans = fc.showOpenDialog(parent);
                if (ans == JFileChooser.APPROVE_OPTION) {
                    try {
                        loadScenario(XMLParserMSProject.parseXML(fc.getSelectedFile().getPath()));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
        item=new JMenuItem("Import SM...");
        menu.add(item);
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc=new JFileChooser(".");
                fc.setFileFilter(new FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        return f.isDirectory() || f.getName().toLowerCase().endsWith(".sm");
                    }

                    @Override
                    public String getDescription() {
                        return "SM File";
                    }
                });
                int ans=fc.showOpenDialog(parent);
                if (ans==JFileChooser.APPROVE_OPTION){
                    try{
                        loadScenario(SMParser.parseSM(fc.getSelectedFile().getPath()));
                    }catch (Exception ex){ex.printStackTrace();}
                }
            }
        });


        menu.addSeparator();
        item=new JMenuItem("Export XML...");

        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser(".");
                fc.setFileFilter(new FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        return f.isDirectory() || f.getName().toLowerCase().endsWith(".xml");
                    }

                    @Override
                    public String getDescription() {
                        return "XML File";
                    }
                });
                int res = fc.showSaveDialog(MonolithicGUI.this);
                if (res == JFileChooser.APPROVE_OPTION) {
                    new Scenario("New", tasks.toArray(new Task[0]), resourcePanel.getResources(), mapPanel.getMaps(),delayPanel.getDelays()).createXMLFile(fc.getSelectedFile(), schedule);
                }
            }
        });


        menu.add(item);
        item=new JMenuItem("Export for GraphViz...");

        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser(".");
                fc.setFileFilter(new FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        return f.isDirectory() || f.getName().toLowerCase().endsWith(".gv");
                    }

                    @Override
                    public String getDescription() {
                        return "GraphViz File";
                    }
                });
                int res = fc.showSaveDialog(MonolithicGUI.this);
                if (res == JFileChooser.APPROVE_OPTION) {
                    try {
                        System.out.println("Saving GraphViz file to " + fc.getSelectedFile().getName());
                        new Scenario("New", tasks.toArray(new Task[0]), resourcePanel.getResources(), mapPanel.getMaps()).createGraphVizFile(fc.getSelectedFile());
                        System.out.println("Saved successfully");
                    } catch (Exception ex) {
                        System.out.println("Error saving file");
                        ex.printStackTrace();
                    }
                }
            }
        });


        menu.add(item);
        //item=new JMenuItem("Export Gantt-Chart as PNG...");
        //menu.add(item);
        menu.addSeparator();
        item=new JMenuItem("Exit");
        menu.add(item);
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        menuBar.add(menu);

        menu=new JMenu("Edit");
        item=new JMenuItem("Batch resource assignment...");
        menu.add(item);
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(){
                    public void run(){
                        BatchResourceDialog dialog=new BatchResourceDialog(listener);
                        if (dialog.showDialog()){
                            String taskFilter=dialog.getFilter();
                            String resource=dialog.getResource();
                            int count=dialog.getCount();
                            for (Task t:tasks){
                                if (t.getName().toLowerCase().contains(taskFilter.toLowerCase())){
                                    ResourceUsage[] newRes=new ResourceUsage[t.getResourceCount()+1];
                                    newRes[0]=new ResourceUsage(resource,count);
                                    for (int x=0;x<t.getResourceCount();x++)
                                        newRes[x+1]=t.getResource(x);
                                    listener.updateTask(t,new Task(
                                            t.getName(),
                                            t.getProvides(),
                                            newRes,
                                            t.cloneDependencies(),
                                            t.getDuration(),
                                            t.getPriority(),
                                            t.getCost(),
                                            t.getColor()
                                    ));
                                }
                            }
                        }
                    }
                }.start();
            }
        });
        menuBar.add(menu);


        //menu=new JMenu("Schedule");
        //item=new JMenuItem("Calculate buffer times...");
        //menu.add(item);
        //menuBar.add(menu);

        menu=new JMenu("Optimization");

        item=new JMenuItem("Duration-Priorization");
        menu.add(item);
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loadScenario(new Scenario("New", tasks.toArray(new Task[0]), resourcePanel.getResources(), mapPanel.getMaps()).getFollowUpPrioritizedScenario());
            }
        });

        //item=new JMenuItem("Genetic Algorithms...");
        //menu.add(item);
        item=new JMenuItem("Simulated Annealing...");
        menu.add(item);
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                optimizeScenario(new SimulatedAnnealingOptimizer());
            }
        });
        item=new JMenuItem("Greedy Algorithm...");
        menu.add(item);
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                optimizeScenario(new GreedyOptimizer());
            }
        });

        //item=new JMenuItem("Tolerance Search...");
        //menu.add(item);
        item=new JMenuItem("Limited Depth Search...");
        menu.add(item);
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                optimizeScenario(new LimitedDepthTreeSearchOptimizer());
            }
        });

        item=new JMenuItem("Tolerance Search...");
        menu.add(item);
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                optimizeScenario(new ToleranceSearchOptimizer());
            }
        });


        //menu.add(new JSeparator());
        //delayCheckbox=new JCheckBoxMenuItem("Include delays");
        //menu.add(delayCheckbox);

        //item=new JMenuItem("Limited Depth Smart Search...");
        //menu.add(item);
        //item=new JMenuItem("Scheduling Functions...");
        //menu.add(item);

        menuBar.add(menu);

        menu=new JMenu("Help");
        //item=new JMenuItem("Help...");
        //menu.add(item);
        item=new JMenuItem("About...");
        menu.add(item);
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                aboutFrame.setVisible(true);
                aboutFrame.requestFocus();
            }
        });

        menuBar.add(menu);



        setJMenuBar(menuBar);





        chartPane.setResizeWeight(0.8);
        leftVerticalPane.setResizeWeight(0.5);
        rightPane.setResizeWeight(0.99);
        chartPane.setResizeWeight(0.9);
        topPane.setResizeWeight(0);
        taskPanel.setDividers();


        setSize(1200,800);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ganttPanel.setMinimumSize(new Dimension((int)(getWidth()*0.6),100));





        //pack();


        validate();
        pack();
        setSize(1200,800);
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                setVisible(true);
            }
        });

    }

    public void optimizeScenario(Optimizer optimizer){

        final Scenario scenario=new Scenario("New", tasks.toArray(new Task[0]), resourcePanel.getResources(), mapPanel.getMaps(),delayPanel.getDelays());



        final Optimizer optimizer1=optimizer;
        final OptimizationStatus status=new OptimizationStatus(this,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                optimizer1.stop();
            }
        });

        optimizer.setOptimizerListener(new OptimizerListener() {
            @Override
            public void setData(Pair<String, Double>[] data) {

            }

            @Override
            public void setCurrentMinimum(long duration) {
                if (duration<status.getOptimum()){
                    status.setOptimum(duration);
                    status.addData(duration);
                }
            }

            @Override
            public void setSimCount(long count) {
                status.setSimCount(count);
            }

            @Override
            public void setProgress(int percent) {
                status.setProgress(percent);
            }

            @Override
            public void setProgressTitle(String title) {
                status.setProgressTitle(title);
            }
        });


        new Thread(){
            public void run(){
                setEnabled(false);
                try{
                    if (optimizer1.configure(MonolithicGUI.this)){
                        status.setOptimizing(true);
                        optimizer1.optimize(scenario);
                        Scenario res=optimizer1.getResultingScenario();
                        loadScenario(res);
                        status.setOptimizing(false);
                    }
                }catch (Exception ex){}
                setEnabled(true);
                requestFocus();
            }
        }.start();
    }

    public void loadScenario(Scenario scenario){


        Simulator simulator=new Simulator(scenario);
        simulator.disableOutput();
        simulator.enableSwapCollection();
        simulator.enableResourceUtilizationCollection();
        simulator.enableCostDataCollection();
        simulator.simulate();
        simInfoPanel.setData(new long[]{
                simulator.getClock(),
                simulator.getLastSimTime(),
                scenario.getTasks().length,
                simulator.getLastFails(),
                simulator.getSwaps().size()
        });
        schedule=simulator.getExecutionTimes();



        boolean error;
        Long start;
        ganttPanel.reset();
        tasks.clear();
        taskMap.clear();
        for (Task t:scenario.getTasks()){
            error=false;
            start=schedule.get(t.getName());
            if (start==null){
                error=true;
                start=simulator.getClock();
            }
            ganttPanel.addData(t,start,t.getColor(),error);
            tasks.add(t);
            taskMap.put(t.getName(),t);
        }
        ganttPanel.setSwaps(simulator.getSwaps());
        ganttPanel.repaint();
        taskListPanel.update();
        resourcePanel.setResources(scenario.getResources());
        mapPanel.setMaps(scenario.getMaps());
        delayPanel.setDelays(scenario.getDelays());

        resourceGraphPanel.setData(simulator.getResourceUtilization(), resourcePanel.getResources(), schedule);
        costGraphPanel.setData(simulator.getCostData());

        /*bufferChart.reset();
        HashMap<String,long[]> bufferTimes=BufferTimeCalculator.calculateBufferTimes(schedule,scenario);
        for (String s:bufferTimes.keySet()){
            long[] vals=bufferTimes.get(s);
            Color c=taskMap.get(s).getColor();
            bufferChart.addData(s,vals[0],vals[1],vals[2],vals[3],c);
        }

        bufferChart.revalidate();
        bufferChart.repaint();*/



        geometryPanel.update();
        taskPanel.update();
        topPane.updateUI();
        rightPane.updateUI();
        leftVerticalPane.updateUI();
        mainPane.updateUI();
        ganttPanel.revalidate();
        ganttPanel.updateUI();
    }

    private class Listener implements GUIListener{
        @Override
        public Resource[] getResources() {
            return resourcePanel.getResources();
        }

        @Override
        public String[] getMaps() {
            return mapPanel.getMaps();
        }

        @Override
        public Task[] getTasks() {
            return tasks.toArray(new Task[0]);
        }

        public void update(){
            taskPanel.update();
            geometryPanel.update();
        }

        @Override
        public void createTask(Task mod) {
            for (int x=0;x<tasks.size();x++){
                if (tasks.get(x).getName().equals(mod.getName())){
                    JOptionPane.showMessageDialog(MonolithicGUI.this, "Task with same name exists");
                    return;
                }
            }

            //Check for duplicate resources and merge 'em
            ArrayList<ResourceUsage> resourceUsages=new ArrayList<ResourceUsage>();
            for (int y=0;y<mod.getResourceCount();y++){
                if (!mod.getResource(y).isGeometric()){
                    boolean found=false;
                    for (int z=0;z<resourceUsages.size();z++){
                        if (!resourceUsages.get(z).isGeometric() && resourceUsages.get(z).getType().equals(mod.getResource(y).getType())){
                            resourceUsages.set(z,new ResourceUsage(resourceUsages.get(z).getType(),resourceUsages.get(z).getCount()+mod.getResource(y).getCount()));
                            found=true;
                            break;
                        }
                    }
                    if (!found){
                        resourceUsages.add(mod.getResource(y));
                    }
                }
            }
            mod=new Task(
                    mod.getName(),
                    mod.getProvides(),
                    resourceUsages.toArray(new ResourceUsage[0]),
                    mod.cloneDependencies(),
                    mod.getDuration(),
                    mod.getPriority(),
                    mod.getCost(),
                    mod.getColor()
            );

            tasks.add(mod);
            taskMap.put(mod.getName(),mod);

            if (autoUpdate.isSelected()){
                loadScenario(new Scenario("New", tasks.toArray(new Task[0]), resourcePanel.getResources(), mapPanel.getMaps(),delayPanel.getDelays()));
            }
            else{
                ganttPanel.addData(mod,ganttPanel.getMaxTime(),mod.getColor(),true);
                ganttPanel.repaint();
                taskListPanel.update();
                taskPanel.update();
            }
        }

        @Override
        public void updateTask(Task orig, Task mod) {
            for (int x=0;x<tasks.size();x++){
                if (tasks.get(x).getName().equals(orig.getName())){


                    //Check for duplicate resources and merge 'em
                    ArrayList<ResourceUsage> resourceUsages=new ArrayList<ResourceUsage>();
                    for (int y=0;y<mod.getResourceCount();y++){
                        if (!mod.getResource(y).isGeometric()){
                            boolean found=false;
                            for (int z=0;z<resourceUsages.size();z++){
                                if (!resourceUsages.get(z).isGeometric() && resourceUsages.get(z).getType().equals(mod.getResource(y).getType())){
                                    resourceUsages.set(z,new ResourceUsage(resourceUsages.get(z).getType(),resourceUsages.get(z).getCount()+mod.getResource(y).getCount()));
                                    found=true;
                                    break;
                                }
                            }
                            if (!found){
                                resourceUsages.add(mod.getResource(y));
                            }
                        }
                    }
                    mod=new Task(
                            mod.getName(),
                            mod.getProvides(),
                            resourceUsages.toArray(new ResourceUsage[0]),
                            mod.cloneDependencies(),
                            mod.getDuration(),
                            mod.getPriority(),
                            mod.getCost(),
                            mod.getColor()
                    );

                    //Replace task
                    tasks.set(x,mod);

                    //Check for dependencies if taskname changed.
                    if (!mod.getName().equals(orig.getName())){
                        taskMap.put(orig.getName(),mod);
                        for (int y=0;y<tasks.size();y++){
                            if (x!=y){
                                TaskDependency[] deps=new TaskDependency[tasks.get(y).getDependencyCount()];
                                for (int z=0;z<tasks.get(y).getDependencyCount();z++){
                                    if (tasks.get(y).getDependency(z).getTask().equals(orig.getName()))
                                        deps[z]=new TaskDependency(mod.getName(),tasks.get(y).getDependency(z).getType());
                                    else
                                        deps[z]=tasks.get(y).getDependency(z);
                                }


                                tasks.set(y,new Task(
                                        tasks.get(y).getName(),
                                        tasks.get(y).getProvides(),
                                        tasks.get(y).cloneResources(),
                                        deps,
                                        tasks.get(y).getDuration(),
                                        tasks.get(y).getColor()
                                        ));
                            }
                        }
                    }

                    //Update taskMap
                    taskMap.put(mod.getName(),mod);
                    if (autoUpdate.isSelected()){
                        loadScenario(new Scenario("New", tasks.toArray(new Task[0]), resourcePanel.getResources(), mapPanel.getMaps()));
                    }
                    else{
                        taskListPanel.update();
                    }
                    return;
                }
            }
        }

        public void deleteTask(Task t){
            for (int y=0;y<tasks.size();y++){
                if (tasks.get(y).getName().equals(t.getName())){
                    tasks.remove(y);
                    break;
                }
            }
            if (autoUpdate.isSelected()){
                loadScenario(new Scenario("New", tasks.toArray(new Task[0]), resourcePanel.getResources(), mapPanel.getMaps()));
            }
            else{
                ganttPanel.deleteTask(t);
                taskListPanel.update();
            }
        }

        @Override
        public void updateGeometry(Task t, GeometricResourceUsage[] geoRes) {
            ResourceUsage[] oldRes=t.cloneResources();
            LinkedList<ResourceUsage> newRes=new LinkedList<ResourceUsage>();
            for (ResourceUsage r:oldRes){
                if (!r.isGeometric())
                    newRes.add(r);
            }
            for (GeometricResourceUsage r:geoRes)
                newRes.add(r);

            for (int x=0;x<tasks.size();x++){
                if (tasks.get(x).getName().equals(t.getName())){
                    tasks.set(x, new Task(
                            t.getName(),
                            t.getProvides(),
                            newRes.toArray(new ResourceUsage[0]),
                            tasks.get(x).cloneDependencies(),
                            t.getDuration(),
                            t.getPriority(),
                            t.getCost(),
                            t.getColor()
                    ));
                    taskMap.put(t.getName(),tasks.get(x));
                    taskPanel.setTask(tasks.get(x));
                    break;
                }
            }
            if (autoUpdate.isSelected()){
                loadScenario(new Scenario("New", tasks.toArray(new Task[0]), resourcePanel.getResources(), mapPanel.getMaps()));
            }
            else{
                taskListPanel.update();
            }

        }

        @Override
        public void selectTask(String t, String source) {
            System.out.println(schedule.get(t));
            taskPanel.setTask(taskMap.get(t));
            resourceGraphPanel.setSelectedTask(taskMap.get(t));
            geometryPanel.select(taskMap.get(t));
            if (!source.equals("GanttChart"))
                ganttPanel.setSelected(t);
            if (!source.equals("TaskList"))
                taskListPanel.setSelected(t);
        }

        @Override
        public void setEnabled(boolean enabled) {
            MonolithicGUI.this.setEnabled(enabled);
            if (enabled)
                requestFocus();
        }
        public JFrame getParent(){
            return MonolithicGUI.this;
        }
    }




    public static void main(String[] args){
        MonolithicGUI gui=new MonolithicGUI();
    }

}
