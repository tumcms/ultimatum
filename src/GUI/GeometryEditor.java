/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import Geometry.RectangularArea;
import Main.Version;
import Scenario.GeometricResourceUsage;
import Scenario.Resource;
import Scenario.ResourceUsage;
import Scenario.Task;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

public class GeometryEditor extends JFrame {
    private JTable table;
    private TableModel tableModel;
    private ArrayList<GeometricResourceUsage> resources;
    private GUIListener guiListener;
    private JComboBox<String> comboBox;
    private String[] availableMaps;
    private ActionListener changeListener;
    private Task selectedTask;

    public GeometryEditor(GUIListener listener){
        super(Version.ULTIMATUM_VERSION+" - Geometry Editor");
        this.guiListener = listener;
        availableMaps=guiListener.getMaps();
        resources=new ArrayList<GeometricResourceUsage>();
        tableModel=new AbstractTableModel() {
            @Override

            public int getRowCount() {
                return resources.size()+1;
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                if (rowIndex>=resources.size())
                    return columnIndex==0 ? "" : 0;
                GeometricResourceUsage res=resources.get(rowIndex);
                switch (columnIndex){
                    case 0:
                        return res.getMap();
                    case 1:
                        return res.getArea().getMinX();
                    case 2:
                        return res.getArea().getMinY();
                    case 3:
                        return res.getArea().getMaxX()-res.getArea().getMinX();
                    case 4:
                        return res.getArea().getMaxY()-res.getArea().getMinY();
                    default:
                        return "";
                }
            }

            @Override
            public String getColumnName(int column) {
                switch (column){
                    case 0:
                        return "Map";
                    case 1:
                        return "x";
                    case 2:
                        return "y";
                    case 3:
                        return "w";
                    case 4:
                        return "h";
                    default:
                        return "";
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return columnIndex==0 || rowIndex<resources.size();
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                if (rowIndex>=resources.size() && columnIndex==0)
                    resources.add(rowIndex,new GeometricResourceUsage("<GEOMETRY>",(String)aValue,new RectangularArea(0,0,1,1,0)));
                else{
                    GeometricResourceUsage res=resources.get(rowIndex);
                    int ax=res.getArea().getMinX();
                    int ay=res.getArea().getMinY();
                    int aw=res.getArea().getMaxX()-res.getArea().getMinX();
                    int ah=res.getArea().getMaxY()-res.getArea().getMinY();
                    String map=res.getMap();
                    switch(columnIndex){
                        case 0:
                            map=(String)aValue;
                            break;
                        case 1:
                            ax=(Integer)aValue;
                            break;
                        case 2:
                            ay=(Integer)aValue;
                            break;
                        case 3:
                            aw=(Integer)aValue;
                            break;
                        case 4:
                            ah=(Integer)aValue;
                            break;
                    }
                    resources.set(rowIndex,new GeometricResourceUsage("<GEOMTERY>",map,new RectangularArea(ax,ay,aw,ah,0)));
                }
                //guiListener.update();
                if (changeListener!=null)
                    changeListener.actionPerformed(null);
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return columnIndex==0 ? String.class : Integer.class;
            }

            @Override
            public int findColumn(String columnName) {
                if(columnName.equals("Map"))
                    return 0;
                if(columnName.equals("x"))
                    return 1;
                if(columnName.equals("y"))
                    return 2;
                if(columnName.equals("w"))
                    return 3;
                if(columnName.equals("h"))
                    return 4;
                return -1;
            }
        };
        table=new JTable(tableModel);
        comboBox=new JComboBox<String>(new ComboBoxModel<String>() {
            private int selected=0;
            @Override
            public void setSelectedItem(Object anItem) {
                for (int x=0;x<availableMaps.length;x++){
                    if (availableMaps[x].equals(anItem))
                        selected=x;
                }
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        table.updateUI();
                    }
                });

            }

            @Override
            public Object getSelectedItem() {
                return selected<availableMaps.length ? availableMaps[selected] : null;
            }

            @Override
            public int getSize() {
                return availableMaps.length;
            }

            @Override
            public String getElementAt(int index) {
                return index<availableMaps.length ? availableMaps[index] : null;
            }

            @Override
            public void addListDataListener(ListDataListener l) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void removeListDataListener(ListDataListener l) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        table.getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(comboBox));

        JScrollPane pane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setLayout(new BorderLayout());
        add(pane,BorderLayout.CENTER);

        JButton button=new JButton("Delete Geometry");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int row=table.getSelectedRow();
                if (row<resources.size()){
                    resources.remove(row);
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            table.updateUI();
                        }
                    });

                }
            }
        });

        JPanel bottomPanel=new JPanel(new GridLayout(2,1));
        JPanel buttonpanel=new JPanel(new FlowLayout());
        buttonpanel.add(button);

        JPanel bottomButtonPanel=new JPanel(new FlowLayout());
        JButton okButton=new JButton("Apply");
        JButton cancelButton=new JButton("Close");

        bottomButtonPanel.add(okButton);
        bottomButtonPanel.add(cancelButton);

        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guiListener.updateGeometry(selectedTask,getResources());
                setVisible(false);
                dispose();
                guiListener.setEnabled(true);
            }
        });

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                dispose();
                guiListener.setEnabled(true);
            }
        });

        bottomPanel.add(buttonpanel);
        bottomPanel.add(bottomButtonPanel);
        add(new JLabel("Required resources:"), BorderLayout.NORTH);
        add(bottomPanel,BorderLayout.SOUTH);

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                guiListener.setEnabled(true);
            }
        });





    }

    public void setChangeListener(ActionListener l){
        this.changeListener=l;
    }

    public void update(){
        availableMaps=guiListener.getMaps();
    }

    public GeometricResourceUsage[] getResources(){
        return resources.toArray(new GeometricResourceUsage[0]);
    }

    public void setResources(Task t,ResourceUsage[] res){
        this.selectedTask=t;
        resources.clear();
        if (t!=null && res!=null){
            for (ResourceUsage r:res)
                if (r.isGeometric())
                    resources.add((GeometricResourceUsage)r);
        }
        setSize(400,400);
        setResizable(false);
        setVisible(true);

        guiListener.setEnabled(false);

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                table.updateUI();
            }
        });

    }

}
