/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import Scenario.Task;
import Scenario.TaskSwap;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;


public class GanttPanel extends JPanel {
    private GanttComponent component;
    private JScrollPane pane;

    public GanttPanel(final GUIListener guiListener){
        super(new BorderLayout());
        component=new GanttComponent(this);
        component.setActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guiListener.selectTask(getSelected(),"GanttChart");
            }
        });
        pane = new JScrollPane(component,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.getVerticalScrollBar().setUnitIncrement(16);
        pane.getHorizontalScrollBar().setUnitIncrement(16);

        add(pane,BorderLayout.CENTER);
        ScrollListener l=new ScrollListener();
        component.addMouseListener(l);
        component.addMouseMotionListener(l);


        JPanel topPanel=new JPanel(new FlowLayout());

        JPanel radioPanel=new JPanel(new FlowLayout());
        radioPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Show links"));
        ButtonGroup buttonGroup=new ButtonGroup();
        final JRadioButton noneButton=new JRadioButton("Only selected",true);
        buttonGroup.add(noneButton);
        radioPanel.add(noneButton);
        final JRadioButton depButton=new JRadioButton("All dependencies",true);
        buttonGroup.add(depButton);
        radioPanel.add(depButton);
        final JRadioButton swapButton=new JRadioButton("All swaps",true);
        buttonGroup.add(swapButton);
        radioPanel.add(swapButton);

        ActionListener radioListener=new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (noneButton.isSelected()){
                    component.setViewmode(GanttComponent.VIEWMODE_NONE);
                }
                else if (depButton.isSelected()){
                    component.setViewmode(GanttComponent.VIEWMODE_ALL_DEPS);
                }
                else{
                    component.setViewmode(GanttComponent.VIEWMODE_ALL_SWAPS);
                }
            }
        };

        noneButton.addActionListener(radioListener);
        depButton.addActionListener(radioListener);
        swapButton.addActionListener(radioListener);

        buttonGroup =new ButtonGroup();
        final JRadioButton colbox=new JRadioButton("Task colors",true);
        final JRadioButton resbox=new JRadioButton("Resources",false);
        buttonGroup.add(colbox);
        buttonGroup.add(resbox);

        ActionListener colListener=new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                component.setResourceColors(resbox.isSelected());
            }
        };
        resbox.addActionListener(colListener);
        colbox.addActionListener(colListener);

        JPanel colPanel=new JPanel(new FlowLayout());
        colPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Colors"));

        colPanel.add(colbox);
        colPanel.add(resbox);

        topPanel.add(radioPanel);
        topPanel.add(colPanel);

        JPanel bottomPanel=new JPanel(new GridLayout(1,3));
        bottomPanel.add(new JLabel());
        bottomPanel.add(new JLabel("Zoom: ",JLabel.RIGHT));
        final JSlider zoomSlider=new JSlider(JSlider.HORIZONTAL,0,100,0);
        bottomPanel.add(zoomSlider);
        zoomSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                component.setZoom(zoomSlider.getValue()/100.0);
            }
        });

        add(topPanel, BorderLayout.NORTH);
        add(bottomPanel, BorderLayout.SOUTH);


    }

    public void addData(Task task, long start, Color color, boolean error){
        component.addData(task,start,color,error);
    }

    public void setSwaps(LinkedList<TaskSwap> s){
        component.setSwaps(s);
    }

    public void deleteTask(Task t){
        component.deleteTask(t);
    }

    public long getMaxTime(){
        return component.getMaxTime();
    }

    public void reset(){
        component.reset();
    }

    public String getSelected(){
        return component.getSelected();
    }

    public void setSelected(String t){
        if (component.getSelected()!=t)
            component.setSelected(t);
    }

    public void updateUI(){
        super.updateUI();
        if (pane!=null)
            pane.updateUI();
    }


    class ScrollListener extends MouseAdapter{
        private final Cursor defCursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
        private final Cursor hndCursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
        private Point pp=new Point();
        private ReentrantLock lock=new ReentrantLock();

        public void mouseDragged(final MouseEvent e)
        {
            JViewport vport = pane.getViewport();
            Point cp = e.getPoint();
            lock.lock();
            try{
                Point vp = vport.getViewPosition();
                vp.translate(pp.x-cp.x, pp.y-cp.y);
                component.scrollRectToVisible(new Rectangle(vp, vport.getSize()));
                pp.setLocation(cp);
            }catch (Exception ex){
                ex.printStackTrace();
            }
            lock.unlock();
        }

        public void mousePressed(MouseEvent e)
        {
            setCursor(hndCursor);
            pp.setLocation(e.getPoint());
        }

        public void mouseReleased(MouseEvent e){
            setCursor(defCursor);
        }
    }
}
