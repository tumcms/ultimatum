/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import Scenario.Task;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GeometryPanel extends JPanel{
    private GeometryViewer geometryViewer;
    private JCheckBox heatMapBox;
    private JComboBox mapBox;
    private GUIListener listener;

    public GeometryPanel(GUIListener listener){
        super(new BorderLayout());
        this.listener=listener;
        this.geometryViewer =new GeometryViewer(listener);
        JPanel topPanel=new JPanel(new FlowLayout());
        mapBox=new JComboBox();
        mapBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                geometryViewer.select((String)mapBox.getSelectedItem());
            }
        });
        heatMapBox=new JCheckBox("Show heatmap",true);
        heatMapBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                geometryViewer.setShowHeatMap(heatMapBox.isSelected());
            }
        });
        topPanel.add(heatMapBox);
        topPanel.add(new JLabel("Map:"));
        topPanel.add(mapBox);
        add(topPanel,BorderLayout.NORTH);
        JPanel mainPanel=new JPanel(new GridLayout(1,1));
        mainPanel.add(geometryViewer);
        add(mainPanel,BorderLayout.CENTER);
    }

    public void update(){
        mapBox.setModel(new DefaultComboBoxModel(listener.getMaps()));
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                mapBox.updateUI();
            }
        });

        geometryViewer.update();
    }

    public void select(Task t){
        geometryViewer.select(t);
    }


}
