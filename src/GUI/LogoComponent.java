/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.net.URL;

public class LogoComponent extends JComponent{
    private BufferedImage img;

    public LogoComponent(){

        try{
            img= ImageIO.read(LogoComponent.class.getResource("logo.png"));
            setPreferredSize(new Dimension(img.getWidth(),img.getHeight()));
            setMinimumSize(new Dimension(img.getWidth(), img.getHeight()));
            setMaximumSize(new Dimension(img.getWidth(), img.getHeight()));
        }
        catch(Exception e){e.printStackTrace();}
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    Desktop.getDesktop().browse(new URL("http://www.cms.bgu.tum.de").toURI());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });
    }
    public void paint(Graphics g){

        if (img!=null){
            int x=(getWidth()-img.getWidth())/2;
            int y=(getHeight()-img.getHeight())/2;
            g.drawImage(img,x,y,null);
            g.setColor(Color.WHITE);
            g.fillRect(0,0,x,getHeight());
            g.fillRect(x+img.getWidth(),0,x+1,getHeight());
            g.fillRect(x,0,img.getWidth(),y);
            g.fillRect(x,y+img.getHeight(),img.getWidth(),y+1);
        }
    }

}
