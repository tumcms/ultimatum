/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class OptimizingAnimation extends JComponent{
    private int t;
    private boolean anim;
    private BufferedImage buffer;

    public OptimizingAnimation(){
        this.anim=false;
        setMinimumSize(new Dimension(170,150));
        setPreferredSize(new Dimension(1000,150));
        new Thread(){
            public void run(){
                while (true){
                    try{
                        Thread.sleep(40);
                    }
                    catch (Exception ex){}
                    if (anim&&isVisible()){
                        t=(t+1)%200;
                        repaint();
                    }
                }
            }
        }.start();
    }

    public void setAnim(boolean anim) {
        this.anim = anim;
        repaint();
    }

    public boolean isAnim() {
        return anim;
    }

    public void paintComponent(Graphics g){

        if (buffer==null || buffer.getWidth()!=getWidth() || buffer.getHeight()!=getHeight()){
            buffer=null;
            buffer=new BufferedImage(getWidth(),getHeight(),BufferedImage.TYPE_INT_RGB);
        }

        Graphics2D g2=(Graphics2D)buffer.getGraphics();
        g2.setColor(Color.WHITE);
        g2.fillRect(0,0,getWidth(),getHeight());

        int w=(getWidth()-20)/4;
        int h=(getHeight()-50)/3;
        int movementWidth=getWidth()-w-20;

        double v=t/200.0;

        int[] xp=new int[3];
        xp[0]=10+(int)Math.round(2*v*movementWidth);
        if (v>0.5){
            xp[0]=10+(int)Math.round((1-2*(v-0.5))*movementWidth);
        }
        xp[0]=(int)Math.round(10+(movementWidth/2.0)*(1+Math.cos(v * 2 * Math.PI + 0.5 * Math.PI)));
        g2.setColor(Color.RED);
        g2.fillRect(xp[0],40,w,15);
        g2.setColor(Color.BLACK);
        g2.drawRect(xp[0],40,w,15);


        xp[1]=10+(int)Math.round(2*(v-0.5)*movementWidth);

        if (v<0.5){
            xp[1]=10+(int)Math.round((1-2*(v))*movementWidth);
        }
        xp[1]=(int)Math.round(10+(movementWidth/2.0)*(1+Math.cos(v * 2 * Math.PI)));
        g2.setColor(Color.BLUE);
        g2.fillRect(xp[1],40+h,w,15);
        g2.setColor(Color.BLACK);
        g2.drawRect(xp[1],40+h,w,15);


        //v=(v+0.25)%1.0;
        xp[2]=(int)Math.round(10+(movementWidth/2.0)*(1+Math.sin(v * 2 * Math.PI)));
        ///xp[2]=10+(int)Math.round(2*(v-0.5)*movementWidth);
        if (v<0.5){
            //xp[2]=10+(int)Math.round((1-2*(v))*movementWidth);
        }
        g2.setColor(Color.GREEN);
        g2.fillRect(xp[2],40+2*h,w,15);
        g2.setColor(Color.BLACK);
        g2.drawRect(xp[2],40+2*h,w,15);

        g2.setStroke(new BasicStroke(2));

        g2.drawPolyline(
                new int[]{xp[0]+w/2,xp[0]+w/2,xp[1]+w/2,xp[1]+w/2},
                new int[]{40+15,40+15+(h-15)/2,40+15+(h-15)/2,40+h},
                4);

        g2.drawPolyline(
                new int[]{xp[1]+w/2,xp[1]+w/2,xp[2]+w/2,xp[2]+w/2},
                new int[]{40+h+15,40+h+15+(h-15)/2,40+h+15+(h-15)/2,40+2*h},
                4);

        g2.fillPolygon(
                new int[]{xp[0]+w/2,xp[0]+w/2-5,xp[0]+w/2+5},
                new int[]{40+15,40+15+5,40+15+5},
                3);

        g2.fillPolygon(
                new int[]{xp[1]+w/2,xp[1]+w/2-5,xp[1]+w/2+5},
                new int[]{40+h,40+h-5,40+h-5},
                3);

        g2.fillPolygon(
                new int[]{xp[1]+w/2,xp[1]+w/2-5,xp[1]+w/2+5},
                new int[]{40+15+h,40+15+h+5,40+15+h+5},
                3);

        g2.fillPolygon(
                new int[]{xp[2]+w/2,xp[2]+w/2-5,xp[2]+w/2+5},
                new int[]{40+2*h,40+2*h-5,40+2*h-5},
                3);

        FontMetrics fM=g2.getFontMetrics();

        if (!anim){
            int tx=(int)Math.round((getWidth()-fM.getStringBounds("Idling...",g2).getWidth())/2.0);
            g2.drawString("Idling...",tx,20);
        }
        else{
            int tx=(int)Math.round((getWidth()-fM.getStringBounds("Optimizing...",g2).getWidth())/2.0);
            g2.drawString("Optimizing...",tx,20);
        }


        g.drawImage(buffer,0,0,null);
    }

    public static void main(String[] args){
        JFrame frame=new JFrame("FUN");
        frame.setSize(300,150);
        frame.setResizable(false);
        OptimizingAnimation anim=new OptimizingAnimation();
        frame.add(anim);
        anim.setAnim(true);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
