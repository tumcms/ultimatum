/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class ColorComponent extends JComponent {
    private Color col;
    private JColorChooser cc;
    private ActionListener changeListener;

    public ColorComponent() {
        this.col = Color.BLUE;
        this.cc=new JColorChooser();
        setPreferredSize(new Dimension(50, 20));
        setMinimumSize(new Dimension(30, 10));
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton()==MouseEvent.BUTTON1){
                    cc.setColor(col);
                    //tcc.setPreviewPanel(new JPanel());
                    JFrame frame=new JFrame("Color Chooser");
                    frame.setLayout(new GridLayout(1,1));
                    frame.add(cc);
                    frame.setSize(500,300);
                    frame.setResizable(false);
                    frame.setVisible(true);
                    int xp=(int)Math.max(10,Math.min(e.getXOnScreen(),Toolkit.getDefaultToolkit().getScreenSize().getWidth()-510));
                    int yp=(int)Math.max(10,Math.min(e.getYOnScreen(),Toolkit.getDefaultToolkit().getScreenSize().getHeight()-310));
                    frame.setLocation(xp,yp);
                    /*Color newColor = JColorChooser.showDialog(
                            ColorComponent.this,
                            "Choose Color",
                            col);*/
                    cc.getSelectionModel().addChangeListener(new ChangeListener() {
                        @Override
                        public void stateChanged(ChangeEvent e) {
                            if (cc.getColor()!=null){
                                col=cc.getColor();
                                repaint();
                                if (changeListener!=null)
                                    changeListener.actionPerformed(null);
                            }
                        }
                    });
                }


            }
        });
    }

    public void setChangeListener(ActionListener l){
        this.changeListener=l;
    }

    public Color getColor() {
        return col;
    }

    public void setColor(Color col) {
        this.col = col;
        repaint();
    }

    public void paintComponent(Graphics g){
        g.setColor(col);
        g.fillRect(0, 0, getWidth(), getHeight());
        g.setColor(Color.BLACK);
        g.drawRect(0,0,getWidth()-1,getHeight()-1);
    }



}
