/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import EpsOutput.EpsGraphics2D;
import Scenario.Resource;
import Scenario.ResourceUsage;
import Scenario.Task;
import Scenario.TaskDependency;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.AttributedString;
import java.util.*;


public class ResourceGraphPanel extends JPanel {
    private ArrayList<long[]> resourceUsages;
    private Resource[] resources;
    private Task selectedTask;
    private HashMap<String,Long> schedule;
    private boolean update;
    private int selectedResource;
    private JComboBox<String> resourceCombo;
    private ResourceGraphComponent component;

    public ResourceGraphPanel(){
        super(new BorderLayout());
        this.update=false;
        this.selectedResource=0;

        component=new ResourceGraphComponent();

        JPanel topPanel=new JPanel();

        resourceCombo=new JComboBox<String>();
        resourceCombo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectedResource=resourceCombo.getSelectedIndex();
                update=true;
                component.repaint();
            }
        });

        topPanel.add(new JLabel("Resource:"));
        topPanel.add(resourceCombo);

        add(topPanel,BorderLayout.NORTH);
        add(component,BorderLayout.CENTER);


    }

    public void setData(ArrayList<long[]> data, Resource[] resources, HashMap<String,Long> schedule){
        this.resourceUsages=data;
        this.resources=resources;
        this.schedule=schedule;
        this.selectedResource=0;
        update=true;
        repaint();
        String[] res=new String[resources.length];
        for (int x=0;x<resources.length;x++)
            res[x]=resources[x].getName();

        resourceCombo.setModel(new DefaultComboBoxModel<String>(res));
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                resourceCombo.updateUI();
            }
        });

    }

    public void setSelectedTask(Task task){
        this.selectedTask =task;
        this.update=true;
    }

    class ResourceGraphComponent extends JComponent{
        public ResourceGraphComponent(){
            setFocusable(true);
            setEnabled(true);
            addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    requestFocus();
                    if (e.getButton()==MouseEvent.BUTTON3){
                        JPopupMenu popup=new JPopupMenu();
                        JMenuItem item=new JMenuItem("Save chart as PNG...");
                        popup.add(item);
                        item.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                JFileChooser chooser=new JFileChooser(".");
                                chooser.setFileFilter(new FileFilter() {
                                    @Override
                                    public boolean accept(File f) {
                                        return f.isDirectory() || f.getName().toLowerCase().endsWith(".png");
                                    }

                                    @Override
                                    public String getDescription() {
                                        return "PNG File";  //To change body of implemented methods use File | Settings | File Templates.
                                    }
                                });
                                int res=chooser.showSaveDialog(ResourceGraphComponent.this);
                                if (res==JFileChooser.APPROVE_OPTION){
                                    try{
                                        ImageIO.write(buffer, "png", chooser.getSelectedFile());
                                    }catch(Exception ex){}

                                }
                            }
                        });
                        item=new JMenuItem("Save chart as EPS...");
                        popup.add(item);
                        item.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                JFileChooser chooser=new JFileChooser(".");
                                chooser.setFileFilter(new FileFilter() {
                                    @Override
                                    public boolean accept(File f) {
                                        return f.isDirectory() || f.getName().toLowerCase().endsWith(".eps");
                                    }

                                    @Override
                                    public String getDescription() {
                                        return "EPS File";  //To change body of implemented methods use File | Settings | File Templates.
                                    }
                                });
                                int res=chooser.showSaveDialog(ResourceGraphComponent.this);
                                if (res==JFileChooser.APPROVE_OPTION){
                                    try{
                                        createEPS(chooser.getSelectedFile());
                                    }catch(Exception ex){}

                                }
                            }
                        });
                        popup.show(ResourceGraphComponent.this,e.getX(),e.getY());
                        return;
                    }
                }
            });
        }
        private BufferedImage buffer;
        public void paintComponent(Graphics g){
            if (buffer==null || buffer.getWidth()!=getWidth() || buffer.getHeight()!=getHeight()){
                buffer=null;
                buffer=new BufferedImage(getWidth(),getHeight(),BufferedImage.TYPE_INT_RGB);
                update=true;
            }
            if (update){
                update=false;
                Graphics2D g2=(Graphics2D)buffer.getGraphics();
                drawGraph(g2);
            }
            g.drawImage(buffer,0,0,null);
        }

        public void drawGraph(Graphics2D g2){
            g2.setColor(Color.WHITE);
            g2.fillRect(0,0,getWidth(),getHeight());

            if (resourceUsages!=null && !resourceUsages.isEmpty() && resources.length>0){
                int xOff=50;
                int yOff=35;
                int width=getWidth()-75;
                int height=getHeight()-90;
                Collections.sort(resourceUsages,new Comparator<long[]>() {
                    @Override
                    public int compare(long[] o1, long[] o2) {
                        return (int)Math.signum(o1[0]-o2[0]);
                    }
                });
                long maxTime=resourceUsages.get(resourceUsages.size()-1)[0];

                double xScale=width/(double)(maxTime+1);

                long maxVal=-Long.MAX_VALUE;

                int resCount=resources[selectedResource].getCount();

                for (long[] l:resourceUsages){
                    maxVal=Math.max(maxVal,resCount-l[selectedResource+1]);
                }

                if (resCount>maxVal)
                    maxVal=resCount;

                double yScale=height/(double)maxVal;

                int bottomY=yOff+height;
                g2.setColor(new Color(50,50,255));
                for (int x=1;x<resourceUsages.size();x++){
                    int pxp=(int)Math.round(xOff+xScale*resourceUsages.get(x-1)[0]);
                    int pyp=(int)Math.round(yOff+(height-yScale*(resCount-resourceUsages.get(x-1)[selectedResource+1])));
                    int xp=(int)Math.round(xOff+xScale*resourceUsages.get(x)[0]);

                    g2.fillRect(pxp,pyp,xp-pxp,bottomY-pyp);


                }
                g2.setColor(Color.BLACK);
                for (int x=1;x<resourceUsages.size();x++){
                    int pxp=(int)Math.round(xOff+xScale*resourceUsages.get(x-1)[0]);
                    int pyp=(int)Math.round(yOff+(height-yScale*(resCount-resourceUsages.get(x-1)[selectedResource+1])));
                    int xp=(int)Math.round(xOff+xScale*resourceUsages.get(x)[0]);
                    int yp=(int)Math.round(yOff+(height-yScale*(resCount-resourceUsages.get(x)[selectedResource+1])));

                    g2.drawLine(pxp,pyp,xp,pyp);
                    g2.drawLine(xp,pyp,xp,yp);

                }

                g2.setColor(Color.RED);
                int yp=(int)Math.round(yOff+(height-yScale*resCount));
                g2.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER, 2.0f, new float[]{2.0f}, 0.0f));
                g2.drawLine(xOff,yp,xOff+width,yp);

                if (selectedTask!=null && schedule.containsKey(selectedTask.getName())){
                    for (ResourceUsage d:selectedTask.cloneResources()){
                        if (d.getType().equals(resources[selectedResource].getType())){
                            long start=schedule.get(selectedTask.getName());
                            long end=start+selectedTask.getDuration();

                            int pxp=(int)Math.round(xOff+xScale*start);
                            int pyp=(int)Math.round(yOff+(height-yScale*d.getCount()));
                            int xp=(int)Math.round(xOff+xScale*end);
                            g2.setColor(Color.ORANGE);
                            g2.fillRect(pxp,pyp,xp-pxp,bottomY-pyp);
                            break;
                        }
                    }

                }

                FontMetrics fm=g2.getFontMetrics();
                g2.setColor(Color.BLACK);
                g2.setStroke(new BasicStroke(1));
                g2.drawLine(xOff-5,yOff+height,xOff+width+10,yOff+height);
                g2.fillPolygon(new int[]{xOff+width+10,xOff+width+5,xOff+width+5},new int[]{yOff+height,yOff+height-5,yOff+height+5},3);
                g2.drawString("t",xOff+width+10,yOff+height+15);

                long stepSize=1;
                long unitSize=1;
                long unitExp=1;
                if (maxTime>10){
                    stepSize=(long)Math.pow(10,Math.floor(Math.log10(maxTime))-1);
                    unitSize=stepSize;
                    unitExp=Math.round(Math.floor(Math.log10(maxTime))-1);
                }

                String unitString="x10";
                if (unitExp==0)unitString="";
                if (unitExp>1)unitString+=superscript(unitExp+"");
                Rectangle bounds=fm.getStringBounds(unitString,g2).getBounds();
                g2.drawString(unitString,(int)(xOff+width+10-bounds.getWidth()),yOff+height+20+fm.getHeight());

                while (maxTime/(double)stepSize>15)
                    stepSize*=2;



                for (long x=0;x<=maxTime-stepSize;x+=stepSize){
                    int pxp=(int)Math.round(xOff+xScale*x);
                    g2.drawLine(pxp,yOff+height-3,pxp,yOff+height+3);
                    bounds=fm.getStringBounds((x/unitSize)+"",g2).getBounds();
                    int tx=(int)Math.round(pxp-bounds.getWidth()/2);
                    int ty=(int)Math.round(yOff+height+3+bounds.getHeight());
                    g2.drawString((x/unitSize)+"",tx,ty);
                }
                //if (maxTime%stepSize!=0){
                int pxp=(int)Math.round(xOff+xScale*maxTime);
                g2.drawLine(pxp, yOff + height - 3, pxp, yOff + height + 3);
                bounds=fm.getStringBounds((maxTime/unitSize)+"",g2).getBounds();
                int tx=(int)Math.round(pxp-bounds.getWidth()/2);
                int ty=(int)Math.round(yOff+height+3+bounds.getHeight());
                g2.drawString((maxTime/unitSize)+"",tx,ty);

                //}

                g2.drawLine(xOff,yOff-10,xOff,yOff+height+5);
                g2.fillPolygon(new int[]{xOff,xOff+5,xOff-5},new int[]{yOff-10,yOff-5,yOff-5},3);
                //g2.drawString("n",xOff-15,yOff-10);


                stepSize=1;
                unitSize=1;
                unitExp=0;
                if (maxVal>10){
                    stepSize=(long)Math.pow(10,Math.floor(Math.log10(maxVal))-1);
                    unitSize=stepSize;
                    unitExp=Math.round(Math.max(0,Math.floor(Math.log10(maxVal))-1));
                }
                //System.out.println("E "+unitExp+" "+maxVal);
                unitString="n x10";
                if (unitExp<=0)unitString="n";
                if (unitExp>1)unitString+=superscript(unitExp+"");
                bounds=fm.getStringBounds(unitString,g2).getBounds();
                g2.drawString(unitString,(int)(xOff-15),yOff-15);


                for (long x=0;x<=maxVal-stepSize;x+=stepSize){
                    int pyp=(int)Math.round(yOff+height-yScale*x);
                    g2.drawLine(xOff-3,pyp,xOff+3,pyp);
                    bounds=fm.getStringBounds((x/unitSize)+"",g2).getBounds();
                    tx=(int)Math.round(xOff-5-bounds.getWidth());
                    ty=(int)Math.round(pyp+bounds.getHeight()/2);
                    g2.drawString((x/unitSize)+"",tx,ty);
                }
                //if (maxVal%stepSize!=0){
                int pyp=(int)Math.round(yOff+height-yScale*maxVal);
                g2.drawLine(xOff-3,pyp,xOff+3,pyp);
                bounds=fm.getStringBounds((maxVal/unitSize)+"",g2).getBounds();
                tx=(int)Math.round(xOff-5-bounds.getWidth());
                ty=(int)Math.round(pyp+bounds.getHeight()/2);
                g2.drawString((maxVal/unitSize)+"",tx,ty);

                //}



            }
        }

        private String superscript(String str) {
            str = str.replaceAll("0", "⁰");
            str = str.replaceAll("1", "¹");
            str = str.replaceAll("2", "²");
            str = str.replaceAll("3", "³");
            str = str.replaceAll("4", "⁴");
            str = str.replaceAll("5", "⁵");
            str = str.replaceAll("6", "⁶");
            str = str.replaceAll("7", "⁷");
            str = str.replaceAll("8", "⁸");
            str = str.replaceAll("9", "⁹");
            return str;
        }
        public void createEPS(File file) throws Exception{
            EpsGraphics2D g2=new EpsGraphics2D(file.getName(),file,0,0,getWidth(),getHeight());
            //g2.setAccurateTextMode(false);
            drawGraph(g2);
            g2.flush();
            g2.close();
        }
    }
}
