/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import Scenario.Resource;
import Scenario.Task;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ResourcePanel extends JPanel {
    private JTable table;
    private TableModel tableModel;
    private ArrayList<Resource> resources;
    private GUIListener guiListener;

    public ResourcePanel(GUIListener listener){
        this.guiListener=listener;
        resources=new ArrayList<Resource>();
        tableModel=new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return resources.size()+1;
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                if (rowIndex>=resources.size())
                    return columnIndex==0 ? "" : 0;
                return columnIndex==0 ? resources.get(rowIndex).getType() : resources.get(rowIndex).getCount();
            }

            @Override
            public String getColumnName(int column) {
                return column==0 ? "Resource" : "Count";
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return columnIndex==0 || rowIndex<resources.size();
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                if (rowIndex>=resources.size() && columnIndex==0)
                        resources.add(rowIndex,new Resource((String)aValue,1));
                else{
                    String name=resources.get(rowIndex).getType();
                    int count=resources.get(rowIndex).getCount();
                    if (columnIndex==0)name=(String)aValue;
                    else count=(Integer)aValue;
                    resources.set(rowIndex,new Resource(name,count));
                }
                guiListener.update();
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return columnIndex==0 ? String.class : Integer.class;
            }

            @Override
            public int findColumn(String columnName) {
                return columnName.equals("Resource") ? 0 : 1;
            }
        };
        table=new JTable(tableModel);
        JScrollPane pane=new JScrollPane(table,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setLayout(new BorderLayout());
        add(pane,BorderLayout.CENTER);

        JButton button=new JButton("Delete Resource");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int row=table.getSelectedRow();
                if (row<resources.size()){
                    resources.remove(row);
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            table.updateUI();
                        }
                    });

                    guiListener.update();
                }
            }
        });

        JPanel buttonpanel=new JPanel(new FlowLayout());
        buttonpanel.add(button);
        add(buttonpanel,BorderLayout.SOUTH);
        add(new JLabel("Resources:"),BorderLayout.NORTH);

    }

    public Resource[] getResources(){
        return resources.toArray(new Resource[0]);
    }

    public void setResources(Resource[] res){
        resources.clear();
        for (Resource r:res)
            resources.add(r);
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                table.updateUI();
            }
        });

    }


}
