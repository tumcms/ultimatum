/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import Scenario.Resource;
import Scenario.Task;
import Scenario.TaskDependency;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class DependencyPanel extends JPanel {
    private JTable table;
    private TableModel tableModel;
    private ArrayList<TaskDependency> dependencies;
    private GUIListener guiListener;
    private JComboBox<String> taskComboBox;
    private JComboBox<String> typeComboBox;
    private Task[] availableTasks;
    private ActionListener changeListener;

    public DependencyPanel(GUIListener guiListener){
        this.guiListener = guiListener;
        availableTasks=guiListener.getTasks();
        dependencies=new ArrayList<TaskDependency>();
        tableModel=new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return dependencies.size()+1;
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                if (rowIndex>=dependencies.size())
                    return columnIndex==0 ? "" : "FS";
                return columnIndex==0 ? dependencies.get(rowIndex).getTask() : ( dependencies.get(rowIndex).getType()== TaskDependency.TYPE_FS ? "FS" : "SS");
            }

            @Override
            public String getColumnName(int column) {
                return column==0 ? "Dependency" : "Type";
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return columnIndex==0 || rowIndex<dependencies.size();
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                if (rowIndex>=dependencies.size()){
                    if (columnIndex==0)
                        dependencies.add(rowIndex,new TaskDependency((String)aValue, TaskDependency.TYPE_FS ));
                }
                else{
                    String name=dependencies.get(rowIndex).getTask();
                    int type=dependencies.get(rowIndex).getType();
                    if (columnIndex==0)name=(String)aValue;
                    else type = ((String)aValue).equals("FS") ? TaskDependency.TYPE_FS : TaskDependency.TYPE_SS;
                    dependencies.set(rowIndex,new TaskDependency(name,type));
                }
                if (changeListener!=null)
                    changeListener.actionPerformed(null);
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return columnIndex==0 ? String.class : Integer.class;
            }

            @Override
            public int findColumn(String columnName) {
                return columnName.equals("Dependency") ? 0 : 1;
            }
        };
        table=new JTable(tableModel);
        taskComboBox =new JComboBox<String>(new ComboBoxModel<String>() {
            private int selected=0;
            @Override
            public void setSelectedItem(Object anItem) {
                for (int x=0;x<availableTasks.length;x++){
                    if (availableTasks[x].getName().equals(anItem))
                        selected=x;
                }
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        table.updateUI();
                    }
                });

            }

            @Override
            public Object getSelectedItem() {
                return selected<availableTasks.length ? availableTasks[selected].getName() : null;
            }

            @Override
            public int getSize() {
                return availableTasks.length;
            }

            @Override
            public String getElementAt(int index) {
                return index<availableTasks.length ? availableTasks[index].getName() : null;
            }

            @Override
            public void addListDataListener(ListDataListener l) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void removeListDataListener(ListDataListener l) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });

        typeComboBox =new JComboBox<String>(new ComboBoxModel<String>() {
            private int selected=0;
            @Override
            public void setSelectedItem(Object anItem) {
                selected= anItem.equals("FS") ? TaskDependency.TYPE_FS : TaskDependency.TYPE_SS;
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        table.updateUI();
                    }
                });

            }

            @Override
            public Object getSelectedItem() {
                return selected==0 ? "FS" : "SS";
            }

            @Override
            public int getSize() {
                return 2;
            }

            @Override
            public String getElementAt(int index) {
                return index==0 ? "FS" : "SS";
            }

            @Override
            public void addListDataListener(ListDataListener l) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void removeListDataListener(ListDataListener l) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        table.getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(taskComboBox));
        table.getColumnModel().getColumn(1).setCellEditor(new DefaultCellEditor(typeComboBox));

        JScrollPane pane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setLayout(new BorderLayout());
        add(pane,BorderLayout.CENTER);

        JButton button=new JButton("Delete Dependency");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int row=table.getSelectedRow();
                if (row<dependencies.size()){
                    dependencies.remove(row);
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            table.updateUI();
                        }
                    });

                }
            }
        });

        JPanel buttonpanel=new JPanel(new FlowLayout());
        buttonpanel.add(button);
        add(buttonpanel,BorderLayout.SOUTH);
        add(new JLabel("Dependencies:"),BorderLayout.NORTH);

    }

    public void setChangeListener(ActionListener l){
        this.changeListener=l;
    }

    public TaskDependency[] getDependencies(){
        return dependencies.toArray(new TaskDependency[0]);
    }

    public void update(){
        availableTasks=guiListener.getTasks();
    }

    public void setDependencies(TaskDependency[] deps){
        dependencies.clear();
        for (TaskDependency dep:deps){
            dependencies.add(dep);
        }
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                table.updateUI();
            }
        });

    }
}
