/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.awt.*;


public class SimInfoPanel extends JPanel {
    private JTable table;
    private TableModel tableModel;
    private long[] data;
    private String[] columnNames=new String[]{"Sim time","CPU time","Tasks","Failed tasks","Swaps"};
    private JScrollPane pane;

    public SimInfoPanel(){
        super(new GridLayout(1,1));
        data=new long[]{0,0,0,0,0};
        tableModel=new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return 1;
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                if (rowIndex!=0)return "";
                return data[columnIndex];
            }

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int findColumn(String columnName) {
                for (int x=0;x<columnNames.length;x++){
                    if (columnNames[x].equals(columnName))
                        return x;
                }
                return -1;
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return String.class;
            }


        };
        table=new JTable(tableModel);
        pane=new JScrollPane(table,JScrollPane.VERTICAL_SCROLLBAR_NEVER,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        add(pane);

        table.setPreferredSize(new Dimension(table.getPreferredSize().width, 50));
        pane.setPreferredSize(new Dimension(table.getPreferredSize().width, 50));
        //setPreferredSize(new Dimension(100, 50));

    }

    @Override
    public void setSize(int w, int h) {
        super.setSize(w,h);    //To change body of overridden methods use File | Settings | File Templates.
        table.setPreferredSize(new Dimension(w, 50));
        pane.setPreferredSize(new Dimension(w, 50));

    }

    public void setData(long[] data){
        this.data=data;
        table.setPreferredSize(new Dimension(table.getPreferredSize().width, 50));
        pane.setPreferredSize(new Dimension(table.getPreferredSize().width, 50));
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                table.updateUI();
            }
        });

    }
}
