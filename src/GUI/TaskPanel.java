/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import Scenario.Resource;
import Scenario.ResourceUsage;
import Scenario.Task;
import Scenario.TaskDependency;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class TaskPanel extends JPanel{
    private JTextField name;
    private JTextField provides;
    private JTextField priority;
    private JTextField duration;
    private JTextField cost;
    private ResourceUsagePanel resourceUsagePanel;
    private DependencyPanel dependencyPanel;
    private GUIListener guiListener;
    private JSplitPane bottomPane,mainPane;
    private ColorComponent color;


    private TextFieldListener[] textFieldListeners;

    private boolean edited;
    private ActionListener editedListener;
    private Task task;


    private class TextFieldListener extends KeyAdapter implements FocusListener{
        private String val;
        private JTextField field;

        private TextFieldListener(JTextField field) {
            this.field = field;
        }

        public void reset(){
            this.val=this.field.getText();
        }

        @Override
        public void focusGained(FocusEvent e) {
            reset();
        }

        @Override
        public void focusLost(FocusEvent e) {
            if (!field.getText().equals(val)){
                edited=true;
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (!field.getText().equals(val)){
                edited=true;
            }
        }

        @Override
        public void keyTyped(KeyEvent e) {
            if (!field.getText().equals(val)){
                edited=true;
            }
        }

    }


    public TaskPanel(GUIListener listener){
        this.guiListener=listener;
        this.edited=false;

        editedListener=new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                edited=true;
            }
        };
        name=new JTextField(20);
        textFieldListeners=new TextFieldListener[5];
        textFieldListeners[0]=new TextFieldListener(name);
        name.addFocusListener(textFieldListeners[0]);
        name.addKeyListener(textFieldListeners[0]);

        provides=new JTextField(10);
        textFieldListeners[1]=new TextFieldListener(provides);
        provides.addFocusListener(textFieldListeners[1]);
        provides.addKeyListener(textFieldListeners[1]);

        priority=new JTextField("0",5);
        textFieldListeners[2]=new TextFieldListener(priority);
        priority.addFocusListener(textFieldListeners[2]);
        priority.addKeyListener(textFieldListeners[2]);

        duration=new JTextField("1",5);
        textFieldListeners[3]=new TextFieldListener(duration);
        duration.addFocusListener(textFieldListeners[3]);
        duration.addKeyListener(textFieldListeners[3]);

        cost=new JTextField("1",5);
        textFieldListeners[4]=new TextFieldListener(cost);
        cost.addFocusListener(textFieldListeners[4]);
        cost.addKeyListener(textFieldListeners[4]);

        resourceUsagePanel=new ResourceUsagePanel(guiListener);
        resourceUsagePanel.setChangeListener(editedListener);
        dependencyPanel=new DependencyPanel(guiListener);
        dependencyPanel.setChangeListener(editedListener);
        color=new ColorComponent();
        color.setChangeListener(editedListener);


        final JPanel mainPanel=new JPanel(new BorderLayout());
        JPanel labelPanel=new JPanel(new GridLayout(5,1));
        labelPanel.add(new JLabel("Name:",JLabel.RIGHT));
        labelPanel.add(new JLabel("Color:",JLabel.RIGHT));
        labelPanel.add(new JLabel("Priority:",JLabel.RIGHT));
        labelPanel.add(new JLabel("Duration:",JLabel.RIGHT));
        labelPanel.add(new JLabel("Cost:",JLabel.RIGHT));
        mainPanel.add(labelPanel,BorderLayout.WEST);
        JPanel fieldPanel=new JPanel(new GridLayout(5,1));
        fieldPanel.add(name);
        fieldPanel.add(color);
        fieldPanel.add(priority);
        fieldPanel.add(duration);
        fieldPanel.add(cost);
        mainPanel.add(fieldPanel,BorderLayout.CENTER);



        /*JPanel linePanel=new JPanel(new BorderLayout());
        linePanel.add(new JLabel("Name:",JLabel.RIGHT),BorderLayout.WEST);
        linePanel.add(name,BorderLayout.CENTER);
        mainPanel.add(linePanel);
        //linePanel=new JPanel(new FlowLayout());
        //linePanel.add(new JLabel("Provides:"));
        //linePanel.add(provides);
        linePanel=new JPanel(new BorderLayout());
        linePanel.add(new JLabel("Color:",JLabel.RIGHT),BorderLayout.WEST);
        linePanel.add(color,BorderLayout.CENTER);
        mainPanel.add(linePanel);
        linePanel=new JPanel(new BorderLayout());
        linePanel.add(new JLabel("Priority:",JLabel.RIGHT),BorderLayout.WEST);
        linePanel.add(priority,BorderLayout.CENTER);
        mainPanel.add(linePanel);
        linePanel=new JPanel(new BorderLayout());
        linePanel.add(new JLabel("Duration:",JLabel.RIGHT),BorderLayout.WEST);
        linePanel.add(duration,BorderLayout.CENTER);
        mainPanel.add(linePanel);
        linePanel=new JPanel(new BorderLayout());
        linePanel.add(new JLabel("Cost:",JLabel.RIGHT),BorderLayout.WEST);
        linePanel.add(cost,BorderLayout.CENTER);
        mainPanel.add(linePanel);


        mainPanel.setMaximumSize(new Dimension(mainPanel.getMaximumSize().width, 300));  */


        //name.addActionListener(actionListener);
        //provides.addActionListener(actionListener);

        bottomPane=new JSplitPane(JSplitPane.VERTICAL_SPLIT,resourceUsagePanel,dependencyPanel);



        JScrollPane scrollpane = new JScrollPane(mainPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);


        scrollpane.setPreferredSize(new Dimension(300,200));
        scrollpane.setMinimumSize(new Dimension(100,200));
        scrollpane.setMaximumSize(new Dimension(1000,200));

        mainPane=new JSplitPane(JSplitPane.VERTICAL_SPLIT,scrollpane,bottomPane);




        JPanel buttonPanel=new JPanel(new GridLayout(4,1));

        JButton geoButton=new JButton("Edit geometry");

        geoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (task!=null){
                    GeometryEditor geoEdit=new GeometryEditor(guiListener);
                    geoEdit.setResources(task,task.cloneResources());
                }
            }
        });

        JButton saveButton=new JButton("Save changes");

        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guiListener.updateTask(getOriginalTask(),getModifiedTask());
                edited=false;
                for (int x=0;x<textFieldListeners.length;x++){
                    textFieldListeners[x].reset();
                }
            }
        });

        JButton newButton=new JButton("Create new task");

        newButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guiListener.createTask(getModifiedTask());
                edited=false;
                for (int x=0;x<textFieldListeners.length;x++){
                    textFieldListeners[x].reset();
                }
            }
        });

        JButton delButton=new JButton("Delete task");

        delButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (getOriginalTask()!=null){
                    guiListener.deleteTask(getOriginalTask());
                    edited=false;
                    for (int x=0;x<textFieldListeners.length;x++){
                        textFieldListeners[x].reset();
                    }
                }
            }
        });

        buttonPanel.add(geoButton);
        buttonPanel.add(newButton);
        buttonPanel.add(delButton);
        buttonPanel.add(saveButton);

        setLayout(new BorderLayout());
        add(new JLabel("Selected task:"),BorderLayout.NORTH);
        add(mainPane,BorderLayout.CENTER);
        add(buttonPanel,BorderLayout.SOUTH);
        setMinimumSize(new Dimension(300,getMinimumSize().height));
    }

    public void setDividers(){
        mainPane.setResizeWeight(0);
        bottomPane.setResizeWeight(0.5);
    }

    public void update(){
        resourceUsagePanel.update();
        dependencyPanel.update();
    }

    public void saveEdits(){
        if (edited){
            int res=JOptionPane.showConfirmDialog(
                    guiListener.getParent(),
                    "Save changes to task?",
                    "Unsaved changes",
                    JOptionPane.YES_NO_OPTION);
            if (res==JOptionPane.YES_OPTION){
                if (getOriginalTask()==null)
                    guiListener.createTask(getModifiedTask());
                else
                    guiListener.updateTask(getOriginalTask(), getModifiedTask());
            }
            edited=false;
            for (int x=0;x<textFieldListeners.length;x++){
                textFieldListeners[x].reset();
            }
        }
    }

    public void setTask(Task t){
        saveEdits();
        edited=false;
        this.task=t;
        if (t==null){
            name.setText("");
            provides.setText("");
            duration.setText("1");
            cost.setText("1");
            priority.setText("0");
            resourceUsagePanel.setResources(new ResourceUsage[0]);
            dependencyPanel.setDependencies(new TaskDependency[0]);
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    updateUI();
                }
            });

            return;
        }
        name.setText(t.getName());
        provides.setText(t.getName());
        duration.setText(t.getDuration()+"");
        priority.setText(t.getPriority()+"");
        color.setColor(t.getColor());
        cost.setText(t.getCost()+"");
        resourceUsagePanel.setResources(t.cloneResources());
        dependencyPanel.setDependencies(t.cloneDependencies());
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                updateUI();
            }
        });

    }


    public Task getOriginalTask(){
        return task;
    }

    public Task getModifiedTask(){
        String prov=name.getText();
        if (!provides.getText().isEmpty())
            prov=provides.getText();
        return new Task(
                name.getText(),
                prov,
                resourceUsagePanel.getResources(),
                dependencyPanel.getDependencies(),
                Long.parseLong(duration.getText()),
                Double.parseDouble(priority.getText()),
                Double.parseDouble(cost.getText()),
                color.getColor()
        );

    }
}
