/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import EpsOutput.EpsGraphics2D;
import Scenario.Resource;
import Scenario.ResourceUsage;
import Scenario.Task;
import Scenario.TaskDependency;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.*;
import java.awt.font.TextAttribute;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.AttributedString;
import java.util.*;


public class CostGraphPanel extends JPanel {

    private ArrayList<double[]> costs;
    private boolean update;

    private CostGraphComponent component;

    public CostGraphPanel(){
        super(new GridLayout(1,1));
        this.update=false;
        component=new CostGraphComponent();
        add(component);

    }



    public void setData(ArrayList<double[]> data){
        this.costs=data;
        update=true;
        repaint();
    }

    class CostGraphComponent extends JComponent{
        public CostGraphComponent(){
            setFocusable(true);
            setEnabled(true);
            addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    requestFocus();
                    if (e.getButton()==MouseEvent.BUTTON3){
                        JPopupMenu popup=new JPopupMenu();
                        JMenuItem item=new JMenuItem("Save chart as PNG...");
                        popup.add(item);
                        item.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                JFileChooser chooser=new JFileChooser(".");
                                chooser.setFileFilter(new FileFilter() {
                                    @Override
                                    public boolean accept(File f) {
                                        return f.isDirectory() || f.getName().toLowerCase().endsWith(".png");
                                    }

                                    @Override
                                    public String getDescription() {
                                        return "PNG File";  //To change body of implemented methods use File | Settings | File Templates.
                                    }
                                });
                                int res=chooser.showSaveDialog(CostGraphComponent.this);
                                if (res==JFileChooser.APPROVE_OPTION){
                                    try{
                                        ImageIO.write(buffer, "png", chooser.getSelectedFile());
                                    }catch(Exception ex){}

                                }
                            }
                        });
                        item=new JMenuItem("Save chart as EPS...");
                        popup.add(item);
                        item.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                JFileChooser chooser=new JFileChooser(".");
                                chooser.setFileFilter(new FileFilter() {
                                    @Override
                                    public boolean accept(File f) {
                                        return f.isDirectory() || f.getName().toLowerCase().endsWith(".eps");
                                    }

                                    @Override
                                    public String getDescription() {
                                        return "EPS File";  //To change body of implemented methods use File | Settings | File Templates.
                                    }
                                });
                                int res=chooser.showSaveDialog(CostGraphComponent.this);
                                if (res==JFileChooser.APPROVE_OPTION){
                                    try{
                                        createEPS(chooser.getSelectedFile());
                                    }catch(Exception ex){}

                                }
                            }
                        });
                        popup.show(CostGraphComponent.this,e.getX(),e.getY());
                        return;
                    }                }
            });
        }
        private BufferedImage buffer;
        public void paintComponent(Graphics g){
            if (buffer==null || buffer.getWidth()!=getWidth() || buffer.getHeight()!=getHeight()){
                buffer=null;
                buffer=new BufferedImage(getWidth(),getHeight(),BufferedImage.TYPE_INT_RGB);
                update=true;
            }
            if (update){
                update=false;
                Graphics2D g2=(Graphics2D)buffer.getGraphics();
                drawGraph(g2);
            }
            g.drawImage(buffer,0,0,null);
        }
        private String superscript(String str) {
            str = str.replaceAll("0", "⁰");
            str = str.replaceAll("1", "¹");
            str = str.replaceAll("2", "²");
            str = str.replaceAll("3", "³");
            str = str.replaceAll("4", "⁴");
            str = str.replaceAll("5", "⁵");
            str = str.replaceAll("6", "⁶");
            str = str.replaceAll("7", "⁷");
            str = str.replaceAll("8", "⁸");
            str = str.replaceAll("9", "⁹");
            return str;
        }
        private void drawGraph(Graphics2D g2){
            g2.setColor(Color.WHITE);
            g2.fillRect(0,0,getWidth(),getHeight());

            if (costs!=null && !costs.isEmpty()){
                int xOff=50;
                int yOff=35;
                int width=getWidth()-75;
                int height=getHeight()-90;
                Collections.sort(costs,new Comparator<double[]>() {
                    @Override
                    public int compare(double[] o1, double[] o2) {
                        return (int)Math.signum(o1[0]-o2[0]);
                    }
                });
                long maxTime=Math.round(costs.get(costs.size()-1)[0]);

                double xScale=width/(double)(maxTime+1);

                double maxVal=-Double.MAX_VALUE;


                for (double[] l:costs){
                    maxVal=Math.max(maxVal,l[1]);
                }

                double yScale=height/maxVal;

                int bottomY=yOff+height;

                for (int x=1;x<costs.size();x++){
                    int pxp=(int)Math.round(xOff+xScale*costs.get(x-1)[0]);
                    int pyp=(int)Math.round(yOff+(height-yScale*(costs.get(x-1)[1])));

                    int xp=(int)Math.round(xOff+xScale*costs.get(x)[0]);
                    int yp=(int)Math.round(yOff+(height-yScale*(costs.get(x)[1])));
                    g2.setColor(new Color(50,220,50));
                    g2.fillPolygon(new int[]{xp,pxp,pxp,xp},new int[]{yp,pyp,yOff+height,yOff+height},4);

                    g2.setColor(Color.BLACK);
                    g2.drawLine(pxp,pyp,xp,yp);
                    //g2.fillRect(pxp,pyp,xp-pxp,bottomY-pyp);


                }

                g2.setColor(Color.RED);
                int yp=(int)Math.round(yOff+(height-yScale*maxVal));
                g2.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER, 2.0f, new float[]{2.0f}, 0.0f));
                g2.drawLine(xOff,yp,xOff+width,yp);


                FontMetrics fm=g2.getFontMetrics();
                g2.setColor(Color.BLACK);
                g2.setStroke(new BasicStroke(1));
                g2.drawLine(xOff-5,yOff+height,xOff+width+10,yOff+height);
                g2.fillPolygon(new int[]{xOff+width+10,xOff+width+5,xOff+width+5},new int[]{yOff+height,yOff+height-5,yOff+height+5},3);
                g2.drawString("t",xOff+width+10,yOff+height+15);

                long stepSize=1;
                long unitSize=1;
                long unitExp=1;
                if (maxTime>10){
                    stepSize=(long)Math.pow(10,Math.floor(Math.log10(maxTime))-1);
                    unitSize=stepSize;
                    unitExp=Math.round(Math.floor(Math.log10(maxTime))-1);
                }

                String unitString="x10";
                if (unitExp==0)unitString="";
                if (unitExp>1)unitString+=superscript(unitExp+"");
                Rectangle bounds=fm.getStringBounds(unitString,g2).getBounds();
                g2.drawString(unitString,(int)(xOff+width+10-bounds.getWidth()),yOff+height+20+fm.getHeight());

                while (maxTime/(double)stepSize>15)
                    stepSize*=2;



                for (long x=0;x<=maxTime-stepSize;x+=stepSize){
                    int pxp=(int)Math.round(xOff+xScale*x);
                    g2.drawLine(pxp,yOff+height-3,pxp,yOff+height+3);
                    bounds=fm.getStringBounds((x/unitSize)+"",g2).getBounds();
                    int tx=(int)Math.round(pxp-bounds.getWidth()/2);
                    int ty=(int)Math.round(yOff+height+3+bounds.getHeight());
                    g2.drawString((x/unitSize)+"",tx,ty);
                }
                //if (maxTime%stepSize!=0){
                int pxp=(int)Math.round(xOff+xScale*maxTime);
                g2.drawLine(pxp, yOff + height - 3, pxp, yOff + height + 3);
                bounds=fm.getStringBounds((maxTime/unitSize)+"",g2).getBounds();
                int tx=(int)Math.round(pxp-bounds.getWidth()/2);
                int ty=(int)Math.round(yOff+height+3+bounds.getHeight());
                g2.drawString((maxTime/unitSize)+"",tx,ty);

                //}

                g2.drawLine(xOff,yOff-10,xOff,yOff+height+5);
                g2.fillPolygon(new int[]{xOff,xOff+5,xOff-5},new int[]{yOff-10,yOff-5,yOff-5},3);
                //g2.drawString("n",xOff-15,yOff-10);


                stepSize=1;
                unitSize=1;
                unitExp=0;
                if (maxVal>10){
                    stepSize=(long)Math.pow(10,Math.floor(Math.log10(maxVal))-1);
                    unitSize=stepSize;
                    unitExp=Math.round(Math.max(0,Math.floor(Math.log10(maxVal))-1));
                }
                //System.out.println("E "+unitExp+" "+maxVal);
                unitString="c x10";
                if (unitExp<=0)unitString="n";
                if (unitExp>1)unitString+=superscript(unitExp+"");
                bounds=fm.getStringBounds(unitString,g2).getBounds();
                g2.drawString(unitString,(int)(xOff-15),yOff-15);


                for (long x=0;x<=maxVal-stepSize;x+=stepSize){
                    int pyp=(int)Math.round(yOff+height-yScale*x);
                    g2.drawLine(xOff-3,pyp,xOff+3,pyp);
                    bounds=fm.getStringBounds((x/unitSize)+"",g2).getBounds();
                    tx=(int)Math.round(xOff-5-bounds.getWidth());
                    ty=(int)Math.round(pyp+bounds.getHeight()/2);
                    g2.drawString((x/unitSize)+"",tx,ty);
                }
                //if (maxVal%stepSize!=0){
                int pyp=(int)Math.round(yOff+height-yScale*maxVal);
                g2.drawLine(xOff-3,pyp,xOff+3,pyp);
                bounds=fm.getStringBounds((maxVal/unitSize)+"",g2).getBounds();
                tx=(int)Math.round(xOff-5-bounds.getWidth());
                ty=(int)Math.round(pyp+bounds.getHeight()/2);
                g2.drawString((maxVal/unitSize)+"",tx,ty);

                //}



            }
        }
        public void createEPS(File file) throws Exception{
            EpsGraphics2D g2=new EpsGraphics2D(file.getName(),file,0,0,getWidth(),getHeight());
            //g2.setAccurateTextMode(false);
            drawGraph(g2);
            g2.flush();
            g2.close();
        }

    }
}
