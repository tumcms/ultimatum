/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import Scenario.GeometricResourceUsage;
import Scenario.Resource;
import Scenario.Task;

import javax.swing.*;
import java.util.HashMap;

public interface GUIListener {
    public Resource[] getResources();
    public String[] getMaps();
    public Task[] getTasks();

    public void createTask(Task t);
    public void deleteTask(Task t);
    public void updateTask(Task orig, Task mod);
    public void update();
    public void updateGeometry(Task t, GeometricResourceUsage[] res);
    public void setEnabled(boolean enabled);
    public void selectTask(String t, String source);
    public JFrame getParent();
}
