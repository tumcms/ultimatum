/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import EpsOutput.EpsGraphics2D;
import Scenario.GeometricResourceUsage;
import Scenario.Task;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.LinkedList;


public class GeometryViewer extends JComponent {
    private BufferedImage buffer, heatmap;
    private Task[] tasks;
    private String[] maps;
    private Task selectedTask;
    private String selectedMap;
    private GUIListener guiListener;
    private boolean updateMap,updateHeatMap;
    private String hover;
    private LinkedList<String> hoverText;
    private Point hoverPoint;
    private LinkedList<Rectangle> hoverRect;
    private double scale;
    private int xOff,yOff;
    private boolean showHeatMap;
    private boolean paintingMap,paintingHeatMap;

    public GeometryViewer(GUIListener guiListener) {
        this.guiListener = guiListener;
        this.updateMap=true;
        this.updateHeatMap=true;
        this.showHeatMap=true;
        this.paintingMap=false;
        this.paintingHeatMap=false;
        hoverRect=new LinkedList<Rectangle>();
        hoverText=new LinkedList<String>();
        MouseAdapter a=new MouseAdapter() {
            //long lastmove=0;
            @Override
            public void mouseClicked(MouseEvent e) {
                requestFocus();
                if (e.getButton()==MouseEvent.BUTTON3){
                    JPopupMenu popup=new JPopupMenu();
                    JMenuItem item=new JMenuItem("Save chart as PNG...");
                    popup.add(item);
                    item.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            JFileChooser chooser=new JFileChooser(".");
                            chooser.setFileFilter(new FileFilter() {
                                @Override
                                public boolean accept(File f) {
                                    return f.isDirectory() || f.getName().toLowerCase().endsWith(".png");
                                }

                                @Override
                                public String getDescription() {
                                    return "PNG File";  //To change body of implemented methods use File | Settings | File Templates.
                                }
                            });
                            int res=chooser.showSaveDialog(GeometryViewer.this);
                            if (res==JFileChooser.APPROVE_OPTION){
                                try{
                                    if (showHeatMap)
                                        ImageIO.write(heatmap, "png", chooser.getSelectedFile());
                                    else
                                        ImageIO.write(buffer, "png", chooser.getSelectedFile());
                                }catch(Exception ex){}

                            }
                        }
                    });
                    item=new JMenuItem("Save chart as EPS...");
                    popup.add(item);
                    item.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            JFileChooser chooser=new JFileChooser(".");
                            chooser.setFileFilter(new FileFilter() {
                                @Override
                                public boolean accept(File f) {
                                    return f.isDirectory() || f.getName().toLowerCase().endsWith(".eps");
                                }

                                @Override
                                public String getDescription() {
                                    return "EPS File";  //To change body of implemented methods use File | Settings | File Templates.
                                }
                            });
                            int res=chooser.showSaveDialog(GeometryViewer.this);
                            if (res==JFileChooser.APPROVE_OPTION){
                                try{
                                    createEPS(chooser.getSelectedFile());
                                }catch(Exception ex){}

                            }
                        }
                    });
                    popup.show(GeometryViewer.this,e.getX(),e.getY());
                    return;
                }

            }

            @Override
            public void mouseMoved(MouseEvent e) {
                if (tasks==null)
                    return;
                String oldhover = hover;
                hover = "";
                hoverRect.clear();
                hoverText.clear();
                for (Task t : tasks){
                    for (int x = 0; x < t.getResourceCount(); x++) {
                        if (t.getResource(x).isGeometric()) {
                            GeometricResourceUsage gru = (GeometricResourceUsage) t.getResource(x);
                            if (gru.getMap().equals(selectedMap)) {
                                if (gru.getArea().isInside((int)Math.round((e.getX()-xOff)/scale), (int)Math.round((e.getY()-yOff)/scale))) {
                                    hover += t.getName() + "\n";
                                    hoverText.add(t.getName());
                                    hoverRect.add(new Rectangle(
                                            (int)Math.round(gru.getArea().getMinX()*scale),
                                            (int)Math.round(gru.getArea().getMinY()*scale),
                                            (int)Math.round((gru.getArea().getMaxX()-gru.getArea().getMinX())*scale),
                                            (int)Math.round((gru.getArea().getMaxY()-gru.getArea().getMinY())*scale)
                                    ));
                                }
                            }
                        }
                    }
                }
                hoverPoint = e.getPoint();
                if (!hover.equals(oldhover)){
                    repaint();
                }
            }
        };
        addMouseListener(a);
        addMouseMotionListener(a);
    }

    public void update(){
        this.tasks=guiListener.getTasks();
        this.maps=guiListener.getMaps();
        this.selectedTask=null;
        if (maps.length>0)
            this.selectedMap=maps[0];
        else
            this.selectedMap=null;
        this.updateMap=true;
        this.updateHeatMap=true;
    }

    public void setShowHeatMap(boolean b){
        this.showHeatMap=b;
        repaint();
    }

    public void select(Task task){
        if (this.selectedTask==null || !this.selectedTask.equals(task)){
            this.selectedTask =task;
            repaint();
        }
    }

    public void select(String map){
        if (this.selectedMap==null || !this.selectedMap.equals(map)){
            this.selectedMap=map;
            this.updateHeatMap=true;
            this.updateMap=true;
            repaint();
        }
    }


    private void createHeatMap(Graphics2D g2){
        paintingHeatMap=true;
        updateHeatMap=false;
        //Graphics2D g2=(Graphics2D)heatmap.getGraphics();
        g2.setColor(Color.WHITE);
        g2.fillRect(0,0,getWidth(),getHeight());

        if (tasks!=null&&maps!=null){

            int minx=Integer.MAX_VALUE;
            int maxx=-Integer.MAX_VALUE;
            int miny=Integer.MAX_VALUE;
            int maxy=-Integer.MAX_VALUE;

            for (Task t:tasks){
                for (int x=0;x<t.getResourceCount();x++){
                    if (t.getResource(x).isGeometric()){
                        GeometricResourceUsage gru=(GeometricResourceUsage)t.getResource(x);
                        if (gru.getMap().equals(selectedMap)){
                            minx=Math.min(gru.getArea().getMinX(),minx);
                            maxx=Math.max(gru.getArea().getMaxX(), maxx);
                            miny=Math.min(gru.getArea().getMinY(),miny);
                            maxy=Math.max(gru.getArea().getMaxY(), maxy);
                        }
                    }
                }
            }

            xOff=20;
            yOff=20;
            int width=getWidth()-40;
            int height=getHeight()-40;
            scale=Math.min(width/(double)(maxx-minx),height/(double)(maxy-miny));
            long maxUse=0;

            long[][] map=new long[getWidth()][getHeight()];

            for (int x=0;x<getWidth();x++){
                for (int y=0;y<getHeight();y++){
                    map[x][y]=0;
                }
            }
            for (Task t:tasks){
                for (int r=0;r<t.getResourceCount();r++){
                    if (t.getResource(r).isGeometric()){
                        GeometricResourceUsage gru=(GeometricResourceUsage)t.getResource(r);
                        if (gru.getMap().equals(selectedMap)){
                            minx=(int)Math.round(gru.getArea().getMinX()*scale);
                            maxx=(int)Math.round(gru.getArea().getMaxX()*scale);
                            miny=(int)Math.round(gru.getArea().getMinY()*scale);
                            maxy=(int)Math.round(gru.getArea().getMaxY()*scale);
                            for (int x=minx;x<maxx;x++){
                                for (int y=miny;y<maxy;y++){
                                    map[x][y]+=t.getDuration();
                                    maxUse=Math.max(maxUse,map[x][y]);
                                }
                            }
                        }
                    }
                }
            }

            for (int x=0;x<getWidth();x++){
                for (int y=0;y<getHeight();y++){
                    if (map[x][y]>0){
                        int col=(int)Math.round(255*map[x][y]/(double)maxUse);
                        g2.setColor(new Color(col,255-col,0));
                        g2.fillRect(xOff+x,yOff+y,1,1);
                    }
                }
            }

        }
        paintingHeatMap=false;
        repaint();
    }

    private void createMap(Graphics2D g2){
        paintingMap=true;
        updateMap=false;
        //Graphics2D g2=(Graphics2D)buffer.getGraphics();
        g2.setColor(Color.WHITE);
        g2.fillRect(0,0,getWidth(),getHeight());

        if (tasks!=null&&maps!=null){

            int minx=Integer.MAX_VALUE;
            int maxx=-Integer.MAX_VALUE;
            int miny=Integer.MAX_VALUE;
            int maxy=-Integer.MAX_VALUE;

            for (Task t:tasks){
                for (int x=0;x<t.getResourceCount();x++){
                    if (t.getResource(x).isGeometric()){
                        GeometricResourceUsage gru=(GeometricResourceUsage)t.getResource(x);
                        if (gru.getMap().equals(selectedMap)){
                            minx=Math.min(gru.getArea().getMinX(),minx);
                            maxx=Math.max(gru.getArea().getMaxX(), maxx);
                            miny=Math.min(gru.getArea().getMinY(),miny);
                            maxy=Math.max(gru.getArea().getMaxY(), maxy);
                        }
                    }
                }
            }

            xOff=20;
            yOff=20;
            int width=getWidth()-40;
            int height=getHeight()-40;
            scale=Math.min(width/(double)(maxx-minx),height/(double)(maxy-miny));

            for (Task t:tasks){
                for (int x=0;x<t.getResourceCount();x++){
                    if (t.getResource(x).isGeometric()){
                        GeometricResourceUsage gru=(GeometricResourceUsage)t.getResource(x);
                        if (gru.getMap().equals(selectedMap)){
                            int xp=(int)Math.round(xOff+gru.getArea().getMinX()*scale);
                            int yp=(int)Math.round(yOff+gru.getArea().getMinY()*scale);
                            int wp=(int)Math.round((gru.getArea().getMaxX()-gru.getArea().getMinX())*scale);
                            int hp=(int)Math.round((gru.getArea().getMaxY()-gru.getArea().getMinY())*scale);
                            g2.setStroke(new BasicStroke(1));
                            g2.setColor(t.getColor());
                            g2.drawRect(xp,yp,wp,hp);
                        }
                    }
                }
            }
        }
        paintingMap=false;
        repaint();
    }

    public void paintComponent(Graphics g){

        if (showHeatMap){
            if (heatmap==null || heatmap.getWidth()!=getWidth() || heatmap.getHeight()!=getHeight()){
                heatmap=null;
                heatmap=new BufferedImage(getWidth(),getHeight(),BufferedImage.TYPE_INT_RGB);
                updateHeatMap=true;
            }
            if (updateHeatMap&&!paintingHeatMap){
                new Thread(){
                    public void run(){
                        createHeatMap((Graphics2D)heatmap.getGraphics());
                    }
                }.start();
                g.setColor(Color.WHITE);
                g.fillRect(0,0,getWidth(),getHeight());
                g.setColor(Color.BLACK);
                g.drawString("Please wait...", 20, 50);
                return;
            }
            if (paintingHeatMap){
                g.setColor(Color.WHITE);
                g.fillRect(0,0,getWidth(),getHeight());
                g.setColor(Color.BLACK);
                g.drawString("Please wait...",20,50);
                return;
            }

            g.drawImage(heatmap,0,0,null);
        }
        else{
            if (buffer==null || buffer.getWidth()!=getWidth() || buffer.getHeight()!=getHeight()){
                buffer=null;
                buffer=new BufferedImage(getWidth(),getHeight(),BufferedImage.TYPE_INT_RGB);
                updateMap=true;
            }
            if (updateMap&&!paintingMap){
                new Thread(){
                    public void run(){
                        createMap((Graphics2D)buffer.getGraphics());
                    }
                }.start();
                return;
            }
            if (paintingMap){
                g.setColor(Color.WHITE);
                g.fillRect(0,0,getWidth(),getHeight());
                g.setColor(Color.BLACK);
                g.drawString("Please wait...",20,50);
                return;
            }
            g.drawImage(buffer,0,0,null);
        }

        if (selectedTask!=null){
            Graphics2D g3=(Graphics2D)g;
            for (int x=0;x<selectedTask.getResourceCount();x++){
                if (selectedTask.getResource(x).isGeometric()){
                    GeometricResourceUsage gru=(GeometricResourceUsage)selectedTask.getResource(x);
                    if (gru.getMap().equals(selectedMap)){
                        int xp=(int)Math.round(xOff+gru.getArea().getMinX()*scale);
                        int yp=(int)Math.round(yOff+gru.getArea().getMinY()*scale);
                        int wp=(int)Math.round((gru.getArea().getMaxX()-gru.getArea().getMinX())*scale);
                        int hp=(int)Math.round((gru.getArea().getMaxY()-gru.getArea().getMinY())*scale);
                        g3.setStroke(new BasicStroke(2));
                        g3.setColor(Color.BLUE);
                        g3.drawRect(xp,yp,wp,hp);
                    }
                }
            }
        }


        if (hover!=null && !hover.isEmpty()){
            Graphics2D g2=(Graphics2D)g;

            g2.setColor(Color.BLACK);
            g2.setStroke(new BasicStroke(2));
            for (Rectangle r:hoverRect){
                g2.drawRect(xOff+r.x,yOff+r.y,r.width,r.height);
            }
            g2.setStroke(new BasicStroke(1));
            FontMetrics fm=g2.getFontMetrics();

            int maxWidth=0;
            for (String s:hoverText){
                maxWidth=(int)Math.round(Math.max(maxWidth, fm.getStringBounds(s, g2).getWidth()));
            }

            int h=hoverText.size()*fm.getHeight();
            g2.setColor(new Color(200,200,200,180));
            g2.fillRect(hoverPoint.x,hoverPoint.y,maxWidth+20,h+20);
            g2.setColor(Color.BLACK);
            g2.drawRect(hoverPoint.x,hoverPoint.y,(int)maxWidth+20,h+20);

            int x=0;
            for (String s:hoverText){
                g2.drawString(s, hoverPoint.x+10,hoverPoint.y+h+5-(x++)*fm.getHeight());
            }

        }
    }

    public void createEPS(File file) throws Exception{
        EpsGraphics2D g2=new EpsGraphics2D(file.getName(),file,0,0,getWidth(),getHeight());
        //g2.setAccurateTextMode(false);
        if (showHeatMap)
            createHeatMap(g2);
        else
            createMap(g2);
        g2.flush();
        g2.close();
    }
}
