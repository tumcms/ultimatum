/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import Main.Version;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.util.ArrayList;

public class OptimizationStatus extends JFrame {
    private OptimizingAnimation animation;
    private OptimizationStatusGraph graph;
    private ArrayList<double[]> data;
    private JButton stopButton;
    private JLabel timeLabel,optimumLabel,simLabel,progressLabel;
    private JProgressBar progressbar;
    private long starttime,simCount, optimum;

    public OptimizationStatus(JFrame parent,ActionListener stopListener){
        super(Version.ULTIMATUM_VERSION+" - Optimization Status");
        this.data=new ArrayList<double[]>();
        this.simCount=0;
        this.optimum=Long.MAX_VALUE;
        graph=new OptimizationStatusGraph();
        animation=new OptimizingAnimation();
        stopButton=new JButton("STOP");
        setLayout(new BorderLayout());
        JPanel mainPanel=new JPanel(new BorderLayout());
        mainPanel.add(animation,BorderLayout.NORTH);
        mainPanel.add(graph,BorderLayout.CENTER);
        add(mainPanel,BorderLayout.CENTER);
        JPanel bottomPanel=new JPanel(new BorderLayout());

        JPanel progessPanel=new JPanel(new GridLayout(2,1));
        progressLabel=new JLabel("Idling...");
        progressbar=new JProgressBar(0,1000);
        progressbar.setValue(0);
        progessPanel.add(progressLabel);
        progessPanel.add(progressbar);
        bottomPanel.add(progessPanel, BorderLayout.NORTH);


        JPanel buttonPanel=new JPanel(new GridLayout(4,1));

        JPanel labelPanel=new JPanel(new GridLayout(1,2));
        labelPanel.add(new JLabel("Min:  ",JLabel.RIGHT));
        optimumLabel=new JLabel("-");
        labelPanel.add(optimumLabel);
        buttonPanel.add(labelPanel);

        labelPanel=new JPanel(new GridLayout(1,2));
        labelPanel.add(new JLabel("Sims:  ",JLabel.RIGHT));
        simLabel=new JLabel("-");
        labelPanel.add(simLabel);
        buttonPanel.add(labelPanel);
        labelPanel=new JPanel(new GridLayout(1,2));
        labelPanel.add(new JLabel("Time:  ",JLabel.RIGHT));
        timeLabel=new JLabel("0:00");
        labelPanel.add(timeLabel);
        buttonPanel.add(labelPanel);

        buttonPanel.add(stopButton);


        bottomPanel.add(buttonPanel, BorderLayout.SOUTH);
        add(bottomPanel, BorderLayout.SOUTH);
        stopButton.addActionListener(stopListener);
        final ActionListener stopListener1=stopListener;
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                stopListener1.actionPerformed(null);
            }
        });
    }

    public void setOptimizing(boolean optimizing){
        animation.setAnim(optimizing);
        if (optimizing){
            this.optimum=Long.MAX_VALUE;
            starttime=System.currentTimeMillis();
            setSize(400,600);
            setResizable(false);
            setVisible(true);
            revalidate();
            repaint();
            requestFocus();
            toFront();
            new ClockThread().start();
        }
    }

    class ClockThread extends Thread{
        public void run(){
            while(animation.isAnim()){
                try{Thread.sleep(100);}catch (Exception ex){}
                long time=(System.currentTimeMillis()-starttime);
                int h=(int)((time/1000)/3600);
                int m=(int)(((time/1000)%3600)/60);
                int s=(int)((time/1000)%60);
                String ts="";
                if (h>0)ts=h+":";
                if (m<10)ts+="0";
                ts+=m+":";
                if (s<10)ts+="0";
                ts+=s;
                if (simCount>0)
                    ts+=" ( "+(Math.round(100*time/(double)simCount)/100.0)+"ms/Sim )";
                if (!timeLabel.getText().equals(ts)){
                    timeLabel.setText(ts);
                }

            }
        }
    }

    public void setOptimum(long optimum){
        this.optimum=optimum;
        optimumLabel.setText(optimum+"");
        long time=(System.currentTimeMillis()-starttime);
        int h=(int)((time/1000)/3600);
        int m=(int)(((time/1000)%3600)/60);
        int s=(int)((time/1000)%60);
        String ts="";
        if (h>0)ts=h+":";
        if (m<10)ts+="0";
        ts+=m+":";
        if (s<10)ts+="0";
        ts+=s;
        System.out.println("[ "+ts+" ] new opt: "+optimum);
    }

    public void addData(long duration){
        if (data.isEmpty()||data.get(data.size()-1)[1]!=duration){
            data.add(new double[]{data.size(),duration});
            graph.setData(data);
        }
    }

    public long getOptimum(){
        return optimum;
    }

    public void setSimCount(long c){
        simCount=c;
        simLabel.setText(c+"");
    }

    public void setProgressTitle(String title){
        progressLabel.setText("  " + title);
        long time=(System.currentTimeMillis()-starttime);
        int h=(int)((time/1000)/3600);
        int m=(int)(((time/1000)%3600)/60);
        int s=(int)((time/1000)%60);
        String ts="";
        if (h>0)ts=h+":";
        if (m<10)ts+="0";
        ts+=m+":";
        if (s<10)ts+="0";
        ts+=s;
        System.out.println("[ "+ts+" ] "+title);
    }

    public void setProgress(int percent){
        progressbar.setValue(percent);
    }


}
