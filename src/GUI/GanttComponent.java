/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package GUI;

import EpsOutput.EpsGraphics2D;
import Scenario.ResourceUsage;
import Scenario.Task;
import Scenario.TaskDependency;
import Scenario.TaskSwap;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.QuadCurve2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;


public class GanttComponent extends JComponent{

    private ArrayList<GanttElement> elements;
    private BufferedImage buffer,overlay;
    private long maxTime;
    //private JScrollPane pane;
    //private ArrayList<Long> ticks;
    private boolean update;
    private HashMap<String,Rectangle> positions;
    private HashMap<String,Task> tasks;
    private HashSet<String> deletedTasks;
    private HashMap<String,Color> resourceColors;
    private int resourceColorCount;

    private ReentrantLock bufferLock;

    private String selected;

    private ActionListener actionListener;

    private HashMap<String,LinkedList<TaskSwap>> swaps;

    private boolean showSingleSwap;

    private boolean resourceColoring;

    private int viewmode;

    public static final int VIEWMODE_NONE=0;
    public static final int VIEWMODE_ALL_DEPS=1;
    public static final int VIEWMODE_ALL_SWAPS=2;

    private static final Color[] COLORS=new Color[]{Color.RED,Color.BLUE,Color.ORANGE,Color.CYAN,Color.MAGENTA,Color.GREEN,Color.PINK,Color.YELLOW,new Color(140,140,0),new Color(0,200,0),Color.BLACK, Color.DARK_GRAY};
    private JComponent parent;

    private double zoom=0;

    public GanttComponent(JComponent parent){
        this.parent=parent;
        this.positions=new HashMap<String, Rectangle>();
        this.tasks=new HashMap<String, Task>();
        this.resourceColors=new HashMap<String, Color>();
        this.resourceColorCount=0;
        this.resourceColoring=false;
        this.deletedTasks=new HashSet<String>();
        this.bufferLock=new ReentrantLock();
        this.swaps=new HashMap<String, LinkedList<TaskSwap>>();
        this.viewmode=VIEWMODE_NONE;
        reset();
        MouseAdapter mouseListener=new MouseListener();
        addMouseListener(mouseListener);
        setFocusable(true);
        setEnabled(true);
    }

    public void setZoom(double zoom){
        this.zoom=zoom;
        this.update=true;
        repaint();
    }

    public void setActionListener(ActionListener actionListener){
        this.actionListener=actionListener;
    }



    //private class GanttPanel extends JPanel {
    public void reset(){
        this.elements=new ArrayList<GanttElement>();
        this.deletedTasks.clear();
        this.resourceColorCount=0;
        this.resourceColors.clear();
        //this.ticks=new ArrayList<Long>();
        this.selected=null;
        this.tasks.clear();
        this.positions.clear();
        this.maxTime=0;
        this.update=true;

        this.swaps.clear();
        this.showSingleSwap=false;
        //setPreferredSize(new Dimension(100,100));
        //setMinimumSize(new Dimension(100,100));
        //updateUI();

    }

    public void setViewmode(int viewmode){
        if (this.viewmode!=viewmode){
            this.viewmode=viewmode;
            this.update=true;
            repaint();
        }


    }

    public void setResourceColors(boolean r){
        if (this.resourceColoring!=r){
            this.resourceColoring=r;
            update=true;
            repaint();
        }
    }

    class MouseListener extends MouseAdapter{
        @Override
        public void mouseClicked(MouseEvent e) {
            requestFocus();
            if (e.getButton()==MouseEvent.BUTTON3){
                JPopupMenu popup=new JPopupMenu();
                JMenuItem item=new JMenuItem("Save chart as PNG...");
                popup.add(item);
                item.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        JFileChooser chooser=new JFileChooser(".");
                        chooser.setFileFilter(new FileFilter() {
                            @Override
                            public boolean accept(File f) {
                                return f.isDirectory() || f.getName().toLowerCase().endsWith(".png");
                            }

                            @Override
                            public String getDescription() {
                                return "PNG File";  //To change body of implemented methods use File | Settings | File Templates.
                            }
                        });
                        int res=chooser.showSaveDialog(GanttComponent.this);
                        if (res==JFileChooser.APPROVE_OPTION){
                            try{
                                ImageIO.write(buffer, "png", chooser.getSelectedFile());
                            }catch(Exception ex){}

                        }
                    }
                });
                item=new JMenuItem("Save chart as EPS...");
                popup.add(item);
                item.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        JFileChooser chooser=new JFileChooser(".");
                        chooser.setFileFilter(new FileFilter() {
                            @Override
                            public boolean accept(File f) {
                                return f.isDirectory() || f.getName().toLowerCase().endsWith(".eps");
                            }

                            @Override
                            public String getDescription() {
                                return "EPS File";  //To change body of implemented methods use File | Settings | File Templates.
                            }
                        });
                        int res=chooser.showSaveDialog(GanttComponent.this);
                        if (res==JFileChooser.APPROVE_OPTION){
                            try{
                                createEPS(chooser.getSelectedFile());
                            }catch(Exception ex){}

                        }
                    }
                });
                popup.show(GanttComponent.this,e.getX(),e.getY());
                return;
            }
            if (e.getButton()==MouseEvent.BUTTON1){
                validate();
                showSingleSwap=e.getClickCount()%2==0;
                boolean found=false;
                for (String t:positions.keySet()){
                    Rectangle r=positions.get(t);
                    if (e.getY()>=r.getY()&&e.getY()<=r.getY()+r.getHeight()){
                        found=true;
                        selected=t;

                        repaint();
                        break;
                    }
                }
                if (!found){
                    selected=null;
                    repaint();
                }
                if (actionListener!=null)
                    actionListener.actionPerformed(null);
            }
        }


    }

    public void addData(Task task, long start, Color color, boolean error){
        //System.out.println(name+" x "+start.toString()+" - "+end.toString());
        long end=start+task.getDuration();
        elements.add(new GanttElement(task.getName(),start,end,task.cloneDependencies(),color,error));
        tasks.put(task.getName(),task);
        if (end>maxTime){
            maxTime=end;
        }
        Collections.sort(elements, new Comparator<GanttElement>(){
            @Override
            public int compare(GanttElement o1, GanttElement o2) {
                int res=(int)Math.signum(o1.getStart()-o2.getStart());
                if (res==0){
                    return (int)Math.signum(o1.getEnd()-o2.getEnd());
                }
                return res;
            }
        });
        /*if (!ticks.contains(new Long(start.toLong()))){
            ticks.add(start.toLong());
        }*/
        //if (!ticks.contains(new Long(end))){
        //    ticks.add(end);
        //}
        for (ResourceUsage rU:task.cloneResources()){
            if (!rU.isGeometric()){
                if (!resourceColors.containsKey(rU.getType())){
                    resourceColorCount+=1;
                    Color c;
                    if (resourceColorCount>=COLORS.length){
                        Random rnd=new Random();
                        c=new Color(rnd.nextInt(255),rnd.nextInt(255),rnd.nextInt(255));
                    }
                    else
                        c=COLORS[resourceColorCount];
                    resourceColors.put(rU.getType(),c);
                }
            }
        }

        update=true;
        repaint();
    }

    public void setSwaps(LinkedList<TaskSwap> s){
        swaps.clear();
        for (TaskSwap t:s){
            if (swaps.containsKey(t.getTaskA())){
               swaps.get(t.getTaskA()).add(t);
            }
            else{
                LinkedList<TaskSwap> newSwaps=new LinkedList<TaskSwap>();
                newSwaps.add(t);
                swaps.put(t.getTaskA(),newSwaps);
            }
        }
    }


    public void createEPS(File file) throws Exception{
        EpsGraphics2D g2=new EpsGraphics2D(file.getName(),file,0,0,getWidth(),getHeight());
        //g2.setAccurateTextMode(false);
        draw(g2);
        g2.flush();
        g2.close();
    }

    public void paintComponent(Graphics g3){

        Graphics2D g2=(Graphics2D)g3;
        bufferLock.lock();
        try{
            if (buffer==null || buffer.getWidth()!=getWidth() || buffer.getHeight()!=getHeight()){
                buffer=null;
                buffer=new BufferedImage(getWidth(),getHeight(),BufferedImage.TYPE_INT_RGB);
                update=true;
            }
            if (update){

                Graphics2D g=(Graphics2D)buffer.getGraphics();

                draw(g);



            }
            g2.drawImage(buffer,0,0,null);

            if (selected!=null){
                Task t=tasks.get(selected);
                if (t!=null && positions!=null){
                    Rectangle tPos=positions.get(t.getName());
                    if (tPos!=null){
                        if (showSingleSwap){
                            LinkedList<TaskSwap> tSwaps=swaps.get(t.getName());
                            if (tSwaps!=null)
                                drawSwaps(tSwaps,tPos,g2,true);
                        }
                        else{
                            drawDependencies(t,tPos,g2,true);

                        }
                        g2.setColor(Color.BLUE);
                        g2.setStroke(new BasicStroke(2));
                        g2.drawRect(tPos.x,tPos.y,tPos.width,tPos.height);
                    }
                }
            }

        }
        catch (ConcurrentModificationException ex){}
        bufferLock.unlock();
    }


    private void draw(Graphics2D g){
        double elementHeight=(getHeight()-50)/(double)elements.size();


        update=false;
        positions.clear();

        g.setColor(Color.WHITE);
        g.fillRect(0,0,getWidth(),getHeight());
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);





        double scale=1;

        int counter=0;
        FontMetrics fM=g.getFontMetrics();

        int maxWidth=0;
        for (GanttElement element:elements){
            maxWidth=Math.max(maxWidth,(int)Math.ceil(fM.getStringBounds(element.getName(),g).getWidth()));
        }
        long minTime=Long.MAX_VALUE;
        for (GanttElement element:elements){
            long l=element.getEnd()-element.getStart();
            if (l>0)
                minTime=Math.min(minTime,l);
        }




        int minWidth=(int)Math.ceil((parent.getWidth()-20)*(1-zoom)+zoom*2048);



        if (elementHeight!=15||getWidth()!=minWidth){//||getWidth()<3*elements.size()){
            int minHeight=15*elements.size()+50;

            //minWidth=Math.min(minWidth,2048);

            if ((getPreferredSize().getHeight()!=minHeight && getMinimumSize().getHeight()!=minHeight) || (getPreferredSize().getWidth()!=minWidth && getMinimumSize().getWidth()!=minWidth)){
                setPreferredSize(new Dimension(minWidth,minHeight));
                setMinimumSize(new Dimension(minWidth,minHeight));
                revalidate();
                repaint();
                //return;
            }
        }

        if (maxTime>0)
            scale=(getWidth()-100-maxWidth)/(double)maxTime;

        g.setColor(new Color(200,200,200));
        g.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER, 2.0f, new float[]{2.0f}, 0.0f));

                /*for (Long tick:ticks){
                    int xp=(int)Math.round(maxWidth+ 10 + scale * tick);
                    g.drawLine(xp,0,xp,getHeight());
                } */

        for (GanttElement element:elements){
            int xp=(int)Math.round(maxWidth+ 90 + scale * element.getStart());
            int w=(int)Math.round(scale*(element.getEnd()-element.getStart()));
            int yp=(int)Math.round(25+counter*elementHeight);
            int h=(int)Math.round(elementHeight-4);

            positions.put(element.getName(),new Rectangle(xp,yp,w,h));

            if (resourceColoring){
                Task t=tasks.get(element.getName());

                ResourceUsage[] rU=t.cloneResources();
                int count=0;
                for (int x=0;x<rU.length;x++){
                    if (!rU[x].isGeometric())
                        count++;
                }
                if (count>0){
                    double width=w/(double)count;
                    int w2=(int)Math.round(width);
                    int y=0;
                    for (int x=0;x<rU.length;x++){
                        if (!rU[x].isGeometric()){
                            int left=(int)Math.round(xp+y*width);
                            int right=(int)Math.round(xp+(y+1)*width);

                            if (right-xp>w){
                                right=xp+w;
                            }
                            //if ((y+1)*width>w){
                            //    width=w-(y*width);
                            //}
                            g.setColor(resourceColors.get(rU[x].getType()));
                            g.fillRect(left,yp,right-left,h);
                            if (y>0){
                                g.setStroke(new BasicStroke(1));
                                g.setColor(Color.BLACK);
                                g.drawLine(left,yp,left,yp+h);
                            }

                            y++;
                        }
                    }
                }
                else{
                    g.setColor(Color.WHITE);
                    g.fillRect(xp,yp,w,h);

                }
                g.setColor(element.getColor());
            }
            else{
                g.setColor(element.getColor());
                g.fillRect(xp,yp,w,h);
            }
            if (counter%2==0)
                g.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER, 5.0f, new float[]{5.0f}, 0.0f));
            else
                g.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER, 5.0f, new float[]{5.0f,2.0f,1.0f,3.0f}, 0.0f));
            int textWidth=(int)fM.getStringBounds(element.getName(),g).getWidth();
            g.drawLine(6+textWidth,yp+h/2,xp,yp+h/2);
            g.setStroke(new BasicStroke(1));

            g.setColor(Color.BLACK);
            if (element.getError()){
                g.setStroke(new BasicStroke(3));
                g.setColor(Color.RED);
                g.drawRect(xp-1, yp-1, w+2, h+2);
                g.setColor(Color.BLACK);
                g.setStroke(new BasicStroke(1));
            }
            g.drawRect(xp, yp, w, h);
            g.drawString(element.getName(),5,yp+h/2+fM.getHeight()/2-2);

            if (deletedTasks.contains(element.getName())){
                g.setStroke(new BasicStroke(3));
                g.setColor(Color.RED);
                g.drawLine(xp-2,yp-2,xp+w+2,yp+h+2);
                g.drawLine(xp-2,yp+h+2,xp+w+2,yp-2);
                g.setStroke(new BasicStroke(1));
                g.setColor(Color.WHITE);
                g.drawLine(xp-2,yp-2,xp+w+2,yp+h+2);
                g.drawLine(xp-2,yp+h+2,xp+w+2,yp-2);
            }



            counter++;
        }
        if (viewmode==VIEWMODE_ALL_DEPS||viewmode==VIEWMODE_ALL_SWAPS){
            for (GanttElement element:elements){
                if (viewmode==VIEWMODE_ALL_DEPS){
                    drawDependencies(tasks.get(element.getName()),positions.get(element.getName()),g,false);
                }
                if (viewmode==VIEWMODE_ALL_SWAPS){
                    LinkedList<TaskSwap> s=swaps.get(element.getName());
                    if (s!=null)
                        drawSwaps(s,positions.get(element.getName()),g,false);
                }

            }
        }

    }




    private void drawSwaps(LinkedList<TaskSwap> tSwaps, Rectangle tPos, Graphics2D g2, boolean highlight){
        for (TaskSwap s:tSwaps){
            if (s.isAntiDelay()){
                g2.setColor(Color.RED);
                //int sx= tPos.x-10;
                int sy=tPos.y+ tPos.height/2;
                g2.fillPolygon(new int[]{tPos.x+tPos.width,tPos.x+tPos.width+5,tPos.x+tPos.width+5},new int[]{sy,sy-5,sy+5},3);
                g2.fillPolygon(new int[]{tPos.x+tPos.width+5,tPos.x+tPos.width+10,tPos.x+tPos.width+10},new int[]{sy,sy-5,sy+5},3);
                g2.drawLine(tPos.x+tPos.width+10,sy,tPos.x+tPos.width+30,sy);
            }
            else if (s.isDelay()){
                g2.setColor(new Color(0,160,0));
                int sy=tPos.y+ tPos.height/2;
                g2.fillPolygon(new int[]{tPos.x,tPos.x-5,tPos.x-5},new int[]{sy,sy-5,sy+5},3);
                g2.fillPolygon(new int[]{tPos.x-5,tPos.x-10,tPos.x-10},new int[]{sy,sy-5,sy+5},3);
                g2.drawLine(tPos.x-10,sy,tPos.x-30,sy);
            }
            Rectangle depPos=positions.get(s.getTaskB());
            if (depPos!=null){
                int ex=depPos.x-10;//+depPos.width;
                int ey=depPos.y+depPos.height/2;
                int sx= tPos.x-10;
                int sy=tPos.y+ tPos.height/2;
                if (ex-sx<3)sx-=3;
                g2.setStroke(new BasicStroke(1));
                g2.setColor(Color.BLACK);
                if (!highlight)
                    g2.setColor(new Color(0,0,0,128));

                CubicCurve2D.Double curve=new CubicCurve2D.Double(
                        sx,sy,Math.max(30,Math.min(sx,ex)-80),sy+1.0*(ey-sy)/8.0,Math.min(sx,ex)-80,sy+7.0*(ey-sy)/8.0,ex,ey
                );
                g2.draw(curve);

                g2.drawLine(sx, sy, tPos.x, sy);
                //g2.drawLine(sx, sy, sx, ey);
                //g2.drawLine(sx, ey, ex, ey);
                g2.drawLine(ex, ey, depPos.x, ey);

                //g2.drawLine(sx, sy, tPos.x, sy);
                //g2.drawLine(sx, sy, sx, ey);
                //g2.drawLine(sx, ey, ex, ey);
                g2.setStroke(new BasicStroke(1));
                ex=depPos.x;
                g2.fillPolygon(new int[]{tPos.x,tPos.x-5,tPos.x-5},new int[]{sy,sy-5,sy+5},3);
                g2.fillPolygon(new int[]{ex,ex-5,ex-5},new int[]{ey,ey-5,ey+5},3);
                if (highlight){
                    g2.setColor(Color.GREEN);
                    g2.setStroke(new BasicStroke(2));
                    g2.drawRect(depPos.x, depPos.y, depPos.width, depPos.height);
                }
            }

        }
    }

    private void drawDependencies(Task t,Rectangle tPos,Graphics2D g2, boolean highlight){
        for (int x=0;x<t.getDependencyCount();x++){
            TaskDependency dep=t.getDependency(x);
            Rectangle depPos=positions.get(dep.getTask());
            if (depPos!=null){
                int sx=depPos.x-10;//+depPos.width;
                int sy=depPos.y+depPos.height/2;
                int ex= tPos.x-10;
                int ey=tPos.y+ tPos.height/2;
                if (ex-sx<3)sx-=3;
                g2.setStroke(new BasicStroke(1));
                g2.setColor(Color.BLACK);
                if (!highlight)
                    g2.setColor(new Color(0,0,0,128));

                CubicCurve2D.Double curve=new CubicCurve2D.Double(
                        sx,sy,Math.max(30,Math.min(sx,ex)-80),sy+1.0*(ey-sy)/8.0,Math.min(sx,ex)-80,sy+7.0*(ey-sy)/8.0,ex,ey
                );
                g2.draw(curve);

                g2.drawLine(sx, sy, depPos.x, sy);
                //g2.drawLine(sx, sy, sx, ey);
                //g2.drawLine(sx, ey, ex, ey);
                g2.drawLine(ex, ey, tPos.x, ey);

                g2.setStroke(new BasicStroke(1));
                ex=tPos.x;
                g2.fillPolygon(new int[]{ex,ex-5,ex-5},new int[]{ey,ey-5,ey+5},3);
                if (highlight){
                    if (dep.getType()==TaskDependency.TYPE_FS)
                        g2.setColor(Color.GREEN);
                    else
                        g2.setColor(Color.MAGENTA);
                    g2.setStroke(new BasicStroke(2));
                    g2.drawRect(depPos.x, depPos.y, depPos.width, depPos.height);
                }
            }
        }

    }

    //}

    public String getSelected(){
        return selected;
    }

    public void setSelected(String t){
        selected=t;
        repaint();
    }

    public long getMaxTime(){
        return maxTime;
    }

    public void deleteTask(Task t){
        deletedTasks.add(t.getName());
        update=true;
        repaint();

    }

    private class GanttElement{
        private long start,end;
        private String name;
        private Color color;
        private TaskDependency[] deps;
        private boolean error;

        private GanttElement(String name,long start, long end, TaskDependency[] deps, Color color, boolean error) {
            this.start = start;
            this.end = end;
            this.name=name;
            this.deps=deps;
            this.error=error;
            if (color==null)
                this.color=Color.RED;
            else
                this.color=color;
        }

        public long getStart() {
            return start;
        }

        public long getEnd() {
            return end;
        }

        public String getName() {
            return name;
        }

        public Color getColor() {
            return color;
        }

        private TaskDependency[] getDeps() {
            return deps;
        }

        public boolean getError(){
            return error;
        }
    }
}
