/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Scenario;

import java.awt.*;

public class Task {
    private String name;
    private String provides;
    private ResourceUsage[] resources;
    private TaskDependency[] dependencies;
    private long duration;
    private double priority;
    private Color color;
    private double cost;



    public Task(String name,String provides, ResourceUsage[] resources, TaskDependency[] dependencies, long duration, double priority, double cost, Color color) {
        this.name = name;
        this.provides=provides;
        this.resources = resources;
        this.dependencies = dependencies;
        this.duration = duration;
        this.priority = priority;
        this.color = color;
        this.cost=cost;
    }

    public Task(String name, ResourceUsage[] resources, TaskDependency[] dependencies, long duration, double priority, double cost, Color color) {
        this(name,name,resources,dependencies,duration,priority,cost,color);
    }

    public Task(String name, ResourceUsage[] resources, TaskDependency[] dependencies, long duration, Color color) {
        this(name,resources,dependencies,duration,1,1,color);
    }

    public Task(String name, String provides, ResourceUsage[] resources, TaskDependency[] dependencies, long duration, Color color) {
        this(name,provides,resources,dependencies,duration,1,1,color);
    }



    public String getName() {
        return name;
    }

    public double getPriority() {
        return priority;
    }

    public double getCost() {
        return cost;
    }

    public String getProvides() {
        return provides;
    }

    public Color getColor() {
        return color;
    }

    public long getDuration() {
        return duration;
    }

    public int getDependencyCount(){
        return dependencies.length;
    }

    public TaskDependency getDependency(int i){
        return dependencies[i];
    }

    public TaskDependency[] cloneDependencies(){
        TaskDependency[] out=new TaskDependency[dependencies.length];
        for (int x=0;x<dependencies.length;x++)
            out[x]=dependencies[x];
        return out;
    }

    public int getResourceCount(){
        return resources.length;
    }

    public ResourceUsage getResource(int i){
        return resources[i];
    }

    public ResourceUsage[] cloneResources(){
        ResourceUsage[] out=new ResourceUsage[resources.length];
        for (int x=0;x<resources.length;x++)
            out[x]=resources[x];
        return out;
    }



    public Task prioritize(double priority){
        return new Task(name,provides,resources,dependencies,duration,priority,cost,color);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (name != null ? !name.equals(task.name) : task.name != null) return false;

        return true;
    }

}
