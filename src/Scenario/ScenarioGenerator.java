/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Scenario;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class ScenarioGenerator {
    private String name;
    private ArrayList<Task> tasks;
    private ArrayList<Resource> resources;
    private ArrayList<String> maps;
    private HashMap<String, Integer> delays;

    public ScenarioGenerator(String name) {
        this.name = name;
        this.tasks = new ArrayList<Task>();
        this.resources = new ArrayList<Resource>();
        this.maps = new ArrayList<String>();
        this.delays=new HashMap<String, Integer>();
    }

    public void addResource(Resource r){
        for (int x=0;x<resources.size();x++){
            Resource res=resources.get(x);
            if (res.getType().equals(r.getType())){
                resources.set(x,new Resource(res.getType(),r.getCount()+res.getCount()));
                return;
            }
        }
        resources.add(r);
    }

    public void addDelay(String task, int count){
        if (delays.containsKey(task))
            delays.put(task,delays.get(task)+count);
        else
            delays.put(task, count);
    }

    public void addAntiDelay(String task, int count){
        //System.out.println(">>ANTIDELAY "+task);
        if (delays.containsKey(task)){
            if (delays.get(task) > count)
                delays.put(task,delays.get(task)-count);
            else
                delays.remove(task);
        }
    }

    public void addTask(Task t){
        tasks.add(t);
    }

    public void addMap(String m){
        maps.add(m);
    }

    public void shuffleTasks(){
        Collections.shuffle(tasks);
    }

    public Scenario generateScenario(){
        return new Scenario(name,tasks.toArray(new Task[0]),resources.toArray(new Resource[0]),maps.toArray(new String[0]),delays);
    }
}
