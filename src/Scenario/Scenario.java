/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Scenario;

import Common.Pair;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;


public class Scenario {

    private String name;
    private Task[] tasks;
    private Resource[] resources;
    private String[] maps;
    private HashMap<String,Resource> resourceMap;
    private HashMap<String,Task> taskMap;
    private HashMap<String,TaskDependency[]> reverseDependencies;
    private HashMap<String,double[]> taskMetrics;
    private HashMap<String, Integer> delays;

    private static final int METRIC_FOLLOW_UP_DURATION=0;
    private static final int METRIC_FOLLOW_UP_COUNT=1;
    private static final int METRIC_RESOURCE_USAGE=2;

    public Scenario(String name, Task[] tasks, Resource[] resources, String[] maps, HashMap<String, Integer> delays) {
        this.name = name;
        this.resources = resources;
        this.tasks = tasks;
        this.maps = maps;
        this.resourceMap = new HashMap<String, Resource>();
        for (Resource r:resources)
            this.resourceMap.put(r.getType(),r);
        this.taskMap = new HashMap<String, Task>();
        for (Task t:tasks)
            this.taskMap.put(t.getName(),t);
        this.delays=delays;

    }



    public Scenario(String name, Task[] tasks, Resource[] resources, String[] maps) {
        this.name = name;
        this.resources = resources;
        this.tasks = tasks;
        this.maps = maps;
        this.resourceMap = new HashMap<String, Resource>();
        for (Resource r:resources)
            this.resourceMap.put(r.getType(),r);
        this.taskMap = new HashMap<String, Task>();
        for (Task t:tasks)
            this.taskMap.put(t.getName(),t);
        this.delays=new HashMap<String, Integer>();

    }

    public String getName() {
        return name;
    }

    public Task[] getTasks(){
        Task[] res=new Task[tasks.length];
        for (int x=0;x<tasks.length;x++){
            res[x]=tasks[x];
        }
        return res;
    }

    public Task[] getTasks(boolean randomize){
        ArrayList<Task> t=new ArrayList<Task>();
        for (int x=0;x<tasks.length;x++){
            t.add(tasks[x]);
        }
        if (randomize)
            Collections.shuffle(t);

        Task[] res=new Task[tasks.length];
        for (int x=0;x<t.size();x++){
            res[x]=t.get(x);
        }

        return res;
    }


    public void shuffleTasks(){
        ArrayList<Task> list=new ArrayList<Task>();
        for (Task t:tasks){
            list.add(t);
        }
        Collections.shuffle(list);
        tasks=list.toArray(new Task[0]);
    }

    public TaskDependency[] getReverseDependency(String task){
        if (reverseDependencies==null){
            createReverseDepedencies();
        }
        return reverseDependencies.get(task);
    }

    private void createReverseDepedencies(){
        HashMap<String, int[]> counts=new HashMap<String, int[]>();
        for (Task t:tasks){
            for (int x=0;x<t.getDependencyCount();x++){
                String name=t.getDependency(x).getTask();
                if (counts.containsKey(name)){
                    counts.get(name)[0]++;
                }
                else{
                    counts.put(name,new int[]{1,0});
                }
            }
        }
        reverseDependencies=new HashMap<String, TaskDependency[]>();
        for (Task t:tasks){
            for (int x=0;x<t.getDependencyCount();x++){
                String name=t.getDependency(x).getTask();
                int type=t.getDependency(x).getType();
                if (reverseDependencies.containsKey(name)){
                    reverseDependencies.get(name)[counts.get(name)[1]++]=new TaskDependency(t.getName(),type);
                }
                else{
                    TaskDependency[] newDep=new TaskDependency[counts.get(name)[0]];
                    newDep[counts.get(name)[1]++]=new TaskDependency(t.getName(),type);
                    reverseDependencies.put(name,newDep);
                }
            }
        }
    }

    public Resource[] getResources(){
        Resource[] res=new Resource[resources.length];
        for (int x=0;x<resources.length;x++){
            res[x]=resources[x];
        }
        return res;
    }

    public String[] getMaps(){
        String[] res=new String[maps.length];
        for (int x=0;x<maps.length;x++){
            res[x]=maps[x];
        }
        return res;
    }

    public Pair<String, Integer>[] getDelays(){
        Pair<String, Integer>[] res=new Pair[delays.size()];
        int x=0;
        for (String delay:delays.keySet()){
            res[x++]=new Pair<String, Integer>(delay,delays.get(delay));
        }
        return res;
    }

    public Integer getDelay(String name){
        return delays.get(name);
    }

    public Resource getResource(String type){
        return resourceMap.get(type);
    }

    public Task getTask(String name){
        return taskMap.get(name);
    }




    public void calculateTaskMetrics(){
        taskMetrics=new HashMap<String, double[]>();

        for (Task t:tasks){
            double[] followUpMetrics=getFollowUpMetrics(t.getName(),new LinkedList<String>());
            taskMetrics.put(t.getName(),followUpMetrics);
        }
    }

    private double[] getFollowUpMetrics(String t,LinkedList<String> visited){
        double[] res=new double[]{0,0};
        TaskDependency[] revDeps=getReverseDependency(t);
        if (revDeps!=null){

            for (TaskDependency dep:revDeps){
                if (!visited.contains(dep.getTask())){
                    res[METRIC_FOLLOW_UP_COUNT]++;
                    res[METRIC_FOLLOW_UP_DURATION]+=taskMap.get(dep.getTask()).getDuration();
                    visited.add(dep.getTask());
                    double[] recRes=getFollowUpMetrics(dep.getTask(),visited);
                    res[METRIC_FOLLOW_UP_COUNT]+=recRes[METRIC_FOLLOW_UP_COUNT];
                    res[METRIC_FOLLOW_UP_DURATION]+=recRes[METRIC_FOLLOW_UP_DURATION];
                }
            }
        }
        return res;
    }

    public Scenario getFollowUpPrioritizedScenario(){
        calculateTaskMetrics();
        ScenarioGenerator scenarioGenerator=new ScenarioGenerator("Opt");
        for (Task t:getTasks()){
            scenarioGenerator.addTask(t.prioritize(taskMetrics.get(t.getName())[METRIC_FOLLOW_UP_DURATION]));
        }
        for (Resource r:getResources())
            scenarioGenerator.addResource(r);
        for (String m:getMaps())
            scenarioGenerator.addMap(m);
        return scenarioGenerator.generateScenario();
    }




    public void createGraphVizFile(File file) throws IOException {
        FileWriter writer=new FileWriter(file);
        writer.write("/* GraphViz dependency graph */\n");
        writer.write("\n");
        writer.write("digraph G {\n");
        for (int x=0;x<tasks.length;x++){
            writer.write("T"+x+" [shape=box,label=\""+tasks[x].getName().replace('"','\'')+" ("+tasks[x].getDuration()+")"+"\",color=\"#"+Integer.toHexString(tasks[x].getColor().getRGB()).substring(2)+"\"]\n");
        }
        int nodeCounter=tasks.length;
        for (int x=0;x<tasks.length;x++){
            for (TaskDependency dep:tasks[x].cloneDependencies()){
                int count=0;
                for (int y=0;y<tasks.length;y++){
                    if (tasks[y].getProvides().compareTo(dep.getTask())==0)
                        count++;
                }
                if (count==1){
                    for (int y=0;y<tasks.length;y++){
                        if (tasks[y].getProvides().compareTo(dep.getTask())==0){
                            writer.write("T"+y+" -> T"+x+"\n");
                            break;
                        }
                    }
                    //int z=tasks.indexOf(dep);
                    //if (z>=0)
                }
                else if (count>1){
                    writer.write("T"+nodeCounter+" [shape=circle,label=\"OR\",color=\"#"+Integer.toHexString(Color.DARK_GRAY.getRGB()).substring(2)+"\"]\n");
                    writer.write("T"+nodeCounter+" -> T"+x+"\n");
                    for (int y=0;y<tasks.length;y++){
                        if (tasks[y].getProvides().compareTo(dep.getTask())==0){
                            writer.write("T"+y+" -> T"+nodeCounter+"\n");
                        }
                    }
                    nodeCounter++;

                }
                else{
                    System.out.println(tasks[x].getName()+"\n\thas unsatisfiable dependency\n\t\t"+dep.getTask());

                }
            }

        }
        writer.write("}\n");
        writer.close();
    }

    public void createXMLFile(File file,HashMap<String,Long> schedule){
        System.out.println("Saving XML File to " + file.getName());
        try{
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            //root elements
            Document doc = docBuilder.newDocument();

            Element rootElement = doc.createElement("Process");
            doc.appendChild(rootElement);


            Element tasksNode = doc.createElement("Tasks");
            rootElement.appendChild(tasksNode);

            for (Task t:tasks){
                Element tn=doc.createElement("Task");
                tn.setAttribute("Name",t.getName());
                tn.setAttribute("Duration",t.getDuration()+"");
                tn.setAttribute("Provides",t.getProvides()+"");
                tn.setAttribute("Priority",t.getPriority()+"");
                tn.setAttribute("Cost",t.getCost()+"");
                tn.setAttribute("Color",t.getColor().getRGB()+"");
                if (schedule!=null && schedule.containsKey(t.getName())){
                    Long start=schedule.get(t.getName());
                    tn.setAttribute("Start",start+"");
                    tn.setAttribute("End",(start+t.getDuration())+"");
                }

                Element resourceNode = doc.createElement("Resources");
                for (ResourceUsage r:t.cloneResources()){
                    if (!r.isGeometric()){
                        Element rn=doc.createElement("Resource");
                        rn.setAttribute("ResourceType",r.getType());
                        rn.setAttribute("Count",r.getCount()+"");
                        //rn.setAttribute("UsageType",r.getType()+"");
                        resourceNode.appendChild(rn);
                    }
                    else{
                        Element rn=doc.createElement("GeometricResource");
                        GeometricResourceUsage gru=(GeometricResourceUsage)r;
                        rn.setAttribute("Name",gru.getName());
                        rn.setAttribute("Area",gru.getArea().toString());
                        rn.setAttribute("Map",gru.getMap()+"");
                        resourceNode.appendChild(rn);
                    }
                }
                tn.appendChild(resourceNode);

                /*Element materialsNode = doc.createElement("Materials");
                for (MaterialUsage m:t.getMaterials()){
                    Element mn=doc.createElement("Material");
                    mn.setAttribute("MaterialType",m.getName());
                    mn.setAttribute("Quantity",m.getQuanity()+"");
                    mn.setAttribute("UsageType",m.getType()+"");
                    materialsNode.appendChild(mn);
                }
                tn.appendChild(materialsNode);*/

                Element dependNode = doc.createElement("Dependencies");
                for (TaskDependency td:t.cloneDependencies()){
                    Element dn=doc.createElement("Dependency");
                    dn.setAttribute("Name",td.getTask());
                    dn.setAttribute("Type",td.getType()+"");
                    dependNode.appendChild(dn);
                }
                tn.appendChild(dependNode);




                tasksNode.appendChild(tn);
            }


            Element resNode = doc.createElement("Resources");
            rootElement.appendChild(resNode);

            for (Resource r:resources){
                Element rn=doc.createElement("Resource");
                rn.setAttribute("Name",r.getName());
                rn.setAttribute("Type",r.getType());
                rn.setAttribute("Count",r.getCount()+"");
                resNode.appendChild(rn);
            }

            /*Element matNode = doc.createElement("Materials");
            rootElement.appendChild(matNode);

            for (Material m:materials){
                Element mn=doc.createElement("Material");
                mn.setAttribute("Name",m.getName());
                mn.setAttribute("Quantity",m.getQuanity()+"");
                mn.setAttribute("StorageSpace",m.getStorageSpace()+"");
                matNode.appendChild(mn);
            }                   */

            Element mapNode = doc.createElement("Maps");
            rootElement.appendChild(mapNode);

            for (String m:maps){
                Element mn=doc.createElement("Map");
                mn.setAttribute("Name",m);
                mapNode.appendChild(mn);
            }

            Element delayNode = doc.createElement("Delays");
            rootElement.appendChild(delayNode);

            for (String task:delays.keySet()){
                Element dn=doc.createElement("Delay");
                dn.setAttribute("Task",task);
                dn.setAttribute("Delay",delays.get(task)+"");
                delayNode.appendChild(dn);
            }

            //write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);

            StreamResult result =  new StreamResult(file);
            transformer.transform(source, result);

            System.out.println("Saved successfully");
        }catch(ParserConfigurationException pce){
            pce.printStackTrace();
        }catch(TransformerException tfe){
            tfe.printStackTrace();
        }
    }
}
