/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Scenario;


import net.sf.mpxj.ProjectFile;
import net.sf.mpxj.Relation;
import net.sf.mpxj.RelationType;
import net.sf.mpxj.*;
import net.sf.mpxj.reader.ProjectReader;
import net.sf.mpxj.reader.ProjectReaderUtility;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import Scenario.Task;

public class XMLParserMSProject {

    public static Scenario parseXML(String file) throws Exception{
        return parseXML(file,file);
    }

    public static Scenario parseXML(String file, String name) throws Exception{
        System.out.println("Parsing MS-Project-XML file "+file);
        ProjectReader reader = ProjectReaderUtility.getProjectReader(file);
        ProjectFile mpx = reader.read(file);
        HashMap<String,List<String>> map=new HashMap<String, List<String>>();
        for (net.sf.mpxj.Task t:mpx.getAllTasks())
            populateMap(t,map);

        ArrayList<Resource> resR=new ArrayList<Resource>();
        ArrayList<Task> resT=new ArrayList<Task>();

        //resR.add(new Resource("Worker", 10));

        for (net.sf.mpxj.Task t:mpx.getAllTasks()){
            if (t.getChildTasks().isEmpty()&&t.getName()!=null){

                List<TaskDependency> deps=new LinkedList<TaskDependency>();
                //if (t.getPredecessors()==null||t.getPredecessors().isEmpty()){
                    //System.out.print(t.getID()+" "+t.getName()+" >>> \t");
                    if (t.getPredecessors()!=null){
                        for (Relation r:t.getPredecessors()){
                            net.sf.mpxj.Task t2=r.getTargetTask();
                            if (t2.getName()!=null){
                                List<String> tasks=map.get(t2.getID().toString());
                                for (String s:tasks){
                                    if (r.getType()== RelationType.START_START){
                                        deps.add(new TaskDependency(s,TaskDependency.TYPE_SS));
                                    }
                                    else{
                                        deps.add(new TaskDependency(s));
                                    }

                                    //System.out.print(s+"{"+r.getType()+"} ||| ");
                                }
                            }


                        }
                        //System.out.println();
                    }
                    //else
                    //    System.out.println();
                TaskDependency[] depsa=new TaskDependency[deps.size()];
                int x=0;
                for (TaskDependency dep:deps){
                    depsa[x++]=dep;
                }
                long duration;
                switch(t.getDuration().getUnits().getValue()){
                    case 0:
                        duration=Math.round(t.getDuration().getDuration()*60);
                        break;
                    case 1:
                        duration=Math.round(t.getDuration().getDuration()*3600);
                        break;
                    case 2:
                        duration=Math.round(t.getDuration().getDuration()*24*3600);
                        break;
                    case 3:
                        duration=Math.round(t.getDuration().getDuration()*7*24*3600);
                        break;
                    case 4:
                        duration=Math.round(t.getDuration().getDuration()*30*24*3600);
                        break;
                    case 6:
                        duration=Math.round(t.getDuration().getDuration()*365*24*3600);
                        break;
                    default:
                        duration=Math.round(t.getDuration().getDuration()*60);

                }

                resT.add(new Task(
                        t.getID() + " " + t.getName(),

                        new ResourceUsage[]{},
                        //new MaterialUsage[0],
                        depsa,
                        duration,
                        new Color(t.getID())

                ));

                //}
            }
        }
        //res.saveDependencygraph(new File("/tmp/test.gv"));
        return new Scenario(name,resT.toArray(new Task[0]),resR.toArray(new Resource[0]),new String[0]);
    }

    public static void populateMap(net.sf.mpxj.Task t,HashMap<String,List<String>> map){
        if (t.getName()==null)return;
        LinkedList<String> children=new LinkedList<String>();
        getChildren(t,children);
        map.put(t.getID().toString(),children);
        for (net.sf.mpxj.Task t2:t.getChildTasks()){
            populateMap(t2,map);
        }
    }

    public static void getChildren(net.sf.mpxj.Task t,List<String> children){
        if (t.getName()==null)return;
        if (t.getChildTasks().isEmpty()){
            children.add(t.getID()+" "+t.getName());
        }
        else{
            for (net.sf.mpxj.Task t2:t.getChildTasks()){
                getChildren(t2,children);
            }
        }
    }

    public static void main(String[] args) throws Exception{
        XMLParserMSProject.parseXML("/home/homer/Documents/TUM/FAUST/ProjektBaustelle Luise-Kisselbackplatz/Gesamttpl LKP 100401_PP.xml","Luise");
    }
}
