/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Scenario;

import java.util.LinkedList;

public class TaskSwapSet extends LinkedList<TaskSwap>{
    private double score;

    public TaskSwapSet(double score) {
        this.score = score;
    }

    public TaskSwapSet(TaskSwap s,double score) {
        this.score = score;
        add(s);
    }

    public TaskSwapSet(TaskSwapSet s,double score) {
        this.score = score;
        for (TaskSwap swap:s)
            add(swap);
    }


    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
