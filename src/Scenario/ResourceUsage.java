/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Scenario;

public class ResourceUsage {
    private String type;
    private int count;

    public ResourceUsage(String type, int count) {
        this.type = type;
        this.count = count;
    }

    public ResourceUsage(String type) {
        this(type,1);
    }

    public String getType() {
        return type;
    }

    public int getCount() {
        return count;
    }

    public boolean isGeometric(){
        return false;
    }
}
