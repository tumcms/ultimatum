/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Scenario;

import Geometry.Area;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.*;
import java.io.File;
import java.util.*;

public class XMLParser {

    public static Scenario parseXML(String file) throws Exception{
        return parseXML(file,file);
    }

    public static Scenario parseXML(String file, String name) throws Exception{

        System.out.println("Parsing XML file "+file);

        Scenario res;
        ArrayList<Task> tasks=new ArrayList<Task>();
        ArrayList<Resource> resources=new ArrayList<Resource>();
        ArrayList<String> maps=new ArrayList<String>();
        HashMap<String, Integer> delays=new HashMap<String, Integer>();

        DocumentBuilderFactory dbf=DocumentBuilderFactory.newInstance();

        DocumentBuilder db = dbf.newDocumentBuilder();


        Document dom = db.parse(new File(file));
        Element docEle = dom.getDocumentElement();
        NodeList children =docEle.getChildNodes();

        for (int x=0;x<children.getLength();x++){
            if (children.item(x).getNodeName().compareToIgnoreCase("Tasks")==0){
                NodeList te=children.item(x).getChildNodes();
                for (int y=0;y<te.getLength();y++){
                    if (te.item(y).getNodeName()!=null&&te.item(y).getNodeName().compareTo("Task")==0){
                        String tname=te.item(y).getAttributes().getNamedItem("Name").getNodeValue();
                        long duration=Long.parseLong(te.item(y).getAttributes().getNamedItem("Duration").getNodeValue());
                        double cost=1;
                        if (te.item(y).getAttributes().getNamedItem("Cost")!=null)
                            cost=Double.parseDouble(te.item(y).getAttributes().getNamedItem("Cost").getNodeValue());

                        //boolean interruptable=Boolean.parseBoolean(te.item(y).getAttributes().getNamedItem("Interruptable").getNodeValue());
                        //boolean optional=Boolean.parseBoolean(te.item(y).getAttributes().getNamedItem("Optional").getNodeValue());
                        String provides=te.item(y).getAttributes().getNamedItem("Provides").getNodeValue();
                        if (provides.trim().length()==0)
                            provides=name;
                        Color col=Color.GREEN;
                        if (te.item(y).getAttributes().getNamedItem("Color")!=null)
                            col=new Color(Integer.parseInt(te.item(y).getAttributes().getNamedItem("Color").getNodeValue()));

                        double priority=1;
                        if (te.item(y).getAttributes().getNamedItem("Priority")!=null){
                            priority=Double.parseDouble(te.item(y).getAttributes().getNamedItem("Priority").getNodeValue());
                        }

                        ResourceUsage[] resUse=new ResourceUsage[0];
                        //MaterialUsage[] matUse=new MaterialUsage[0];
                        TaskDependency[] dep=new TaskDependency[0];
                        NodeList tcn=te.item(y).getChildNodes();
                        for (int z=0;z<tcn.getLength();z++){
                            if (tcn.item(z).getNodeName()!=null){
                                if (tcn.item(z).getNodeName().compareTo("Resources")==0){
                                    resUse=parseResourceUsages(tcn.item(z).getChildNodes());
                                }
//                                else if (tcn.item(z).getNodeName().compareTo("Materials")==0){
//                                    matUse=parseMaterialUsages(tcn.item(z).getChildNodes());
//                                }
                                else if (tcn.item(z).getNodeName().compareTo("Dependencies")==0){
                                    dep=parseDependencies(tcn.item(z).getChildNodes());
                                }
                            }

                        }
                        tasks.add(new Task(tname,provides,resUse,dep,duration,priority,cost,col));
                    }


                }

            }
            else if (children.item(x).getNodeName()!=null&&children.item(x).getNodeName().compareToIgnoreCase("Resources")==0){
                NodeList te=children.item(x).getChildNodes();
                for (int y=0;y<te.getLength();y++){
                    if (te.item(y).getNodeName()!=null&&te.item(y).getNodeName().compareTo("Resource")==0){
                        String rname=te.item(y).getAttributes().getNamedItem("Name").getNodeValue();
                        String type=te.item(y).getAttributes().getNamedItem("Type").getNodeValue();
                        int count=1;
                        if (te.item(y).getAttributes().getNamedItem("Count")!=null)
                            count=Integer.parseInt(te.item(y).getAttributes().getNamedItem("Count").getNodeValue());
                        //for (int r=0;r<3;r++){
                        resources.add(new Resource(rname,type,count));
                        //}
                    }
                }
            }
/*            else if (children.item(x).getNodeName()!=null&&children.item(x).getNodeName().compareToIgnoreCase("Materials")==0){
                NodeList te=children.item(x).getChildNodes();
                for (int y=0;y<te.getLength();y++){
                    if (te.item(y).getNodeName()!=null&&te.item(y).getNodeName().compareTo("Material")==0){
                        String name=te.item(y).getAttributes().getNamedItem("Name").getNodeValue();
                        double quantity=Double.parseDouble(te.item(y).getAttributes().getNamedItem("Quantity").getNodeValue());
                        double space=Double.parseDouble(te.item(y).getAttributes().getNamedItem("StorageSpace").getNodeValue());
                        res.addMaterial(new Material(name,quantity,space));
                    }
                }
            }*/
            else if (children.item(x).getNodeName()!=null&&children.item(x).getNodeName().compareToIgnoreCase("Maps")==0){
                NodeList te=children.item(x).getChildNodes();
                for (int y=0;y<te.getLength();y++){
                    if (te.item(y).getNodeName()!=null&&te.item(y).getNodeName().compareTo("Map")==0){
                        String map=te.item(y).getAttributes().getNamedItem("Name").getNodeValue();
                        maps.add(map);
                    }
                }

            }  //TODO
            if (children.item(x).getNodeName().compareToIgnoreCase("Delays")==0){
                NodeList te=children.item(x).getChildNodes();
                for (int y=0;y<te.getLength();y++){
                    if (te.item(y).getNodeName()!=null&&te.item(y).getNodeName().compareTo("Delay")==0){
                        String tname=te.item(y).getAttributes().getNamedItem("Task").getNodeValue();
                        int delay=Integer.parseInt(te.item(y).getAttributes().getNamedItem("Delay").getNodeValue());
                        if (delays.containsKey(tname))
                            delays.put(tname,delays.get(tname)+delay);
                        else
                            delays.put(tname,delay);
                    }
                }
            }
        }

        res=new Scenario(name,tasks.toArray(new Task[0]),resources.toArray(new Resource[0]),maps.toArray(new String[0]),delays);
        return res;
    }

    private static ResourceUsage[] parseResourceUsages(NodeList nodes){
        ArrayList<ResourceUsage> tmp=new ArrayList<ResourceUsage>();
        for (int x=0;x<nodes.getLength();x++){
            if (nodes.item(x).getNodeName()!=null&&nodes.item(x).getNodeName().compareTo("GeometricResource")==0){
                String name=nodes.item(x).getAttributes().getNamedItem("Name").getNodeValue();
                String map=nodes.item(x).getAttributes().getNamedItem("Map").getNodeValue();
                String area=nodes.item(x).getAttributes().getNamedItem("Area").getNodeValue();
                tmp.add(new GeometricResourceUsage(name,map, Area.parseGeometricResourceUsage(area)));
            }
            else{
                int count=1;
                //int usageType=ResourceUsage.TYPE_USAGE;
                if (nodes.item(x).getAttributes()!=null){

                    if (nodes.item(x).getAttributes().getNamedItem("Count")!=null){
                        count=Integer.parseInt(nodes.item(x).getAttributes().getNamedItem("Count").getNodeValue());
                    }
                    /*if (nodes.item(x).getAttributes().getNamedItem("UsageType")!=null){
                        usageType=Integer.parseInt(nodes.item(x).getAttributes().getNamedItem("UsageType").getNodeValue());
                    } */
                    String resourceType=nodes.item(x).getAttributes().getNamedItem("ResourceType").getNodeValue();
                    tmp.add(new ResourceUsage(resourceType,count));
                }
            }
        }
        ResourceUsage[] res=new ResourceUsage[tmp.size()];
        int x=0;
        for (ResourceUsage r:tmp)
            res[x++]=r;
        return res;
    }

    /*private MaterialUsage[] parseMaterialUsages(NodeList nodes){
        java.util.List<MaterialUsage> tmp=new ArrayList<MaterialUsage>();
        for (int x=0;x<nodes.getLength();x++){
            double quantity=Double.parseDouble(nodes.item(x).getAttributes().getNamedItem("Quantity").getNodeValue());
            int usageType=Integer.parseInt(nodes.item(x).getAttributes().getNamedItem("UsageType").getNodeValue());
            String materialType=nodes.item(x).getAttributes().getNamedItem("MaterialType").getNodeValue();
            tmp.add(new MaterialUsage(new Material(materialType,quantity),usageType));
        }
        MaterialUsage[] res=new MaterialUsage[tmp.size()];
        int x=0;
        for (MaterialUsage r:tmp)
            res[x++]=r;
        return res;
    }*/

    private static TaskDependency[] parseDependencies(NodeList nodes){
        java.util.List<TaskDependency> tmp=new ArrayList<TaskDependency>();
        for (int x=0;x<nodes.getLength();x++){
            if (nodes.item(x).getAttributes()!=null){
                String name=nodes.item(x).getAttributes().getNamedItem("Name").getNodeValue();
                int type=TaskDependency.TYPE_FS;
                if (nodes.item(x).getAttributes().getNamedItem("Type")!=null){
                    type=Integer.parseInt(nodes.item(x).getAttributes().getNamedItem("Type").getNodeValue());
                }
                tmp.add(new TaskDependency(name,type));
            }
        }
        TaskDependency[] res=new TaskDependency[tmp.size()];
        int x=0;
        for (TaskDependency r:tmp)
            res[x++]=r;
        return res;
    }
}
