/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Scenario;

public class TaskSwap {
    private String taskA,taskB;
    private long time;
    private boolean delay, antiDelay;

    public TaskSwap(String task, boolean antiDelay) {
        this.taskA=task;
        this.delay=true;
        this.antiDelay=antiDelay;
    }

    public TaskSwap(String taskA, String taskB, long time) {
        this.taskA = taskA;
        this.taskB = taskB;
        this.time=time;
        this.delay=false;
        this.antiDelay=false;
    }

    public String getTaskA() {
        return taskA;
    }

    public String getTaskB() {
        return taskB;
    }

    public long getTime() {
        return time;
    }

    public boolean isDelay() {
        return delay;
    }

    public boolean isAntiDelay() {
        return delay && antiDelay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskSwap taskSwap = (TaskSwap) o;
        if (((TaskSwap) o).isDelay() || isDelay())return false;
        if (taskA.equals(taskSwap.taskA)&&taskB.equals(taskSwap.taskB))return true;
        if (taskA.equals(taskSwap.taskB)&&taskB.equals(taskSwap.taskA))return true;

        return false;
    }

    public String toString(){
        return "[ "+time+" ] "+taskA+" <=> "+taskB;
    }

    public int hashCode(){
        return taskA.hashCode()+taskB.hashCode();
    }
}
