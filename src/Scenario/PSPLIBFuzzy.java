/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Scenario;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Random;
import java.util.StringTokenizer;

public class PSPLIBFuzzy {

    private static double[][][][] PSPLIB30,PSPLIB60,PSPLIB90,PSPLIB120;

    public static void loadData30(File f){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(f));
            PSPLIB30 = new double[48][10][30][3];
            for (int x = 0; x < 48; x++) {
                for (int y = 0; y < 10; y++) {
                    for (int z = 0; z < 30; z++) {
                        String in = reader.readLine();
                        StringTokenizer t = new StringTokenizer(in);
                        PSPLIB30[x][y][z][0] = Double.parseDouble(t.nextToken(","));
                        PSPLIB30[x][y][z][1] = Double.parseDouble(t.nextToken(","));
                        PSPLIB30[x][y][z][2] = Double.parseDouble(t.nextToken(","));
                    }
                }
            }
            reader.close();
        }catch (Exception ex){ex.printStackTrace();}
    }

    public static void loadData60(File f){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(f));
            PSPLIB60 = new double[48][10][60][3];
            for (int x = 0; x < 48; x++) {
                for (int y = 0; y < 10; y++) {
                    for (int z = 0; z < 60; z++) {
                        String in = reader.readLine();
                        StringTokenizer t = new StringTokenizer(in);
                        PSPLIB60[x][y][z][0] = Double.parseDouble(t.nextToken(","));
                        PSPLIB60[x][y][z][1] = Double.parseDouble(t.nextToken(","));
                        PSPLIB60[x][y][z][2] = Double.parseDouble(t.nextToken(","));
                    }
                }
            }
            reader.close();
        }catch (Exception ex){ex.printStackTrace();}
    }

    public static void loadData90(File f){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(f));
            PSPLIB90 = new double[48][10][90][3];
            for (int x = 0; x < 48; x++) {
                for (int y = 0; y < 10; y++) {
                    for (int z = 0; z < 90; z++) {
                        String in = reader.readLine();
                        StringTokenizer t = new StringTokenizer(in);
                        PSPLIB90[x][y][z][0] = Double.parseDouble(t.nextToken(","));
                        PSPLIB90[x][y][z][1] = Double.parseDouble(t.nextToken(","));
                        PSPLIB90[x][y][z][2] = Double.parseDouble(t.nextToken(","));
                    }
                }
            }
            reader.close();
        }catch (Exception ex){ex.printStackTrace();}
    }


    public static void loadData120(File f){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(f));
            PSPLIB120 = new double[60][10][120][3];
            for (int x = 0; x < 60; x++) {
                for (int y = 0; y < 10; y++) {
                    for (int z = 0; z < 120; z++) {
                        String in = reader.readLine();
                        StringTokenizer t = new StringTokenizer(in);
                        PSPLIB120[x][y][z][0] = Double.parseDouble(t.nextToken(","));
                        PSPLIB120[x][y][z][1] = Double.parseDouble(t.nextToken(","));
                        PSPLIB120[x][y][z][2] = Double.parseDouble(t.nextToken(","));
                    }
                }
            }
            reader.close();
        }catch (Exception ex){ex.printStackTrace();}
    }

    public static double[][] getFuzzyParameters(int set, int parameter,int instance){
        switch(set){
            case 30:
                if (PSPLIB30==null)
                    loadData30(new File("/home/homer/Documents/TUM/Projekte/FAUST/MySim3/psplib-data/fuzzyj30.txt"));
                return PSPLIB30[parameter][instance];
            case 60:
                if (PSPLIB60==null)
                    loadData60(new File("/home/homer/Documents/TUM/Projekte/FAUST/MySim3/psplib-data/fuzzyj60.txt"));
                return PSPLIB60[parameter][instance];
            case 90:
                if (PSPLIB90==null)
                    loadData90(new File("/home/homer/Documents/TUM/Projekte/FAUST/MySim3/psplib-data/fuzzyj90.txt"));
                return PSPLIB90[parameter][instance];
            case 120:
                if (PSPLIB120==null)
                    loadData120(new File("/home/homer/Documents/TUM/Projekte/FAUST/MySim3/psplib-data/fuzzyj120.txt"));
                return PSPLIB120[parameter][instance];

        }
        return null;
    };

    public static void main(String[] args) throws Exception{
        //loadData(new File("/home/homer/Documents/TUM/Projekte/FAUST/MySim3/psplib-data/fuzzyj30.txt"));
        System.exit(0);
        Random rnd = new Random();
        for (int set=30;set<=120;set+=30) {
            FileWriter file2 = new FileWriter(new File("/home/homer/Documents/TUM/Projekte/FAUST/MySim3/psplib-data/fuzzyj" + set + ".txt"));
            for (int x = 1; x <= (set<120 ? 48 : 60); x++) {
                for (int y = 1; y <= 10; y++) {
                    FileWriter file = new FileWriter(new File("/home/homer/Documents/TUM/Projekte/FAUST/MySim3/psplib-data/fuzzy/j" + set + "" + x + "_" + y + ".fz"));
                    file.write("0 0 0\n");
                    for (int z = 1; z <= set; z++) {
                        long maxDelay = Math.round(1+rnd.nextDouble() * 4);
                        long parmA = Math.round(1+rnd.nextDouble() * 9);
                        long parmB = Math.round(1+rnd.nextDouble() * 9);
                        file.write(maxDelay + " " + parmA + " " + parmB + "\n");
                        file2.write(maxDelay + "," + parmA + "," + parmB + "\n");
                    }
                    file.write("0 0 0\n");
                    file.close();
                }
            }
            file2.close();
        }
    }
}
