/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Scenario;

public class TaskDependency{

    public static final int TYPE_FS=0;
    public static final int TYPE_SS=1;

    private String task;
    private int type;

    public TaskDependency(String task) {
        this.task=task;
        this.type = TYPE_FS;
    }

    public TaskDependency(String task, int type) {
        this.task=task;
        this.type = type;
    }

    public String getTask() {
        return task;
    }

    public int getType() {
        return type;
    }
}