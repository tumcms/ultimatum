/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Scenario;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.StringTokenizer;

public class RCPParser {
    public static Scenario parseRCP(String filename) throws Exception{
        //System.out.println("Parsing RCP file "+filename);
        File file=new File(filename);
        BufferedReader reader=new BufferedReader(new FileReader(file));
        String in=reader.readLine();
        int mode=0;
        ArrayList<String[]> tasks=new ArrayList<String[]>();
        ArrayList<String[]> resources=new ArrayList<String[]>();
        String[] rescount=null;
        String in2;
        StringTokenizer stringTokenizer;
        String[] res;
        int tn=1;
        while(in!=null){
            //System.out.println(in);
            if (in.isEmpty()){
                mode++;
            }
            else{
                switch (mode){
                    case 1:
                        in2=removeDoubleSpaces(in);
                        stringTokenizer=new StringTokenizer(in2,"\t");
                        rescount=new String[stringTokenizer.countTokens()];
                        for (int x=0;x<rescount.length;x++){
                            rescount[x]=stringTokenizer.nextToken();

                        }
                        break;
                    case 2:

                        in2=removeDoubleSpaces(in);
                        stringTokenizer=new StringTokenizer(in2,"\t");
                        String duration=stringTokenizer.nextToken();
                        res=new String[rescount.length+1];
                        res[0]=tn+++"";
                        for (int x=1;x<rescount.length+1;x++){
                            res[x]=stringTokenizer.nextToken();
                        }
                        resources.add(res);



                        stringTokenizer.nextToken();
                        res=new String[stringTokenizer.countTokens()+1];
                        res[0]=duration;
                        int x=1;
                        while(stringTokenizer.hasMoreTokens()){
                            res[x++]=stringTokenizer.nextToken();
                        }
                        tasks.add(res);

                        //System.out.print("dep ");
                        //for (x=0;x<res.length;x++){
                        //    System.out.print(res[x]+"\t");
                        //}
                        //System.out.println();


                        break;
                }
            }
            in=reader.readLine();
            //System.out.println(mode);
        }
        /*for (String[] s:tasks){
            for (String s2:s){
                System.out.print(s2+"\t");
            }
            System.out.println();
        }
        for (String[] s:resources){
            for (String s2:s){
                if (!s2.equals("0"))
                    System.out.print(s2+"\t");
            }
            System.out.println();
        }
        for (String s2:rescount){
            System.out.print(s2+"\t");
        }
        System.out.println();*/

        ScenarioGenerator gen=new ScenarioGenerator("RCP");
        int n=1;

        for (String[] s:tasks){
            String name=n+++"";
            LinkedList<TaskDependency> deps=new LinkedList<TaskDependency>();
            LinkedList<ResourceUsage> resourceUsages=new LinkedList<ResourceUsage>();
            int dn=1;
            for (String[] s2:tasks){
                String dname=dn+++"";
                for (int x=1;x<s2.length;x++){
                    //System.out.println(s2[x]+" eqs on "+name+"??");
                    if (s2[x].equals(name)){
                        deps.add(new TaskDependency(dname));
                        //System.out.println(name+" dep on "+dname);
                    }
                }
            }
            long duration=Long.parseLong(s[0]);
            for (String[] s2:resources){
                if (s2[0].equals(name)){

                    for (int x=1;x<s2.length;x++){
                        if (!s2[x].equals("0")){
                            resourceUsages.add(new ResourceUsage((x)+"",Integer.parseInt(s2[x])));
                            //System.out.println(name+" req "+resourceUsages.get(resourceUsages.size()-1).getType()+" "+resourceUsages.get(resourceUsages.size()-1).getCount());
                        }
                    }
                }
            }
            /*System.out.println("Name" + name + " ");
            for (ResourceUsage r:resourceUsages){
                System.out.print(r.getCount()+"x"+r.getType()+" ; ");
            }
            System.out.println();
            for (TaskDependency d:deps){
                System.out.print(d.getTask() + " ; ");
            }
            System.out.println();*/
            gen.addTask(new Task(
                    name,name,
                    resourceUsages.toArray(new ResourceUsage[0]),
                    deps.toArray(new TaskDependency[0]),
                    duration,
                    1,
                    1,
                    Color.RED
            ));
        }
        int x=1;
        for (String s:rescount){
            gen.addResource(new Resource(x+"",x+"",Integer.parseInt(s)));
            //System.out.println(x+" "+Integer.parseInt(s));
            x++;
        }

        reader.close();

        return gen.generateScenario();
    }

    private static String removeDoubleSpaces(String in){
        in=in.trim();
        String out="";
        char previous='.';
        for (char c:in.toCharArray()){
            if (c==' '){
                if (previous!=' ')
                    out+=c;
            }
            else
                out+=c;
            previous=c;
        }
        return out;
    }

    public static void main(String[] args) throws Exception{
        RCPParser.parseRCP("/home/homer/Documents/TUM/FAUST/MySim3/psplib-data/patterson.sm/pat007.rcp").createGraphVizFile(new File("/tmp/pat7.gv"));
    }
}
