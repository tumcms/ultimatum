/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Scenario;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.StringTokenizer;

public class SMParser {
    public static Scenario parseSM(String filename) throws Exception{
        //System.out.println("Parsing SM file "+filename);
        File file=new File(filename);
        BufferedReader reader=new BufferedReader(new FileReader(file));
        String in=reader.readLine();
        int mode=0;
        ArrayList<String[]> tasks=new ArrayList<String[]>();
        ArrayList<String[]> resources=new ArrayList<String[]>();
        String[] rescount=null;
        String in2;
        StringTokenizer stringTokenizer;
        String[] res;
        while(in!=null && rescount==null){
            if (in.startsWith("PRECEDENCE RELATIONS:")){
                mode=1;
                reader.readLine();
            }
            else if (in.startsWith("REQUESTS/DURATIONS:")){
                mode=2;
                reader.readLine();
                reader.readLine();
            }
            else if (in.startsWith("RESOURCEAVAILABILITIES:")){
                mode=3;
                reader.readLine();
            }
            else if (!in.startsWith("*") && !in.startsWith("-")){
                switch (mode){
                    case 1:
                        in2=removeDoubleSpaces(in);
                        stringTokenizer=new StringTokenizer(in2," ");
                        res=new String[stringTokenizer.countTokens()];
                        for (int x=0;x<res.length;x++){
                            res[x]=stringTokenizer.nextToken();
                        }
                        tasks.add(res);
                        break;
                    case 2:
                        in2=removeDoubleSpaces(in);
                        stringTokenizer=new StringTokenizer(in2," ");
                        res=new String[stringTokenizer.countTokens()];
                        for (int x=0;x<res.length;x++){
                            res[x]=stringTokenizer.nextToken();
                        }
                        resources.add(res);
                        break;
                    case 3:
                        in2=removeDoubleSpaces(in);
                        stringTokenizer=new StringTokenizer(in2," ");
                        rescount=new String[stringTokenizer.countTokens()];
                        for (int x=0;x<rescount.length;x++){
                            rescount[x]=stringTokenizer.nextToken();
                        }
                        break;
                }
            }
            in=reader.readLine();
        }
        /*for (String[] s:tasks){
            for (String s2:s){
                System.out.print(s2+"\t");
            }
            System.out.println();
        }
        for (String[] s:resources){
            for (String s2:s){
                if (!s2.equals("0"))
                    System.out.print(s2+"\t");
            }
            System.out.println();
        }
        for (String s2:rescount){
            System.out.print(s2+"\t");
        }
        System.out.println();*/

        ScenarioGenerator gen=new ScenarioGenerator("SM");

        for (String[] s:tasks){
            String name=s[0];
            LinkedList<TaskDependency> deps=new LinkedList<TaskDependency>();
            LinkedList<ResourceUsage> resourceUsages=new LinkedList<ResourceUsage>();
            for (String[] s2:tasks){
                for (int x=3;x<s2.length;x++){
                    if (s2[x].equals(s[0])){
                        deps.add(new TaskDependency(s2[0]));
                    }
                }
            }
            long duration=0;
            for (String[] s2:resources){
                if (s2[0].equals(name)){
                    duration=Long.parseLong(s2[2]);
                    for (int x=3;x<s2.length;x++){
                        if (!s2[x].equals("0")){
                            resourceUsages.add(new ResourceUsage((x-2)+"",Integer.parseInt(s2[x])));
                            //System.out.println(name+" req "+resourceUsages.get(resourceUsages.size()-1).getType());
                        }
                    }
                }
            }
            gen.addTask(new Task(
                    name,name,
                    resourceUsages.toArray(new ResourceUsage[0]),
                    deps.toArray(new TaskDependency[0]),
                    duration,
                    1,
                    1,
                    Color.RED
            ));
        }
        int x=1;
        for (String s:rescount){
            gen.addResource(new Resource(x+"",x+"",Integer.parseInt(s)));
            x++;
        }
        reader.close();

        return gen.generateScenario();
    }

    private static String removeDoubleSpaces(String in){
        in=in.trim();
        String out="";
        char previous='.';
        for (char c:in.toCharArray()){
            if (c==' '){
                if (previous!=' ')
                    out+=c;
            }
            else
                out+=c;
            previous=c;
        }
        return out;
    }

    public static void main(String[] args) throws Exception{
        SMParser.parseSM("/home/homer/Documents/TUM/FAUST/MySim3/psplib-data/j301_1.sm");
    }
}
