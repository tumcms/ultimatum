/*
    This file is part of ultimaTUM.
    Copyright (c) 2016 Technical University of Munich
    Chair of Computational Modeling and Simulation.

    ultimaTUM is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    ultimaTUM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package Scenario;

import Geometry.RectangularArea;
import Main.Version;
import Scenario.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Random;

public class ExampleGenerator {
    public static Scenario generate(){
        ScenarioGenerator scenarioGenerator=new ScenarioGenerator("Bohrpfähle");


        scenarioGenerator.addMap("Map");
        //Mehrere Karten koennten fuer unterschiedliche Hoehen oder Stockwerke verwendet werden

        Task task;
        RectangularArea area;
        Resource resource;

        //Verfuegbare Maschinen

        int anzahlArbeiter=10;

        for (int x=0;x<anzahlArbeiter;x++){
            resource=new Resource("Arbeiter "+x,"Arbeiter");
            scenarioGenerator.addResource(resource);

        }


        resource=new Resource("Bohrpfahl-Bohrer 1","Bohrpfahl-Bohrer");
        scenarioGenerator.addResource(resource);

        resource=new Resource("Bohrpfahl-Bohrer 2","Bohrpfahl-Bohrer");
        scenarioGenerator.addResource(resource);

        resource=new Resource("Kran 1","Kran");
        scenarioGenerator.addResource(resource);

        resource=new Resource("Betonpumpe 1","Betonpumpe");
        scenarioGenerator.addResource(resource);

        resource=new Resource("Betonpumpe 2","Betonpumpe");
        scenarioGenerator.addResource(resource);

        resource=new Resource("Schleifmaschine 1","Schleifmaschine");
        scenarioGenerator.addResource(resource);

        resource=new Resource("Schleifmaschine 2","Schleifmaschine");
        scenarioGenerator.addResource(resource);

        resource=new Resource("Schleifmaschine 3","Schleifmaschine");
        scenarioGenerator.addResource(resource);

        resource=new Resource("Bagger 1","Bagger");
        scenarioGenerator.addResource(resource);

        resource=new Resource("Presslufthammer 1","Presslufthammer");
        scenarioGenerator.addResource(resource);

        resource=new Resource("Presslufthammer 2","Presslufthammer");
        scenarioGenerator.addResource(resource);

        resource=new Resource("Presslufthammer 3","Presslufthammer");
        scenarioGenerator.addResource(resource);

        resource=new Resource("Presslufthammer 4","Presslufthammer");
        scenarioGenerator.addResource(resource);

        resource=new Resource("Presslufthammer 5","Presslufthammer");
        scenarioGenerator.addResource(resource);


        //Verfuegbare Arbeiter



        //Zeit Bedarf

        long bohrZeit=4;
        long bewehrZeit=2;
        long betonierZeit=3;
        long trockenZeit=24;
        long schleifZeit=3;
        long aushubZeit=24;
        long verdichtungsZeit=1;

        //Platz Bedarf

        int pfahlDurchmesser=10;

        int bohrerBreite=40;
        int bohrerTiefe=40;

        int schleifBreite=15;
        int schleifTiefe=10;

        int bewehrBreite=10;
        int bewehrTiefe=10;

        int betonierBreite=10;
        int betonierTiefe=10;

        int grubenBreite=200;
        int grubenTiefe=300;

        //Arbeiter Bedarf

        int bohrArbeiter=1;
        int bewehrArbeiter=3;
        int betonierArbeiter=2;
        int schleifArbeiter=1;
        int aushubArbeiter=1;
        int verdichtungsArbeiter=1;

        //Tasks

        //10 Bohrpfaehle
        for (int x=0;x<10;x++){

            area=new RectangularArea(x*2*pfahlDurchmesser,0,bohrerBreite,bohrerTiefe,0);
            task=new Task(
                    "Bohrpfahl "+(x*2)+" bohren",   //Name
                    new ResourceUsage[]{new ResourceUsage("Bohrpfahl-Bohrer"), new GeometricResourceUsage("Bohrplatz "+(x*2),"Map",area), new ResourceUsage("Arbeiter",bohrArbeiter)}, //Required Resources
//                    new MaterialUsage[0],    //Required Material
                    new TaskDependency[0],    //Prerequisite Tasks
                    bohrZeit,   //TimeSpan
                    Color.RED   //Chart color
            );
            scenarioGenerator.addTask(task);

            area=new RectangularArea(x*2*pfahlDurchmesser,0,bewehrBreite,bewehrTiefe,0);
            task=new Task(
                    "Bohrpfahl "+(x*2)+" bewehren",
                    new ResourceUsage[]{new ResourceUsage("Kran"), new GeometricResourceUsage("Bohrplatz "+(x*2),"Map",area), new ResourceUsage("Arbeiter",bewehrArbeiter)},
                    new TaskDependency[]{new TaskDependency("Bohrpfahl "+(x*2)+" bohren",TaskDependency.TYPE_FS)},
                    bewehrZeit,
                    Color.GREEN
            );
            scenarioGenerator.addTask(task);

            area=new RectangularArea(x*2*pfahlDurchmesser,0,betonierBreite,betonierTiefe,0);
            task=new Task(
                    "Bohrpfahl "+(x*2)+" betonieren",
                    new ResourceUsage[]{new ResourceUsage("Betonpumpe"), new GeometricResourceUsage("Bohrplatz "+(x*2),"Map",area), new ResourceUsage("Arbeiter",betonierArbeiter)},
                    new TaskDependency[]{new TaskDependency("Bohrpfahl "+(x*2)+" bewehren",TaskDependency.TYPE_FS)},
                    betonierZeit,
                    Color.BLUE
            );
            scenarioGenerator.addTask(task);

            area=new RectangularArea(x*2*pfahlDurchmesser,0,pfahlDurchmesser,pfahlDurchmesser,0);
            task=new Task(
                    "Bohrpfahl "+(x*2)+" Trocknung",
                    new ResourceUsage[]{new GeometricResourceUsage("Bohrplatz "+(x*2),"Map",area)},
                    new TaskDependency[]{new TaskDependency("Bohrpfahl "+(x*2)+" betonieren")},
                    trockenZeit,
                    Color.ORANGE
            );
            scenarioGenerator.addTask(task);


        }

        // 9 Bohrpfaehle zwischen den vorherigen
        for (int x=0;x<9;x++){

            area=new RectangularArea((x*2+1)*pfahlDurchmesser,0,bohrerBreite,bohrerTiefe,0);
            task=new Task(
                    "Bohrpfahl "+(x*2+1)+" bohren",
                    new ResourceUsage[]{new ResourceUsage("Bohrpfahl-Bohrer"), new GeometricResourceUsage("Bohrplatz "+(x*2+1),"Map",area), new ResourceUsage("Arbeiter",bohrArbeiter)},
                    new TaskDependency[]{new TaskDependency("Bohrpfahl "+(x*2)+" Trocknung"),new TaskDependency("Bohrpfahl "+((x+1)*2)+" Trocknung")},
                    bohrZeit,
                    Color.RED
            );
            scenarioGenerator.addTask(task);

            area=new RectangularArea((x*2+1)*pfahlDurchmesser,0,bewehrBreite,bewehrTiefe,0);
            task=new Task(
                    "Bohrpfahl "+(x*2+1)+" bewehren",
                    new ResourceUsage[]{new ResourceUsage("Kran"), new GeometricResourceUsage("Bohrplatz "+(x*2+1),"Map",area), new ResourceUsage("Arbeiter",bewehrArbeiter)},
                    new TaskDependency[]{new TaskDependency("Bohrpfahl "+(x*2+1)+" bohren")},
                    bewehrZeit,
                    Color.GREEN
            );
            scenarioGenerator.addTask(task);

            area=new RectangularArea((x*2+1)*pfahlDurchmesser,0,betonierBreite,betonierTiefe,0);
            task=new Task(
                    "Bohrpfahl "+(x*2+1)+" betonieren",
                    new ResourceUsage[]{new ResourceUsage("Betonpumpe"), new GeometricResourceUsage("Bohrplatz "+(x*2+1),"Map",area), new ResourceUsage("Arbeiter",betonierArbeiter)},
                    new TaskDependency[]{new TaskDependency("Bohrpfahl "+(x*2+1)+" bewehren")},
                    betonierZeit,
                    Color.BLUE
            );
            scenarioGenerator.addTask(task);

            area=new RectangularArea((x*2+1)*pfahlDurchmesser,0,pfahlDurchmesser,pfahlDurchmesser,0);
            task=new Task(
                    "Bohrpfahl "+(x*2+1)+" Trocknung",
                    new ResourceUsage[]{new GeometricResourceUsage("Bohrplatz "+(x*2+1),"Map",area)},
                    new TaskDependency[]{new TaskDependency("Bohrpfahl "+(x*2+1)+" betonieren")},
                    trockenZeit,
                    Color.ORANGE
            );
            scenarioGenerator.addTask(task);
        }


        //Alle Bohrpfaehle abschleifen
        for (int x=1;x<18;x++){

            area=new RectangularArea(x*pfahlDurchmesser,0,schleifBreite,schleifTiefe,0);
            task=new Task(
                    "Bohrpfahl "+x+" schleifen",
                    new ResourceUsage[]{new ResourceUsage("Schleifmaschine"), new GeometricResourceUsage("Bohrplatz "+x,"Map",area), new ResourceUsage("Arbeiter",schleifArbeiter)},
                    new TaskDependency[]{new TaskDependency("Bohrpfahl "+(x-1)+" Trocknung"),new TaskDependency("Bohrpfahl "+x+" Trocknung"),new TaskDependency("Bohrpfahl "+(x+1)+" Trocknung")},
                    schleifZeit,
                    Color.GRAY
            );
            scenarioGenerator.addTask(task);

        }

        int x=0;
        area=new RectangularArea(x*pfahlDurchmesser,0,schleifBreite,schleifTiefe,0);
        task=new Task(
                "Bohrpfahl "+x+" schleifen",
                new ResourceUsage[]{new ResourceUsage("Schleifmaschine"), new GeometricResourceUsage("Bohrplatz "+x,"Map",area), new ResourceUsage("Arbeiter",schleifArbeiter)},
                new TaskDependency[]{new TaskDependency("Bohrpfahl "+x+" Trocknung"),new TaskDependency("Bohrpfahl "+(x+1)+" Trocknung")},
                schleifZeit,
                Color.GRAY
        );
        scenarioGenerator.addTask(task);

        x=18;
        area=new RectangularArea(x*pfahlDurchmesser,0,schleifBreite,schleifTiefe,0);
        task=new Task(
                "Bohrpfahl "+x+" schleifen",
                new ResourceUsage[]{new ResourceUsage("Schleifmaschine"), new GeometricResourceUsage("Bohrplatz "+x,"Map",area), new ResourceUsage("Arbeiter",schleifArbeiter)},
                new TaskDependency[]{new TaskDependency("Bohrpfahl "+x+" Trocknung"),new TaskDependency("Bohrpfahl "+(x-1)+" Trocknung")},
                schleifZeit,
                Color.GRAY
        );
        scenarioGenerator.addTask(task);

        //Grube ausheben

        TaskDependency[] grubenPrerequisites=new TaskDependency[19];
        for (x=0;x<19;x++){
            grubenPrerequisites[x]=new TaskDependency("Bohrpfahl "+x+" Trocknung");
        }


        area=new RectangularArea(0,pfahlDurchmesser,grubenBreite,grubenTiefe,0);

        task=new Task(
                "Grube ausheben",
                new ResourceUsage[]{new ResourceUsage("Bagger"), new GeometricResourceUsage("Grubenflaeche","Map",area), new ResourceUsage("Arbeiter",aushubArbeiter)},
                grubenPrerequisites,
                aushubZeit,
                Color.PINK
        );
        scenarioGenerator.addTask(task);

        for (x=0;x<10;x++){

            area=new RectangularArea(x*grubenBreite/10,pfahlDurchmesser,grubenBreite/10,grubenTiefe,0);

            task=new Task(
                    "Boden Abschnitt "+x+" verdichten",
                    new ResourceUsage[]{new ResourceUsage("Presslufthammer"), new GeometricResourceUsage("Grubenflaeche Abschnitt"+x,"Map",area), new ResourceUsage("Arbeiter",verdichtungsArbeiter)},
                    new TaskDependency[]{new TaskDependency("Grube ausheben")},
                    verdichtungsZeit,
                    Color.MAGENTA
            );
            scenarioGenerator.addTask(task);
        }

        //scenarioGenerator.shuffleTasks();
        return scenarioGenerator.generateScenario();
    }

    public static Scenario generateRandomScenario(int tasks,int deps, int resources, int maxrescount, int maxresuse){
        Random rnd=new Random();
        ScenarioGenerator generator=new ScenarioGenerator("Random");
        Resource[] res=new Resource[resources];
        for (int x=0;x<resources;x++){
            res[x]=new Resource("Resource "+(x+1),"Resource "+(x+1),1+rnd.nextInt(maxrescount));
            generator.addResource(res[x]);
        }

        for (int x=0;x<deps;x++){
            generator.addTask(new Task("Task "+(x+1),"Task "+(x+1),generateRandomResourceUsages(rnd,res,maxresuse),new TaskDependency[0],1+rnd.nextInt(100),rnd.nextInt(tasks),rnd.nextInt(10),new Color(rnd.nextInt(256),rnd.nextInt(256),rnd.nextInt(256))));
        }
        for (int x=deps;x<tasks;x++){
            generator.addTask(new Task("Task "+(x+1),"Task "+(x+1),generateRandomResourceUsages(rnd,res,maxresuse),generateRandomTaskDependencies(rnd,x,deps),1+rnd.nextInt(100),rnd.nextInt(tasks),rnd.nextInt(10),new Color(rnd.nextInt(256),rnd.nextInt(256),rnd.nextInt(256))));
        }
        return generator.generateScenario();
    }

    private static ResourceUsage[] generateRandomResourceUsages(Random rnd, Resource[] resources, int maxresuse){
        ResourceUsage[] res=new ResourceUsage[Math.min(resources.length,1+rnd.nextInt(maxresuse))];
        for (int x=0;x<res.length;x++){

            boolean duplicate=false;
            while(res[x]==null||duplicate){
                duplicate=false;
                int r=rnd.nextInt(resources.length);
                res[x]=new ResourceUsage("Resource "+(r+1),1+rnd.nextInt(resources[r].getCount()));
                for (int y=0;y<x;y++){
                    if (res[y].getType().equals(res[x].getType()))
                        duplicate=true;
                }
            }
        }
        return res;
    }

    private static TaskDependency[] generateRandomTaskDependencies(Random rnd, int tasks, int deps){
        TaskDependency[] res=new TaskDependency[deps];
        for (int x=0;x<res.length;x++){
            int t=rnd.nextInt(tasks);
            res[x]=new TaskDependency("Task "+(t+1));
        }
        return res;
    }

    public static Scenario generateRandomScenario(){
        final JFrame dialog=new JFrame(Version.ULTIMATUM_VERSION+" - Generate Random Scenario");
        dialog.setLayout(new BorderLayout());
        JPanel mainPanel=new JPanel(new GridLayout(5,2));
        mainPanel.add(new JLabel("Tasks:  ",JLabel.RIGHT));
        final JTextField taskField=new JTextField("100");
        mainPanel.add(taskField);
        mainPanel.add(new JLabel("Dependencies:  ",JLabel.RIGHT));
        final JTextField depField=new JTextField("2");
        depField.setToolTipText("Each task depends on n other tasks");
        mainPanel.add(depField);
        mainPanel.add(new JLabel("Resources:  ",JLabel.RIGHT));
        final JTextField resField=new JTextField("5");
        resField.setToolTipText("Number of resource types");
        mainPanel.add(resField);
        mainPanel.add(new JLabel("Resource availability:  ",JLabel.RIGHT));
        final JTextField maxResField=new JTextField("3");
        maxResField.setToolTipText("Maximum number of resources available per type");
        mainPanel.add(maxResField);
        mainPanel.add(new JLabel("Resource usage:  ",JLabel.RIGHT));
        final JTextField maxResUseField=new JTextField("3");
        maxResUseField.setToolTipText("Maximum number of resource types used by a task");
        mainPanel.add(maxResUseField);

        JPanel buttonPanel=new JPanel();
        JButton okButton=new JButton("OK");
        JButton cancelButton=new JButton("Cancel");
        buttonPanel.add(cancelButton);
        buttonPanel.add(okButton);
        dialog.add(mainPanel,BorderLayout.CENTER);
        dialog.add(buttonPanel,BorderLayout.SOUTH);
        dialog.pack();

        dialog.setResizable(false);

        int x=(int)(Toolkit.getDefaultToolkit().getScreenSize().getWidth()-dialog.getWidth())/2;
        int y=(int)(Toolkit.getDefaultToolkit().getScreenSize().getHeight()-dialog.getHeight())/2;
        dialog.setLocation(x, y);

        dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);


        dialog.setVisible(true);
        dialog.requestFocus();
        dialog.setSize(400, 200);


        class Listener  extends WindowAdapter implements ActionListener{
            private int state=0;
            @Override
            public void actionPerformed(ActionEvent e) {
                state=1;
                dialog.setVisible(false);
                dialog.dispose();
            }

            @Override
            public void windowClosed(WindowEvent e) {
                state=1;
            }

            int getState() {
                return state;
            }
        }

        final Listener okListener=new Listener();
        final Listener cancelListener=new Listener();

        okButton.addActionListener(okListener);
        cancelButton.addActionListener(cancelListener);
        dialog.addWindowListener(cancelListener);

        boolean done=false;
        while (!done){
            try{Thread.sleep(100);}catch (Exception ex){}
            if (okListener.getState()!=0)
                done=true;
            else if (cancelListener.getState()!=0)
                return null;
        }
        int tasks=Integer.parseInt(taskField.getText());
        int deps=Integer.parseInt(depField.getText());
        int res=Integer.parseInt(resField.getText());
        int maxRes=Integer.parseInt(maxResField.getText());

        int maxResUse=Integer.parseInt(maxResField.getText());
        if (maxResUse>res)
            maxResUse=res;
        return generateRandomScenario(tasks,deps,res,maxRes,maxResUse);
    }


}
