# ultimaTUM

### Info

ultimaTUM is a intelligent schedule optimziation toolbox for resource-constrained project scheduling problems (RCPSP) written by Dr.-Ing. Maximilian Bügler, M.Sc. ( http://www.maxbuegler.eu/ ) during his Ph.D. thesis at Technical University Munich.

### License

Copyright (c) 2016 Technical University of Munich Chair of Computational Modeling and Simulation.

ultimaTUM is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License Version 3 as published by the Free Software Foundation.

ultimaTUM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.

### Used packages

This software uses the EPSGraphics2D package by Paul James Mutton which can be obtained on http://www.jibble.org/ and which is under the GNU General Public License (GPL).
Furthermore the mpxj library is used, which can be downloaded at https://sourceforge.net/projects/mpxj/files/ and which is under the Lesser GNU General Public License (LGPL).
Also the apache commons math library is used, which can be downloaded at http://commons.apache.org/proper/commons-math/ and which is under the Apache License.
